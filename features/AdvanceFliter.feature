Feature: AdvanceFilter_Grade

Scenario: AdvanceFilter_Grade_per-k:[<UsingData>]
	Given user navigate application
	When user selects "PreK" Items from GRADE filter
	Then page loads with Products tagged with "PreK" Items
	
Scenario: AdvanceFilter_Grade_k:[<UsingData>]
	Given user navigate application
	When user selects "K" Items from GRADE filter
	Then page loads with Products tagged with "K" Items 
	
	
Scenario: AdvanceFilter_Grade1:[<UsingData>]
	Given user navigate application
	When user selects "Grade 1" Items from GRADE filter
	Then page loads with Products tagged with "Grade 1" Items
	
	
Scenario: AdvanceFilter_Grade2:[<UsingData>]
	Given user navigate application
	When user selects "Grade 2" Items from GRADE filter
	Then page loads with Products tagged with "2nd-grade" Items
	

Scenario: AdvanceFilter_Grade3:[<UsingData>]
	Given user navigate application
	When user selects "Grade 3" Items from GRADE filter
	Then page loads with Products tagged with "3rd-grade" Items
	

Scenario: AdvanceFilter_Grade4:[<UsingData>]
	Given user navigate application
	When user selects "Grade 4" Items from GRADE filter
	Then page loads with Products tagged with "4th-grade" Items 
	

Scenario: AdvanceFilter_Grade5:[<UsingData>]
	Given user navigate application
	When user selects "Grade 5" Items from GRADE filter
	Then page loads with Products tagged with "5th-grade" Items 
	

Scenario: AdvanceFilter_Grade6:[<UsingData>]
	Given user navigate application
	When user selects "Grade 6" Items from GRADE filter
	Then page loads with Products tagged with "6th-grade" Items 
	

Scenario: AdvanceFilter_Grade7:[<UsingData>]
	Given user navigate application
	When user selects "Grade 7" Items from GRADE filter
	Then page loads with Products tagged with "7th-grade" Items 
	
Scenario: AdvanceFilter_Grade8:[<UsingData>]
	Given user navigate application
	When user selects "Grade 8" Items from GRADE filter
	Then page loads with Products tagged with "8th-grade" Items 
	
	
Scenario: AdvanceFilter_Grade9:[<UsingData>]
	Given user navigate application
	When user selects "Grade 9" Items from GRADE filter 
	Then page loads with Products tagged with "9th-grade" Items 
	
Scenario: AdvanceFilter_Grade10:[<UsingData>]
	Given user navigate application
	When user selects "Grade 10" Items from GRADE filter
	Then page loads with Products tagged with "10th-grade" Items 
	

Scenario: AdvanceFilter_Grade11:[<UsingData>]
	Given user navigate application
	When user selects "Grade 11" Items from GRADE filter
	Then page loads with Products tagged with "11th-grade" Items 
	
	
Scenario: AdvanceFilter_Grade12:[<UsingData>]
	Given user navigate application
	When user selects "Grade 12" Items from GRADE filter
	Then page loads with Products tagged with "12th-grade" Items

    Scenario: AdvanceFilter_Grade1_Grade5:[<UsingData>]
	Given user navigate application
	When user selects "Grade 1 Grade 5" Items from GRADE filter
	Then page loads with Products tagged with "Grade 1 Grade 5" Items
	

	Scenario: AdvanceFilter_Grade2_Grade8:[<UsingData>]
	Given user navigate application
	When user selects "Grade 2 Grade 8" Items from GRADE filter
	Then page loads with Products tagged with "Grade 2 Grade 8" Items
	
	
	Scenario: AdvanceFilter_Grade prek Grade k:[<UsingData>]
	Given user navigate application
	When user selects "	Grade prek Grade k" Items from GRADE filter
	Then page loads with Products tagged with "	Grade prek Grade k" Items
	
	Scenario: AdvanceFilter_Grade prek Grade 12:[<UsingData>]
	Given user navigate application
	When user selects "	Grade prek Grade 12" Items from GRADE filter
	Then page loads with Products tagged with "Grade prek Grade 12" Items
