Feature: ASO_HealthCheck

  @allstores
  Scenario Outline: ScholasticStore:[<UsingData>]
    Given user navigates to "ParentStore" page
    And user click on sign in link
    When user navigates to login page with username and password [UserInformation]
    When user hovers on the minicart
    And user empty the mini cart
    When user searches for an "isbn" in global search box
    And user navigates to PDP page
    When user searches for an "isbn" in global search box on PDP
    And user navigates to PDP page
    And user adds that product into cart from product detail page
    When user searches for an "keyword" in global search box
    And user adds that product into cart from search results page
    When user searches for an "author" in global search box
    And user adds that product into cart from search results page
    When user click on mini cart icon
    When user verify Check Out Now on DW page
    Then user will log out of the "parent" store

    Examples: 
      | UsingData          |
      | HealthCheck_Parent |

  @allstores
  Scenario Outline: TeacherStore:[<UsingData>]
    Given user navigates to "TeacherStore" page
    And user click on sign in link
    When user navigates to login page with username and password [UserInformation]
    When user hovers on the minicart
    And user empty the mini cart
    When user searches for an "isbn" in global search box
    And user navigates to PDP page
    When user searches for an "isbn" in global search box on PDP
    And user navigates to PDP page
    And user adds that product into cart from product detail page
    When user searches for an "keyword" in global search box
    And user adds that product into cart from search results page
    When user searches for an "author" in global search box
    And user adds that product into cart from search results page
    When user click on mini cart icon
    When user verify Check Out Now on DW page
    Then user will log out of the "teacher" store

    Examples: 
      | UsingData           |
      | HealthCheck_Teacher |

  @allstores
  Scenario Outline: TeachablesStore :[<UsingData>]
    Given user navigates to "Teachables" page
    When user navigates to the Printables site
    When Guest user logs into the application
    And a user navigates to Printables product details page of product title
    When user searches for keyword in search box
    And user search from product detail page of teachables
    And user navigates to the Printables subscription landing page using free trial product
    When user selects Logout option from left nav pane
    Examples: 
      | UsingData              |
      | HealthCheck_Printables |
      
      @allstores
  Scenario Outline: TeacherStore_Login from Shopping Cart:[<UsingData>]
    Given user navigates to "TeacherStore" page
    When user hovers on the minicart
    And user empty the mini cart
    When user searches for an "isbn" in global search box
    And user navigates to PDP page
    When user searches for an "isbn" in global search box on PDP
    And user navigates to PDP page
    And user adds that product into cart from product detail page
    When user searches for an "keyword" in global search box
    And user adds that product into cart from search results page
    When user searches for an "author" in global search box
    And user adds that product into cart from search results page
    When user click on mini cart icon
    When user click Check Out Now on DW page
    When user navigates to login page with username and password [UserInformation]
    Then user will log out of the "teacher" store

    Examples: 
      | UsingData           |
      | HealthCheck_Teacher |
