Feature: AdvanceFilter_Price


Scenario: AdvanceFilter_Price_Under $5:[<UsingData>]
	Given user navigate application
	When user selects "Under $5" Items from Price filter
	Then page loads with Products tagged with "Under $5" Items
	
Scenario: AdvanceFilter_Price_Under $25 - $50:[<UsingData>]
	Given user navigate application
	When user selects "$25 - $50" Items from Price filter
	Then page loads with Products tagged with "$25 - $50" Items
	
	
Scenario: AdvanceFilter_Price_Under $50 - $100:[<UsingData>]
	Given user navigate application
	When user selects "$50 - $100" Items from Price filter
	Then page loads with Products tagged with "$50 - $100" Items
	
	
Scenario: AdvanceFilter_Price_Under $100 - $200:[<UsingData>]
	Given user navigate application
	When user selects "$100 - $200" Items from Price filter
	Then page loads with Products tagged with "$100 - $200" Items
	

Scenario: AdvanceFilter_Price_Under Over $200:[<UsingData>]
	Given user navigate application
	When user selects "$200" Items from Price filter
	Then page loads with Products tagged with "Over $200" Items
	

