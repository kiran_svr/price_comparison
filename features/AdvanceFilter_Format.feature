Feature: AdvanceFilter_Format

    Scenario: AdvanceFilter_Format_Books:[<UsingData>]
	Given user navigate application
	When user selects "Books" Items from Format filter
	Then page loads with Products tagged with "Books" Items
	
    Scenario: AdvanceFilter_Format_Books_Individual Titles:[<UsingData>]
	Given user navigate application
	When user selects "Books | Individual Titles" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles" Items
	
    Scenario: AdvanceFilter_Format_Books_Individual Titles_Activity Book:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | Activity Book" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Activity Book" Items
	
	Scenario: AdvanceFilter_Format_Books_Individual Titles_Activity Sheet:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | Activity Sheet" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Activity Sheet" Items
	
	
    Scenario: AdvanceFilter_Format_Books_Individual Titles_Big_Book:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | Big_Book" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Big_Book" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Individual Titles_Board_Books:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | Board_Books" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Board_Books" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Individual Titles_eBook:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | eBook" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | eBook" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Individual Titles_Hardcover_Books:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | Hardcover_Books" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Hardcover_Books" Items
	
	
		
	Scenario: AdvanceFilter_Format_Books_Individual Titles_HB_Hardcover_Book_and_CD:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | HB_Hardcover_Book_and_CD" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | HB_Hardcover_Book_and_CD" Items
	
	
			
     Scenario: AdvanceFilter_Format_Books_Individual Titles_Interactive_&_Novelty_Book:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | Interactive_&_Novelty_Book" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Interactive_&_Novelty_Book" Items
	

     Scenario: AdvanceFilter_Format_Books_Individual Titles_Library_Binding:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | Library_Binding" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Library_Binding" Items
	
		
    Scenario: AdvanceFilter_Format_Books_Individual Titles_Paperback_Book:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | Paperback Book" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Library_Binding" Items
	
	
			
    Scenario: AdvanceFilter_Format_Books_Individual Titles_PB_Paperback_Book_and_Cassette:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | PB_Paperback_Book_and_Cassette" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | PB_Paperback_Book_and_Cassette" Items
	
	
	
	Scenario: AdvanceFilter_Format_Books_Individual Titles_PB_Paperback_Book_and_CD:[<UsingData>]
	Given user navigate application
	When user selects "Books | IT | PB_Paperback_Book_and_CD" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | PB_Paperback_Book_and_CD" Items
	
    Scenario: AdvanceFilter_Format_Books_Individual Titles_Picture_Book:[<UsingData>]
	Given user navigate application
	When user selects "Books| IT | Picture_Book" Items from Format filter
	Then page loads with Products tagged with "Books | Individual Titles | Picture_Book" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries:[<UsingData>]
	Given user navigate application
	When user selects "Books | Collections_and_Libraries" Items from Format filter
	Then page loads with Products tagged with "Books | Collections_and_Libraries" Items
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Assessment_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Assessment_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Assessment_Collection" Items
	
	
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Assessment_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Big_Book_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Assessment_Collection" Items
	
		
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Board_Book_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Board_Book_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Board_Book_Collection" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Boxed_Set:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Boxed_Set" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Boxed_Set" Items
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Classroom_Library:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Classroom_Library" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Classroom_Library" Items
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Classroom_Program:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Classroom_Program" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Classroom_Program" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Family_Engagement_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Family_Engagement_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Family_Engagement_Collection" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Guided_Reading_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Guided_Reading_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Guided_Reading_Collection" Items
	
		

	
	
   Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Literacy_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Literacy_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Literacy_Collection" Items
	

	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Mentoring_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Mentoring_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Mentoring_Collection" Items
	
	Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Paperback_Book_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Paperback_Book_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Paperback_Book_Collection" Items
	
		Scenario: AdvanceFilter_Format_Books_Collections_and_Libraries_Take_Home_Collection:[<UsingData>]
	Given user navigate application
	When user selects "Books | CL | Take_Home_Collection" Items from Format filter
	Then page loads with Products tagged with "Books | CL | Take_Home_Collection" Items
	

    Scenario: AdvanceFilter_Format_Books_Magazines:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines" Items
	

	Scenario: AdvanceFilter_Format_Books_Magazines_Classroom_Magazines:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | Classroom_Magazines" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | Classroom_Magazines" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Magazines_Professional_Resources:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | Professional_Resources" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | Professional_Resources" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Magazines_Activity_Pack:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | Activity_Pack" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | Activity_Pack" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Magazines_eBook:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | eBook" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | eBook" Items
	
	
		
	Scenario: AdvanceFilter_Format_Books_Magazines_Literature_Guide:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | Literature_Guide" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | Literature_Guide" Items
	
	
	Scenario: AdvanceFilter_Format_Books_Magazines_Professional_Book:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | Professional_Book" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | Professional_Book" Items
	
	Scenario: AdvanceFilter_Format_Books_Magazines_PB_Professional_Book_and_CD:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | PB_and_CD" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | PB_and_CD" Items
	
	
	
	Scenario: AdvanceFilter_Format_Books_Magazines_PB_Professional_Book_and_DVD:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | PB_and_DVD" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | PB_and_DVD" Items
	
		
	Scenario: AdvanceFilter_Format_Books_Magazines_Teaching_Guide:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | Teaching_Guide" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | Teaching_Guide" Items
	
	Scenario: AdvanceFilter_Format_Books_Magazines_TG_Student_Edition:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | TG_Student_Edition" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | TG_Student_Edition" Items
	
	Scenario: AdvanceFilter_Format_Books_Magazines_TG_Teacher_Edition:[<UsingData>]
	Given user navigate application
	When user selects "Books | Magazines | TG_Teacher_Edition" Items from Format filter
	Then page loads with Products tagged with "Books | Magazines | TG_Teacher_Edition" Items
	

	Scenario: AdvanceFilter_Format_Digital Media Resources:[<UsingData>]
	Given user navigate application
	When user selects "Digital Media Resources" Items from Format filter
	Then page loads with Products tagged with "Digital Media Resources" Items
	
	
	Scenario: AdvanceFilter_Format_Digital Media Resources_Audio:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Audio" Items from Format filter
	Then page loads with Products tagged with "DMR | Audio" Items
	

	Scenario: AdvanceFilter_Format_Digital Media Resources_Audio_CD:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Audio | CD" Items from Format filter
	Then page loads with Products tagged with "DMR | Audio | CD" Items
	

	Scenario: AdvanceFilter_Format_Digital Media Resources_Audio_CD_Hardcover_Book_and_CD:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Audio | CD | Hardcover_Book_and_CD" Items from Format filter
	Then page loads with Products tagged with "DMR | Audio | CD | Hardcover_Book_and_CD" Items
	
	
	Scenario: AdvanceFilter_Format_Digital Media Resources_Audio_CD_Paperback_Book_and_CD:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Audio | CD | Paperback_Book_and_CD" Items from Format filter
	Then page loads with Products tagged with "DMR | Audio | CD | Paperback_Book_and_CD" Items
	
    Scenario: AdvanceFilter_Format_Digital Media Resources_Audio_CD_Playaway_Audio:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Audio | CD_Playaway_Audio" Items from Format filter
	Then page loads with Products tagged with "DMR | Audio | CD_Playaway_Audio" Items
	
	
    Scenario: AdvanceFilter_Format_Digital Media Resources_Audio_DVD-Audio:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Audio | DVD-Audio" Items from Format filter
	Then page loads with Products tagged with "DMR | Audio | DVD-Audio" Items
	

	Scenario: AdvanceFilter_Format_Digital Media Resources_Software:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Software" Items from Format filter
	Then page loads with Products tagged with "DMR | Software" Items
	
	
     Scenario: AdvanceFilter_Format_Digital Media Resources_Software_DVD:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Software | DVD" Items from Format filter
	Then page loads with Products tagged with "DMR | Software | DVD" Items
	
	
	 Scenario: AdvanceFilter_Format_Digital Media Resources_Subscriptions:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Subscriptions" Items from Format filter
	Then page loads with Products tagged with "DMR | Subscriptions" Items
	
	
	Scenario: AdvanceFilter_Format_Digital Media Resources_Subscriptions_FreedomFlix:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Subscriptions | FreedomFlix" Items from Format filter
	Then page loads with Products tagged with "DMR | Subscriptions | FreedomFlix" Items
	
	
    Scenario: AdvanceFilter_Format_Digital Media Resources_Subscriptions_New_Book_of_Knowledge:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Subscriptions | New_Book_of_Knowledge" Items from Format filter
	Then page loads with Products tagged with "DMR | Subscriptions | New_Book_of_Knowledge" Items
	
	
	Scenario: AdvanceFilter_Format_Digital Media Resources_Subscriptions_Scholastic_Teachables:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Subscriptions | Scholastic_Teachables" Items from Format filter
	Then page loads with Products tagged with "DMR | Subscriptions | Scholastic_Teachables" Items
	
	
    Scenario: AdvanceFilter_Format_Digital Media Resources_Subscriptions_ScienceFlix:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Subscriptions | ScienceFlix" Items from Format filter
	Then page loads with Products tagged with "DMR | Subscriptions | ScienceFlix" Items
	

	 Scenario: AdvanceFilter_Format_Digital Media Resources_Subscriptions_Storia:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Subscriptions | Storia" Items from Format filter
	Then page loads with Products tagged with "DMR | Subscriptions | Storia" Items
	
    Scenario: AdvanceFilter_Format_Digital Media Resources_Subscriptions_Video:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Video" Items from Format filter
	Then page loads with Products tagged with "DMR | Video" Items
	
	    Scenario: AdvanceFilter_Format_Digital Media Resources_Subscriptions_Video_DVD:[<UsingData>]
	Given user navigate application
	When user selects "DMR | Video | DVD" Items from Format filter
	Then page loads with Products tagged with "DMR | Video | DVD" Items
	
     Scenario: AdvanceFilter_Format_Classroom Materials:[<UsingData>]
	Given user navigate application
	When user selects "Classroom Materials" Items from Format filter
	Then page loads with Products tagged with "Classroom Materials" Items
	
	
	     Scenario: AdvanceFilter_Format_Sales_Materials:[<UsingData>]
	Given user navigate application
	When user selects "Sales_Materials" Items from Format filter
	Then page loads with Products tagged with "Sales_Materials" Items
	
	
	