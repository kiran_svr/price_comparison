Feature: AdvanceFilter_Fiction/Non-Fiction-Genre

#-------------------------------------------------------------------------------------------------------------

Scenario: AdvanceFilter_Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction" Items from Genre filter
	Then page loads with Products tagged with "Fiction" Items
	
	 
#-------------------------------------------------------------------------------------------------------------
	
	Scenario: AdvanceFilter_Genre_Fiction_Adventure:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Adventure" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Adventure" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Advice and Self-Help:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Advice and Self-Help" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Advice and Self-Help" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Anthologies:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Anthologies" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Anthologies" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Biography and Autobiography:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Biography and Autobiography" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Biography and Autobiography" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Classics:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Classics" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Classics" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Comedy and Humor:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Comedy and Humor" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Comedy and Humor" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Comic Books and Graphic Novels:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Comic Books and Graphic Novels" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Comic Books and Graphic Novels" Items 
	
Scenario: AdvanceFilter_Genre_Fiction_Cookbooks:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Cookbooks" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Cookbooks" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Diaries and Journals:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Diaries and Journals" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Diaries and Journals" Items 
	
Scenario: AdvanceFilter_Genre_Fiction_Drama:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Drama" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Drama" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Dystopian Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Dystopian Fiction" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Dystopian Fiction" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Essays and Speeches:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Essays and Speeches" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Essays and Speeches" Items 
	
Scenario: AdvanceFilter_Genre_Fiction_Fairy_Tales_Folk_Tales_Fables:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Fairy Tales, Folk Tales, Fables" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Fairy Tales, Folk Tales, Fables" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Fantasy:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Fantasy" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Fantasy" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Fiction_Functional and How-To:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Functional and How-To" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Functional and How-To" Items 
	
Scenario: AdvanceFilter_Genre_Fiction_Historical Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Historical Fiction" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Historical Fiction" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Horror:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Horror" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Horror" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Fiction_Informational Text:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Informational Text" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Informational Text" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Jokes and Riddles:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Jokes and Riddles" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Jokes and Riddles" Items 
	
	
	
	
	
Scenario: AdvanceFilter_Genre_Fiction_Media Tie-Ins:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Media Tie-Ins" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Media Tie-Ins" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Mystery and Suspense:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Mystery and Suspense" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Mystery and Suspense" Items 
	
	

	
	
Scenario: AdvanceFilter_Genre_Fiction_Myths and Legends:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Myths and Legends" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Myths and Legends" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Poetry, Songs, Verse:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Poetry, Songs, Verse" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Poetry, Songs, Verse" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Realistic Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Realistic Fiction" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Realistic Fiction" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Fiction_Reference:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Reference" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Reference" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Romance:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Romance" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Romance" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Fiction_Science Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Science Fiction" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Science Fiction" Items 
	
	
	
	
Scenario: AdvanceFilter_Genre_Fiction_Short Stories:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Short Stories" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Short Stories" Items 
	
	
Scenario: AdvanceFilter_Genre_Fiction_Young Adult:[<UsingData>] 
	Given user navigate application 
	When user selects "Fiction;Young Adult" Items from Genre filter
	Then page loads with Products tagged with "Fiction;Young Adult" Items 
	
	
	#-----------------------------------------------------------------------------------------------------------
	
	Scenario: AdvanceFilter_Non-Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction" Items from Genre filter
	Then page loads with Products tagged with "Non-Fiction" Items 
	
	#------------------------------------------------------------------------------------------------------------
	
	Scenario: AdvanceFilter_Genre_Adventure:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Adventure" Items from Genre filter
	Then page loads with Products tagged with "Adventure" Items 
	
	
Scenario: AdvanceFilter_Genre_Advice and Self-Help:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Advice and Self-Help" Items from Genre filter
	Then page loads with Products tagged with "Advice and Self-Help" Items 
	
	
Scenario: AdvanceFilter_Genre_Anthologies:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Anthologies" Items from Genre filter
	Then page loads with Products tagged with "Anthologies" Items 
	
	
Scenario: AdvanceFilter_Genre_Biography and Autobiography:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Biography and Autobiography" Items from Genre filter
	Then page loads with Products tagged with "Biography and Autobiography" Items 
	
	
Scenario: AdvanceFilter_Genre_Classics:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Classics" Items from Genre filter
	Then page loads with Products tagged with "Classics" Items 
	
	
Scenario: AdvanceFilter_Genre_Comedy and Humor:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Comedy and Humor" Items from Genre filter
	Then page loads with Products tagged with "Comedy and Humor" Items 
	
	
Scenario: AdvanceFilter_Genre_Comic Books and Graphic Novels:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Comic Books and Graphic Novels" Items from Genre filter
	Then page loads with Products tagged with "Comic Books and Graphic Novels" Items 
	
	
Scenario: AdvanceFilter_Genre_Diaries and Journals:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Diaries and Journals" Items from Genre filter
	Then page loads with Products tagged with "Diaries and Journals" Items 
	
Scenario: AdvanceFilter_Genre_Drama:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Drama" Items from Genre filter
	Then page loads with Products tagged with "Drama" Items 
	

	
Scenario: AdvanceFilter_Genre_Essays and Speeches:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Essays and Speeches" Items from Genre filter
	Then page loads with Products tagged with "Essays and Speeches" Items 
	
Scenario: AdvanceFilter_Genre_Fairy_Tales_Folk_Tales_Fables:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Fairy Tales, Folk Tales, Fables" Items from Genre filter
	Then page loads with Products tagged with "Fairy Tales, Folk Tales, Fables" Items 
	
	
Scenario: AdvanceFilter_Genre_Fantasy:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Fantasy" Items from Genre filter
	Then page loads with Products tagged with "Fantasy" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Functional and How-To:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Functional and How-To" Items from Genre filter
	Then page loads with Products tagged with "Functional and How-To" Items 
	
Scenario: AdvanceFilter_Genre_Historical Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Historical Fiction" Items from Genre filter
	Then page loads with Products tagged with "Historical Fiction" Items 
	
	
Scenario: AdvanceFilter_Genre_Horror:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Horror" Items from Genre filter
	Then page loads with Products tagged with "Horror" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Informational Text:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Informational Text" Items from Genre filter
	Then page loads with Products tagged with "Informational Text" Items 
	
	
	
		Scenario: AdvanceFilter_Genre_Interviews:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Interviews" Items from Genre filter
	Then page loads with Products tagged with "Interviews" Items 
	
	
Scenario: AdvanceFilter_Genre_Jokes and Riddles:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Jokes and Riddles" Items from Genre filter
	Then page loads with Products tagged with "Jokes and Riddles" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Jokes and Riddles:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Jokes and Riddles" Items from Genre filter
	Then page loads with Products tagged with "Jokes and Riddles" Items 
	
	
Scenario: AdvanceFilter_Genre_Media Tie-Ins:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Media Tie-Ins" Items from Genre filter
	Then page loads with Products tagged with "Media Tie-Ins" Items 
	
	
Scenario: AdvanceFilter_Genre_Mystery and Suspense:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Mystery and Suspense" Items from Genre filter
	Then page loads with Products tagged with "Mystery and Suspense" Items 
	

	
Scenario: AdvanceFilter_Genre_Myths and Legends:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Myths and Legends" Items from Genre filter
	Then page loads with Products tagged with "Myths and Legends" Items 
	
	
Scenario: AdvanceFilter_Genre_Poetry, Songs, Verse:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Poetry, Songs, Verse" Items from Genre filter
	Then page loads with Products tagged with "Poetry, Songs, Verse" Items 
	
	
Scenario: AdvanceFilter_Genre_Realistic Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Realistic Fiction" Items from Genre filter
	Then page loads with Products tagged with "Realistic Fiction" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Reference:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Reference" Items from Genre filter
	Then page loads with Products tagged with "Reference" Items 
	
 		
Scenario: AdvanceFilter_Genre_Science Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Science Fiction" Items from Genre filter
	Then page loads with Products tagged with "Science Fiction" Items 
	
	
	
	
Scenario: AdvanceFilter_Genre_Short Stories:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Short Stories" Items from Genre filter
	Then page loads with Products tagged with "Short Stories" Items 
	
	
Scenario: AdvanceFilter_Genre_Young Adult:[<UsingData>] 
	Given user navigate application 
	When user selects "Nonfiction;Young Adult" Items from Genre filter
	Then page loads with Products tagged with "Young Adult" Items 
	
	
	
	
