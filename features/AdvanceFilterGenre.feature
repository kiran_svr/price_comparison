Feature: AdvanceFilter_Genre

	
	
Scenario: AdvanceFilter_Genre_Adventure:[<UsingData>] 
	Given user navigate application 
	When user selects "Adventure" Items from Genre filter
	Then page loads with Products tagged with "Adventure" Items 
	
	
Scenario: AdvanceFilter_Genre_Advice and Self-Help:[<UsingData>] 
	Given user navigate application 
	When user selects "Advice and Self-Help" Items from Genre filter
	Then page loads with Products tagged with "Advice and Self-Help" Items 
	
	
Scenario: AdvanceFilter_Genre_Anthologies:[<UsingData>] 
	Given user navigate application 
	When user selects "Anthologies" Items from Genre filter
	Then page loads with Products tagged with "Anthologies" Items 
	
	
Scenario: AdvanceFilter_Genre_Biography and Autobiography:[<UsingData>] 
	Given user navigate application 
	When user selects "Biography and Autobiography" Items from Genre filter
	Then page loads with Products tagged with "Biography and Autobiography" Items 
	
	
Scenario: AdvanceFilter_Genre_Classics:[<UsingData>] 
	Given user navigate application 
	When user selects "Classics" Items from Genre filter
	Then page loads with Products tagged with "Classics" Items 
	
	
Scenario: AdvanceFilter_Genre_Comedy and Humor:[<UsingData>] 
	Given user navigate application 
	When user selects "Comedy and Humor" Items from Genre filter
	Then page loads with Products tagged with "Comedy and Humor" Items 
	
	
Scenario: AdvanceFilter_Genre_Comic Books and Graphic Novels:[<UsingData>] 
	Given user navigate application 
	When user selects "Comic Books and Graphic Novels" Items from Genre filter
	Then page loads with Products tagged with "Comic Books and Graphic Novels" Items 
	
Scenario: AdvanceFilter_Genre_Cookbooks:[<UsingData>] 
	Given user navigate application 
	When user selects "Cookbooks" Items from Genre filter
	Then page loads with Products tagged with "Cookbooks" Items 
	
	
Scenario: AdvanceFilter_Genre_Diaries and Journals:[<UsingData>] 
	Given user navigate application 
	When user selects "Diaries and Journals" Items from Genre filter
	Then page loads with Products tagged with "Diaries and Journals" Items 
	
Scenario: AdvanceFilter_Genre_Drama:[<UsingData>] 
	Given user navigate application 
	When user selects "Drama" Items from Genre filter
	Then page loads with Products tagged with "Drama" Items 
	
	
Scenario: AdvanceFilter_Genre_Dystopian Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Dystopian Fiction" Items from Genre filter
	Then page loads with Products tagged with "Dystopian Fiction" Items 
	
	
Scenario: AdvanceFilter_Genre_Essays and Speeches:[<UsingData>] 
	Given user navigate application 
	When user selects "Essays and Speeches" Items from Genre filter
	Then page loads with Products tagged with "Essays and Speeches" Items 
	
Scenario: AdvanceFilter_Genre_Fairy_Tales_Folk_Tales_Fables:[<UsingData>] 
	Given user navigate application 
	When user selects "Fairy Tales, Folk Tales, Fables" Items from Genre filter
	Then page loads with Products tagged with "Fairy Tales, Folk Tales, Fables" Items 
	
	
Scenario: AdvanceFilter_Genre_Fantasy:[<UsingData>] 
	Given user navigate application 
	When user selects "Fantasy" Items from Genre filter
	Then page loads with Products tagged with "Fantasy" Items 
	
		
	
Scenario: AdvanceFilter_Genre_Functional and How-To:[<UsingData>] 
	Given user navigate application 
	When user selects "Functional and How-To" Items from Genre filter
	Then page loads with Products tagged with "Functional and How-To" Items 
	
Scenario: AdvanceFilter_Genre_Historical Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Historical Fiction" Items from Genre filter
	Then page loads with Products tagged with "Historical Fiction" Items 
	
	
Scenario: AdvanceFilter_Genre_Horror:[<UsingData>] 
	Given user navigate application 
	When user selects "Horror" Items from Genre filter
	Then page loads with Products tagged with "Horror" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Informational Text:[<UsingData>] 
	Given user navigate application 
	When user selects "Informational Text" Items from Genre filter
	Then page loads with Products tagged with "Informational Text" Items 
	
	
	
	Scenario: AdvanceFilter_Genre_Interviews:[<UsingData>] 
	Given user navigate application 
	When user selects "Interviews" Items from Genre filter
	Then page loads with Products tagged with "Interviews" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Jokes and Riddles:[<UsingData>] 
	Given user navigate application 
	When user selects "Jokes and Riddles" Items from Genre filter
	Then page loads with Products tagged with "Jokes and Riddles" Items 
	
	

Scenario: AdvanceFilter_Genre_Media Tie-Ins:[<UsingData>] 
	Given user navigate application 
	When user selects "Media Tie-Ins" Items from Genre filter
	Then page loads with Products tagged with "Media Tie-Ins" Items 
	
	
Scenario: AdvanceFilter_Genre_Mystery and Suspense:[<UsingData>] 
	Given user navigate application 
	When user selects "Mystery and Suspense" Items from Genre filter
	Then page loads with Products tagged with "Mystery and Suspense" Items 
	

	
	
Scenario: AdvanceFilter_Genre_Myths and Legends:[<UsingData>] 
	Given user navigate application 
	When user selects "Myths and Legends" Items from Genre filter
	Then page loads with Products tagged with "Myths and Legends" Items 
	
	
Scenario: AdvanceFilter_Genre_Poetry, Songs, Verse:[<UsingData>] 
	Given user navigate application 
	When user selects "Poetry, Songs, Verse" Items from Genre filter
	Then page loads with Products tagged with "Poetry, Songs, Verse" Items 
	
	
Scenario: AdvanceFilter_Genre_Realistic Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Realistic Fiction" Items from Genre filter
	Then page loads with Products tagged with "Realistic Fiction" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Reference:[<UsingData>] 
	Given user navigate application 
	When user selects "Reference" Items from Genre filter
	Then page loads with Products tagged with "Reference" Items 
	
	
Scenario: AdvanceFilter_Genre_Romance:[<UsingData>] 
	Given user navigate application 
	When user selects "Romance" Items from Genre filter
	Then page loads with Products tagged with "Romance" Items 
	
	
	
Scenario: AdvanceFilter_Genre_Science Fiction:[<UsingData>] 
	Given user navigate application 
	When user selects "Science Fiction" Items from Genre filter
	Then page loads with Products tagged with "Science Fiction" Items 
	
	
	
	
Scenario: AdvanceFilter_Genre_Short Stories:[<UsingData>] 
	Given user navigate application 
	When user selects "Short Stories" Items from Genre filter
	Then page loads with Products tagged with "Short Stories" Items 
	
	
Scenario: AdvanceFilter_Genre_Young Adult:[<UsingData>] 
	Given user navigate application 
	When user selects "Young Adult" Items from Genre filter
	Then page loads with Products tagged with "Young Adult" Items 
	
	