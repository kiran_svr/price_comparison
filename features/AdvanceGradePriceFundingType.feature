Feature: AdvanceFilter_Grade_Price_FundingType


Scenario: AdvanceFilter_Grade PreK Price Under $5 FundingType TitleI Part A:[<UsingData>] 
	Given user navigate application
	When user selects "PreK;Under $5;TitleI Part A" Items from Advance filter
	Then page loads with Products tagged with "Grade PreK Price Under $5 TitleI Part A" Items
	
Scenario: AdvanceFilter_Grade PreK Price $5 - $25 FundingType TitleI Part A:[<UsingData>] 
	Given user navigate application
	When user selects "PreK;$5 - $25;TitleI Part A" Items from Advance filter
	Then page loads with Products tagged with "Grade PreK Price Under $5" Items
	
	
Scenario: AdvanceFilter_Grade PreK Price $25 - $50 FundingType TitleI Part A:[<UsingData>] 
	Given user navigate application
	When user selects "PreK;$25 - $50;TitleI Part A" Items from Advance filter
	Then page loads with Products tagged with "Grade PreK Price $25 - $50 TitleI Part A" Items
	
	
	
Scenario: AdvanceFilter_Grade PreK Price $50 - $100 FundingType TitleI Part A:[<UsingData>] 
	Given user navigate application
	When user selects "PreK;$50 - $100;TitleI Part A" Items from Advance filter
	Then page loads with Products tagged with "Grade PreK Price $50 - $100 TitleI Part A" Items
	
	
Scenario: AdvanceFilter_Grade PreK Price $100 - $200 FundingType TitleI Part A:[<UsingData>] 
	Given user navigate application
	When user selects "PreK;$100 - $200;TitleI Part A" Items from Advance filter
	Then page loads with Products tagged with "Grade PreK Price $100 - $200 TitleI Part A" Items
	
Scenario: AdvanceFilter_Grade PreK Price over200 FundingType TitleI Part A :[<UsingData>] 
	Given user navigate application
	When user selects "PreK;$200;TitleI Part A" Items from Advance filter
	Then page loads with Products tagged with "Grade PreK Price over200 TitleI Part A" Items
	
	
	
Scenario: AdvanceFilter_Grade 12 Price Under $5 FundingType SpecialEducationRTI:[<UsingData>] 
	Given user navigate application
	When user selects "Grade 12;Under $5;SpecialEducationRTI" Items from Advance filter
	Then page loads with Products tagged with "Grade 12 Price Under $5 SpecialEducationRTI" Items
	
Scenario: AdvanceFilter_Grade 12 Price $5 - $25 FundingType SpecialEducationRTI:[<UsingData>] 
	Given user navigate application
	When user selects "Grade 12;$5 - $25;SpecialEducationRTI" Items from Advance filter
	Then page loads with Products tagged with "Grade 12 Price Under $5 SpecialEducationRTI" Items
	
	
Scenario: AdvanceFilter_Grade 12 Price $25 - $50 FundingType SpecialEducationRTI:[<UsingData>] 
	Given user navigate application
	When user selects "Grade 12;$25 - $50;SpecialEducationRTI" Items from Advance filter
	Then page loads with Products tagged with "Grade 12 Price $25 - $50 SpecialEducationRTI" Items
	
	
	
Scenario: AdvanceFilter_Grade 12 Price $50 - $100 FundingType SpecialEducationRTI:[<UsingData>] 
	Given user navigate application
	When user selects "Grade 12;$50 - $100;SpecialEducationRTI" Items from Advance filter
	Then page loads with Products tagged with "Grade 12 Price $50 - $100 SpecialEducationRTI" Items
	
	
Scenario: AdvanceFilter_Grade 12 Price $100 - $200 FundingType SpecialEducationRTI:[<UsingData>] 
	Given user navigate application
	When user selects "Grade 12;$100 - $200;SpecialEducationRTI" Items from Advance filter
	Then page loads with Products tagged with "Grade 12 Price $100 - $200 SpecialEducationRTI" Items
	
Scenario: AdvanceFilter_Grade 12 Price over200 FundingType SpecialEducationRTI:[<UsingData>] 
	Given user navigate application
	When user selects "Grade 12;$200;SpecialEducationRTI" Items from Advance filter
	Then page loads with Products tagged with "Grade 12 Price over200 SpecialEducationRTI" Items
	
	
	
	
	
	