Feature: AdvanceFilter_subject 


Scenario: AdvanceFilter_Subject_Adventures and Adventurers:[<UsingData>] 
	Given user navigate application 
	When user selects "Adventures and Adventurers" Items from Subject filter 
	Then page loads with Products tagged with "Adventures and Adventurers" Items 
	
Scenario: AdvanceFilter_Subject_Animals:[<UsingData>] 
	Given user navigate application 
	When user selects "Animals" Items from Subject filter 
	Then page loads with Products tagged with "Animals" Items 
	
Scenario: AdvanceFilter_Arts and Creativity:[<UsingData>] 
	Given user navigate application 
	When user selects "Arts and Creativity" Items from Subject filter 
	Then page loads with Products tagged with "Arts and Creativity" Items 
	
	
	
Scenario: AdvanceFilter_Subject_Bedtime, Sleep, Dreams:[<UsingData>] 
	Given user navigate application 
	When user selects "Bedtime, Sleep, Dreams" Items from Subject filter 
	Then page loads with Products tagged with "Bedtime, Sleep, Dreams" Items 
	
Scenario: AdvanceFilter_Subject_Character and Values:[<UsingData>] 
	Given user navigate application 
	When user selects "Character and Values" Items from Subject filter 
	Then page loads with Products tagged with "Character and Values" Items 
	
	
Scenario: AdvanceFilter_Subject_Child Development and Behavior:[<UsingData>] 
	Given user navigate application 
	When user selects "Child Development and Behavior" Items from Subject filter 
	Then page loads with Products tagged with "Child Development and Behavior" Items 
	
	
Scenario: AdvanceFilter_Subject_Families and Relationships:[<UsingData>] 
	Given user navigate application 
	When user selects "Families and Relationships" Items from Subject filter 
	Then page loads with Products tagged with "Families and Relationships" Items 
	
	
Scenario: AdvanceFilter_Subject_Feelings and Emotions:[<UsingData>] 
	Given user navigate application 
	When user selects "Feelings and Emotions" Items from Subject filter 
	Then page loads with Products tagged with "Feelings and Emotions" Items 
	
	
Scenario: AdvanceFilter_Subject_Food and Drinks:[<UsingData>] 
	Given user navigate application 
	When user selects "Food and Drinks" Items from Subject filter 
	Then page loads with Products tagged with "Food and Drinks" Items 
	
Scenario: AdvanceFilter_Subject_Foreign Languages and English Language Learning:[<UsingData>] 
	Given user navigate application 
	When user selects "FL and EL Learning" Items from Subject filter 
	Then page loads with Products tagged with "Foreign Languages and English Language Learning" Items 
	
	
Scenario: AdvanceFilter_Subject_Health and Safety:[<UsingData>] 
	Given user navigate application 
	When user selects "Health and Safety" Items from Subject filter 
	Then page loads with Products tagged with "Health and Safety" Items 
	
	
	
Scenario: AdvanceFilter_Subject_Hobbies, Play, Recreation:[<UsingData>] 
	Given user navigate application 
	When user selects "Hobbies, Play, Recreation" Items from Subject filter 
	Then page loads with Products tagged with "Hobbies, Play, Recreation" Items 
	
Scenario: AdvanceFilter_Subject_Holidays and Celebrations:[<UsingData>] 
	Given user navigate application 
	When user selects "Holidays and Celebrations" Items from Subject filter 
	Then page loads with Products tagged with "Holidays and Celebrations" Items 
	
Scenario: AdvanceFilter_Subject_Language Arts:[<UsingData>] 
	Given user navigate application 
	When user selects "Language Arts" Items from Subject filter 
	Then page loads with Products tagged with "Language Arts" Items 
	
	
Scenario: AdvanceFilter_Subject_Life Experiences:[<UsingData>] 
	Given user navigate application 
	When user selects "Life Experiences" Items from Subject filter 
	Then page loads with Products tagged with "Life Experiences" Items 
	
	
Scenario: AdvanceFilter_Subject_Magic and Supernatural:[<UsingData>] 
	Given user navigate application 
	When user selects "Magic and Supernatural" Items from Subject filter 
	Then page loads with Products tagged with "Magic and_Supernatural" Items 
	
	
	
Scenario: AdvanceFilter_Subject_Popular Culture:[<UsingData>] 
	Given user navigate application 
	When user selects "Popular Culture" Items from Subject filter 
	Then page loads with Products tagged with "Popular Culture" Items 
	
Scenario: AdvanceFilter_Subject_Religion and Philosophy:[<UsingData>] 
	Given user navigate application 
	When user selects "Religion and Philosophy" Items from Subject filter 
	Then page loads with Products tagged with "Religion and Philosophy" Items 
	
	
	
Scenario: AdvanceFilter_Subject_School Life:[<UsingData>] 
	Given user navigate application 
	When user selects "School Life" Items from Subject filter 
	Then page loads with Products tagged with "School Life" Items 
	
	
	
Scenario: AdvanceFilter_Subject_Science and Technology:[<UsingData>] 
	Given user navigate application 
	When user selects "Science and Technology" Items from Subject filter 
	Then page loads with Products tagged with "Science and Technology" Items 
	
Scenario: AdvanceFilter_Subject_Seasons:[<UsingData>] 
	Given user navigate application 
	When user selects "Seasons" Items from Subject filter 
	Then page loads with Products tagged with "Seasons" Items 
	
Scenario: AdvanceFilter_Subject_Social Studies:[<UsingData>] 
	Given user navigate application 
	When user selects "Social Studies" Items from Subject filter 
	Then page loads with Products tagged with "Social Studies" Items 
	
	
Scenario: AdvanceFilter_Subject_Sports:[<UsingData>] 
	Given user navigate application 
	When user selects "Sports" Items from Subject filter 
	Then page loads with Products tagged with "Sports" Items 
	
Scenario: AdvanceFilter_Subject_Transportation :[<UsingData>] 
	Given user navigate application 
	When user selects "Transportation" Items from Subject filter 
	Then page loads with Products tagged with "Transportation" Items 
	
	
	
	