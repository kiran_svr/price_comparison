Feature: AdvanceFilter_Grade_Price 


Scenario: AdvanceFilter_Grade PreK Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "PreK;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade PreK Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade PreK Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "PreK;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade PreK Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade PreK Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "PreK;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade PreK Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade PreK Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "PreK;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade PreK Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade PreK Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "PreK;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade PreK Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade PreK Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "PreK;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade PreK Price over200" Items 
	
	
	
Scenario: AdvanceFilter_Grade K Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "K;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade K Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade K Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "K;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade K Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade K Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "K;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade K Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade K Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "K;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade K Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade K Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "K;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade K Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade K Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "K;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade K Price over200" Items 
	
	
Scenario: AdvanceFilter_Grade 1 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 1;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 1 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 1 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 1;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 1 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 1 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 1;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 1 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 1 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 1;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 1 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 1 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 1;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 1 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 1 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 1;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 1 Price over200" Items 
	
	
	
	
Scenario: AdvanceFilter_Grade 2 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 2;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 2 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 2 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 2;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 2 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 2 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 2;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 2 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 2 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 2;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 2 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 2 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 2;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 2 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 2 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 2;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 2 Price over200" Items 
	
	
	
	
Scenario: AdvanceFilter_Grade 3 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 3;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 3 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 3 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 3;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 3 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 3 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 3;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 3 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 3 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 3;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 3 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 3 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 3;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 3 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 3 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 3;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 3 Price over200" Items 
	
	
	
	
Scenario: AdvanceFilter_Grade 4 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 4;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 4 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 4 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 4;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 4 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 4 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 4;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 4 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 4 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 4;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 4 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 4 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 4;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 4 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 4 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 4;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 4 Price over200" Items 
	
	
	
Scenario: AdvanceFilter_Grade 5 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 5;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 5 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 5 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 5;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 5 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 5 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 5;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 5 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 5 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 5;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 5 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 5 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 5;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 5 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 5 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 5;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 5 Price over200" Items 
	
	
	
	
Scenario: AdvanceFilter_Grade 6 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 6;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 6 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 6 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 6;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 6 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 6 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 6;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 6 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 6 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 6;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 6 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 6 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 6;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 6 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 6 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 6;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 6 Price over200" Items 
	
	
	
Scenario: AdvanceFilter_Grade 7 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 7;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 7 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 7 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 7;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 7 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 7 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 7;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 7 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 7 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 7;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 7 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 7 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 7;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 7 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 7 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 7;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 7 Price over200" Items 
	
	
Scenario: AdvanceFilter_Grade 8 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 8;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 8 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 8 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 8;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 8 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 8 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 8;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 8 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 8 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 8;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 8 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 8 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 8;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 8 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 8 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 8;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 8 Price over200" Items 
	
	
Scenario: AdvanceFilter_Grade 9 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 9;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 9 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 9 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 9;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 9 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 9 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 9;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 9 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 9 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 9;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 9 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 9 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 9;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 9 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 9 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 9;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 9 Price over200" Items 
	
	
	
	
Scenario: AdvanceFilter_Grade 10 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 10;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 10 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 10 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 10;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 10 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 10 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 10;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 10 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 10 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 10;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 10 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 10 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 10;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 3 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 10 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 10;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 10 Price over200" Items 
	
	
	
Scenario: AdvanceFilter_Grade 11 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 11;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 11 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 11 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 11;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 11 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 11 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 11;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 11 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 11 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 11;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 11 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 11 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 11;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 11 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 11 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 11;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 11 Price over200" Items 
	
	
	
	
Scenario: AdvanceFilter_Grade 12 Price Under $5:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 12;Under $5" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 12 Price Under $5" Items 
	
Scenario: AdvanceFilter_Grade 12 Price $5 - $25:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 12;$5 - $25" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 12 Price Under $5" Items 
	
	
Scenario: AdvanceFilter_Grade 12 Price $25 - $50:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 12;$25 - $50" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 12 Price $25 - $50" Items 
	
	
	
Scenario: AdvanceFilter_Grade 12 Price $50 - $100:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 12;$50 - $100" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 12 Price $50 - $100" Items 
	
	
Scenario: AdvanceFilter_Grade 12 Price $100 - $200:[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 12;$100 - $200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 12 Price $100 - $200" Items 
	
Scenario: AdvanceFilter_Grade 12 Price over200 :[<UsingData>] 
	Given user navigate application 
	When user selects "Grade 12;$200" Items from GRADE filter 
	Then page loads with Products tagged with "Grade 12 Price over200" Items