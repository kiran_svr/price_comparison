Feature: TSO_Sorting

Scenario: TSO_Sorting_Relevance:[<UsingData>]
	Given user navigate application
	When user selects "Relevance" Items from Sort
	Then page loads with Products tagged with "Relevance" Items
	
Scenario: TSO_Sorting_Title A-Z:[<UsingData>]
	Given user navigate application
	When user selects "Title A-Z" Items from Sort
	Then page loads with Products tagged with "Title A-Z" Items 
	
	
Scenario: TSO_Sorting_Title_Z-A:[<UsingData>]
	Given user navigate application
	When user selects "Title Z-A" Items from Sort
	Then page loads with Products tagged with "Title Z-A" Items
	
	
Scenario: TSO_Sorting_Price_Low_to_High:[<UsingData>]
	Given user navigate application
	When user selects "Price Low to High" Items from Sort
	Then page loads with Products tagged with "Price Low to High" Items
	

Scenario: TSO_Sorting_Price_High_to_Low:[<UsingData>]
	Given user navigate application
	When user selects "Price High to Low" Items from Sort
	Then page loads with Products tagged with "Price High to Low" Items
	

Scenario: TSO_Sorting_Most_Recent:[<UsingData>]
	Given user navigate application
	When user selects "Most Recent" Items from Sort
	Then page loads with Products tagged with "Most Recent" Items 
	

Scenario: TSO_Sorting_Best_Sellers:[<UsingData>]
	Given user navigate application
	When user selects "Best Sellers" Items from Sort
	Then page loads with Products tagged with "Best Sellers" Items 
	
	
	
	
	
	
	#----------------------------------Title A-Z---------------------------------------------------
	
	
	Scenario: TSO_Sorting_Grade__Title A-Z:[<UsingData>]
	Given user navigate application
	When user selects "Title A-Z;Grade 1" Items from Sort
	Then page loads with Products tagged with "Title A-Z;Grade 1" Items 
	
	
	Scenario: TSO_Sorting_Price_Title A-Z_:[<UsingData>]
	Given user navigate application
	When user selects "$5 - $25;Title A-Z" Items from Sort
	Then page loads with Products tagged with "$5 - $25;Title A-Z" Items 
	
	
	Scenario: TSO_Sorting_Genre_Title A-Z:[<UsingData>]
	Given user navigate application
	When user selects "Adventure;Title A-Z" Items from Sort
	Then page loads with Products tagged with "Adventure;Title A-Z" Items
	
	
	Scenario: TSO_Sorting_FundingType_Title A-Z:[<UsingData>]
	Given user navigate application
	When user selects "TitleI Part A;Title A-Z" Items from Sort
	Then page loads with Products tagged with "TitleI Part A;Title A-Z" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Title A-Z:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Title A-Z" Items from Sort
	Then page loads with Products tagged with "Fiction;Title A-Z" Items 
	
	
	Scenario: TSO_Sorting_Nonfiction_Title A-Z:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Title A-Z" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Title A-Z" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Genre_Title A-Z:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Adventure;Title A-Z" Items from Sort
	Then page loads with Products tagged with "Fiction;Adventure;Title A-Z" Items 
	
	
		Scenario: TSO_Sorting_NonFiction_Genre_Title A-Z:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Adventure;Title A-Z" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Adventure;Title A-Z" Items 
	
	
	
	#----------------------------------Title Z-A----------------------------------------------------
	
		Scenario: TSO_Sorting_Grade_Title Z-A:[<UsingData>]
	Given user navigate application
	When user selects "Title Z-A;Grade 1" Items from Sort
	Then page loads with Products tagged with "Title Z-A;Grade 1" Items 
	
	
	Scenario: TSO_Sorting_Price_Title Z-A_:[<UsingData>]
	Given user navigate application
	When user selects "$5 - $25;Title Z-A" Items from Sort
	Then page loads with Products tagged with "$5 - $25;Title Z-A" Items 
	
	
	Scenario: TSO_Sorting_Genre_Title Z-A:[<UsingData>]
	Given user navigate application
	When user selects "Adventure;Title Z-A" Items from Sort
	Then page loads with Products tagged with "Adventure;Title Z-A" Items
	
	
	Scenario: TSO_Sorting_FundingType_Title Z-A:[<UsingData>]
	Given user navigate application
	When user selects "TitleI Part A;Title Z-A" Items from Sort
	Then page loads with Products tagged with "TitleI Part A;Title Z-A" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Title Z-A:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Title Z-A" Items from Sort
	Then page loads with Products tagged with "Fiction;Title Z-A" Items 
	
	
	Scenario: TSO_Sorting_Nonfiction_Title Z-A:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Title Z-A" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Title Z-A" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Genre_Title Z-A:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Adventure;Title Z-A" Items from Sort
	Then page loads with Products tagged with "Fiction;Adventure;Title Z-A" Items 
	

		Scenario: TSO_Sorting_NonFiction_Genre_Title Z-A:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Adventure;Title Z-A" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Adventure;" Items 
	
	
	#--------------------------------------Low_to_High---------------------------------------------------------
	
	
		Scenario: TSO_Sorting_Grade_Low_to_High:[<UsingData>]
	Given user navigate application
	When user selects "Price_Low_to_High;Grade 1" Items from Sort
	Then page loads with Products tagged with "Low_to_High;Grade 1" Items 
	
	
	Scenario: TSO_Sorting_Price_Low_to_High_:[<UsingData>]
	Given user navigate application
	When user selects "$5 - $25;Low_to_High" Items from Sort
	Then page loads with Products tagged with "$5 - $25;Low_to_High" Items 
	
	
	Scenario: TSO_Sorting_Genre_Low_to_High:[<UsingData>]
	Given user navigate application
	When user selects "Adventure;Low_to_High" Items from Sort
	Then page loads with Products tagged with "Adventure;Low_to_High" Items
	
	
	Scenario: TSO_Sorting_FundingType_Low_to_High:[<UsingData>]
	Given user navigate application
	When user selects "TitleI Part A;Low_to_High" Items from Sort
	Then page loads with Products tagged with "TitleI Part A;Low_to_High" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Low_to_High:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Low_to_High" Items from Sort
	Then page loads with Products tagged with "Fiction;Low_to_High" Items 
	
	
	Scenario: TSO_Sorting_Nonfiction_Low_to_High:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Low_to_High" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Low_to_High" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Genre_Low_to_High:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Adventure;Low_to_High" Items from Sort
	Then page loads with Products tagged with "Fiction;Adventure;Low_to_High" Items 
	

		Scenario: TSO_Sorting_NonFiction_Genre_Low_to_High:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Adventure;Low_to_High" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Adventure;Low_to_High" Items 
	
	
	#--------------------------------------High to Low---------------------------------------------------------
	
			Scenario: TSO_Sorting_Grade_High to Low:[<UsingData>]
	Given user navigate application
	When user selects "High to Low;Grade 1" Items from Sort
	Then page loads with Products tagged with "High to Low;Grade 1" Items 
	
	
	Scenario: TSO_Sorting_Price_High to Low_:[<UsingData>]
	Given user navigate application
	When user selects "$5 - $25;High to Low" Items from Sort
	Then page loads with Products tagged with "$5 - $25;High to Low" Items 
	
	
	Scenario: TSO_Sorting_Genre_High to Low:[<UsingData>]
	Given user navigate application
	When user selects "Adventure;High to Low" Items from Sort
	Then page loads with Products tagged with "Adventure;High to Low" Items
	
	
	Scenario: TSO_Sorting_FundingType_High to Low:[<UsingData>]
	Given user navigate application
	When user selects "TitleI Part A;High to Low" Items from Sort
	Then page loads with Products tagged with "TitleI Part A;High to Low" Items 
	
	
	Scenario: TSO_Sorting_Fiction_High to Low:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;High to Low" Items from Sort
	Then page loads with Products tagged with "Fiction;High to Low" Items 
	
	
	Scenario: TSO_Sorting_Nonfiction_High to Low:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;High to Low" Items from Sort
	Then page loads with Products tagged with "Nonfiction;High to Low" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Genre_High to Low:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Adventure;High to Low" Items from Sort
	Then page loads with Products tagged with "Fiction;Adventure;High to Low" Items 
	

		Scenario: TSO_Sorting_NonFiction_Genre_High to Low:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Adventure;High to Low" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Adventure;High to Low" Items 
	
		#--------------------------------------Most Recent---------------------------------------------------------
	
	
				Scenario: TSO_Sorting_Grade_Most Recent:[<UsingData>]
	Given user navigate application
	When user selects "Most Recent;Grade 1" Items from Sort
	Then page loads with Products tagged with "Most Recent;Grade 1" Items 
	
	
	Scenario: TSO_Sorting_Price_Most Recent_:[<UsingData>]
	Given user navigate application
	When user selects "$5 - $25;Most Recent" Items from Sort
	Then page loads with Products tagged with "$5 - $25;Most Recent" Items 
	
	
	Scenario: TSO_Sorting_Genre_Most Recent:[<UsingData>]
	Given user navigate application
	When user selects "Adventure;Most Recent" Items from Sort
	Then page loads with Products tagged with "Adventure;Most Recent" Items
	
	
	Scenario: TSO_Sorting_FundingType_Most Recent:[<UsingData>]
	Given user navigate application
	When user selects "TitleI Part A;Most Recent" Items from Sort
	Then page loads with Products tagged with "TitleI Part A;Most Recent" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Most Recent:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Most Recent" Items from Sort
	Then page loads with Products tagged with "Fiction;Most Recent" Items 
	
	
	Scenario: TSO_Sorting_Nonfiction_Most Recent:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Most Recent" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Most Recent" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Genre_Most Recent:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Adventure;Most Recent" Items from Sort
	Then page loads with Products tagged with "Fiction;Adventure;Most Recent" Items 
	

		Scenario: TSO_Sorting_NonFiction_Genre_Most Recent:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Adventure;Most Recent" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Adventure;Most Recent" Items 
	
		#--------------------------------------Best Sellers---------------------------------------------------------
	
		
				Scenario: TSO_Sorting_Grade_Best Sellers:[<UsingData>]
	Given user navigate application
	When user selects "Best Sellers;Grade 1" Items from Sort
	Then page loads with Products tagged with "Best Sellers;Grade 1" Items 
	
	
	Scenario: TSO_Sorting_Price_Best Sellers_:[<UsingData>]
	Given user navigate application
	When user selects "$5 - $25;Best Sellers" Items from Sort
	Then page loads with Products tagged with "$5 - $25;Best Sellers" Items 
	
	
	Scenario: TSO_Sorting_Genre_Best Sellers:[<UsingData>]
	Given user navigate application
	When user selects "Adventure;Best Sellers" Items from Sort
	Then page loads with Products tagged with "Adventure;Best Sellers" Items
	
	
	Scenario: TSO_Sorting_FundingType_Best Sellers:[<UsingData>]
	Given user navigate application
	When user selects "TitleI Part A;Best Sellers" Items from Sort
	Then page loads with Products tagged with "TitleI Part A;Best Sellers" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Best Sellers:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Best Sellers" Items from Sort
	Then page loads with Products tagged with "Fiction;Best Sellers" Items 
	
	
	Scenario: TSO_Sorting_Nonfiction_Best Sellers:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Best Sellers" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Best Sellers" Items 
	
	
	Scenario: TSO_Sorting_Fiction_Genre_Best Sellers:[<UsingData>]
	Given user navigate application
	When user selects "Fiction;Adventure;Best Sellers" Items from Sort
	Then page loads with Products tagged with "Fiction;Adventure;Best Sellers" Items 
	

		Scenario: TSO_Sorting_NonFiction_Genre_Best Sellers:[<UsingData>]
	Given user navigate application
	When user selects "Nonfiction;Adventure;Best Sellers" Items from Sort
	Then page loads with Products tagged with "Nonfiction;Adventure;Best Sellers" Items
	