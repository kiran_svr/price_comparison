Feature: AdvanceFilter_FundingType 


Scenario: AdvanceFilter_FundingType_Title I_Part_A:[<UsingData>] 
	Given user navigate application 
	When user selects "TitleI Part A" Items from FundingType filter
	Then page loads with Products tagged with "Title I, Part A" Items 
	
Scenario: AdvanceFilter_FundingType_Title+I_School Improvement:[<UsingData>] 
	Given user navigate application 
	When user selects "Title I School Improvement" Items from FundingType filter 
	Then page loads with Products tagged with "Title+I, SchoolImprovement" Items 
	
Scenario: AdvanceFilter_Title_I_DirectStudentServices:[<UsingData>] 
	Given user navigate application 
	When user selects "Title I DirectStudentServices" Items from FundingType filter 
	Then page loads with Products tagged with "Title I DirectStudentServices" Items 
	
	
	
Scenario: AdvanceFilter_FundingType_TitleII PartA:[<UsingData>] 
	Given user navigate application 
	When user selects "Title II PartA" Items from FundingType filter 
	Then page loads with Products tagged with "TitleII PartA" Items 
	
	
Scenario: AdvanceFilter_FundingType_TitleII_PartB_LEARN:[<UsingData>] 
	Given user navigate application 
	When user selects "Title II PartB LEARN" Items from FundingType filter 
	Then page loads with Products tagged with "Title II PartB LEARN" Items 
	
	
Scenario: AdvanceFilter_FundingType_TitleIII:[<UsingData>] 
	Given user navigate application 
	When user selects "Title III" Items from FundingType filter 
	Then page loads with Products tagged with "TitleIII" Items 
	
	
Scenario: AdvanceFilter_FundingType_TitleIV_PartA_SSAEG:[<UsingData>] 
	Given user navigate application 
	When user selects "Title IV PartA SSAEG" Items from FundingType filter 
	Then page loads with Products tagged with "TitleIV PartA SSAEG" Items 
	
	
Scenario: AdvanceFilter_FundingType_TitleIV_PartB_21stCCLC:[<UsingData>] 
	Given user navigate application 
	When user selects "Title IV PartB 21stCCLC" Items from FundingType filter 
	Then page loads with Products tagged with "TitleIV PartB 21stCCLC" Items 
	
	
Scenario: AdvanceFilter_FundingType_IDEA:[<UsingData>] 
	Given user navigate application 
	When user selects "IDEA" Items from FundingType filter 
	Then page loads with Products tagged with "IDEA" Items 
	

Scenario: AdvanceFilter_FundingType_TitleII_PartB_Learn:[<UsingData>] 
	Given user navigate application 
	When user selects "Title II PartB Learn" Items from FundingType filter 
	Then page loads with Products tagged with "TitleII PartB Learn" Items 
	
	
Scenario: AdvanceFilter_FundingType_SpecialEducationRTI:[<UsingData>] 
	Given user navigate application 
	When user selects "SpecialEducationRTI" Items from FundingType filter 
	Then page loads with Products tagged with "SpecialEducationRTI" Items 
	
	
	