package com.scholastic.healthcheck.db;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import Utils.Utility;

/**
 * 
 * @author Haroon.Rashid This class connects to DB and has methods to insert
 *         data into DB
 */
public class DBUtility {
	private static final String DB_URL = "jdbc:mysql://engineering-best-practice-rds-mysql.cejfaeg7tbwh.us-east-1.rds.amazonaws.com";
	private static final String DB_NAME = "root";
	private static String DB_PWD = "attd123#";
	private static Connection mConn = null;
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static Map<String, Map<String, String>> exeSuites = new HashMap<String, Map<String, String>>();
	static Map<String, String> eSuites;
	static List<String> exeQ = new ArrayList<String>();
	// Parent Table column constants
	private static final String id = "id";
	private static final String BUILDID = "build_id";
	private static final String MAINPROJECT = "main_project";
	private static final String PROJECTNAME = "project_name";
	private static final String MODULENAME = "module_name";
	private static final String SCENARIO = "scenario";
	private static final String PASSED = "passed";
	private static final String FAILED = "failed";
	private static final String LASTRUN = "last_run";
	private static final String TOTAL_TIME_TO_EXECUTE_STEP = "total_time_to_execute_step";
	private static final String TIME = "time";
	static String tempSuiteID;

	// Initializes Database connection
	static {
		getDBConnection();
		tempSuiteID  = "Suite_"+Utility.generateRandomString();
	}
	/**
	 * Establishes connection to DB
	 * 
	 * @return
	 */
	public static Connection getDBConnection() {
		String DBServerURL = null;
		String DBServerUserName = null;
		String DBServerPassword = null;

		try {
			if (null != mConn && !mConn.isClosed()) {
				return mConn;
			}
		} catch (SQLException se) {
		}

		try {
			DBServerURL = DB_URL;
			DBServerUserName = DB_NAME;
			DBServerPassword = DB_PWD;

			Class.forName(JDBC_DRIVER).newInstance();
			mConn = DriverManager.getConnection(DBServerURL, DBServerUserName, DBServerPassword);

		} catch (Exception e) {
			System.err.println("Exception: " + e.getMessage());
		}
		return mConn;
	}
	/**
	 * This method will insert the Execution Details into report_master table.
	 * 
	 * @param prop
	 */

	public static void insertFeatureScenarioCount(String mainProject, String projectName, String moduleName, String scenario, int passed, int failed,
			String lastRun, String totalTimeToExecuteStep, int time) {
		//Result Variable
		Connection objcon=DBUtility.getDBConnection();
		ResultSet objres = null;
		PreparedStatement objpstmt = null;
		try {
			String Sqlquery="insert into HEALTHCHECK_DB.test_health_check_results"
					+"("+id+","+
					BUILDID+","+
					MAINPROJECT+","+
					PROJECTNAME+","+
					MODULENAME+","+
					SCENARIO+","+
					PASSED+","+
					FAILED+","+
					LASTRUN+","+
					TOTAL_TIME_TO_EXECUTE_STEP+","+
					TIME+")"+
					" values ("
					+null+",\""
					+tempSuiteID+"\""+",\""
					+mainProject+"\""+",\""
					+projectName+"\""+",\""
					+moduleName+"\""+",\""
					+scenario+"\""+",\""
					+passed+"\""+",\""
					+failed+"\""+",\""
					+lastRun+"\""+",\""
					+totalTimeToExecuteStep+"\""+",\""
					+time+"\")";

			System.out.println("Query: "+Sqlquery);
			objcon.setAutoCommit(false);
			objpstmt = (PreparedStatement) objcon.prepareStatement(Sqlquery);
			// Execute SQL Query
			objpstmt.executeUpdate();
			/* save insert */
			objcon.commit();
		} catch (SQLException objSQlexc) {
			System.out.println(objSQlexc.getMessage());
		} catch (Exception objExc) {
			System.out.println(objExc.getMessage());
		} finally {
			try {
				if (objcon != null) {
					objcon.close();
				}
			} catch (SQLException objSqlExc) {
				System.out.println("Exception in Finally" + objSqlExc.getMessage());
			}
		}
	}
	public static void main(String[] args) {
		java.util.Date date=new java.util.Date();
		java.sql.Date sqlDate=new java.sql.Date(date.getTime());
		java.sql.Timestamp sqlTime=new java.sql.Timestamp(date.getTime());
		
		System.out.println(sqlDate);
		System.out.println(sqlTime);
		//insertFeatureScenarioCount("ASO","Scholastic Store", "Login", "Login to application", 1, 0, "2018-03-15 11:02:16","00:01:12");
	}
}