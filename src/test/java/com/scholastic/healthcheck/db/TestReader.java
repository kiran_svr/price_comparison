package com.scholastic.healthcheck.db;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.scholastic.googleapi.sheets.GoogleSheetFunctions;

public class TestReader {

	public static void main(String[] args) throws InterruptedException {
		/*
		 * List<List<Object>> list = GoogleSheetFunctions.readFromSheet("ASO",
		 * "A2:E1000");
		 * 
		 * Iterator<List<Object>> it = list.iterator(); while(it.hasNext()) {
		 * System.out.println(it.next().get(0)); }
		 */

		long start = System.currentTimeMillis();
		Thread.sleep(10000);
		long end = System.currentTimeMillis();

		NumberFormat formatter = new DecimalFormat("#0");
		long time = end - start;
		System.out.println(time);
		// long minutes = TimeUnit.MILLISECONDS.toMinutes(time);
		// System.out.print("Execution time is " + formatter.format((end - start) /
		// 1000d) + " seconds");
		System.out.print("Execution time is " + TimeUnit.MILLISECONDS.toMinutes(time) + " seconds");
	}

}
