package com.scholastic.healthcheck.db;

import java.util.concurrent.TimeUnit;

public class Test {
	public static void main(String[] args) throws InterruptedException {

		long start = System.currentTimeMillis();
		Thread.sleep(10000);
		long end = System.currentTimeMillis();

		long total = end - start;
		System.out.println(total);
		long milliseconds = 10001;

		long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);
		long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);

		System.out.format("%d Milliseconds = %d minutes\n", milliseconds, minutes);
		System.out.println("Or");
		System.out.format("%d Milliseconds = %d seconds", milliseconds, seconds);

	}
}
