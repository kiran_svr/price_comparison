package com.scholastic.healthcheck.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.formula.SheetNameFormatter;
import Utils.Utility;
import com.scholastic.googleapi.constants.Constant;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.healthcheck.db.DBUtility;
import com.scholastic.torque.common.TestBaseProvider;

public class InsertHealthCheckResultsIntoDB {

	static String sheetName= "ASO";
	static String cellRange = "A2:F100";

	public static String projectName="";
	public static String moduleName=""; 

	public static String getProjectName() {
		return projectName;
	}

	public static void setProjectName(String projectName) {
		InsertHealthCheckResultsIntoDB.projectName = projectName;
	}

	public static String getModuleName() {
		return moduleName;
	}

	public static void setModuleName(String moduleName) {
		InsertHealthCheckResultsIntoDB.moduleName = moduleName;
	}

	public static void main(String[] args) {
		int passed = 0;
		int failed = 0;
		List<List<Object>> list = GoogleSheetFunctions.readFromSheet(sheetName, cellRange);
		Iterator<List<Object>> it = list.iterator();
		while (it.hasNext()) {
			List<Object> row = it.next();

			System.out.println(row);
			if (!row.get(0).toString().isEmpty()) {setProjectName(row.get(0).toString());}
			if (!row.get(1).toString().isEmpty()) {setModuleName(row.get(1).toString());	}
			if (row.get(3).toString().equals("Pass")) {
				passed = 1;
				failed = 0;
			}else {
				passed = 0;
				failed=1;
			}
			if(!getProjectName().isEmpty() && !getModuleName().isEmpty() && !row.get(2).toString().isEmpty()) {
				DBUtility.insertFeatureScenarioCount(
						//Integer.parseInt(TestBaseProvider.getTestBase().getContext().getProperty("build_number").toString()),
						sheetName,
						getProjectName(),
						getModuleName(),
						row.get(2).toString(),
						passed,
						failed,
						row.get(4).toString(), 
						Utility.convertMillisToHHMMSS_Format(Long.parseLong(row.get(5).toString())),
						Integer.parseInt(row.get(5).toString()));
			}
		}
	}
}
