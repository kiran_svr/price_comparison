
package com.scholastic.coreApiService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "isbn13",
    "id",
    "author",
    "title",
    "customGrade",
    "formatDescription",
    "pdpUrl",
    "saleMessage",
    "legacyProductType",
    "productId",
    "productStartDate",
    "saleStartDate",
    "saleEndDate",
    "viewableFlag",
    "searchable",
    "showLexile",
    "active",
    "inventoryThreshold",
    "unitCost",
    "salePrice",
    "ourPrice",
    "listPrice",
    "sortPrice",
    "sortByPriceVDL",
    "sortByPriceVDK",
    "sortByPriceNYS",
    "sortByPriceCOS",
    "sortByPriceNAT",
    "lowAge",
    "highAge",
    "age",
    "lowGrade",
    "highGrade",
    "grade",
    "smallImage",
    "extraSmallImage",
    "largeImage",
    "extraLargeImage",
    "siteId",
    "fund",
    "partner",
    "available"
})
public class Product {

    @JsonProperty("isbn13")
    private String isbn13;
    @JsonProperty("id")
    private String id;
    @JsonProperty("author")
    private String author;
    @JsonProperty("title")
    private String title;
    @JsonProperty("customGrade")
    private String customGrade;
    @JsonProperty("formatDescription")
    private Object formatDescription;
    @JsonProperty("pdpUrl")
    private String pdpUrl;
    @JsonProperty("saleMessage")
    private Object saleMessage;
    @JsonProperty("legacyProductType")
    private String legacyProductType;
    @JsonProperty("productId")
    private String productId;
    @JsonProperty("productStartDate")
    private String productStartDate;
    @JsonProperty("saleStartDate")
    private Object saleStartDate;
    @JsonProperty("saleEndDate")
    private Object saleEndDate;
    @JsonProperty("viewableFlag")
    private String viewableFlag;
    @JsonProperty("searchable")
    private String searchable;
    @JsonProperty("showLexile")
    private String showLexile;
    @JsonProperty("active")
    private String active;
    @JsonProperty("inventoryThreshold")
    private String inventoryThreshold;
    @JsonProperty("unitCost")
    private Integer unitCost;
    @JsonProperty("salePrice")
    private Integer salePrice;
    @JsonProperty("ourPrice")
    private Double ourPrice;
    @JsonProperty("listPrice")
    private Double listPrice;
    @JsonProperty("sortPrice")
    private Double sortPrice;
    @JsonProperty("sortByPriceVDL")
    private Double sortByPriceVDL;
    @JsonProperty("sortByPriceVDK")
    private Double sortByPriceVDK;
    @JsonProperty("sortByPriceNYS")
    private Double sortByPriceNYS;
    @JsonProperty("sortByPriceCOS")
    private Double sortByPriceCOS;
    @JsonProperty("sortByPriceNAT")
    private Double sortByPriceNAT;
    @JsonProperty("lowAge")
    private String lowAge;
    @JsonProperty("highAge")
    private String highAge;
    @JsonProperty("age")
    private List<String> age = null;
    @JsonProperty("lowGrade")
    private String lowGrade;
    @JsonProperty("highGrade")
    private String highGrade;
    @JsonProperty("grade")
    private List<String> grade = null;
    @JsonProperty("smallImage")
    private String smallImage;
    @JsonProperty("extraSmallImage")
    private String extraSmallImage;
    @JsonProperty("largeImage")
    private String largeImage;
    @JsonProperty("extraLargeImage")
    private String extraLargeImage;
    @JsonProperty("siteId")
    private String siteId;
    @JsonProperty("fund")
    private Object fund;
    @JsonProperty("partner")
    private String partner;
    @JsonProperty("available")
    private Boolean available;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("isbn13")
    public String getIsbn13() {
        return isbn13;
    }

    @JsonProperty("isbn13")
    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("author")
    public String getAuthor() {
        return author;
    }

    @JsonProperty("author")
    public void setAuthor(String author) {
        this.author = author;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("customGrade")
    public String getCustomGrade() {
        return customGrade;
    }

    @JsonProperty("customGrade")
    public void setCustomGrade(String customGrade) {
        this.customGrade = customGrade;
    }

    @JsonProperty("formatDescription")
    public Object getFormatDescription() {
        return formatDescription;
    }

    @JsonProperty("formatDescription")
    public void setFormatDescription(Object formatDescription) {
        this.formatDescription = formatDescription;
    }

    @JsonProperty("pdpUrl")
    public String getPdpUrl() {
        return pdpUrl;
    }

    @JsonProperty("pdpUrl")
    public void setPdpUrl(String pdpUrl) {
        this.pdpUrl = pdpUrl;
    }

    @JsonProperty("saleMessage")
    public Object getSaleMessage() {
        return saleMessage;
    }

    @JsonProperty("saleMessage")
    public void setSaleMessage(Object saleMessage) {
        this.saleMessage = saleMessage;
    }

    @JsonProperty("legacyProductType")
    public String getLegacyProductType() {
        return legacyProductType;
    }

    @JsonProperty("legacyProductType")
    public void setLegacyProductType(String legacyProductType) {
        this.legacyProductType = legacyProductType;
    }

    @JsonProperty("productId")
    public String getProductId() {
        return productId;
    }

    @JsonProperty("productId")
    public void setProductId(String productId) {
        this.productId = productId;
    }

    @JsonProperty("productStartDate")
    public String getProductStartDate() {
        return productStartDate;
    }

    @JsonProperty("productStartDate")
    public void setProductStartDate(String productStartDate) {
        this.productStartDate = productStartDate;
    }

    @JsonProperty("saleStartDate")
    public Object getSaleStartDate() {
        return saleStartDate;
    }

    @JsonProperty("saleStartDate")
    public void setSaleStartDate(Object saleStartDate) {
        this.saleStartDate = saleStartDate;
    }

    @JsonProperty("saleEndDate")
    public Object getSaleEndDate() {
        return saleEndDate;
    }

    @JsonProperty("saleEndDate")
    public void setSaleEndDate(Object saleEndDate) {
        this.saleEndDate = saleEndDate;
    }

    @JsonProperty("viewableFlag")
    public String getViewableFlag() {
        return viewableFlag;
    }

    @JsonProperty("viewableFlag")
    public void setViewableFlag(String viewableFlag) {
        this.viewableFlag = viewableFlag;
    }

    @JsonProperty("searchable")
    public String getSearchable() {
        return searchable;
    }

    @JsonProperty("searchable")
    public void setSearchable(String searchable) {
        this.searchable = searchable;
    }

    @JsonProperty("showLexile")
    public String getShowLexile() {
        return showLexile;
    }

    @JsonProperty("showLexile")
    public void setShowLexile(String showLexile) {
        this.showLexile = showLexile;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("inventoryThreshold")
    public String getInventoryThreshold() {
        return inventoryThreshold;
    }

    @JsonProperty("inventoryThreshold")
    public void setInventoryThreshold(String inventoryThreshold) {
        this.inventoryThreshold = inventoryThreshold;
    }

    @JsonProperty("unitCost")
    public Integer getUnitCost() {
        return unitCost;
    }

    @JsonProperty("unitCost")
    public void setUnitCost(Integer unitCost) {
        this.unitCost = unitCost;
    }

    @JsonProperty("salePrice")
    public Integer getSalePrice() {
        return salePrice;
    }

    @JsonProperty("salePrice")
    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }

    @JsonProperty("ourPrice")
    public Double getOurPrice() {
        return ourPrice;
    }

    @JsonProperty("ourPrice")
    public void setOurPrice(Double ourPrice) {
        this.ourPrice = ourPrice;
    }

    @JsonProperty("listPrice")
    public Double getListPrice() {
        return listPrice;
    }

    @JsonProperty("listPrice")
    public void setListPrice(Double listPrice) {
        this.listPrice = listPrice;
    }

    @JsonProperty("sortPrice")
    public Double getSortPrice() {
        return sortPrice;
    }

    @JsonProperty("sortPrice")
    public void setSortPrice(Double sortPrice) {
        this.sortPrice = sortPrice;
    }

    @JsonProperty("sortByPriceVDL")
    public Double getSortByPriceVDL() {
        return sortByPriceVDL;
    }

    @JsonProperty("sortByPriceVDL")
    public void setSortByPriceVDL(Double sortByPriceVDL) {
        this.sortByPriceVDL = sortByPriceVDL;
    }

    @JsonProperty("sortByPriceVDK")
    public Double getSortByPriceVDK() {
        return sortByPriceVDK;
    }

    @JsonProperty("sortByPriceVDK")
    public void setSortByPriceVDK(Double sortByPriceVDK) {
        this.sortByPriceVDK = sortByPriceVDK;
    }

    @JsonProperty("sortByPriceNYS")
    public Double getSortByPriceNYS() {
        return sortByPriceNYS;
    }

    @JsonProperty("sortByPriceNYS")
    public void setSortByPriceNYS(Double sortByPriceNYS) {
        this.sortByPriceNYS = sortByPriceNYS;
    }

    @JsonProperty("sortByPriceCOS")
    public Double getSortByPriceCOS() {
        return sortByPriceCOS;
    }

    @JsonProperty("sortByPriceCOS")
    public void setSortByPriceCOS(Double sortByPriceCOS) {
        this.sortByPriceCOS = sortByPriceCOS;
    }

    @JsonProperty("sortByPriceNAT")
    public Double getSortByPriceNAT() {
        return sortByPriceNAT;
    }

    @JsonProperty("sortByPriceNAT")
    public void setSortByPriceNAT(Double sortByPriceNAT) {
        this.sortByPriceNAT = sortByPriceNAT;
    }

    @JsonProperty("lowAge")
    public String getLowAge() {
        return lowAge;
    }

    @JsonProperty("lowAge")
    public void setLowAge(String lowAge) {
        this.lowAge = lowAge;
    }

    @JsonProperty("highAge")
    public String getHighAge() {
        return highAge;
    }

    @JsonProperty("highAge")
    public void setHighAge(String highAge) {
        this.highAge = highAge;
    }

    @JsonProperty("age")
    public List<String> getAge() {
        return age;
    }

    @JsonProperty("age")
    public void setAge(List<String> age) {
        this.age = age;
    }

    @JsonProperty("lowGrade")
    public String getLowGrade() {
        return lowGrade;
    }

    @JsonProperty("lowGrade")
    public void setLowGrade(String lowGrade) {
        this.lowGrade = lowGrade;
    }

    @JsonProperty("highGrade")
    public String getHighGrade() {
        return highGrade;
    }

    @JsonProperty("highGrade")
    public void setHighGrade(String highGrade) {
        this.highGrade = highGrade;
    }

    @JsonProperty("grade")
    public List<String> getGrade() {
        return grade;
    }

    @JsonProperty("grade")
    public void setGrade(List<String> grade) {
        this.grade = grade;
    }

    @JsonProperty("smallImage")
    public String getSmallImage() {
        return smallImage;
    }

    @JsonProperty("smallImage")
    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    @JsonProperty("extraSmallImage")
    public String getExtraSmallImage() {
        return extraSmallImage;
    }

    @JsonProperty("extraSmallImage")
    public void setExtraSmallImage(String extraSmallImage) {
        this.extraSmallImage = extraSmallImage;
    }

    @JsonProperty("largeImage")
    public String getLargeImage() {
        return largeImage;
    }

    @JsonProperty("largeImage")
    public void setLargeImage(String largeImage) {
        this.largeImage = largeImage;
    }

    @JsonProperty("extraLargeImage")
    public String getExtraLargeImage() {
        return extraLargeImage;
    }

    @JsonProperty("extraLargeImage")
    public void setExtraLargeImage(String extraLargeImage) {
        this.extraLargeImage = extraLargeImage;
    }

    @JsonProperty("siteId")
    public String getSiteId() {
        return siteId;
    }

    @JsonProperty("siteId")
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    @JsonProperty("fund")
    public Object getFund() {
        return fund;
    }

    @JsonProperty("fund")
    public void setFund(Object fund) {
        this.fund = fund;
    }

    @JsonProperty("partner")
    public String getPartner() {
        return partner;
    }

    @JsonProperty("partner")
    public void setPartner(String partner) {
        this.partner = partner;
    }

    @JsonProperty("available")
    public Boolean getAvailable() {
        return available;
    }

    @JsonProperty("available")
    public void setAvailable(Boolean available) {
        this.available = available;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
