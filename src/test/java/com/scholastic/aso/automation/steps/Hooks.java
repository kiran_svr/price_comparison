package com.scholastic.aso.automation.steps;

import static com.scholastic.torque.common.TestBaseProvider.getTestBase;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import Utils.Utility;
import com.saucelabs.saucerest.SauceREST;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.torque.common.TestBase;
import com.scholastic.torque.common.TestBaseProvider;
import com.tdm.client.TDMClient;
import com.tdm.support.ASOUserEntity;
import com.torque.automation.core.TestDataUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	TDMClient tdm = new TDMClient(getTestBase().getContext().getString("tdm.server.url"));

	@Before
	public void beforeHook(Scenario scenario) {
		synchronized (this) {

			TestBase testBase = TestBaseProvider.getTestBase();
			testBase.getContext().clearProperty("tdm.UserProfile");
			testBase.getContext().subset("testexecution").clear();
			System.setProperty("webdriver.firefox.marionette", "servers/geckodriver.exe");
			String session = testBase.getSessionID();
			String[] nameArray = scenario.getName().split(":\\[");
			String testcaseid = null;
			if (nameArray.length == 2) {
				testcaseid = nameArray[1].replace("]", "");
			} else {
				System.out.println("Test data key was not passed");
			}
			TestBaseProvider.getTestBase().getContext().setProperty("testcaseid", testcaseid.trim());
			TestBaseProvider.getTestBase().setTestDataFromXml(testcaseid);
			if (!session.equalsIgnoreCase("") && !testBase.getContext().getString("sauce").equalsIgnoreCase("false")) {
				SauceREST sClient = new SauceREST(testBase.getContext().getString("sauce.username"),
						testBase.getContext().getString("sauce.access.key"));
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("name", scenario.getName());
				sClient.updateJobInfo(session, params);
			}
			String scenarioName = scenario.getName();
			/*
			 * if (!scenarioName.contains("Create New Test Data")) {
			 * populateTestdata(scenarioName); }
			 */

		}
		getTestBase().getDriver().manage().deleteAllCookies();
		if (!(TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("Android")
				|| TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("IOs")
				|| TestBaseProvider.getTestBase().getContext().getString("driver.name").equalsIgnoreCase("Remote"))) {
			TestBaseProvider.getTestBase().getDriver().manage().window().maximize();
		}
		// testBase.getDriver().get(testBase.getString("otbpm.url"));
	}

	@After
	public void afterHook(Scenario scenario) {
		synchronized (this) {
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			if (scenario.isFailed()) {
				try {
					scenario.write("Current Page URL is " + driver.getCurrentUrl());
					byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
					scenario.embed(screenshot, "image/png");

				} catch (WebDriverException somePlatformsDontSupportScreenshots) {
					System.err.println(somePlatformsDontSupportScreenshots.getMessage());
				}

			}
			String session = TestBaseProvider.getTestBase().getSessionID();
			String status = "";
			if (!scenario.isFailed()) {
				System.out.println(
						"ScenarioFailed=" + scenario.getName() + "<>Session=" + session + "<>Status=Passed<>Platform="
								+ TestBaseProvider.getTestBase().getContext().getString("driver.name"));
				status = "Passed";
			} else {
				status = "Failed";
				System.out.println(
						"ScenarioFailed=" + scenario.getName() + "<>Session=" + session + "<>Status=Failed<>Platform="
								+ TestBaseProvider.getTestBase().getContext().getString("driver.name"));
			}
			System.out.println("SauceOnDemandSessionID=" + session + " job-name=" + scenario.getName());

			/*
			 * String cellRange = ""; // Here write a logic to get cell range based on
			 * scenario Name
			 * GoogleSheetFunctions.writeToSheet(TestDataUtils.getString("sheetName"),
			 * cellRange, Utility.createObject(status));
			 */

			if (!session.equalsIgnoreCase("")
					&& !TestBaseProvider.getTestBase().getContext().getString("sauce").equalsIgnoreCase("false")) {
				TestBase testBase = TestBaseProvider.getTestBase();
				SauceREST sClient = new SauceREST(testBase.getContext().getString("sauce.username"),
						testBase.getContext().getString("sauce.access.key"));
				System.out.println("SessionID::" + session);
				if (scenario.isFailed())
					sClient.jobFailed(session);
				else
					sClient.jobPassed(session);
			}
			TestBaseProvider.getTestBase().tearDown();
		}
	}

	private void populateTestdata(String scenarioName) {
		String[] nameArray = scenarioName.split(":\\[");
		String testcaseid = null;
		if (nameArray.length == 2) {
			testcaseid = nameArray[1].replace("]", "");
		} else {
			System.out.println("Test data key was not passed");
		}
		TestBaseProvider.getTestBase().getContext().setProperty("testcaseid", testcaseid.trim());
		TestBaseProvider.getTestBase().setTestDataFromXml(testcaseid);

		if (getTestBase().getContext().getString("tdm.data").equalsIgnoreCase("on") && testcaseid != null) {
			if (getTestBase().getTestData().containsKey("tdm.type")) {
				try {
					ASOUserEntity profile = null;

					profile = tdm.getASOUser(getTestBase().getTestData().getString("tdm.type"),
							getTestBase().getTestData().getBoolean("tdm.isReserved"));

					System.out.println("TDMUserType=" + getTestBase().getTestData().getString("tdm.type"));
					TestBaseProvider.getTestBase().getContext().setProperty("tdm.UserProfile", profile);

					System.out.println("Alternate TDM ==> UserName:: " + profile.getEmail());
					System.out.println("Alternate TDM ==> Password:: " + profile.getPassword());

					if (null != profile) {
						TestBaseProvider.getTestBase().getTestData().setProperty("username", profile.getEmail());
						TestBaseProvider.getTestBase().getTestData().setProperty("password", profile.getPassword());
						TestBaseProvider.getTestBase().getTestData().setProperty("tdm.userid", profile.getEmail());
						TestBaseProvider.getTestBase().getTestData().setProperty("tdm.password", profile.getPassword());

					}
				} catch (Exception ex) {
					System.out.println("---As no Alternate TDM records found, it is going for test data xml file---");
				}
			} else {
				System.out.println("---As no Alternate TDM tag is provided, it is going for test data xml file---");
			}
		} else {
			System.out.println("---As tdm data is off, it is going for test data xml file---");
		}
	}

	// public String getSessionID()
	// {
	// return ((RemoteWebDriver)
	// TestBaseProvider.getTestBase().getDriver()).getSessionId().toString();
	// }

}