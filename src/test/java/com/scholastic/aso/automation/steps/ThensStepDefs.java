package com.scholastic.aso.automation.steps;

import com.scholastic.aso.automation.pages.web.HomePage;
import com.scholastic.aso.automation.pages.web.PrintablesPage;
import cucumber.api.java.en.Then;

public class ThensStepDefs {

	HomePage homePage = new HomePage();
	PrintablesPage printablePage = new PrintablesPage();

	// ***********TSO SSO************
	@Then("^user will log out of the \"([^\"]*)\" store$")
	public void userLogoutFromStore(String storeType) {
		homePage.doLogout(storeType);
	}

	// ***************TCB*****************
	@Then("^signin and register options will be visible on left nav pane$")
	public void VerifyHomePage() {
		printablePage.VerifyHomePage();
	}
}