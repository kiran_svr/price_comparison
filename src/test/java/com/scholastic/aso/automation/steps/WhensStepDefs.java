package com.scholastic.aso.automation.steps;

import com.scholastic.aso.automation.pages.web.CartPage;
import com.scholastic.aso.automation.pages.web.HomePage;
import com.scholastic.aso.automation.pages.web.MiniCartPage;
import com.scholastic.aso.automation.pages.web.PrintablesPage;
import com.scholastic.aso.automation.pages.web.ProductDetailPage;
import com.scholastic.aso.automation.pages.web.SearchResultPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class WhensStepDefs {

	SearchResultPage searchResultPage = new SearchResultPage();
	CartPage cartPage = new CartPage();
	HomePage homePage = new HomePage();

	ProductDetailPage productDetailPage = new ProductDetailPage();
	MiniCartPage miniCartPage = new MiniCartPage();
	PrintablesPage printablePage = new PrintablesPage();

	// ************************TSO/SSO

	@When("^user click on sign in link$")
	public void clickOnSignIn() {
		homePage.clickProfile();
		homePage.clickOnSignInLink();
	}

	@When("^user navigates to login page with username and password \\[UserInformation\\]$")
	public void userNavigatesToLoginPageWithUsernameAndPassword() {
		homePage.doLogin();
	}

	@When("^user hovers on the minicart$")
	public void userHoverOnTheMiniCart() {
		miniCartPage.hoverCartIcon();
	}

	@And("^user empty the mini cart$")
	public void removeProductsFromMiniCart() {
		homePage.removeProductsFromMiniCart();
	}

	@When("^user searches for an \"([^\"]*)\" in global search box$")
	public void userSearchForItemInGlobalSearchBox(String searchType) {
		homePage.searchForItemInGlobalSearchBox(searchType);
	}

	@When("^user searches for an \"([^\"]*)\" in global search box on PDP$")
	public void userSearchForItemInGlobalSearchBoxPDP(String searchType) {
		homePage.searchForItemInGlobalSearchBoxPDP(searchType);
	}

	@And("^user navigates to PDP page$")
	public void navigatesToPDP() throws Throwable {
		searchResultPage.selectAnyItem();
		productDetailPage.verifyPDPPageLoaded();
	}

	@And("^user adds that product into cart from product detail page$")
	public void userAddProductToCart() {
		productDetailPage.addProductToCart();
	}

	@When("^user adds that product into cart from search results page$")
	public void userAddProductIntoCartFromSearchResultPage() {
		searchResultPage.addProductIntoCartFromSearchResultPage();
	}

	@When("^user click on mini cart icon$")
	public void clickMiniCartIcon() {
		miniCartPage.clickMiniCartIcon();
	}

	@When("^user verify Check Out Now on DW page$")
	public void userSelectsCheckOutNowButtonOnDWPage() {
		cartPage.verifyCheckOutNow();
	}
	
	@When("^user click Check Out Now on DW page$")
	public void clickCheckOutNowButtonOnDWPage() {
		cartPage.clickCheckOutNow();
	}

	// ***************TCB*****************
	@When("^user navigates to the Printables site$")
	public void user_Navigates_To_Printables_Page() {
		printablePage.launchPage();

		if (printablePage.CloseCutoverIcon().size() != 0)
			printablePage.CloseCutoverIcon().get(0).click();

	}

	@When("^Guest user logs into the application$")
	public void GuestUserLogin() {
		printablePage.UserLogin();
	}

	@And("^a user navigates to Printables product details page of product title$")
	public void homePage_SearchSpecificProductTitle() {
		printablePage.searchProductTitleInSearchPage();
	}

	@And("^user navigates to the Printables subscription landing page using free trial product$")
	public void userLoadesSubscriptionLandingPageForFreeTrial() {
		printablePage.userLoadesSubscriptionLandingPageForFreeTrial();
	}

	@When("^user selects Logout option from left nav pane$")
	public void UserLogOut() {
		printablePage.UserLogOut();
	}

	@When("^user searches for keyword in search box$")
	public void searchForKeyword() {
		printablePage.searchForKeyword();
	}

	@And("^user search from product detail page of teachables$")
	public void searchFromPDP() {
		printablePage.searchFromPDP();
	}
}
