package com.scholastic.aso.automation.steps;



import com.scholastic.cs.SolrAEMServices.SolrAemController;
import com.scholastic.cs.SolrAEMServices.Subject;
import com.scholastic.cs.SolrAEMServices.TSOSorting;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AdvanceFliterStepDefs {

    @Given("^user navigate application$")
    public void user_navigate_application() throws Throwable {
    	
    }

    @When("^user selects \"([^\"]*)\" Items from GRADE filter$")
    public void user_selects_something_items_from_grade_filter(String strArg1) throws Throwable {
     	SolrAemController.generateDataToExcel(strArg1);
     
    }
    
    @When("^user selects \"([^\"]*)\" Items from Price filter$")
    public void user_selects_something_items_from_price_filter(String strArg1) throws Throwable {
    	SolrAemController.generateDataToExcel(strArg1);
    }
    
    
    
    @When("^user selects \"([^\"]*)\" Items from Subject filter$")
    public void user_selects_something_items_from_subject_filter(String strArg1) throws Throwable {
    	SolrAemController.generateDataToExcel(strArg1);
    	
    }

        

    @Then("^page loads with Products tagged with \"([^\"]*)\" Items$")
    public void page_loads_with_products_tagged_with_something_items(String strArg1) throws Throwable {
    	        
    }

    

    @When("^user selects \"([^\"]*)\" Items from FundingType filter$")
    public void user_selects_something_items_from_fundingtype_filter(String strArg1) throws Throwable {
    	SolrAemController.generateDataToExcel(strArg1);
    }
    
    
    @When("^user selects \"([^\"]*)\" Items from Genre filter$")
    public void user_selects_something_items_from_genre_filter(String strArg1) throws Throwable {
    	SolrAemController.generateDataToExcel(strArg1);
    }
    
    
    @When("^user selects \"([^\"]*)\" Items from Sort$")
    public void user_selects_something_items_from_sort(String strArg1) throws Throwable {
    	TSOSorting.generateDataToExcel(strArg1);
    }
    
    


    @When("^user selects \"([^\"]*)\" Items from Advance filter$")
    public void user_selects_something_items_from_advance_filter(String strArg1) throws Throwable {
    }

    
    @When("^user selects \"([^\"]*)\" Items from Format filter$")
    public void user_selects_something_items_from_format_filter(String strArg1) throws Throwable {
    	SolrAemController.generateDataToExcel(strArg1);
    }
}