package com.scholastic.aso.automation.steps;

import org.openqa.selenium.WebDriver;

import com.scholastic.aso.automation.pages.web.HomePage;
import com.scholastic.aso.automation.support.ASOUtils;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.torque.common.TestBaseProvider;

import cucumber.api.java.en.Given;

public class GivensStepDefs {

	HomePage homePage = new HomePage();

	@Given("^user navigates to \"([^\"]*)\" page$")
	public void user_navigates_page(String page) {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		switch (page) {
		case "TeacherStore":
			driver.get(TestBaseProvider.getTestBase().getString("tso.url"));
			GoogleSheetFunctions.clearData("ASO", "D13:F21");
			GoogleSheetFunctions.clearData("ASO", "D12:E12");
			break;
		case "ParentStore":
			driver.get(TestBaseProvider.getTestBase().getString("sso.url"));
			GoogleSheetFunctions.clearData("ASO", "D3:F11");
			GoogleSheetFunctions.clearData("ASO", "D2:E2");
			break;
		case "Teachables":
			driver.get(TestBaseProvider.getTestBase().getString("tcb.url"));
			GoogleSheetFunctions.clearData("ASO", "D23:F29");
			GoogleSheetFunctions.clearData("ASO", "D22:E22");
			break;
		}

		ASOUtils.pause(20);
	}

}