package com.scholastic.aso.automation.pages.web;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.scholastic.aso.automation.support.ASOUtils;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;
import com.torque.automation.core.TestDataUtils;

import Utils.CellRanges;
import Utils.GoogleSheetRowModel;
import Utils.Utility;

public class PrintablesPage extends BaseTestPage<TestPage> {

	String sheetName = TestDataUtils.getString("sheetName");

	public String psoLoggedInUser;
	public static Cookie sps_bus_value;
	public static String urlBeforeLogin;
	public static String urlAfterLogin;
	public static boolean sps_bus_value_for_guest;

	@FindBy(locator = "home.PSOLoggedInUser")
	private WebElement PSOLoggedInUser;
	@FindBy(locator = "home.LoginSignInLink")
	private WebElement LoginSignInLink;
	@FindBy(locator = "home.RegisterLink")
	private WebElement RegisterLink;
	@FindBy(locator = "home.CloseCutoverIcon")
	private List<WebElement> CloseCutoverIcon;
	@FindBy(locator = "home.SearchTryForFreeButton")
	private WebElement SearchTryForFreeButton;
	@FindBy(locator = "home.LogoutLink")
	private WebElement LogoutLink;
	@FindBy(locator = "sbslandingpage.GoldButton")
	private WebElement GoldButton;
	@FindBy(locator = "home.LoginUsername")
	private WebElement LoginUsername;
	@FindBy(locator = "home.LoginPassword")
	private WebElement LoginPassword;
	@FindBy(locator = "home.SearchGOButton")
	private WebElement SearchBoxGoButton;
	@FindBy(locator = "home.inputSearchtextbox")
	private WebElement inputSearchtextbox;
	@FindBy(locator = "home.SearchPageResultListFirstElement")
	private WebElement SearchPageResultListFirstElement;
	@FindBy(locator = "home.SearchSignIn")
	private WebElement SearchSignIn;
	@FindBy(locator = "home.search.products")
	List<WebElement> productTitles;
	@FindBy(locator = "home.produtdetailpage.section")
	WebElement pdpSection;

	private WebElement getPDPSection() {
		return pdpSection;
	}

	private List<WebElement> getProductTitles() {
		return productTitles;
	}

	private WebElement SearchTryForFreeButton() {
		return SearchTryForFreeButton;
	}

	private WebElement LogoutLink() {
		return LogoutLink;
	}

	private WebElement GoldButton() {
		return GoldButton;
	}

	private WebElement SearchSignIn() {
		return SearchSignIn;
	}

	private WebElement LoginPassword() {
		return LoginPassword;
	}

	private WebElement LoginUsername() {
		return LoginUsername;
	}

	public WebElement gethomepageSearchGoButton() {
		return SearchBoxGoButton;
	}

	public WebElement gethomepageSearchbox() {
		return inputSearchtextbox;
	}

	public WebElement gethomepageSearchResultListFirstElement() {
		return SearchPageResultListFirstElement;
	}

	private WebElement LoginSignInLink() {
		return LoginSignInLink;
	}

	private WebElement PSOLoggedInUser() {
		return PSOLoggedInUser;
	}

	private WebElement RegisterLink() {
		return RegisterLink;
	}

	public List<WebElement> CloseCutoverIcon() {
		return CloseCutoverIcon;
	}

	@Override
	public void launchPage() {
		openPage();
	}

	@Override
	public void openPage() {
		ASOUtils.pause(10);
	}

	private void handleWelcomePopupWindow() {
		ASOUtils.pause(1);
		List<WebElement> welcomePopUpTeacher = getDriver().findElements(By.id("teacher"));
		System.out.println("popup size is : " + welcomePopUpTeacher.size());
		if (welcomePopUpTeacher.size() > 0) {
			try {
				welcomePopUpTeacher.get(0).click();
			} catch (Exception e) {
				System.err.println();
			}
		}
	}

	public boolean IsCookiePresent(String cookieName) {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		return driver.manage().getCookieNamed(cookieName) != null;
	}

	public void homeSearchVerifyTryForFreeButton() {

		Assert.assertTrue(SearchTryForFreeButton().getText()
				.equals(TestBaseProvider.getTestBase().getTestData().getString("TryForFree")));
	}

	public void closeSurveyPopUp() {
		List<WebElement> closePopUp = getDriver().findElements(By.id("cv-survey-close"));
		if (closePopUp.size() != 0) {
			try {
				closePopUp.get(0).click();
			} catch (Exception e) {
				System.err.println();
			}
		}
	}

	public void VerifyHomePage() {
		WebDriver driver = TestBaseProvider.getTestBase().getDriver();
		WebDriverWait wait = new WebDriverWait(driver, 5);
		wait.until(ExpectedConditions.visibilityOf(SearchSignIn()));
		Assert.assertTrue(SearchSignIn().isDisplayed(), "Sign In Link Verified");
		Assert.assertTrue(RegisterLink().isDisplayed(), "Register Link Verified");
	}

	public void userLoadesSubscriptionLandingPageForFreeTrial() {
		try {
			GoogleSheetRowModel.setStartTime();
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			String urlCheck = driver.getCurrentUrl();
			if (urlCheck.contains("teachables-qa")) {
				driver.get(
						"https://teachables-qa.scholastic.com/teachables/tools/subscriptionlandingpage.html?partNumber=9780545697958-tcb-us&campaignCode=buyannualsubscription");
			} else if (urlCheck.contains("teachables-stage")) {
				driver.get(
						"https://teachables-stage.scholastic.com/teachables/tools/subscriptionlandingpage.html?partNumber=9780545697958-tcb-us&campaignCode=buyannualsubscription");
			} else if (urlCheck.contains("teachables.")) {
				driver.get(
						"https://teachables.scholastic.com/teachables/tools/subscriptionlandingpage.html?partNumber=9780545697958-tcb-us&campaignCode=buyannualsubscription");
			}
			ASOUtils.pause(2);
			WaitUtils.waitForDisplayed(GoldButton());
			ASOUtils.pause(3);
			GoogleSheetRowModel.setEndTime();
			GoogleSheetRowModel.setStatus("Pass");

			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_Add_SUBSCRIPTION, Utility.createObject());
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_CHECKOUT_SUBSCRIPTION,
					Utility.createObject());
			System.out.println("(TestBaseProvider.getTestBase().getString(\"tcb.url\"));"
					+ (TestBaseProvider.getTestBase().getString("tcb.url")));
			System.out.println("Pass - ISBN Search +Subscription- Google Sheet Inject");
		} catch (Exception e) {
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_Add_SUBSCRIPTION, Utility.createObject());
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_CHECKOUT_SUBSCRIPTION,
					Utility.createObject());
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_STATUS, Utility.createObject());
			Assert.fail("Fail - ISBN Search=Subscription- Google Sheet Inject");
		}
	}

	public void UserLogOut() {
		try {
		
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.visibilityOf(LogoutLink()));
			GoogleSheetRowModel.setStartTime();
			LogoutLink().click();
			ASOUtils.pause(5);
			GoogleSheetRowModel.setEndTime();
			ASOUtils.pause(15);
			GoogleSheetRowModel.setStatus("Pass");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_LOGOUT, Utility.createObject());
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_STATUS, Utility.statusCreateObject());
			System.out.println("(TestBaseProvider.getTestBase().getString(\"tcb.url\"));"
					+ (TestBaseProvider.getTestBase().getString("tcb.url")));
			System.out.println("Pass-LogOut");
		} catch (Exception e) {
			Assert.fail("Fail-LogOut");
			GoogleSheetRowModel.setStatus("Fail");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_LOGOUT, Utility.createObject());
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_STATUS, Utility.statusCreateObject());
		}
	}

	public void UserLogin() {
		try {
			
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			urlBeforeLogin = driver.getCurrentUrl();
			WebDriverWait wait = new WebDriverWait(driver, 20);
			driver.manage().deleteAllCookies();
			driver.navigate().refresh();
			ASOUtils.pause(2);
			wait.until(ExpectedConditions.visibilityOf(SearchSignIn()));
			handleWelcomePopupWindow();
			ASOUtils.pause(2);
			SearchSignIn().click();
			handleWelcomePopupWindow();
			wait.until(ExpectedConditions.visibilityOf(LoginUsername()));
			GoogleSheetRowModel.setStartTime();
			LoginUsername().sendKeys(TestBaseProvider.getTestBase().getTestData().getString("Email"));
			LoginPassword().sendKeys(TestBaseProvider.getTestBase().getTestData().getString("Password"));
			ASOUtils.pause(2);
			handleWelcomePopupWindow();
			ASOUtils.clickUsingJavaScript(LoginSignInLink());
			GoogleSheetRowModel.setEndTime();
			handleWelcomePopupWindow();
			ASOUtils.pause(10);
			wait.until(ExpectedConditions.visibilityOf(LogoutLink()));
			
			GoogleSheetRowModel.setStatus("Pass");
			System.out.println("Pass-Login");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_LOGIN, Utility.createObject());
			System.out.println("(TestBaseProvider.getTestBase().getString(\"tcb.url\"));"
					+ (TestBaseProvider.getTestBase().getString("tcb.url")));
		} catch (Exception e) {
			{	
				Utility.setAllCellsSkipped(23, 29, CellRanges.TCB_STORE_STATUS, sheetName);
				Assert.fail("Fail-Login");
			}
		}
	}

	public void searchProductTitleInSearchPage() {
		try {
			GoogleSheetRowModel.setStartTime();
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			WebDriverWait wait = new WebDriverWait(driver, 6);
			wait.until(ExpectedConditions.visibilityOf(gethomepageSearchGoButton()));
			gethomepageSearchbox().clear();
			String productTtile = TestBaseProvider.getTestBase().getTestData().getString("producttitle");
			gethomepageSearchbox().sendKeys(productTtile);
			gethomepageSearchGoButton().click();
			ASOUtils.waitForElementToBeClickable(gethomepageSearchResultListFirstElement(), 10);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					gethomepageSearchResultListFirstElement());
			GoogleSheetRowModel.setEndTime();
			GoogleSheetRowModel.setStatus("Pass");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_SEARCH_ISBNSEARCH,
					Utility.createObject());
			System.out.println("(TestBaseProvider.getTestBase().getString(\"tcb.url\"));"
					+ (TestBaseProvider.getTestBase().getString("tcb.url")));
		} catch (Exception e) {
			GoogleSheetRowModel.setStatus("Fail");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_SEARCH_ISBNSEARCH,
					Utility.createObject());
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_STATUS, Utility.createObject());
			Assert.fail("ISBN SEarch");
		}
	}

	public void searchForKeyword() {
		try {
			
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			WebDriverWait wait = new WebDriverWait(driver, 6);
			wait.until(ExpectedConditions.visibilityOf(gethomepageSearchGoButton()));
			gethomepageSearchbox().clear();
			GoogleSheetRowModel.setStartTime();
			String keyword = TestBaseProvider.getTestBase().getTestData().getString("keyword");
			gethomepageSearchbox().sendKeys(keyword);
			gethomepageSearchGoButton().click();
			WebDriverWait wait1 = new WebDriverWait(driver, 20);
			wait1.until(ExpectedConditions.visibilityOfAllElements(getProductTitles()));
			GoogleSheetRowModel.setEndTime();
			GoogleSheetRowModel.setStatus("Pass");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_SEARCH_KEYWORD, Utility.createObject());
			getProductTitles().get(0).click();
		} catch (Exception e) {
			GoogleSheetRowModel.setStatus("Fail");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_SEARCH_KEYWORD, Utility.createObject());
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_STATUS, Utility.createObject());

		}
	}

	public void searchFromPDP() {
		try {
			GoogleSheetRowModel.setStartTime();
			WebDriver driver = TestBaseProvider.getTestBase().getDriver();
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(getPDPSection()));
			wait.until(ExpectedConditions.visibilityOf(gethomepageSearchGoButton()));
			gethomepageSearchbox().clear();
			String keyword = TestBaseProvider.getTestBase().getTestData().getString("keyword");
			gethomepageSearchbox().sendKeys(keyword);
			gethomepageSearchGoButton().click();
			WebDriverWait wait1 = new WebDriverWait(driver, 20);
			wait1.until(ExpectedConditions.visibilityOfAllElements(getProductTitles()));
			GoogleSheetRowModel.setEndTime();
			GoogleSheetRowModel.setStatus("Pass");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_SEARCH_PDP, Utility.createObject());
		} catch (Exception e) {
			GoogleSheetRowModel.setStatus("Fail");
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_SEARCH_PDP, Utility.createObject());
			GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TCB_STORE_STATUS, Utility.createObject());

		}
	}
}
