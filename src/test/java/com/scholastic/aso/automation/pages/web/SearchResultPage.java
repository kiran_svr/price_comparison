package com.scholastic.aso.automation.pages.web;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import com.scholastic.aso.automation.beans.ProductInformationBean;
import com.scholastic.aso.automation.support.ASOUtils;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;
import com.torque.automation.core.TestDataUtils;

import Utils.CellRanges;
import Utils.GoogleSheetRowModel;
import Utils.Utility;

public class SearchResultPage extends BaseTestPage<TestPage> {

	String sheetName = TestDataUtils.getString("sheetName");

	@FindBy(locator = "SEARCH_RESULT_FOR_LABEL")
	private WebElement searchResultForLabel;
	@FindBy(locator = "SHOWING_RESULT_FOR_LABEL")
	private WebElement showingResultForLabel;
	@FindBy(locator = "PRODUCT_TITLE_LINKS_LIST")
	private List<WebElement> productTitleLinksList;
	@FindBy(locator = "RESULT_LOADING_CONTAINER")
	private WebElement resultLoadingContainer;
	@FindBy(locator = "PRODUCT_GRID_TITLE")
	private List<WebElement> productGridTitle;

	public List<WebElement> getProductGridTitle() {
		return productGridTitle;
	}

	public WebElement getResultLoadingContainer() {
		return resultLoadingContainer;
	}

	public WebElement getSearchResultForLabel() {
		return searchResultForLabel;
	}

	public WebElement getShowingResultForLabel() {
		return showingResultForLabel;
	}

	public List<WebElement> getProductTitleLinksList() {
		return productTitleLinksList;
	}

	@Override
	protected void openPage() {
	}

	public void selectAnyItem() {
		try {
			ASOUtils.pause(10);
			ASOUtils.waitForAjaxToComplete();
			ASOUtils.waitForLoaderToDismiss();
			WaitUtils.waitForDisplayed(getProductTitleLinksList().get(0));
			ASOUtils.clickUsingJavaScript(getProductTitleLinksList().get(0));
			System.out.println("search result page" + getSearchResultForLabel());
		} catch (Exception e) {
			AssertUtils.assertDisplayed(getProductTitleLinksList().get(0));
		}
	}

	public void addProductIntoCartFromSearchResultPage() {

		WaitUtils.waitForDisplayed(getSearchResultForLabel());
		WaitUtils.waitForDisplayed(getResultLoadingContainer());
		try {
			GoogleSheetRowModel.setStartTime();
			WaitUtils.waitForDisplayed(getShowingResultForLabel());
			// Logic is added to handle out of stock products
			By PRODUCT_GRID = By.cssSelector(".card");
			By ADD_TO_CART = By.tagName("button");
			By SELECTED_PRODUCT_TITLE = By.cssSelector(".card-title>a");
			By SELECTED_PRODUCT_PRICE = By.cssSelector(".price-wrapper>div.our-price");
			List<WebElement> productList = getDriver().findElements(PRODUCT_GRID);
			for (WebElement product : productList) {
				List<WebElement> addToCart = product.findElements(ADD_TO_CART);
				if (addToCart.size() != 0) {
					WebElement productTitle = product.findElement(SELECTED_PRODUCT_TITLE);
					ProductInformationBean p = new ProductInformationBean();
					p.setProductTitle(productTitle.getText());
					WebElement productPrice = product.findElement(SELECTED_PRODUCT_PRICE);
					p.setProductPrice(productPrice.getText());
					ASOUtils.addProductItem(p);
					addToCart.get(0).click();
					GoogleSheetRowModel.setEndTime();
					break;

				}
			}
			ASOUtils.waitForAjaxToComplete();
			ASOUtils.pause(2);
			GoogleSheetRowModel.setStatus("Pass");
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_SHOPPINGCART, Utility.createObject());

				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_SHOPPINGCART, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
		} catch (Exception e) {
			GoogleSheetRowModel.setStatus("Fail");
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				Utility.setAllCellsSkipped(19, 21, CellRanges.TSO_STORE_STATUS, sheetName);
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				Utility.setAllCellsSkipped(9, 11, CellRanges.SSO_STORE_STATUS, sheetName);
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			Assert.fail("Search Have No Item");
		}
	}
}
