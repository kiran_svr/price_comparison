package com.scholastic.aso.automation.pages.web;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import Utils.CellRanges;
import Utils.GoogleSheetRowModel;
import Utils.Utility;

import com.scholastic.aso.automation.support.ASOUtils;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;
import com.torque.automation.core.TestDataUtils;

public class HomePage extends BaseTestPage<TestPage> {

	String sheetName = TestDataUtils.getString("sheetName");

	@FindBy(locator = "SIGN_IN_LINK")
	private WebElement signInLink;
	@FindBy(locator = "SIGN_IN_LINK")
	private List<WebElement> sizeSignInLink;
	@FindBy(locator = "EMAIL_INPUT")
	private WebElement emailInput;
	@FindBy(locator = "PASSWORD_INPUT")
	private WebElement passwordInput;
	@FindBy(locator = "SIGN_IN_BUTTON")
	private WebElement signInButton;
	@FindBy(locator = "SEARCH_TOP_RIGHT_BUTTON")
	private WebElement searchTopRightButton;
	@FindBy(locator = "SEARCH_TOP_INPUT")
	private WebElement searchTopInput;
	@FindBy(locator = "SEARCH_GO_TOP_BUTTON")
	private WebElement searchGoTopButton;
	@FindBy(locator = "LOGGED_IN_USER_TXT")
	private WebElement loggedInUserTxt;
	@FindBy(locator = "LOGOUT_LINK")
	private WebElement logoutLink;
	@FindBy(locator = "LOGGED_IN_USER_TXT")
	private List<WebElement> sizeloggedInUserTxt;
	@FindBy(locator = "LOGOUT_LINK")
	private List<WebElement> sizeLogoutLink;
	@FindBy(locator = "SEARCH_DROPDOWN")
	private WebElement searchDropDown;
	@FindBy(locator = "SEARCH_DROPDOWN_LIST")
	private WebElement searchDropDownList;
	@FindBy(locator = "POPUP_CLOSE_LINK")
	private WebElement popUpCloseLink;
	@FindBy(locator = "ENTER_SITE_POPUP")
	private WebElement enterSitePopUp;
	@FindBy(locator = "we_help_you_find_some_thing")
	private WebElement we_help_you_find_some_thing;
	@FindBy(locator = "we_help_you_find_some_thing_POPUp")
	private List<WebElement> we_help_you_find_some_thing_POPUp;
	@FindBy(locator = "PROFILE_ICON")
	private WebElement profileIcon;
	@FindBy(locator = "MINI_CART_REMOVE_PRODUCT")
	private List<WebElement> removeProduct;
	@FindBy(locator = "PRODUCT_TITLE_LINKS_LIST")
	private List<WebElement> productTitleLinksList;
	@FindBy(locator="PROMO_CODE_POP_UP")
	private List<WebElement> promoCodePopUp;
	
	public List<WebElement> getPromoCodePopUp(){
		return promoCodePopUp;
	}
	public List<WebElement> getRemoveProduct() {
		return removeProduct;
	}

	public WebElement getProfileIcon() {
		return profileIcon;
	}

	public WebElement we_help_you_find_some_thing() {
		return we_help_you_find_some_thing;
	}

	public List<WebElement> we_help_you_find_some_thing_POPUp() {
		return we_help_you_find_some_thing_POPUp;
	}

	public WebElement getEnterSitePopUp() {
		return enterSitePopUp;
	}

	public List<WebElement> getSizeloggedInUserTxt() {
		return sizeloggedInUserTxt;
	}

	public List<WebElement> getSizeLogoutLink() {
		return sizeLogoutLink;
	}

	public WebElement getPopUpCloseLink() {
		return popUpCloseLink;
	}

	public WebElement getSearchDropDown() {
		return searchDropDown;
	}

	public WebElement getSearchDropDownList() {
		return searchDropDownList;
	}

	public List<WebElement> sizeGetSignInLink() {
		return sizeSignInLink;
	}

	public List<WebElement> sizeGetLogoutLink() {
		return sizeLogoutLink;
	}

	public List<WebElement> sizeLoggedInUserTxt() {
		return sizeloggedInUserTxt;
	}

	public WebElement getSignInLink() {
		return signInLink;
	}

	public WebElement getEmailInput() {
		return emailInput;
	}

	public WebElement getPasswordInput() {
		return passwordInput;
	}

	public WebElement getSignInButton() {
		return signInButton;
	}

	public WebElement getSearchTopRightButton() {
		return searchTopRightButton;
	}

	public WebElement getSearchTopInput() {
		return searchTopInput;
	}

	public WebElement getSearchGoTopButton() {
		return searchGoTopButton;
	}

	public WebElement getLoggedInUserTxt() {
		return loggedInUserTxt;
	}

	public WebElement getLogoutLink() {
		return logoutLink;
	}

	public List<WebElement> getProductTitleLinksList() {
		return productTitleLinksList;
	}

	@Override
	protected void openPage() {
		getDriver().get(getTestBase().getString("base.url"));
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void doLogin() {
		try {
			GoogleSheetRowModel.setStartTime();
			String username = TestBaseProvider.getTestBase().getTestData().getString("username");
			String password = TestBaseProvider.getTestBase().getTestData().getString("password");
			WaitUtils.waitForDisplayed(getEmailInput());
			getEmailInput().sendKeys(username);
			getPasswordInput().sendKeys(password);
			ASOUtils.clickUsingJavaScript(ASOUtils.waitForElementToBeClickable(getSignInButton(), 30));
			GoogleSheetRowModel.setEndTime();
			ASOUtils.pause(2);
			ASOUtils.waitForLoaderToDismiss();
			clickProfile();
			WaitUtils.waitForDisplayed(getLogoutLink());
			ASOUtils.pause(2);
			TestBaseProvider.getTestBase().getDriver().navigate().refresh();
			ASOUtils.pause(4);
			
			
			// Below method write the status and time into google sheet under specified cell
			// range
			// Call this method to set end time
		
			GoogleSheetRowModel.setStatus("Pass");
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
			//	GoogleSheetFunctions.clearData("Test", "D12:E12");
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_LOGIN, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				//GoogleSheetFunctions.clearData("Test", "D2:E2");
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_LOGIN, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
		} catch (Exception e) {
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				Utility.setAllCellsSkipped(13, 21, CellRanges.TSO_STORE_STATUS, sheetName);
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				Utility.setAllCellsSkipped(3, 11, CellRanges.SSO_STORE_STATUS, sheetName);
			}
			Assert.fail("Login Failed");
		}
	}

	public void doLogout(String storeType) {
		try {
			GoogleSheetRowModel.setStartTime();
/*			if (storeType.equals("parent"))
				getDriver().get(getTestBase().getString("sso.url"));
			else
				getDriver().get(TestBaseProvider.getTestBase().getString("tso.url"));
			ASOUtils.pause(3);*/
			we_help_you_find_something_POPUp();
			clickProfile();
			WaitUtils.waitForDisplayed(getLogoutLink());
			if (sizeGetLogoutLink().size() == 0) {
				Assert.fail("Logout link is not displayed");
				ASOUtils.pause(2);
				TestBaseProvider.getTestBase().getDriver().navigate().refresh();
				we_help_you_find_something_POPUp();
			}
			//TestBaseProvider.getTestBase().getDriver().navigate().refresh();
			/*try {
				ASOUtils.pause(3);
				we_help_you_find_something_POPUp();
				clickProfile();
				getLogoutLink().click();
				
			} catch (Exception e) {
				we_help_you_find_something_POPUp();
				doLogout(storeType);
			}*/
		
			ASOUtils.pause(2);
			getLogoutLink().click();
			ASOUtils.waitForAjaxToComplete();
			GoogleSheetRowModel.setStatus("Pass");
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_LOGOUT, Utility.createObject());
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_STATUS, Utility.statusCreateObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_LOGOUT, Utility.createObject());
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_STATUS, Utility.statusCreateObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			GoogleSheetRowModel.setEndTime();
			System.out.println("Pass-Logout");
		} catch (Exception e) {
			GoogleSheetRowModel.setStatus("Fail");
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null)
			{
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_LOGOUT, Utility.createObject());
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_STATUS, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_LOGOUT, Utility.createObject());
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_STATUS, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			Assert.fail("Fail-Logout");
		}
	}

	public void clickOnSignInLink() {
		WaitUtils.waitForDisplayed(getSignInLink());
		ASOUtils.clickUsingJavaScript(getSignInLink());
		ASOUtils.pause(2);
	}

	public void removeProductsFromMiniCart() {
		ASOUtils.pause(4);
		we_help_you_find_something_POPUp();
		promo_code_POPUp();
		ASOUtils.pause(4);
		for (WebElement pdt : getRemoveProduct()) {
			ASOUtils.scrollElementIntoView(pdt);
			pdt.click();
		}
	}

	public void searchForItemInGlobalSearchBoxPDP(String searchType) {
		String text = searchType;
		try {
			if (searchType.equals("isbn")) {
				String isbnNumber = TestBaseProvider.getTestBase().getTestData().getString("physicalbookisbn");
				ASOUtils.pause(2);
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				WaitUtils.waitForDisplayed(getSearchTopInput());
				getSearchTopInput().clear();
				getSearchTopInput().sendKeys(isbnNumber);
				we_help_you_find_something_POPUp();
				ASOUtils.pause(5);
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				WaitUtils.waitForDisplayed(getProductTitleLinksList().get(0));
				AssertUtils.assertDisplayed(getProductTitleLinksList().get(0));
				if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
					GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_SEARCH_PDP,
							Utility.createObject());
					System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("tso.url")));
				} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
					GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_SEARCH_PDP,
							Utility.createObject());
					System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("sso.url")));
				}
				System.out.println("Pass-ISBN Search");
			}
			we_help_you_find_something_POPUp();
		} catch (Exception e) {
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_SEARCH_PDP, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			}
			if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_SEARCH_PDP, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			Assert.fail("FAil " + text + "Google Sheet");
		}
	}

	public void searchForItemInGlobalSearchBox(String searchType) {
		String text = searchType;
		try {
			if (searchType.equals("keyword")) {
				WaitUtils.waitForDisplayed(getSearchTopRightButton());
				
				String searchItem = TestBaseProvider.getTestBase().getTestData().getString("item");
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				WaitUtils.waitForDisplayed(getSearchTopInput());
				getSearchTopInput().clear();
				getSearchTopInput().sendKeys(searchItem);
				we_help_you_find_something_POPUp();
				GoogleSheetRowModel.setStartTime();
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				GoogleSheetRowModel.setEndTime();
				WaitUtils.waitForDisplayed(getProductTitleLinksList().get(0));
				AssertUtils.assertDisplayed(getProductTitleLinksList().get(0));			
				GoogleSheetRowModel.setStatus("Pass");
				if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
					GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_SEARCH_KEYWORDSEARCH,
							Utility.createObject());
					System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("tso.url")));
				} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
					GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_SEARCH_KEYWORDSEARCH,
							Utility.createObject());
					System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("sso.url")));
				}
				System.out.println("Pass-Keyword Search");
			} else if (searchType.equals("author")) {
				WaitUtils.waitForDisplayed(getSearchTopRightButton());
				String searchItem = TestBaseProvider.getTestBase().getTestData().getString("author");
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				WaitUtils.waitForDisplayed(getSearchTopInput());
				getSearchTopInput().clear();	
				getSearchTopInput().sendKeys(searchItem);
				we_help_you_find_something_POPUp();
				GoogleSheetRowModel.setStartTime();
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				GoogleSheetRowModel.setEndTime();
				GoogleSheetRowModel.setStatus("Pass");
				if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
					GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_SEARCH_AUTHOR,
							Utility.createObject());
					System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("tso.url")));
				} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
					GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_SEARCH_AUTHOR,
							Utility.createObject());
					System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("sso.url")));
				}
				System.out.println("Pass-Author Search");
			} else if (searchType.equals("isbn")) {
				String isbnNumber = TestBaseProvider.getTestBase().getTestData().getString("physicalbookisbn");
				ASOUtils.pause(2);
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				WaitUtils.waitForDisplayed(getSearchTopInput());
				getSearchTopInput().clear();
				
				getSearchTopInput().sendKeys(isbnNumber);
				
				we_help_you_find_something_POPUp();
				ASOUtils.pause(5);
				GoogleSheetRowModel.setStartTime();
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				WaitUtils.waitForDisplayed(getProductTitleLinksList().get(0));
				AssertUtils.assertDisplayed(getProductTitleLinksList().get(0));
				GoogleSheetRowModel.setEndTime();
				System.out.println("Pass-ISBN Search");
				GoogleSheetRowModel.setStatus("Pass");
				System.out.println("(TestBaseProvider.getTestBase().getTestData().getString(\"ssourl\""
						+ (TestBaseProvider.getTestBase().getTestData().getString("ssourl")));
				if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
					GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_SEARCH_ISBNSEARCH,
							Utility.createObject());
					System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("tso.url")));
				} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
					GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_SEARCH_ISBNSEARCH,
							Utility.createObject());
					System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("sso.url")));
				}
			} else if (searchType.equals("title")) {
				GoogleSheetRowModel.setStartTime();
				WaitUtils.waitForDisplayed(getSearchTopRightButton());
				String searchTitle = getTestBase().getTestData().getString("title");
				ASOUtils.clickUsingJavaScript(getSearchTopRightButton());
				WaitUtils.waitForDisplayed(getSearchTopInput());
				getSearchTopInput().clear();
				getSearchTopInput().sendKeys(searchTitle);
				we_help_you_find_something_POPUp();
				getSearchGoTopButton().click();
				System.out.println("Pass-ISBN Search");
			}
			we_help_you_find_something_POPUp();
		} catch (Exception e) {
			GoogleSheetRowModel.setStatus("Fail");
			if (searchType.equals("isbn")) {
				if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
					Utility.setAllCellsSkipped(14, 21, CellRanges.TSO_STORE_STATUS, sheetName);
				}
				if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
					Utility.setAllCellsSkipped(4, 11, CellRanges.SSO_STORE_STATUS, sheetName);
					System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("sso.url")));
				}
			} else if (searchType.equals("keyword")) {
				if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
					Utility.setAllCellsSkipped(15, 21, CellRanges.TSO_STORE_STATUS, sheetName);
				}
				if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
					Utility.setAllCellsSkipped(5, 11, CellRanges.SSO_STORE_STATUS, sheetName);
					System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("sso.url")));
				}
			} else if (searchType.equals("author")) {
				if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
					Utility.setAllCellsSkipped(17, 21, CellRanges.TSO_STORE_STATUS, sheetName);
				}
				if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
					Utility.setAllCellsSkipped(7, 11, CellRanges.SSO_STORE_STATUS, sheetName);
					System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
							+ (TestBaseProvider.getTestBase().getString("sso.url")));
				}
			}
			Assert.fail("FAil");
		}
	}

	public void verifyLogoutLink() {
		if (sizeLogoutLink.size() == 0) {
			Assert.fail("Logoutlink is  missing");
		}
	}

	public void verifyLoggedInUserText() {
		if (sizeLoggedInUserTxt().size() == 0) {
			Assert.fail("Logged in user text is missing");
		}
	}

	public void verifySignInLink() {
		if (sizeGetSignInLink().size() == 0) {
			Assert.fail("User not logged out succesfully");
		}
	}

	public void handleNewScholasticStorePopUp() {
		try {
			// WebElement enterSite = TestBaseProvider.getTestBase().getDriver()
			// .findElement(By.xpath("//a[@class='cart_enter']"));
			WaitUtils.waitForDisplayed(getEnterSitePopUp());
			ASOUtils.clickUsingJavaScript(getEnterSitePopUp());
			System.out.println("Enter Site Pop Up Caught");
		} catch (Exception e) {
			System.out.println("Pop Up Not Caught");
		}
	}

	public void verifyParentHomePageOpened() {
		String currentURL = getDriver().getCurrentUrl();
		if (!currentURL.contains("/parent-ecommerce/parent-store.html"))
			Assert.fail("parent home page not opened");
	}

	public boolean we_help_you_find_something_POPUp() {
		boolean flag = false;
		ASOUtils.pause(10);
		try {
			boolean condition = we_help_you_find_some_thing_POPUp().size() != 0
					&& we_help_you_find_some_thing_POPUp().get(0).isDisplayed();
			if (condition) {
				new Actions(getDriver()).moveToElement(we_help_you_find_some_thing_POPUp().get(0)).perform();
				we_help_you_find_some_thing().click();
				System.out.println("####################:Handled PopUp");
				flag = true;
			} else {
				System.out.println("@@@@@@@@@@@@@@@@@:Pop Up is not displayed");
			}
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
			return flag;
		}
	}
public void promo_code_POPUp() {
		
		ASOUtils.pause(10);
		
		try {
			boolean condition = getPromoCodePopUp().size() != 0 && getPromoCodePopUp().get(0).isDisplayed();
			if (condition) {
				new Actions(getDriver()).moveToElement(getPromoCodePopUp().get(0)).perform();
				getPromoCodePopUp().get(0).click();
				
			}		
			
			
		} catch (Exception e) {			
			e.printStackTrace();
		
		}
	}

	public void clickProfile() {
		WaitUtils.waitForDisplayed(getProfileIcon());
		getProfileIcon().click();
	}
}