package com.scholastic.aso.automation.pages.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.scholastic.aso.automation.beans.ProductInformationBean;
import com.scholastic.aso.automation.support.ASOUtils;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.torque.automation.core.TestDataUtils;

import Utils.CellRanges;
import Utils.GoogleSheetRowModel;
import Utils.Utility;

public class ProductDetailPage extends BaseTestPage<SearchResultPage> {

	String sheetName = TestDataUtils.getString("sheetName");

	@FindBy(locator = "ADD_TO_CART_BUTTON")
	private WebElement addToCart;
	@FindBy(locator = "PRODUCT_PRICE_LABEL")
	private WebElement productPrice;
	@FindBy(locator = "PRODUCT_TITLE_LABEL")
	private WebElement productTitle;
	@FindBy(locator = "PDP_ISBN_NUMBER")
	WebElement isbnNumber;

	public WebElement getISBN() {
		return isbnNumber;
	}

	public WebElement getProductTitle() {
		return productTitle;
	}

	public WebElement getAddToCart() {
		return addToCart;
	}

	public WebElement getProductPrice() {
		return productPrice;
	}

	@Override
	protected void openPage() {

	}

	public void addProductToCart() {
		try {
			
			HomePage homepageobj = new HomePage();
			WaitUtils.waitForDisplayed(getAddToCart());
			WaitUtils.waitForDisplayed(getProductPrice());
			WaitUtils.waitForDisplayed(getProductTitle());
			ASOUtils.waitForAjaxToComplete();
			ASOUtils.handleFeedbackPopUp();
			ProductInformationBean p = new ProductInformationBean();
			p.setProductTitle(getProductTitle().getText());
			p.setProductPrice(getProductPrice().getText());
			WebElement isbnNumber = getDriver().findElement(
					By.xpath("//section[@id='product-details-section']/div/div[2]/div/div[1]/ul/li[2]/span[2]"));
			p.setIsbn(isbnNumber.getText());
			p.setQuantity("1");
			
			ASOUtils.addProductItem(p);	
			homepageobj.we_help_you_find_something_POPUp();
			GoogleSheetRowModel.setStartTime();
			ASOUtils.clickUsingJavaScript(getAddToCart());
			GoogleSheetRowModel.setEndTime();
			homepageobj.we_help_you_find_something_POPUp();
			ASOUtils.waitForAjaxToComplete();
			
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_ADDITEM_TO_CART,
						Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_ADDITEM_TO_CART,
						Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			System.out.println("Pass-Add to cart-Google Inject");
		} catch (Exception e) {
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_ADDITEM_TO_CART,
						Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_ADDITEM_TO_CART,
						Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			Assert.fail("Fail-Add to cart-Google Inject");
		}

	}

	public void verifyPDPPageLoaded() {

		ASOUtils.pause(5);
		String searchedISBN = TestBaseProvider.getTestBase().getTestData().getString("physicalbookisbn");
		boolean flag = getDriver().getCurrentUrl().contains(searchedISBN) && searchedISBN.equals(getISBN().getText());
		if (!flag)
			Assert.fail("User not landed of  the  selected ISBN:" + searchedISBN + " of Product Detail  Page");
	}
}
