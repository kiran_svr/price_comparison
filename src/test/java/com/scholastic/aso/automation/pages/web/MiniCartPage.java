package com.scholastic.aso.automation.pages.web;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.scholastic.aso.automation.support.ASOUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;

public class MiniCartPage extends BaseTestPage<TestPage> {

	HomePage homepage = new HomePage();

	@FindBy(locator = "MINI_CART_ICON")
	private WebElement cartIcon;
	@FindBy(locator = "MINI_CART_FLYOUT")
	private WebElement miniCartFlyout;
	@FindBy(locator = "POPUP_CLOSE_LINK")
	private WebElement popUpCloseLink;
	@FindBy(locator = "MINI_CART_REMOVE_PRODUCT")
	private WebElement removeProduct;

	public WebElement getPopUpCloseLink() {
		return popUpCloseLink;
	}

	public WebElement getRemoveProduct() {
		return removeProduct;
	}

	public WebElement getMiniCartIcon() {
		return cartIcon;
	}

	public WebElement getMiniCartFlyout() {
		return miniCartFlyout;
	}

	@Override
	protected void openPage() {

	}

	public void hoverCartIcon() {
		if (getMiniCartIcon() != null && getMiniCartIcon().isDisplayed()) {

			closePopUp();
			WaitUtils.waitForDisplayed(getMiniCartIcon());
			WebDriver driver = getDriver();
			((JavascriptExecutor) driver).executeScript(
					"var evtObj=null;document.createEvent?(evtObj=document.createEvent(\"MouseEvents\"),evtObj.initMouseEvent(\"mouseover\",!0,!0,document.defaultView,0,0,0,0,0,!1,!1,!1,!1,0,null),arguments[0].dispatchEvent(evtObj)):"
							+ "(evtObj=document.createEventObject(),evtObj.detail=0,evtObj.screenX=0,evtObj.screenY=0,evtObj.clientX=0,evtObj.clientY=0,evtObj.ctrlKey=!1,evtObj.altKey=!1,evtObj.shiftKey=!1,evtObj.metaKey=!1,evtObj.button=1,arguments[0].fireEvent(\"onmouseover\",evtObj));",
					getMiniCartIcon(), 0, 0);

		} else
			Assert.fail("Mini Cart Icon is missing");
	}

	public void closePopUp() {
		try {
			WaitUtils.waitForDisplayed(getPopUpCloseLink());
			getPopUpCloseLink().click();
			System.out.println("PopUp Handled.");
		} catch (Exception e) {
			System.out.println("No PopUp Found.");
		}
	}

	public void clickMiniCartIcon() {
		ASOUtils.pause(20);
		homepage.we_help_you_find_something_POPUp();
		homepage.promo_code_POPUp();
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(getMiniCartIcon()));
		if (getMiniCartIcon() != null && getMiniCartIcon().isDisplayed()) {
			closePopUp();
			WaitUtils.waitForDisplayed(getMiniCartIcon());
			ASOUtils.clickUsingActions(getMiniCartIcon());
		} else
			Assert.fail("Mini Cart Icon is missing");
	}

}
