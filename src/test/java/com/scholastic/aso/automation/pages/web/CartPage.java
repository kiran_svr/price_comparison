package com.scholastic.aso.automation.pages.web;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.scholastic.aso.automation.support.ASOUtils;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.torque.common.AssertUtils;
import com.scholastic.torque.common.BaseTestPage;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.TestPage;
import com.scholastic.torque.common.WaitUtils;
import com.torque.automation.core.TestDataUtils;

import Utils.CellRanges;
import Utils.GoogleSheetRowModel;
import Utils.Utility;

public class CartPage extends BaseTestPage<TestPage> {

	String sheetName = TestDataUtils.getString("sheetName");

	@FindBy(locator = "CHECKOUT_BUTTON")
	private WebElement checkOutNow;

	public WebElement getCheckOutNow() {
		return checkOutNow;
	}

	@Override
	protected void openPage() {

	}

	public void verifyCheckOutNow() {
		try {
			GoogleSheetRowModel.setStartTime();
			WaitUtils.waitForDisplayed(getCheckOutNow());
			AssertUtils.assertDisplayed(getCheckOutNow());
			ASOUtils.waitForAjaxToComplete();
			ASOUtils.pause(3);
			GoogleSheetRowModel.setEndTime();

			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_CHECKOUT, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_CHECKOUT, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			System.out.println("Pass-Checkout-google Inject");
		} catch (Exception e) {
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_CHECKOUT, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_CHECKOUT, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			Assert.fail("Fail-Checkout-google Inject");
		}
	}
	
	public void clickCheckOutNow(){
		try {
			GoogleSheetRowModel.setStartTime();
			WaitUtils.waitForDisplayed(getCheckOutNow());
			getCheckOutNow().click();
			ASOUtils.waitForAjaxToComplete();
			ASOUtils.pause(3);
			GoogleSheetRowModel.setEndTime();
			GoogleSheetRowModel.setStatus("Pass");
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_CHECKOUT, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_CHECKOUT, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			System.out.println("Pass-Checkout-google Inject");
		} catch (Exception e) {
			GoogleSheetRowModel.setStatus("Fail");
			if (TestBaseProvider.getTestBase().getTestData().getString("tsourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_CHECKOUT, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("tso.url")));
			} else if (TestBaseProvider.getTestBase().getTestData().getString("ssourl") != null) {
				GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.SSO_STORE_CHECKOUT, Utility.createObject());
				System.out.println("(TestBaseProvider.getTestBase().getString(\"sso.url\"));"
						+ (TestBaseProvider.getTestBase().getString("sso.url")));
			}
			Assert.fail("Fail-Checkout-google Inject");
		}
	}
}
