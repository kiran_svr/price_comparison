package com.scholastic.aso.automation.beans;

public class ProductInformationBean {
	private String productTitle;
	private String productPrice;
	private String itemID;
	private String isbn;
	private String format;
	private String fileType;
	private String pages;
	private String publisher;
	private String grades;
	private String Quantity;

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	public String getItemID() {
		return itemID;
	}

	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getGrades() {
		return grades;
	}

	public void setGrades(String grades) {
		this.grades = grades;
	}

	public String getQuantity() {
		return Quantity;
	}

	public void setQuantity(String quantity) {
		Quantity = quantity;
	}

	@Override
	public String toString() {
		return "ProductInformationBean [productTitle=" + productTitle + ", productPrice=" + productPrice + ", itemID="
				+ itemID + ", isbn=" + isbn + ", format=" + format + ", fileType=" + fileType + ", pages=" + pages
				+ ", publisher=" + publisher + ", grades=" + grades + ", Quantity=" + Quantity + "]";
	}

}
