package com.scholastic.aso.automation.support;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.scholastic.aso.automation.beans.ProductInformationBean;
import com.scholastic.aso.automation.pages.web.HomePage;
import com.scholastic.torque.common.LocatorUtils;
import com.scholastic.torque.common.TestBaseProvider;
import com.scholastic.torque.common.WaitUtils;
import com.tdm.client.TDMClient;

public class ASOUtils {
	private static final String CURRENCY_SYMBOLS = "\\p{Sc}\u0024\u060B";
	static WebDriverWait wait = null;

	public static void pause(long seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
		}
	}
	public static void waitForAjaxToComplete() {
		try {
			(new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 30)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					JavascriptExecutor js = (JavascriptExecutor) TestBaseProvider.getTestBase().getDriver();
					return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");

				}
			});
		} catch (Exception e) {
		}
	}
	public static void waitForLoaderToDismiss() {
		wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(),
				Integer.parseInt(TestBaseProvider.getTestBase().getContext().getString("wait.timeout.sec")));
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver wdriver) {
					return ((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver())
							.executeScript("return document.readyState").equals("complete");
				}
			});
		} catch (Exception e) {
			System.out.println("Waiting for Loader to dismiss timed out");
		}
	}
	public static void waitForelementToBeClickable(final WebElement webElement) {
		wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 30);
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
	}
	public static WebElement waitForElementToBeClickable(WebElement element, long timeOut) {
		try {
			WebDriverWait wait = new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), timeOut);
			element = wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
		}
		return element;
	}
	public static void addProductItem(ProductInformationBean productDetails) {
		HashMap<String, ProductInformationBean> items = getProductItems();
		if (productDetails.getIsbn() != null && productDetails.getIsbn().trim().length() > 0) {
			items.put(productDetails.getIsbn(), productDetails);
		} else {
			items.put(productDetails.getProductTitle(), productDetails);
		}
		setProductItems(items);
	}
	public static void setProductItems(HashMap<String, ProductInformationBean> productsList) {
		TestBaseProvider.getTestBase().getContext().setProperty(ConstantUtils.ASO_CART_PRODUCTS, productsList);
	}
	@SuppressWarnings("unchecked")
	public static HashMap<String, ProductInformationBean> getProductItems() {
		HashMap<String, ProductInformationBean> productsList;
		if (TestBaseProvider.getTestBase().getContext().containsKey(ConstantUtils.ASO_CART_PRODUCTS)) {

			productsList = ((HashMap<String, ProductInformationBean>) TestBaseProvider.getTestBase().getContext()
					.getProperty(ConstantUtils.ASO_CART_PRODUCTS));

		} else {
			productsList = new HashMap<String, ProductInformationBean>();
			TestBaseProvider.getTestBase().getContext().setProperty(ConstantUtils.ASO_CART_PRODUCTS, productsList);
		}
		return productsList;
	}
	/**
	 * This method return total no of amount
	 *
	 * @return
	 */
	public static Double getOrderTotal() {
		Map<String, ProductInformationBean> Products = ASOUtils.getProductItems();
		String quantity;
		double price;
		double orderTotal = 0;
		for (String key : Products.keySet()) {
			quantity = Products.get(key).getQuantity();
			price = ASOUtils.getPriceWithOutCurrency(Products.get(key).getProductPrice());
			orderTotal += ((Integer.parseInt(quantity) * price));
		}
		return orderTotal;
	}
	/**
	 * This Method Return price Without Currency
	 *
	 * @param text
	 * @return
	 */
	public static Double getPriceWithOutCurrency(String text) {

		String textDouble = getPriceWithCurrency(text);
		System.out.println("Parameter passed ->" + text);
		Pattern pattern = Pattern.compile("[\\d{0,9}[,\\.]?(\\d{1,2})?]+");
		Matcher matcher = pattern.matcher(textDouble);
		if (matcher.find()) {
			try {
				return Double.parseDouble(matcher.group(0).replaceAll(",", ""));
			} catch (NumberFormatException e) {
				return Double.parseDouble(matcher.group(0).replaceAll(",", "").replaceAll("[^0-9]", ""));
			}
		}

		return 0.0;
	}
	/**
	 * this method will return WebElement for dynamic locators which needs String
	 * format
	 *
	 * @param loc
	 *            -Locator
	 */
	public static WebElement findElement(String loc, String... value) {
		switch (value.length) {
		case 0:
			return TestBaseProvider.getTestBase().getDriver().findElement(LocatorUtils.getBy(loc));
		case 1:
			try {
				return TestBaseProvider.getTestBase().getDriver()
						.findElement(LocatorUtils.getBy(String.format(loc, value[0])));
			} catch (Exception e) {
				return TestBaseProvider.getTestBase().getDriver()
						.findElement(LocatorUtils.getBy(String.format(loc, value[0].toUpperCase(Locale.ENGLISH))));
			}
		case 2:
			return TestBaseProvider.getTestBase().getDriver()
					.findElement(LocatorUtils.getBy(String.format(loc, value[0], value[1])));
		case 3:
			return TestBaseProvider.getTestBase().getDriver()
					.findElement(LocatorUtils.getBy(String.format(loc, value[0], value[1], value[2])));
		default:
			return null;
		}
	}
	public static void clickUsingActions(WebElement ele) {
		Actions actions = new Actions(TestBaseProvider.getTestBase().getDriver());
		actions.moveToElement(ele).click().build().perform();
	}
	public static void clickUsingJavaScript(WebElement ele) {
		try {
			ele.click();
		} catch (Exception e) {
			((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver()).executeScript("arguments[0].click()",
					ele);
			e.printStackTrace();
		}
	}
	public static String getPriceWithCurrency(String text) {
		Pattern p = Pattern.compile("[" + CURRENCY_SYMBOLS + "][\\d{0,9}[,\\.]?(\\d{1,2})?]+");
		Matcher m = p.matcher(text);
		while (m.find()) {
			return m.group();
		}
		return text;
	}
	public static void handleFeedbackPopUp() {
		HomePage homePage = new HomePage();
		try {
			WaitUtils.waitForDisplayed(homePage.getPopUpCloseLink());
			homePage.getPopUpCloseLink().click();
			System.out.println("PopUp Handled.");
		} catch (Exception e) {
			System.out.println("No PopUp Found.");
		}
	}
	public static void scrollElementIntoView(WebElement element) {
		((JavascriptExecutor) TestBaseProvider.getTestBase().getDriver())
				.executeScript("arguments[0].scrollIntoView(true);", element);
	}
	public static void waitForAjaxToComplete(long... second) {
		try {
			(new WebDriverWait(TestBaseProvider.getTestBase().getDriver(), 60)).until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver d) {
					JavascriptExecutor js = (JavascriptExecutor) TestBaseProvider.getTestBase().getDriver();
					return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");
				}
			});
		} catch (Exception e) {
		}
	}

}
