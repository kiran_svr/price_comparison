
package com.scholastic.cs.pricecompare.dw;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_v",
    "P_type",
    "brand",
    "currency",
    "id",
    "long_description",
    "manufacturer_name",
    "min_order_quantity",
    "name",
    "step_quantity",
    "type",
    "c_author",
    "c_backOrderFlag",
    "c_bookType",
    "c_borderId",
    "c_characterAndSeries",
    "c_classValue",
    "c_customGrade",
    "c_department",
    "c_depth",
    "c_displayTitle",
    "c_fictionType",
    "c_format",
    "c_height",
    "c_highAge",
    "c_highGradeTSOonly",
    "c_highGuidedReadingLevel",
    "c_illustrator",
    "c_inventoryThreshold",
    "c_isbn",
    "c_isbn13",
    "c_keywords",
    "c_language",
    "c_listPrice",
    "c_lowAge",
    "c_lowDRALevel",
    "c_lowGradeTSOonly",
    "c_lowGuidedReadingLevel",
    "c_numberofAlternateImages",
    "c_otherFormat",
    "c_pages",
    "c_productType",
    "c_publisher",
    "c_rank",
    "c_searchable",
    "c_shippingMessagePDP",
    "c_shops",
    "c_showACR",
    "c_showDRALevel",
    "c_showGuidedReadingLevel",
    "c_showLanguage",
    "c_showLexile",
    "c_subClass",
    "c_subject",
    "c_unitCost",
    "c_vendor1",
    "c_weight",
    "c_width",
    "c_workId"
})
public class DWData {

    @JsonProperty("_v")
    private String v;
    @JsonProperty("P_type")
    private String pType;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("id")
    private String id;
    @JsonProperty("long_description")
    private String longDescription;
    @JsonProperty("manufacturer_name")
    private String manufacturerName;
    @JsonProperty("min_order_quantity")
    private Integer minOrderQuantity;
    @JsonProperty("name")
    private String name;
    @JsonProperty("step_quantity")
    private Integer stepQuantity;
    @JsonProperty("type")
    private Type type;
    @JsonProperty("c_author")
    private String cAuthor;
    @JsonProperty("c_backOrderFlag")
    private Boolean cBackOrderFlag;
    @JsonProperty("c_bookType")
    private String cBookType;
    @JsonProperty("c_borderId")
    private String cBorderId;
    @JsonProperty("c_characterAndSeries")
    private String cCharacterAndSeries;
    @JsonProperty("c_classValue")
    private String cClassValue;
    @JsonProperty("c_customGrade")
    private String cCustomGrade;
    @JsonProperty("c_department")
    private String cDepartment;
    @JsonProperty("c_depth")
    private String cDepth;
    @JsonProperty("c_displayTitle")
    private String cDisplayTitle;
    @JsonProperty("c_fictionType")
    private String cFictionType;
    @JsonProperty("c_format")
    private String cFormat;
    @JsonProperty("c_height")
    private String cHeight;
    @JsonProperty("c_highAge")
    private Integer cHighAge;
    @JsonProperty("c_highGradeTSOonly")
    private Integer cHighGradeTSOonly;
    @JsonProperty("c_highGuidedReadingLevel")
    private String cHighGuidedReadingLevel;
    @JsonProperty("c_illustrator")
    private String cIllustrator;
    @JsonProperty("c_inventoryThreshold")
    private Integer cInventoryThreshold;
    @JsonProperty("c_isbn")
    private String cIsbn;
    @JsonProperty("c_isbn13")
    private String cIsbn13;
    @JsonProperty("c_keywords")
    private String cKeywords;
    @JsonProperty("c_language")
    private String cLanguage;
    @JsonProperty("c_listPrice")
    private Double cListPrice;
    @JsonProperty("c_lowAge")
    private Integer cLowAge;
    @JsonProperty("c_lowDRALevel")
    private String cLowDRALevel;
    @JsonProperty("c_lowGradeTSOonly")
    private Integer cLowGradeTSOonly;
    @JsonProperty("c_lowGuidedReadingLevel")
    private String cLowGuidedReadingLevel;
    @JsonProperty("c_numberofAlternateImages")
    private Integer cNumberofAlternateImages;
    @JsonProperty("c_otherFormat")
    private List<String> cOtherFormat = null;
    @JsonProperty("c_pages")
    private String cPages;
    @JsonProperty("c_productType")
    private String cProductType;
    @JsonProperty("c_publisher")
    private String cPublisher;
    @JsonProperty("c_rank")
    private String cRank;
    @JsonProperty("c_searchable")
    private Boolean cSearchable;
    @JsonProperty("c_shippingMessagePDP")
    private String cShippingMessagePDP;
    @JsonProperty("c_shops")
    private List<String> cShops = null;
    @JsonProperty("c_showACR")
    private Boolean cShowACR;
    @JsonProperty("c_showDRALevel")
    private Boolean cShowDRALevel;
    @JsonProperty("c_showGuidedReadingLevel")
    private Boolean cShowGuidedReadingLevel;
    @JsonProperty("c_showLanguage")
    private Boolean cShowLanguage;
    @JsonProperty("c_showLexile")
    private Boolean cShowLexile;
    @JsonProperty("c_subClass")
    private String cSubClass;
    @JsonProperty("c_subject")
    private String cSubject;
    @JsonProperty("c_unitCost")
    private Double cUnitCost;
    @JsonProperty("c_vendor1")
    private String cVendor1;
    @JsonProperty("c_weight")
    private String cWeight;
    @JsonProperty("c_width")
    private String cWidth;
    @JsonProperty("c_workId")
    private String cWorkId;
    @JsonProperty("c_genre")
    private String cGenre;
    
    
    @JsonProperty("c_genre")
    public String getcGenre() {
		return cGenre;
	}
    @JsonProperty("c_genre")
	public void setcGenre(String cGenre) {
		this.cGenre = cGenre;
	}

	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("_v")
    public String getV() {
        return v;
    }

    @JsonProperty("_v")
    public void setV(String v) {
        this.v = v;
    }

    @JsonProperty("P_type")
    public String getPType() {
        return pType;
    }

    @JsonProperty("P_type")
    public void setPType(String pType) {
        this.pType = pType;
    }

    @JsonProperty("brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("long_description")
    public String getLongDescription() {
        return longDescription;
    }

    @JsonProperty("long_description")
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    @JsonProperty("manufacturer_name")
    public String getManufacturerName() {
        return manufacturerName;
    }

    @JsonProperty("manufacturer_name")
    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    @JsonProperty("min_order_quantity")
    public Integer getMinOrderQuantity() {
        return minOrderQuantity;
    }

    @JsonProperty("min_order_quantity")
    public void setMinOrderQuantity(Integer minOrderQuantity) {
        this.minOrderQuantity = minOrderQuantity;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("step_quantity")
    public Integer getStepQuantity() {
        return stepQuantity;
    }

    @JsonProperty("step_quantity")
    public void setStepQuantity(Integer stepQuantity) {
        this.stepQuantity = stepQuantity;
    }

    @JsonProperty("type")
    public Type getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(Type type) {
        this.type = type;
    }

    @JsonProperty("c_author")
    public String getCAuthor() {
        return cAuthor;
    }

    @JsonProperty("c_author")
    public void setCAuthor(String cAuthor) {
        this.cAuthor = cAuthor;
    }

    @JsonProperty("c_backOrderFlag")
    public Boolean getCBackOrderFlag() {
        return cBackOrderFlag;
    }

    @JsonProperty("c_backOrderFlag")
    public void setCBackOrderFlag(Boolean cBackOrderFlag) {
        this.cBackOrderFlag = cBackOrderFlag;
    }

    @JsonProperty("c_bookType")
    public String getCBookType() {
        return cBookType;
    }

    @JsonProperty("c_bookType")
    public void setCBookType(String cBookType) {
        this.cBookType = cBookType;
    }

    @JsonProperty("c_borderId")
    public String getCBorderId() {
        return cBorderId;
    }

    @JsonProperty("c_borderId")
    public void setCBorderId(String cBorderId) {
        this.cBorderId = cBorderId;
    }

    @JsonProperty("c_characterAndSeries")
    public String getCCharacterAndSeries() {
        return cCharacterAndSeries;
    }

    @JsonProperty("c_characterAndSeries")
    public void setCCharacterAndSeries(String cCharacterAndSeries) {
        this.cCharacterAndSeries = cCharacterAndSeries;
    }

    @JsonProperty("c_classValue")
    public String getCClassValue() {
        return cClassValue;
    }

    @JsonProperty("c_classValue")
    public void setCClassValue(String cClassValue) {
        this.cClassValue = cClassValue;
    }

    @JsonProperty("c_customGrade")
    public String getCCustomGrade() {
        return cCustomGrade;
    }

    @JsonProperty("c_customGrade")
    public void setCCustomGrade(String cCustomGrade) {
        this.cCustomGrade = cCustomGrade;
    }

    @JsonProperty("c_department")
    public String getCDepartment() {
        return cDepartment;
    }

    @JsonProperty("c_department")
    public void setCDepartment(String cDepartment) {
        this.cDepartment = cDepartment;
    }

    @JsonProperty("c_depth")
    public String getCDepth() {
        return cDepth;
    }

    @JsonProperty("c_depth")
    public void setCDepth(String cDepth) {
        this.cDepth = cDepth;
    }

    @JsonProperty("c_displayTitle")
    public String getCDisplayTitle() {
        return cDisplayTitle;
    }

    @JsonProperty("c_displayTitle")
    public void setCDisplayTitle(String cDisplayTitle) {
        this.cDisplayTitle = cDisplayTitle;
    }

    @JsonProperty("c_fictionType")
    public String getCFictionType() {
        return cFictionType;
    }

    @JsonProperty("c_fictionType")
    public void setCFictionType(String cFictionType) {
        this.cFictionType = cFictionType;
    }

    @JsonProperty("c_format")
    public String getCFormat() {
        return cFormat;
    }

    @JsonProperty("c_format")
    public void setCFormat(String cFormat) {
        this.cFormat = cFormat;
    }

    @JsonProperty("c_height")
    public String getCHeight() {
        return cHeight;
    }

    @JsonProperty("c_height")
    public void setCHeight(String cHeight) {
        this.cHeight = cHeight;
    }

    @JsonProperty("c_highAge")
    public Integer getCHighAge() {
        return cHighAge;
    }

    @JsonProperty("c_highAge")
    public void setCHighAge(Integer cHighAge) {
        this.cHighAge = cHighAge;
    }

    @JsonProperty("c_highGradeTSOonly")
    public Integer getCHighGradeTSOonly() {
        return cHighGradeTSOonly;
    }

    @JsonProperty("c_highGradeTSOonly")
    public void setCHighGradeTSOonly(Integer cHighGradeTSOonly) {
        this.cHighGradeTSOonly = cHighGradeTSOonly;
    }

    @JsonProperty("c_highGuidedReadingLevel")
    public String getCHighGuidedReadingLevel() {
        return cHighGuidedReadingLevel;
    }

    @JsonProperty("c_highGuidedReadingLevel")
    public void setCHighGuidedReadingLevel(String cHighGuidedReadingLevel) {
        this.cHighGuidedReadingLevel = cHighGuidedReadingLevel;
    }

    @JsonProperty("c_illustrator")
    public String getCIllustrator() {
        return cIllustrator;
    }

    @JsonProperty("c_illustrator")
    public void setCIllustrator(String cIllustrator) {
        this.cIllustrator = cIllustrator;
    }

    @JsonProperty("c_inventoryThreshold")
    public Integer getCInventoryThreshold() {
        return cInventoryThreshold;
    }

    @JsonProperty("c_inventoryThreshold")
    public void setCInventoryThreshold(Integer cInventoryThreshold) {
        this.cInventoryThreshold = cInventoryThreshold;
    }

    @JsonProperty("c_isbn")
    public String getCIsbn() {
        return cIsbn;
    }

    @JsonProperty("c_isbn")
    public void setCIsbn(String cIsbn) {
        this.cIsbn = cIsbn;
    }

    @JsonProperty("c_isbn13")
    public String getCIsbn13() {
        return cIsbn13;
    }

    @JsonProperty("c_isbn13")
    public void setCIsbn13(String cIsbn13) {
        this.cIsbn13 = cIsbn13;
    }

    @JsonProperty("c_keywords")
    public String getCKeywords() {
        return cKeywords;
    }

    @JsonProperty("c_keywords")
    public void setCKeywords(String cKeywords) {
        this.cKeywords = cKeywords;
    }

    @JsonProperty("c_language")
    public String getCLanguage() {
        return cLanguage;
    }

    @JsonProperty("c_language")
    public void setCLanguage(String cLanguage) {
        this.cLanguage = cLanguage;
    }

    @JsonProperty("c_listPrice")
    public Double getCListPrice() {
        return cListPrice;
    }

    @JsonProperty("c_listPrice")
    public void setCListPrice(Double cListPrice) {
        this.cListPrice = cListPrice;
    }

    @JsonProperty("c_lowAge")
    public Integer getCLowAge() {
        return cLowAge;
    }

    @JsonProperty("c_lowAge")
    public void setCLowAge(Integer cLowAge) {
        this.cLowAge = cLowAge;
    }

    @JsonProperty("c_lowDRALevel")
    public String getCLowDRALevel() {
        return cLowDRALevel;
    }

    @JsonProperty("c_lowDRALevel")
    public void setCLowDRALevel(String cLowDRALevel) {
        this.cLowDRALevel = cLowDRALevel;
    }

    @JsonProperty("c_lowGradeTSOonly")
    public Integer getCLowGradeTSOonly() {
        return cLowGradeTSOonly;
    }

    @JsonProperty("c_lowGradeTSOonly")
    public void setCLowGradeTSOonly(Integer cLowGradeTSOonly) {
        this.cLowGradeTSOonly = cLowGradeTSOonly;
    }

    @JsonProperty("c_lowGuidedReadingLevel")
    public String getCLowGuidedReadingLevel() {
        return cLowGuidedReadingLevel;
    }

    @JsonProperty("c_lowGuidedReadingLevel")
    public void setCLowGuidedReadingLevel(String cLowGuidedReadingLevel) {
        this.cLowGuidedReadingLevel = cLowGuidedReadingLevel;
    }

    @JsonProperty("c_numberofAlternateImages")
    public Integer getCNumberofAlternateImages() {
        return cNumberofAlternateImages;
    }

    @JsonProperty("c_numberofAlternateImages")
    public void setCNumberofAlternateImages(Integer cNumberofAlternateImages) {
        this.cNumberofAlternateImages = cNumberofAlternateImages;
    }

    @JsonProperty("c_otherFormat")
    public List<String> getCOtherFormat() {
        return cOtherFormat;
    }

    @JsonProperty("c_otherFormat")
    public void setCOtherFormat(List<String> cOtherFormat) {
        this.cOtherFormat = cOtherFormat;
    }

    @JsonProperty("c_pages")
    public String getCPages() {
        return cPages;
    }

    @JsonProperty("c_pages")
    public void setCPages(String cPages) {
        this.cPages = cPages;
    }

    @JsonProperty("c_productType")
    public String getCProductType() {
        return cProductType;
    }

    @JsonProperty("c_productType")
    public void setCProductType(String cProductType) {
        this.cProductType = cProductType;
    }

    @JsonProperty("c_publisher")
    public String getCPublisher() {
        return cPublisher;
    }

    @JsonProperty("c_publisher")
    public void setCPublisher(String cPublisher) {
        this.cPublisher = cPublisher;
    }

    @JsonProperty("c_rank")
    public String getCRank() {
        return cRank;
    }

    @JsonProperty("c_rank")
    public void setCRank(String cRank) {
        this.cRank = cRank;
    }

    @JsonProperty("c_searchable")
    public Boolean getCSearchable() {
        return cSearchable;
    }

    @JsonProperty("c_searchable")
    public void setCSearchable(Boolean cSearchable) {
        this.cSearchable = cSearchable;
    }

    @JsonProperty("c_shippingMessagePDP")
    public String getCShippingMessagePDP() {
        return cShippingMessagePDP;
    }

    @JsonProperty("c_shippingMessagePDP")
    public void setCShippingMessagePDP(String cShippingMessagePDP) {
        this.cShippingMessagePDP = cShippingMessagePDP;
    }

    @JsonProperty("c_shops")
    public List<String> getCShops() {
        return cShops;
    }

    @JsonProperty("c_shops")
    public void setCShops(List<String> cShops) {
        this.cShops = cShops;
    }

    @JsonProperty("c_showACR")
    public Boolean getCShowACR() {
        return cShowACR;
    }

    @JsonProperty("c_showACR")
    public void setCShowACR(Boolean cShowACR) {
        this.cShowACR = cShowACR;
    }

    @JsonProperty("c_showDRALevel")
    public Boolean getCShowDRALevel() {
        return cShowDRALevel;
    }

    @JsonProperty("c_showDRALevel")
    public void setCShowDRALevel(Boolean cShowDRALevel) {
        this.cShowDRALevel = cShowDRALevel;
    }

    @JsonProperty("c_showGuidedReadingLevel")
    public Boolean getCShowGuidedReadingLevel() {
        return cShowGuidedReadingLevel;
    }

    @JsonProperty("c_showGuidedReadingLevel")
    public void setCShowGuidedReadingLevel(Boolean cShowGuidedReadingLevel) {
        this.cShowGuidedReadingLevel = cShowGuidedReadingLevel;
    }

    @JsonProperty("c_showLanguage")
    public Boolean getCShowLanguage() {
        return cShowLanguage;
    }

    @JsonProperty("c_showLanguage")
    public void setCShowLanguage(Boolean cShowLanguage) {
        this.cShowLanguage = cShowLanguage;
    }

    @JsonProperty("c_showLexile")
    public Boolean getCShowLexile() {
        return cShowLexile;
    }

    @JsonProperty("c_showLexile")
    public void setCShowLexile(Boolean cShowLexile) {
        this.cShowLexile = cShowLexile;
    }

    @JsonProperty("c_subClass")
    public String getCSubClass() {
        return cSubClass;
    }

    @JsonProperty("c_subClass")
    public void setCSubClass(String cSubClass) {
        this.cSubClass = cSubClass;
    }

    @JsonProperty("c_subject")
    public String getCSubject() {
        return cSubject;
    }

    @JsonProperty("c_subject")
    public void setCSubject(String cSubject) {
        this.cSubject = cSubject;
    }

    @JsonProperty("c_unitCost")
    public Double getCUnitCost() {
        return cUnitCost;
    }

    @JsonProperty("c_unitCost")
    public void setCUnitCost(Double cUnitCost) {
        this.cUnitCost = cUnitCost;
    }

    @JsonProperty("c_vendor1")
    public String getCVendor1() {
        return cVendor1;
    }

    @JsonProperty("c_vendor1")
    public void setCVendor1(String cVendor1) {
        this.cVendor1 = cVendor1;
    }

    @JsonProperty("c_weight")
    public String getCWeight() {
        return cWeight;
    }

    @JsonProperty("c_weight")
    public void setCWeight(String cWeight) {
        this.cWeight = cWeight;
    }

    @JsonProperty("c_width")
    public String getCWidth() {
        return cWidth;
    }

    @JsonProperty("c_width")
    public void setCWidth(String cWidth) {
        this.cWidth = cWidth;
    }

    @JsonProperty("c_workId")
    public String getCWorkId() {
        return cWorkId;
    }

    @JsonProperty("c_workId")
    public void setCWorkId(String cWorkId) {
        this.cWorkId = cWorkId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
