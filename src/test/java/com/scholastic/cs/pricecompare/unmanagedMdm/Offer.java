
package com.scholastic.cs.pricecompare.unmanagedMdm;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Price List Name",
    "Price in USD",
    "Budget Code",
    "Start Date",
    "End Date"
})
public class Offer {

    @JsonProperty("Price List Name")
    private String priceListName;
    @JsonProperty("Price in USD")
    private String priceInUSD;
    @JsonProperty("Budget Code")
    private String budgetCode;
    @JsonProperty("Start Date")
    private String startDate;
    @JsonProperty("End Date")
    private String endDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Price List Name")
    public String getPriceListName() {
        return priceListName;
    }

    @JsonProperty("Price List Name")
    public void setPriceListName(String priceListName) {
        this.priceListName = priceListName;
    }

    @JsonProperty("Price in USD")
    public String getPriceInUSD() {
        return priceInUSD;
    }

    @JsonProperty("Price in USD")
    public void setPriceInUSD(String priceInUSD) {
        this.priceInUSD = priceInUSD;
    }

    @JsonProperty("Budget Code")
    public String getBudgetCode() {
        return budgetCode;
    }

    @JsonProperty("Budget Code")
    public void setBudgetCode(String budgetCode) {
        this.budgetCode = budgetCode;
    }

    @JsonProperty("Start Date")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("Start Date")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("End Date")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("End Date")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
