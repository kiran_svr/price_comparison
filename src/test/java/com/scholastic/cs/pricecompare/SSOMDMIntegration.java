package com.scholastic.cs.pricecompare;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.scholastic.cs.pricecompare.MDMTOJSON.Product;
import com.scholastic.cs.pricecompare.aem.AEM_Data;
import com.scholastic.cs.pricecompare.dw.DWData;
import com.scholastic.cs.pricecompare.unmanagedMdm.UnManagedMDM;

public class SSOMDMIntegration {
	private static final Logger log = Logger.getLogger(SSOMDMIntegration.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("SSPMDMOutput.xlsx").getPath();
	private static final String FILE_NAME_READ = ClassLoader.getSystemClassLoader().getResource("SSOMDMInput.xlsx").getPath();
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) {
		log.log(Level.INFO, "Before the readUrlsFromExcel");
		Map<String, List<String>> mapList = readUrlsFromExcel();
		log.log(Level.INFO, "Map values", mapList);
		generateDataToExcel(mapList);
	}

	private static void generateDataToExcel(Map<String, List<String>> mapList) {
		log.log(Level.INFO, "INTO the generateDataToExcel", mapList);

		try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
			XSSFSheet worksheet = workbook.getSheet("ssoOutput");
			int rowNumber = 1;
			XSSFRow row1 = worksheet.createRow(0);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue("ISBN_NUMBER");
			Cell cell2 = row1.createCell(1);
			cell2.setCellValue("MDM ID");
			Cell cell3 = row1.createCell(2);
			cell3.setCellValue("AEM COST PRICES");
			Cell cell4 = row1.createCell(3);
			cell4.setCellValue("MDM LIST PRICES");
			Cell cell5 = row1.createCell(4);
			cell5.setCellValue("DW LISTPRICES");
			Cell cell6 = row1.createCell(5);
			cell6.setCellValue("MDMUnmanaged LISTPRICES");
			for (Map.Entry<String, List<String>> entry : mapList.entrySet()) {
				log.info("Item : " + entry.getKey() + " Count : " + entry.getValue());
				List<String> stringList = entry.getValue();
				XSSFRow row = worksheet.createRow(rowNumber);
				Cell cell = row.createCell(0);
				cell.setCellValue(entry.getKey());
				for (int j = 1; j < entry.getValue().size() + 1; j++) {

					cell = row.createCell(j);
					cell.setCellValue(stringList.get(j - 1));
					
				}
				rowNumber++;

			}
			// Save the workbook in .xls file
			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);
					){
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
			}
			// Save the workbook in .xls file
			
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

		}

	}
	private static MultiValueMap<String, String> getHeaders() {
		log.log(Level.INFO, "into the  getHeaders");

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");
		headers.add("Authorization", "Bearer " + "8b802a4f8fe513a6501f2abdab448275");
		return headers;
	}
	private static Map<String, List<String>> readUrlsFromExcel() {
		log.log(Level.INFO, "into the  readUrlsFromExcel");

		Map<String, List<String>> mapList = new HashMap<>();
		System.out.println(ClassLoader.getSystemClassLoader().getResource("").getPath());
		try (FileInputStream excelFile = new FileInputStream(FILE_NAME_READ);
				
				XSSFWorkbook workbook = new XSSFWorkbook(excelFile);) {
			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				String lastTwo = null;

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					if (!cell.getStringCellValue().isEmpty()) {
						String cellValue = cell.getStringCellValue();
						log.info(cellValue);
						if (cellValue != null && cellValue.length() >= 2) {
							lastTwo = cellValue.substring(cellValue.length() - 2);
						}

						mapList.put(cellValue, retreiveDataFromURLToCompare(cellValue, lastTwo));

					} else {
						log.log(Level.INFO, "into the  readUrlsFromExcel check whether the cell type is"
							);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.INFO, "into the exception in the readURL", e.fillInStackTrace());
		}
		return mapList;
	}
	private static List<String> retreiveDataFromURLToCompare(String cellValue, String lastTwo) {
		lastTwo = trimLeadingZeroes(lastTwo);

		
		// QA Unmanaged MDM (Boomerang API)
		String urlUnManagedMDM = "https://nonprod.api.scholastic.com/qa/ondemand/2.1/aso/" + cellValue +"sso-us?StoreID=sso-us" ;
		//https://nonprod.api.scholastic.com/qa/ondemand/2.1/aso/9780545464789sso-us?StoreID=sso-us
		//AEM 
		String urlOne = "https://www-qa2.scholastic.com/content/scholastic/books/aso/sso/" + lastTwo + "/" + cellValue
				+ "/data.json";
		
		
		
		
		// QA MDM Managed MDM API (STEP)
		String urlTwo = "https://nonprod.api.scholastic.com/qa/ondemand/2.1/product/2.1/isbn13/"+ cellValue+"?channel=scholasticStore";
		
		//QA Demandware
		String urlThree = "https://development-rco-scholastic.demandware.net/s/Sites-tso-us-Site/dw/shop/v16_9/products/"
				+ cellValue + "-sso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8";
		
	//	https://development-rco-scholastic.demandware.net/s/Sites-sso-us-Site/dw/shop/v16_9/products/9780375849916-sso-us/prices?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8
	

		List<String> list = new ArrayList<>();
		String splitData = getReponseEntity(urlOne);
		String[] zee = splitData.split(";");
		list.add(zee[0]);
		list.add(zee[1]);
		list.add(zee[2]);
		list.add(zee[3]);
		list.add(zee[4]);
		list.add(zee[5]);
		list.add(zee[6]);
		list.add(zee[7]);
		list.add(zee[8]);
		list.add(zee[9]);
		list.add(zee[10]);
		list.add(zee[11]);
		list.add(zee[12]);
		list.add(zee[13]);
		list.add(zee[14]);
		list.add(zee[15]);
		list.add(zee[16]);
		list.add(zee[17]);
		list.add(zee[18]);
		list.add(zee[19]);
		list.add(zee[20]);
		list.add(zee[21]);
		list.add(zee[22]);
		list.add(zee[23]);
		list.add(zee[24]);
		list.add(zee[25]);
		list.add(zee[26]);
		list.add(zee[27]);
		list.add(zee[28]);
		list.add(zee[29]);
		list.add(zee[30]);
		list.add(zee[31]);
		list.add(zee[32]);
		list.add(zee[33]);
		list.add(zee[34]);
		list.add(zee[35]);
		list.add(zee[36]);
		list.add(zee[37]);
		list.add(zee[38]);
		list.add(zee[39]);
		list.add(zee[40]);
		list.add(zee[41]);
		list.add(zee[42]);
		list.add(zee[43]);
		list.add(zee[44]);
		list.add(zee[45]);
		list.add(zee[46]);
		list.add(zee[47]);
		list.add(zee[48]);
		list.add(zee[49]);
		list.add(zee[50]);
		list.add(zee[51]);
		list.add(zee[52]);
		list.add(zee[53]);
		list.add(zee[54]);
		list.add(zee[55]);
		list.add(zee[56]);
		list.add(zee[57]);
		list.add(zee[58]);
		list.add(zee[59]);
		list.add(zee[60]);
		list.add(zee[61]);
		list.add(zee[62]);
		list.add(zee[63]);
		list.add(zee[64]);
		list.add(zee[65]);
		list.add(zee[66]);
		list.add(zee[67]);
		list.add(zee[68]);
		list.add(zee[69]);
		list.add(zee[70]);
		list.add(zee[71]);
		list.add(zee[72]);
		list.add(zee[73]);
		list.add(zee[74]);
		list.add(zee[75]);
		list.add(zee[76]);
		list.add(zee[77]);
		list.add(zee[78]);

		//list.add(getReponseSecondEntity(urlTwo));
		//list.add(getReponseThirdEntity(urlThree));
		//list.add(getReponseUnManagedMDM(urlUnManagedMDM));
		log.log(Level.INFO, "get the list of data ", list);

		return list;
	}
	private static String getReponseEntity(String url) {
		log.log(Level.INFO, url, "URL in THE RESPONSE");
		log.info( "URL in THE RESPONSE error---"+ url);
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		ResponseEntity<AEM_Data> exdate = null;
		try {
			exdate = template.exchange(url, HttpMethod.GET, null, AEM_Data.class);
			//exdate.getBody().getListPrice().isEmpty()? "EMPTY" :exdate.getBody().getListPrice()
			if (exdate.getStatusCode().equals(HttpStatus.OK)) {
				jsonValue.append(exdate.getBody().getProductID()).append(";").
				append(exdate.getBody().getListPrice().isEmpty()? "EMPTY" :exdate.getBody().getListPrice()).append(";").
				append(exdate.getBody().getHighAge().isEmpty()? "EMPTY" :exdate.getBody().getHighAge()).append(";").
				append(exdate.getBody().getLowAge().isEmpty()? "EMPTY" :exdate.getBody().getLowAge()).append(";").
				append(exdate.getBody().getAvailabilityDate().isEmpty()? "EMPTY" :exdate.getBody().getAvailabilityDate()).append(";").
				append(exdate.getBody().getAwardList().isEmpty()? "EMPTY" :exdate.getBody().getAwardList()).append(";").
				//append(exdate.getBody().getBackOrderFlag().isEmpty()? "EMPTY" :exdate.getBody().getBackOrderFlag()).append(";").
				//append(exdate.getBody().getBookType().isEmpty()? "EMPTY" :exdate.getBody().getBookType()).append(";").
				append(exdate.getBody().getBorderID().isEmpty()? "EMPTY" :exdate.getBody().getBorderID()).append(";").
				append(exdate.getBody().getBrand().isEmpty()? "EMPTY" :exdate.getBody().getBrand()).append(";").
				append(exdate.getBody().getBudgetCode().isEmpty()? "EMPTY" :exdate.getBody().getBudgetCode()).append(";").
				//append(exdate.getBody().getSSOClass().isEmpty()? "EMPTY" :exdate.getBody().getSSOClass()).append(";").
				//append(exdate.getBody().getSubClass().isEmpty()? "EMPTY" :exdate.getBody().getSubClass()).append(";").
				append(exdate.getBody().getCollectionTitles().isEmpty()? "EMPTY" :exdate.getBody().getCollectionTitles()).append(";").
				append(exdate.getBody().getComponentListing().isEmpty()? "EMPTY" :exdate.getBody().getComponentListing()).append(";").
				append(exdate.getBody().getAuthor().isEmpty()? "EMPTY" :exdate.getBody().getAuthor()).append(";").
				append(exdate.getBody().getEditor().isEmpty()? "EMPTY" :exdate.getBody().getEditor()).append(";").
				append(exdate.getBody().getIllustrator().isEmpty()? "EMPTY" :exdate.getBody().getIllustrator()).append(";").
				append(exdate.getBody().getNarrator().isEmpty()? "EMPTY" :exdate.getBody().getNarrator()).append(";").
				append(exdate.getBody().getCrossSellISBNs().isEmpty()? "EMPTY" :exdate.getBody().getCrossSellISBNs()).append(";").
				append(exdate.getBody().getCustomGrade().isEmpty()? "EMPTY" :exdate.getBody().getCustomGrade()).append(";").
				//append(exdate.getBody().getDepartment().isEmpty()? "EMPTY" :exdate.getBody().getDepartment()).append(";").
				append(exdate.getBody().getLongDescription().isEmpty()? "EMPTY" :exdate.getBody().getLongDescription()).append(";").
				append(exdate.getBody().getDisplayTitle().isEmpty()? "EMPTY" :exdate.getBody().getDisplayTitle()).append(";").
				append(exdate.getBody().getExtraLargeImage().isEmpty()? "EMPTY" :exdate.getBody().getExtraLargeImage()).append(";").
				append(exdate.getBody().getExtraSmallImage().isEmpty()? "EMPTY" :exdate.getBody().getExtraSmallImage()).append(";").
				append(exdate.getBody().getFictionType().isEmpty()? "EMPTY" :exdate.getBody().getFictionType()).append(";").
				//append(exdate.getBody().getSSOFormat().isEmpty()? "EMPTY" :exdate.getBody().getSSOFormat()).append(";").
				append(exdate.getBody().getFormatDescription().isEmpty()? "EMPTY" :exdate.getBody().getFormatDescription()).append(";").
				//append(exdate.getBody().get().isEmpty()? "EMPTY" :exdate.getBody().get()).append(";").
				append(exdate.getBody().getGenre().isEmpty()? "EMPTY" :exdate.getBody().getGenre()).append(";").
				append(exdate.getBody().getHighGrade().isEmpty()? "EMPTY" :exdate.getBody().getHighGrade()).append(";").
				append(exdate.getBody().getLowGrade().isEmpty()? "EMPTY" :exdate.getBody().getLowGrade()).append(";").
				append(exdate.getBody().getISBN().isEmpty()? "EMPTY" :exdate.getBody().getISBN()).append(";").
				append(exdate.getBody().getISBN13().isEmpty()? "EMPTY" :exdate.getBody().getISBN13()).append(";").
				append(exdate.getBody().getWorkID().isEmpty()? "EMPTY" :exdate.getBody().getWorkID()).append(";").
				append(exdate.getBody().getInventoryThreshold().isEmpty()? "EMPTY" :exdate.getBody().getInventoryThreshold()).append(";").
				append(exdate.getBody().getKeywords().isEmpty()? "EMPTY" :exdate.getBody().getKeywords()).append(";").
				append(exdate.getBody().getLanguage().isEmpty()? "EMPTY" :exdate.getBody().getLanguage()).append(";").
				append(exdate.getBody().getLargeImage().isEmpty()? "EMPTY" :exdate.getBody().getLargeImage()).append(";").
				append(exdate.getBody().getHighAcceleratedReader().isEmpty()? "EMPTY" :exdate.getBody().getHighAcceleratedReader()).append(";").
				append(exdate.getBody().getLowAcceleratedReader().isEmpty()? "EMPTY" :exdate.getBody().getLowAcceleratedReader()).append(";").
				append(exdate.getBody().getHighDRALevel().isEmpty()? "EMPTY" :exdate.getBody().getHighDRALevel()).append(";").
				append(exdate.getBody().getLowDRALevel().isEmpty()? "EMPTY" :exdate.getBody().getLowDRALevel()).append(";").
				append(exdate.getBody().getHighGuidedReadingLevel().isEmpty()? "EMPTY" :exdate.getBody().getHighGuidedReadingLevel()).append(";").
				append(exdate.getBody().getLowGuidedReadingLevel().isEmpty()? "EMPTY" :exdate.getBody().getLowGuidedReadingLevel()).append(";").
				append(exdate.getBody().getHighLexileLevel().isEmpty()? "EMPTY" :exdate.getBody().getHighLexileLevel()).append(";").
				append(exdate.getBody().getLowLexileLevel().isEmpty()? "EMPTY" :exdate.getBody().getLowLexileLevel()).append(";").
				//append(exdate.getBody().getHighScholasticLevel().isEmpty()? "EMPTY" :exdate.getBody().getHighScholasticLevel()).append(";").
				//append(exdate.getBody().getLowScholasticLevel().isEmpty()? "EMPTY" :exdate.getBody().getLowScholasticLevel()).append(";").
				//append(exdate.getBody().getManufacturer().isEmpty()? "EMPTY" :exdate.getBody().getManufacturer()).append(";").
				append(exdate.getBody().getManufacturerPartNumber().isEmpty()? "EMPTY" :exdate.getBody().getManufacturerPartNumber()).append(";").
				append(exdate.getBody().getMultimedia().isEmpty()? "EMPTY" :exdate.getBody().getMultimedia()).append(";").
				append(exdate.getBody().getNumberofAlternateImages().isEmpty()? "EMPTY" :exdate.getBody().getNumberofAlternateImages()).append(";").
				append(exdate.getBody().getNumberofSeeInsidePages().isEmpty()? "EMPTY" :exdate.getBody().getNumberofSeeInsidePages()).append(";").
				append(exdate.getBody().getOtherformat().isEmpty()? "EMPTY" :exdate.getBody().getOtherformat()).append(";").
				append(exdate.getBody().getOurPrice().isEmpty()? "EMPTY" :exdate.getBody().getOurPrice()).append(";").
				append(exdate.getBody().getPages().isEmpty()? "EMPTY" :exdate.getBody().getPages()).append(";").
				append(exdate.getBody().getListPrice().isEmpty()? "EMPTY" :exdate.getBody().getListPrice()).append(";").
				append(exdate.getBody().getProductEmail().isEmpty()? "EMPTY" :exdate.getBody().getProductEmail()).append(";").
				append(exdate.getBody().getProductEndDate().isEmpty()? "EMPTY" :exdate.getBody().getProductEndDate()).append(";").
				append(exdate.getBody().getProductStartDate().isEmpty()? "EMPTY" :exdate.getBody().getProductStartDate()).append(";").
				append(exdate.getBody().getProductType().isEmpty()? "EMPTY" :exdate.getBody().getProductType()).append(";").
				append(exdate.getBody().getPublisher().isEmpty()? "EMPTY" :exdate.getBody().getPublisher()).append(";").
				append(exdate.getBody().getRank().isEmpty()? "EMPTY" :exdate.getBody().getRank()).append(";").
				append(exdate.getBody().getReviews().isEmpty()? "EMPTY" :exdate.getBody().getReviews()).append(";").
				append(exdate.getBody().getSaleEndDate().isEmpty()? "EMPTY" :exdate.getBody().getSaleEndDate()).append(";").
				append(exdate.getBody().getSaleMessage().isEmpty()? "EMPTY" :exdate.getBody().getSaleMessage()).append(";").
				append(exdate.getBody().getSalePrice().isEmpty()? "EMPTY" :exdate.getBody().getSalePrice()).append(";").
				append(exdate.getBody().getSaleStartDate().isEmpty()? "EMPTY" :exdate.getBody().getSaleStartDate()).append(";").
				append(exdate.getBody().getSearchable().isEmpty()? "EMPTY" :exdate.getBody().getSearchable()).append(";").
				append(exdate.getBody().getSeeInsideDescription().isEmpty()? "EMPTY" :exdate.getBody().getSeeInsideDescription()).append(";").
				append(exdate.getBody().getCharactersandSeries().isEmpty()? "EMPTY" :exdate.getBody().getCharactersandSeries()).append(";").
				//append(exdate.getBody().getShippingMessagePDP().isEmpty()? "EMPTY" :exdate.getBody().getShippingMessagePDP()).append(";").
				append(exdate.getBody().getShops().isEmpty()? "EMPTY" :exdate.getBody().getShops()).append(";").
				append(exdate.getBody().getShowACR().isEmpty()? "EMPTY" :exdate.getBody().getShowACR()).append(";").
				append(exdate.getBody().getShowDRALevel().isEmpty()? "EMPTY" :exdate.getBody().getShowDRALevel()).append(";").
				append(exdate.getBody().getShowGuidedReadingLevel().isEmpty()? "EMPTY" :exdate.getBody().getShowGuidedReadingLevel()).append(";").
				append(exdate.getBody().getShowLanguage().isEmpty()? "EMPTY" :exdate.getBody().getShowLanguage()).append(";").
				append(exdate.getBody().getShowLexile().isEmpty()? "EMPTY" :exdate.getBody().getShowLexile()).append(";").
				append(exdate.getBody().getSmallImage().isEmpty()? "EMPTY" :exdate.getBody().getSmallImage()).append(";").
				append(exdate.getBody().getSpecialNote().isEmpty()? "EMPTY" :exdate.getBody().getSpecialNote()).append(";").
				//append(exdate.getBody().getStandardRating().isEmpty()? "EMPTY" :exdate.getBody().getStandardRating()).append(";").
				append(exdate.getBody().getSubject().isEmpty()? "EMPTY" :exdate.getBody().getSubject()).append(";").
				append(exdate.getBody().getTitle().isEmpty()? "EMPTY" :exdate.getBody().getTitle()).append(";").
				append(exdate.getBody().getDepth().isEmpty()? "EMPTY" :exdate.getBody().getDepth()).append(";").
				append(exdate.getBody().getHeight().isEmpty()? "EMPTY" :exdate.getBody().getHeight()).append(";").
				append(exdate.getBody().getWidth().isEmpty()? "EMPTY" :exdate.getBody().getWidth()).append(";").
				append(exdate.getBody().getUnitCost().isEmpty()? "EMPTY" :exdate.getBody().getUnitCost()).append(";").
				//append(exdate.getBody().getUPC().isEmpty()? "EMPTY" :exdate.getBody().getUPC()).append(";").
				append(exdate.getBody().getVendor1().isEmpty()? "EMPTY" :exdate.getBody().getVendor1()).append(";").
				append(exdate.getBody().getViewableFlag().isEmpty()? "EMPTY" :exdate.getBody().getViewableFlag()).append(";").
				//append(exdate.getBody().getWarningList().isEmpty()? "EMPTY" :exdate.getBody().getWarningList()).append(";").
				append(exdate.getBody().getWeight().isEmpty()? "EMPTY" :exdate.getBody().getWeight()).append(";");
			} else {
				jsonValue.append(EMPTY_STRING).append(";").append(EMPTY_STRING);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonValue.append(EMPTY_STRING).append(";").append(EMPTY_STRING);
			// jsonValue.append(exdate.getBody().getProductID()).append(";").append(EMPTY_STRING);
			log.log(Level.INFO, "jsonValue in exception of responseSecondEntity" + jsonValue);
		}
		log.log(Level.INFO, jsonValue.toString());
		return jsonValue.toString();
	}

	private static String trimLeadingZeroes(String value) {
		while (value.length() > 1 && value.indexOf('0') == 0) {
			value = value.substring(1);
		}
		return value;
	}

	private static String getReponseSecondEntity(String url) {
		log.log(Level.INFO, "INTO getReponseSecondEntity", url);

		String jsonValue;
		RestTemplate template = new RestTemplate();

		try {
			ResponseEntity<Product> exdate = template.exchange(url, HttpMethod.GET,
					new HttpEntity<String>(getHeaders()), Product.class);

			if (exdate.getBody().getProduct().getData().getCorpListPrice() != null
					&& exdate.getStatusCode().equals(HttpStatus.OK)) {
				jsonValue = exdate.getBody().getProduct().getData().getCorpListPrice();
			} else {
				jsonValue = EMPTY_STRING;
			}
		} catch (Exception e) {
			e.printStackTrace();

			jsonValue = EMPTY_STRING;
			log.log(Level.INFO, jsonValue + "in exception");
		}
		log.log(Level.INFO, jsonValue);

		return jsonValue;
	}
	
	private static String getReponseThirdEntity(String url) {
		log.log(Level.INFO, "INTO getReponseSecondEntity", url);
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		try {
			ResponseEntity<DWData> exdate = template.exchange(url, HttpMethod.GET, null, DWData.class);
			if (exdate.getBody().getCListPrice() != null && exdate.getStatusCode().equals(HttpStatus.OK)) {
				jsonValue.append(String.valueOf(exdate.getBody().getCListPrice()));
			} else {
				jsonValue.append(EMPTY_STRING);
			}
		} catch (Exception e) {
			jsonValue.append(EMPTY_STRING);
			log.log(Level.INFO, jsonValue + "in exception");
		}
		return jsonValue.toString();
	}
	
	private static String getReponseUnManagedMDM(String url) {
		System.out.println(url);
		log.log(Level.INFO, "INTO getReponseSecondEntity", url);
		
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		try {
			ResponseEntity<UnManagedMDM> exdate = template.exchange(url, HttpMethod.GET, new HttpEntity<String>(getHeaders()), UnManagedMDM.class);
			if (exdate.getBody().getRecord().getData().getListPrice() != null && exdate.getStatusCode().equals(HttpStatus.OK)) {
				System.out.println(exdate.getBody().getRecord().getData().getListPrice());
				jsonValue.append(String.valueOf(exdate.getBody().getRecord().getData().getListPrice()));
			} else {
				jsonValue.append(EMPTY_STRING);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonValue.append(EMPTY_STRING);
			log.log(Level.INFO, jsonValue + "in exception");
		}
		return jsonValue.toString();
	}
	
	private static MultiValueMap<String, String> getUnManagedMDM() {
		log.log(Level.INFO, "into the  getHeaders");

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");
		headers.add("Authorization", "Bearer " + "8b802a4f8fe513a6501f2abdab448275");
		return headers;
	}
}
