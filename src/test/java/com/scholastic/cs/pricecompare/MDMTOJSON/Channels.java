
package com.scholastic.cs.pricecompare.MDMTOJSON;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "scholasticStore"
})
public class Channels {

    @JsonProperty("scholasticStore")
    private ScholasticStore scholasticStore;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("scholasticStore")
    public ScholasticStore getScholasticStore() {
        return scholasticStore;
    }

    @JsonProperty("scholasticStore")
    public void setScholasticStore(ScholasticStore scholasticStore) {
        this.scholasticStore = scholasticStore;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
