
package com.scholastic.cs.pricecompare.MDMTOJSON;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "propertyCode",
    "propertyTitle"
})
public class WEPProperty {

    @JsonProperty("id")
    private String id;
    @JsonProperty("propertyCode")
    private String propertyCode;
    @JsonProperty("propertyTitle")
    private String propertyTitle;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("propertyCode")
    public String getPropertyCode() {
        return propertyCode;
    }

    @JsonProperty("propertyCode")
    public void setPropertyCode(String propertyCode) {
        this.propertyCode = propertyCode;
    }

    @JsonProperty("propertyTitle")
    public String getPropertyTitle() {
        return propertyTitle;
    }

    @JsonProperty("propertyTitle")
    public void setPropertyTitle(String propertyTitle) {
        this.propertyTitle = propertyTitle;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
