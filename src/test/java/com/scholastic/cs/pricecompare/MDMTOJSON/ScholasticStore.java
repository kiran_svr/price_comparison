
package com.scholastic.cs.pricecompare.MDMTOJSON;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "scholasticStoreSSOFormat",
    "scholasticStoreDepartment",
    "scholasticStoreSSOClass",
    "scholasticStoreSubClass",
    "scholasticStoreShops",
    "scholasticStoreOurPrice",
    "scholasticStoreListPrice",
    "scholasticStoreViewableFlag",
    "scholasticStoreSearchableFlag",
    "scholasticStoreInventoryThreshold",
    "scholasticStoreBackOrderFlag",
    "scholasticStoreBudgetCode",
    "scholasticStorePublisher",
    "scholasticStoreKeywords",
    "scholasticStoreBrand",
    "scholasticStoreCharacterAndSeries",
    "scholasticStoreSubjects",
    "scholasticStoreBookType",
    "scholasticStoreAgeHigh",
    "scholasticStoreAgeLow",
    "scholasticStoreGradeHigh",
    "scholasticStoreGradeLow",
    "scholasticStoreUnitCost",
    "scholasticStoreWorkID",
    "scholasticStoreManufacturer",
    "scholasticStoreProductType",
    "scholasticStoreDisplayTitle",
    "scholasticStoreFormat",
    "scholasticStoreProductStartDate",
    "scholasticStoreOtherFormat",
    "scholasticStoreNumberOfAlternateImages",
    "scholasticStoreShippingMessagePDP",
    "scholasticStoreTitleOwner",
    "scholasticStoreLongDescriptionOwner",
    "scholasticStoreTitleByOwner",
    "scholasticStoreLongDescriptionByOwner"
})
public class ScholasticStore {

    @JsonProperty("scholasticStoreSSOFormat")
    private String scholasticStoreSSOFormat;
    @JsonProperty("scholasticStoreDepartment")
    private String scholasticStoreDepartment;
    @JsonProperty("scholasticStoreSSOClass")
    private String scholasticStoreSSOClass;
    @JsonProperty("scholasticStoreSubClass")
    private String scholasticStoreSubClass;
    @JsonProperty("scholasticStoreShops")
    private List<String> scholasticStoreShops = null;
    @JsonProperty("scholasticStoreOurPrice")
    private String scholasticStoreOurPrice;
    @JsonProperty("scholasticStoreListPrice")
    private String scholasticStoreListPrice;
    @JsonProperty("scholasticStoreViewableFlag")
    private String scholasticStoreViewableFlag;
    @JsonProperty("scholasticStoreSearchableFlag")
    private String scholasticStoreSearchableFlag;
    @JsonProperty("scholasticStoreInventoryThreshold")
    private String scholasticStoreInventoryThreshold;
    @JsonProperty("scholasticStoreBackOrderFlag")
    private String scholasticStoreBackOrderFlag;
    @JsonProperty("scholasticStoreBudgetCode")
    private String scholasticStoreBudgetCode;
    @JsonProperty("scholasticStorePublisher")
    private String scholasticStorePublisher;
    @JsonProperty("scholasticStoreKeywords")
    private List<String> scholasticStoreKeywords = null;
    @JsonProperty("scholasticStoreBrand")
    private String scholasticStoreBrand;
    @JsonProperty("scholasticStoreCharacterAndSeries")
    private String scholasticStoreCharacterAndSeries;
    @JsonProperty("scholasticStoreSubjects")
    private List<String> scholasticStoreSubjects = null;
    @JsonProperty("scholasticStoreBookType")
    private String scholasticStoreBookType;
    @JsonProperty("scholasticStoreAgeHigh")
    private String scholasticStoreAgeHigh;
    @JsonProperty("scholasticStoreAgeLow")
    private String scholasticStoreAgeLow;
    @JsonProperty("scholasticStoreGradeHigh")
    private String scholasticStoreGradeHigh;
    @JsonProperty("scholasticStoreGradeLow")
    private String scholasticStoreGradeLow;
    @JsonProperty("scholasticStoreUnitCost")
    private String scholasticStoreUnitCost;
    @JsonProperty("scholasticStoreWorkID")
    private String scholasticStoreWorkID;
    @JsonProperty("scholasticStoreManufacturer")
    private String scholasticStoreManufacturer;
    @JsonProperty("scholasticStoreProductType")
    private String scholasticStoreProductType;
    @JsonProperty("scholasticStoreDisplayTitle")
    private String scholasticStoreDisplayTitle;
    @JsonProperty("scholasticStoreFormat")
    private String scholasticStoreFormat;
    @JsonProperty("scholasticStoreProductStartDate")
    private String scholasticStoreProductStartDate;
    @JsonProperty("scholasticStoreOtherFormat")
    private List<String> scholasticStoreOtherFormat = null;
    @JsonProperty("scholasticStoreNumberOfAlternateImages")
    private String scholasticStoreNumberOfAlternateImages;
    @JsonProperty("scholasticStoreShippingMessagePDP")
    private String scholasticStoreShippingMessagePDP;
    @JsonProperty("scholasticStoreTitleOwner")
    private String scholasticStoreTitleOwner;
    @JsonProperty("scholasticStoreLongDescriptionOwner")
    private String scholasticStoreLongDescriptionOwner;
    @JsonProperty("scholasticStoreTitleByOwner")
    private String scholasticStoreTitleByOwner;
    @JsonProperty("scholasticStoreLongDescriptionByOwner")
    private String scholasticStoreLongDescriptionByOwner;
    
    
    @JsonProperty("scholasticStoreTarget")
    private List<String> scholasticStoreTarget = null;
    

	@JsonProperty("scholasticStoreNumberOfSeeInsidePages")
    private String scholasticStoreNumberOfSeeInsidePages;
    @JsonProperty("scholasticStoreVendor1")
    private String scholasticStoreVendor1;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("scholasticStoreSSOFormat")
    public String getScholasticStoreSSOFormat() {
        return scholasticStoreSSOFormat;
    }

    @JsonProperty("scholasticStoreSSOFormat")
    public void setScholasticStoreSSOFormat(String scholasticStoreSSOFormat) {
        this.scholasticStoreSSOFormat = scholasticStoreSSOFormat;
    }

    @JsonProperty("scholasticStoreDepartment")
    public String getScholasticStoreDepartment() {
        return scholasticStoreDepartment;
    }

    @JsonProperty("scholasticStoreDepartment")
    public void setScholasticStoreDepartment(String scholasticStoreDepartment) {
        this.scholasticStoreDepartment = scholasticStoreDepartment;
    }

    @JsonProperty("scholasticStoreSSOClass")
    public String getScholasticStoreSSOClass() {
        return scholasticStoreSSOClass;
    }

    @JsonProperty("scholasticStoreSSOClass")
    public void setScholasticStoreSSOClass(String scholasticStoreSSOClass) {
        this.scholasticStoreSSOClass = scholasticStoreSSOClass;
    }

    @JsonProperty("scholasticStoreSubClass")
    public String getScholasticStoreSubClass() {
        return scholasticStoreSubClass;
    }

    @JsonProperty("scholasticStoreSubClass")
    public void setScholasticStoreSubClass(String scholasticStoreSubClass) {
        this.scholasticStoreSubClass = scholasticStoreSubClass;
    }

    @JsonProperty("scholasticStoreShops")
    public List<String> getScholasticStoreShops() {
        return scholasticStoreShops;
    }

    @JsonProperty("scholasticStoreShops")
    public void setScholasticStoreShops(List<String> scholasticStoreShops) {
        this.scholasticStoreShops = scholasticStoreShops;
    }

    @JsonProperty("scholasticStoreOurPrice")
    public String getScholasticStoreOurPrice() {
        return scholasticStoreOurPrice;
    }

    @JsonProperty("scholasticStoreOurPrice")
    public void setScholasticStoreOurPrice(String scholasticStoreOurPrice) {
        this.scholasticStoreOurPrice = scholasticStoreOurPrice;
    }

    @JsonProperty("scholasticStoreListPrice")
    public String getScholasticStoreListPrice() {
        return scholasticStoreListPrice;
    }

    @JsonProperty("scholasticStoreListPrice")
    public void setScholasticStoreListPrice(String scholasticStoreListPrice) {
        this.scholasticStoreListPrice = scholasticStoreListPrice;
    }

    @JsonProperty("scholasticStoreViewableFlag")
    public String getScholasticStoreViewableFlag() {
        return scholasticStoreViewableFlag;
    }

    @JsonProperty("scholasticStoreViewableFlag")
    public void setScholasticStoreViewableFlag(String scholasticStoreViewableFlag) {
        this.scholasticStoreViewableFlag = scholasticStoreViewableFlag;
    }

    @JsonProperty("scholasticStoreSearchableFlag")
    public String getScholasticStoreSearchableFlag() {
        return scholasticStoreSearchableFlag;
    }

    @JsonProperty("scholasticStoreSearchableFlag")
    public void setScholasticStoreSearchableFlag(String scholasticStoreSearchableFlag) {
        this.scholasticStoreSearchableFlag = scholasticStoreSearchableFlag;
    }

    @JsonProperty("scholasticStoreInventoryThreshold")
    public String getScholasticStoreInventoryThreshold() {
        return scholasticStoreInventoryThreshold;
    }

    @JsonProperty("scholasticStoreInventoryThreshold")
    public void setScholasticStoreInventoryThreshold(String scholasticStoreInventoryThreshold) {
        this.scholasticStoreInventoryThreshold = scholasticStoreInventoryThreshold;
    }

    @JsonProperty("scholasticStoreBackOrderFlag")
    public String getScholasticStoreBackOrderFlag() {
        return scholasticStoreBackOrderFlag;
    }

    @JsonProperty("scholasticStoreBackOrderFlag")
    public void setScholasticStoreBackOrderFlag(String scholasticStoreBackOrderFlag) {
        this.scholasticStoreBackOrderFlag = scholasticStoreBackOrderFlag;
    }

    @JsonProperty("scholasticStoreBudgetCode")
    public String getScholasticStoreBudgetCode() {
        return scholasticStoreBudgetCode;
    }

    @JsonProperty("scholasticStoreBudgetCode")
    public void setScholasticStoreBudgetCode(String scholasticStoreBudgetCode) {
        this.scholasticStoreBudgetCode = scholasticStoreBudgetCode;
    }

    @JsonProperty("scholasticStorePublisher")
    public String getScholasticStorePublisher() {
        return scholasticStorePublisher;
    }

    @JsonProperty("scholasticStorePublisher")
    public void setScholasticStorePublisher(String scholasticStorePublisher) {
        this.scholasticStorePublisher = scholasticStorePublisher;
    }

    @JsonProperty("scholasticStoreKeywords")
    public List<String> getScholasticStoreKeywords() {
        return scholasticStoreKeywords;
    }

    @JsonProperty("scholasticStoreKeywords")
    public void setScholasticStoreKeywords(List<String> scholasticStoreKeywords) {
        this.scholasticStoreKeywords = scholasticStoreKeywords;
    }

    @JsonProperty("scholasticStoreBrand")
    public String getScholasticStoreBrand() {
        return scholasticStoreBrand;
    }

    @JsonProperty("scholasticStoreBrand")
    public void setScholasticStoreBrand(String scholasticStoreBrand) {
        this.scholasticStoreBrand = scholasticStoreBrand;
    }

    @JsonProperty("scholasticStoreCharacterAndSeries")
    public String getScholasticStoreCharacterAndSeries() {
        return scholasticStoreCharacterAndSeries;
    }

    @JsonProperty("scholasticStoreCharacterAndSeries")
    public void setScholasticStoreCharacterAndSeries(String scholasticStoreCharacterAndSeries) {
        this.scholasticStoreCharacterAndSeries = scholasticStoreCharacterAndSeries;
    }

    @JsonProperty("scholasticStoreSubjects")
    public List<String> getScholasticStoreSubjects() {
        return scholasticStoreSubjects;
    }

    @JsonProperty("scholasticStoreSubjects")
    public void setScholasticStoreSubjects(List<String> scholasticStoreSubjects) {
        this.scholasticStoreSubjects = scholasticStoreSubjects;
    }

    @JsonProperty("scholasticStoreBookType")
    public String getScholasticStoreBookType() {
        return scholasticStoreBookType;
    }

    @JsonProperty("scholasticStoreBookType")
    public void setScholasticStoreBookType(String scholasticStoreBookType) {
        this.scholasticStoreBookType = scholasticStoreBookType;
    }

    @JsonProperty("scholasticStoreAgeHigh")
    public String getScholasticStoreAgeHigh() {
        return scholasticStoreAgeHigh;
    }

    @JsonProperty("scholasticStoreAgeHigh")
    public void setScholasticStoreAgeHigh(String scholasticStoreAgeHigh) {
        this.scholasticStoreAgeHigh = scholasticStoreAgeHigh;
    }

    @JsonProperty("scholasticStoreAgeLow")
    public String getScholasticStoreAgeLow() {
        return scholasticStoreAgeLow;
    }

    @JsonProperty("scholasticStoreAgeLow")
    public void setScholasticStoreAgeLow(String scholasticStoreAgeLow) {
        this.scholasticStoreAgeLow = scholasticStoreAgeLow;
    }

    @JsonProperty("scholasticStoreGradeHigh")
    public String getScholasticStoreGradeHigh() {
        return scholasticStoreGradeHigh;
    }

    @JsonProperty("scholasticStoreGradeHigh")
    public void setScholasticStoreGradeHigh(String scholasticStoreGradeHigh) {
        this.scholasticStoreGradeHigh = scholasticStoreGradeHigh;
    }

    @JsonProperty("scholasticStoreGradeLow")
    public String getScholasticStoreGradeLow() {
        return scholasticStoreGradeLow;
    }

    @JsonProperty("scholasticStoreGradeLow")
    public void setScholasticStoreGradeLow(String scholasticStoreGradeLow) {
        this.scholasticStoreGradeLow = scholasticStoreGradeLow;
    }

    @JsonProperty("scholasticStoreUnitCost")
    public String getScholasticStoreUnitCost() {
        return scholasticStoreUnitCost;
    }

    @JsonProperty("scholasticStoreUnitCost")
    public void setScholasticStoreUnitCost(String scholasticStoreUnitCost) {
        this.scholasticStoreUnitCost = scholasticStoreUnitCost;
    }

    @JsonProperty("scholasticStoreWorkID")
    public String getScholasticStoreWorkID() {
        return scholasticStoreWorkID;
    }

    @JsonProperty("scholasticStoreWorkID")
    public void setScholasticStoreWorkID(String scholasticStoreWorkID) {
        this.scholasticStoreWorkID = scholasticStoreWorkID;
    }

    @JsonProperty("scholasticStoreManufacturer")
    public String getScholasticStoreManufacturer() {
        return scholasticStoreManufacturer;
    }

    @JsonProperty("scholasticStoreManufacturer")
    public void setScholasticStoreManufacturer(String scholasticStoreManufacturer) {
        this.scholasticStoreManufacturer = scholasticStoreManufacturer;
    }

    @JsonProperty("scholasticStoreProductType")
    public String getScholasticStoreProductType() {
        return scholasticStoreProductType;
    }

    @JsonProperty("scholasticStoreProductType")
    public void setScholasticStoreProductType(String scholasticStoreProductType) {
        this.scholasticStoreProductType = scholasticStoreProductType;
    }

    @JsonProperty("scholasticStoreDisplayTitle")
    public String getScholasticStoreDisplayTitle() {
        return scholasticStoreDisplayTitle;
    }

    @JsonProperty("scholasticStoreDisplayTitle")
    public void setScholasticStoreDisplayTitle(String scholasticStoreDisplayTitle) {
        this.scholasticStoreDisplayTitle = scholasticStoreDisplayTitle;
    }

    @JsonProperty("scholasticStoreFormat")
    public String getScholasticStoreFormat() {
        return scholasticStoreFormat;
    }

    @JsonProperty("scholasticStoreFormat")
    public void setScholasticStoreFormat(String scholasticStoreFormat) {
        this.scholasticStoreFormat = scholasticStoreFormat;
    }

    @JsonProperty("scholasticStoreProductStartDate")
    public String getScholasticStoreProductStartDate() {
        return scholasticStoreProductStartDate;
    }

    @JsonProperty("scholasticStoreProductStartDate")
    public void setScholasticStoreProductStartDate(String scholasticStoreProductStartDate) {
        this.scholasticStoreProductStartDate = scholasticStoreProductStartDate;
    }

    @JsonProperty("scholasticStoreOtherFormat")
    public List<String> getScholasticStoreOtherFormat() {
        return scholasticStoreOtherFormat;
    }

    @JsonProperty("scholasticStoreOtherFormat")
    public void setScholasticStoreOtherFormat(List<String> scholasticStoreOtherFormat) {
        this.scholasticStoreOtherFormat = scholasticStoreOtherFormat;
    }

    @JsonProperty("scholasticStoreNumberOfAlternateImages")
    public String getScholasticStoreNumberOfAlternateImages() {
        return scholasticStoreNumberOfAlternateImages;
    }

    @JsonProperty("scholasticStoreNumberOfAlternateImages")
    public void setScholasticStoreNumberOfAlternateImages(String scholasticStoreNumberOfAlternateImages) {
        this.scholasticStoreNumberOfAlternateImages = scholasticStoreNumberOfAlternateImages;
    }

    @JsonProperty("scholasticStoreShippingMessagePDP")
    public String getScholasticStoreShippingMessagePDP() {
        return scholasticStoreShippingMessagePDP;
    }

    @JsonProperty("scholasticStoreShippingMessagePDP")
    public void setScholasticStoreShippingMessagePDP(String scholasticStoreShippingMessagePDP) {
        this.scholasticStoreShippingMessagePDP = scholasticStoreShippingMessagePDP;
    }

    @JsonProperty("scholasticStoreTitleOwner")
    public String getScholasticStoreTitleOwner() {
        return scholasticStoreTitleOwner;
    }

    @JsonProperty("scholasticStoreTitleOwner")
    public void setScholasticStoreTitleOwner(String scholasticStoreTitleOwner) {
        this.scholasticStoreTitleOwner = scholasticStoreTitleOwner;
    }

    @JsonProperty("scholasticStoreLongDescriptionOwner")
    public String getScholasticStoreLongDescriptionOwner() {
        return scholasticStoreLongDescriptionOwner;
    }

    @JsonProperty("scholasticStoreLongDescriptionOwner")
    public void setScholasticStoreLongDescriptionOwner(String scholasticStoreLongDescriptionOwner) {
        this.scholasticStoreLongDescriptionOwner = scholasticStoreLongDescriptionOwner;
    }

    @JsonProperty("scholasticStoreTitleByOwner")
    public String getScholasticStoreTitleByOwner() {
        return scholasticStoreTitleByOwner;
    }

    @JsonProperty("scholasticStoreTitleByOwner")
    public void setScholasticStoreTitleByOwner(String scholasticStoreTitleByOwner) {
        this.scholasticStoreTitleByOwner = scholasticStoreTitleByOwner;
    }

    @JsonProperty("scholasticStoreLongDescriptionByOwner")
    public String getScholasticStoreLongDescriptionByOwner() {
        return scholasticStoreLongDescriptionByOwner;
    }

    @JsonProperty("scholasticStoreLongDescriptionByOwner")
    public void setScholasticStoreLongDescriptionByOwner(String scholasticStoreLongDescriptionByOwner) {
        this.scholasticStoreLongDescriptionByOwner = scholasticStoreLongDescriptionByOwner;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public List<String> getScholasticStoreTarget() {
		return scholasticStoreTarget;
	}

	public void setScholasticStoreTarget(List<String> scholasticStoreTarget) {
		this.scholasticStoreTarget = scholasticStoreTarget;
	}

	public String getScholasticStoreNumberOfSeeInsidePages() {
		return scholasticStoreNumberOfSeeInsidePages;
	}

	public void setScholasticStoreNumberOfSeeInsidePages(String scholasticStoreNumberOfSeeInsidePages) {
		this.scholasticStoreNumberOfSeeInsidePages = scholasticStoreNumberOfSeeInsidePages;
	}

	public String getScholasticStoreVendor1() {
		return scholasticStoreVendor1;
	}

	public void setScholasticStoreVendor1(String scholasticStoreVendor1) {
		this.scholasticStoreVendor1 = scholasticStoreVendor1;
	}
}
