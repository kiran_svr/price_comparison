
package com.scholastic.cs.pricecompare.MDMTOJSON;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "channels",
    "active",
    "bookTypeID",
    "Border_ID",
    "corpListPrice",
    "deweyDecimal",
    "English_Guided_Reading_Level",
    "englishLexileLevel",
    "epicBindingTypeDesc",
    "epicMajorProductClass",
    "epicProductSpecType",
    "epicProductTypeCode",
    "epicProductTypeDesc",
    "fictionNonfiction",
    "Golden_Record_Source",
    "id",
    "imageLinks",
    "link",
    "Page_Count",
    "Playing_Time_Min",
    "Product_Channels",
    "Publication_Date",
    "Owner",
    "productStatus",
    "Title",
    "Trade_Onix_Imprint",
    "Trade_Onix_Publisher",
    "Trim_Size_Height",
    "Trim_Size_Length",
    "Trim_Size_Width",
    "weightPounds",
    "Word_Count",
    "Work_Channel",
    "workGenre",
    "workID",
    "workInterestAge",
    "workInterestGrades",
    "workSubject",
    "writingStyle",
    "clubsNote",
    "clubsTheme",
    "clubsGenre",
    "awards",
    "firstUseDate",
    "inventoryOnHand",
    "publisher",
    "clubsBookType",
    "isShippedSeparate",
    "language",
    "clubsInterestAgeLow",
    "clubsInterestAgeHigh",
    "clubsInterestGradeLow",
    "clubsInterestGradeHigh",
    "clubsProductType",
    "clubsIllustratorDisplayName",
    "clubsAuthorDisplayName",
    "clubsTitle",
    "englishLexileLevelLow",
    "lexilePrefixLow",
    "countryOfOriginCode",
    "countryOfOrigin",
    "epic30CharTitle",
    "harmonizedTariffSystemCode",
    "inkPaperIndicator",
    "safetyTestingRequired",
    "customerOrdersEnabled",
    "defaultBuyerNonCommandPlanner",
    "expenseItemIndicator",
    "inventoryAssetValue",
    "isProductPurchasable",
    "locationIdPrimaryVendor",
    "makeOrBuyProduct",
    "manufacturingBookTrimSize",
    "bookFairsBrandCode",
    "bookFairsBrandDesc",
    "bookFairsCaseCode",
    "bookFairsCaseDesc",
    "bookFairsGroup",
    "bookFairsItemNumber",
    "bookFairsActiveStatus",
    "bookFairsItemType",
    "bookFairsItemTypeDesc",
    "bookFairsPublisher",
    "bookFairsPublisherID",
    "bookFairsTitle",
    "bookFairsSupplier",
    "bookFairsSupplierID",
    "bookFairsCategoryClassCode",
    "bookFairsCategoryClassDesc",
    "bookFairsFirstPurchaseSeason",
    "bookFairsLastPurchaseSeason",
    "bookFairsLatestSeason",
    "bookFairsLeadTime",
    "bookFairsPALCost",
    "bookFairsPermChangeReasonCode",
    "bookFairsPermChangeReason",
    "bookFairsPermanentPriceAmount",
    "bookFairsPermanentPriceEffectiveDate",
    "bookFairsPurchaseReasonCode",
    "bookFairsPurchaseReason",
    "bookFairsPurchaseUM",
    "bookFairsReorderLeadTime",
    "bookFairsRepackCartonQuantity",
    "cartonQuantity",
    "bookFairsShelfLifeMonths",
    "bookFairsUnitOfMeasure",
    "bookFairsSubCategoryCode",
    "bookFairsSubCategory",
    "bookFairsTempPriceAmt",
    "epicMillenniumTitle",
    "epicMillenniumRCDNO",
    "bookFairsPriorityCode",
    "bookFairsPriorityDesc",
    "majorProductClassCode",
    "majorProductClassDesc",
    "epicTitle",
    "tradeInStoreDate",
    "editorialPageCount",
    "lexileLevelLow",
    "strippableIndicator",
    "canadaMakeOrBuyProduct",
    "insideOutsideFlag",
    "majorProductClass",
    "oracleItemType",
    "isProductInventoriable",
    "ecmsPrimaryCoverImageFlag",
    "epicWestonWoodsID",
    "Planning_ISBN",
    "sourceSystem",
    "formattedISBN",
    "formattedISBN10",
    "WEP_Property",
    "SRCPoints",
    "SRCQuizNumber",
    "SRCQuizzes",
    "ARQuizFlag",
    "DRA_Level",
    "EBook_Enriched",
    "EPIC_Title_Grouping",
    "EPICTitleGroupEditionCode",
    "ISBN_10",
    "PCD_Product_ID",
    "OPP_OutsidePublisher_ISBN",
    "UPN",
    "ISBN_13",
    "GR_WE_Contributor_Text",
    "GR_WE_Contributor_Art"
})
public class Data {

    @JsonProperty("channels")
    private Channels channels;
    @JsonProperty("active")
    private String active;
    @JsonProperty("bookTypeID")
    private String bookTypeID;
    @JsonProperty("Border_ID")
    private String borderID;
    @JsonProperty("corpListPrice")
    private String corpListPrice;
    @JsonProperty("deweyDecimal")
    private String deweyDecimal;
    @JsonProperty("English_Guided_Reading_Level")
    private String englishGuidedReadingLevel;
    @JsonProperty("englishLexileLevel")
    private String englishLexileLevel;
    @JsonProperty("epicBindingTypeDesc")
    private String epicBindingTypeDesc;
    @JsonProperty("epicMajorProductClass")
    private String epicMajorProductClass;
    @JsonProperty("epicProductSpecType")
    private String epicProductSpecType;
    @JsonProperty("epicProductTypeCode")
    private String epicProductTypeCode;
    @JsonProperty("epicProductTypeDesc")
    private String epicProductTypeDesc;
    @JsonProperty("fictionNonfiction")
    private String fictionNonfiction;
    @JsonProperty("Golden_Record_Source")
    private List<GoldenRecordSource> goldenRecordSource = null;
    @JsonProperty("id")
    private String id;
    @JsonProperty("imageLinks")
    private List<String> imageLinks = null;
    @JsonProperty("link")
    private String link;
    @JsonProperty("Page_Count")
    private String pageCount;
    @JsonProperty("Playing_Time_Min")
    private String playingTimeMin;
    @JsonProperty("Product_Channels")
    private List<String> productChannels = null;
    @JsonProperty("Publication_Date")
    private String publicationDate;
    @JsonProperty("Owner")
    private String owner;
    @JsonProperty("productStatus")
    private String productStatus;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("Trade_Onix_Imprint")
    private String tradeOnixImprint;
    @JsonProperty("Trade_Onix_Publisher")
    private String tradeOnixPublisher;
    @JsonProperty("Trim_Size_Height")
    private String trimSizeHeight;
    @JsonProperty("Trim_Size_Length")
    private String trimSizeLength;
    @JsonProperty("Trim_Size_Width")
    private String trimSizeWidth;
    @JsonProperty("weightPounds")
    private String weightPounds;
    @JsonProperty("Word_Count")
    private String wordCount;
    @JsonProperty("Work_Channel")
    private List<String> workChannel = null;
    @JsonProperty("workGenre")
    private List<WorkGenre> workGenre = null;
    @JsonProperty("workID")
    private String workID;
    @JsonProperty("workInterestAge")
    private List<String> workInterestAge = null;
    @JsonProperty("workInterestGrades")
    private List<String> workInterestGrades = null;
    @JsonProperty("workSubject")
    private List<WorkSubject> workSubject = null;
    @JsonProperty("writingStyle")
    private List<WritingStyle> writingStyle = null;
    @JsonProperty("clubsNote")
    private List<String> clubsNote = null;
    @JsonProperty("clubsTheme")
    private List<String> clubsTheme = null;
    @JsonProperty("clubsGenre")
    private List<String> clubsGenre = null;
    @JsonProperty("awards")
    private List<String> awards = null;
    @JsonProperty("firstUseDate")
    private String firstUseDate;
    @JsonProperty("inventoryOnHand")
    private String inventoryOnHand;
    @JsonProperty("publisher")
    private String publisher;
    @JsonProperty("clubsBookType")
    private String clubsBookType;
    @JsonProperty("isShippedSeparate")
    private String isShippedSeparate;
    @JsonProperty("language")
    private String language;
    @JsonProperty("clubsInterestAgeLow")
    private String clubsInterestAgeLow;
    @JsonProperty("clubsInterestAgeHigh")
    private String clubsInterestAgeHigh;
    @JsonProperty("clubsInterestGradeLow")
    private String clubsInterestGradeLow;
    @JsonProperty("clubsInterestGradeHigh")
    private String clubsInterestGradeHigh;
    @JsonProperty("clubsProductType")
    private String clubsProductType;
    @JsonProperty("clubsIllustratorDisplayName")
    private String clubsIllustratorDisplayName;
    @JsonProperty("clubsAuthorDisplayName")
    private String clubsAuthorDisplayName;
    @JsonProperty("clubsTitle")
    private String clubsTitle;
    @JsonProperty("englishLexileLevelLow")
    private String englishLexileLevelLow;
    @JsonProperty("lexilePrefixLow")
    private String lexilePrefixLow;
    @JsonProperty("countryOfOriginCode")
    private String countryOfOriginCode;
    @JsonProperty("countryOfOrigin")
    private String countryOfOrigin;
    @JsonProperty("epic30CharTitle")
    private String epic30CharTitle;
    @JsonProperty("harmonizedTariffSystemCode")
    private String harmonizedTariffSystemCode;
    @JsonProperty("inkPaperIndicator")
    private String inkPaperIndicator;
    @JsonProperty("safetyTestingRequired")
    private String safetyTestingRequired;
    @JsonProperty("customerOrdersEnabled")
    private String customerOrdersEnabled;
    @JsonProperty("defaultBuyerNonCommandPlanner")
    private String defaultBuyerNonCommandPlanner;
    @JsonProperty("expenseItemIndicator")
    private String expenseItemIndicator;
    @JsonProperty("inventoryAssetValue")
    private String inventoryAssetValue;
    @JsonProperty("isProductPurchasable")
    private String isProductPurchasable;
    @JsonProperty("locationIdPrimaryVendor")
    private String locationIdPrimaryVendor;
    @JsonProperty("makeOrBuyProduct")
    private String makeOrBuyProduct;
    @JsonProperty("manufacturingBookTrimSize")
    private String manufacturingBookTrimSize;
    @JsonProperty("bookFairsBrandCode")
    private String bookFairsBrandCode;
    @JsonProperty("bookFairsBrandDesc")
    private String bookFairsBrandDesc;
    @JsonProperty("bookFairsCaseCode")
    private String bookFairsCaseCode;
    @JsonProperty("bookFairsCaseDesc")
    private String bookFairsCaseDesc;
    @JsonProperty("bookFairsGroup")
    private String bookFairsGroup;
    @JsonProperty("bookFairsItemNumber")
    private String bookFairsItemNumber;
    @JsonProperty("bookFairsActiveStatus")
    private String bookFairsActiveStatus;
    @JsonProperty("bookFairsItemType")
    private String bookFairsItemType;
    @JsonProperty("bookFairsItemTypeDesc")
    private String bookFairsItemTypeDesc;
    @JsonProperty("bookFairsPublisher")
    private String bookFairsPublisher;
    @JsonProperty("bookFairsPublisherID")
    private String bookFairsPublisherID;
    @JsonProperty("bookFairsTitle")
    private String bookFairsTitle;
    @JsonProperty("bookFairsSupplier")
    private String bookFairsSupplier;
    @JsonProperty("bookFairsSupplierID")
    private String bookFairsSupplierID;
    @JsonProperty("bookFairsCategoryClassCode")
    private String bookFairsCategoryClassCode;
    @JsonProperty("bookFairsCategoryClassDesc")
    private String bookFairsCategoryClassDesc;
    @JsonProperty("bookFairsFirstPurchaseSeason")
    private String bookFairsFirstPurchaseSeason;
    @JsonProperty("bookFairsLastPurchaseSeason")
    private String bookFairsLastPurchaseSeason;
    @JsonProperty("bookFairsLatestSeason")
    private String bookFairsLatestSeason;
    @JsonProperty("bookFairsLeadTime")
    private String bookFairsLeadTime;
    @JsonProperty("bookFairsPALCost")
    private String bookFairsPALCost;
    @JsonProperty("bookFairsPermChangeReasonCode")
    private String bookFairsPermChangeReasonCode;
    @JsonProperty("bookFairsPermChangeReason")
    private String bookFairsPermChangeReason;
    @JsonProperty("bookFairsPermanentPriceAmount")
    private String bookFairsPermanentPriceAmount;
    @JsonProperty("bookFairsPermanentPriceEffectiveDate")
    private String bookFairsPermanentPriceEffectiveDate;
    @JsonProperty("bookFairsPurchaseReasonCode")
    private String bookFairsPurchaseReasonCode;
    @JsonProperty("bookFairsPurchaseReason")
    private String bookFairsPurchaseReason;
    @JsonProperty("bookFairsPurchaseUM")
    private String bookFairsPurchaseUM;
    @JsonProperty("bookFairsReorderLeadTime")
    private String bookFairsReorderLeadTime;
    @JsonProperty("bookFairsRepackCartonQuantity")
    private String bookFairsRepackCartonQuantity;
    @JsonProperty("cartonQuantity")
    private String cartonQuantity;
    @JsonProperty("bookFairsShelfLifeMonths")
    private String bookFairsShelfLifeMonths;
    @JsonProperty("bookFairsUnitOfMeasure")
    private String bookFairsUnitOfMeasure;
    @JsonProperty("bookFairsSubCategoryCode")
    private String bookFairsSubCategoryCode;
    @JsonProperty("bookFairsSubCategory")
    private String bookFairsSubCategory;
    @JsonProperty("bookFairsTempPriceAmt")
    private String bookFairsTempPriceAmt;
    @JsonProperty("epicMillenniumTitle")
    private String epicMillenniumTitle;
    @JsonProperty("epicMillenniumRCDNO")
    private String epicMillenniumRCDNO;
    @JsonProperty("bookFairsPriorityCode")
    private String bookFairsPriorityCode;
    @JsonProperty("bookFairsPriorityDesc")
    private String bookFairsPriorityDesc;
    @JsonProperty("majorProductClassCode")
    private String majorProductClassCode;
    @JsonProperty("majorProductClassDesc")
    private String majorProductClassDesc;
    @JsonProperty("epicTitle")
    private String epicTitle;
    @JsonProperty("tradeInStoreDate")
    private String tradeInStoreDate;
    @JsonProperty("editorialPageCount")
    private String editorialPageCount;
    @JsonProperty("lexileLevelLow")
    private String lexileLevelLow;
    @JsonProperty("strippableIndicator")
    private String strippableIndicator;
    @JsonProperty("canadaMakeOrBuyProduct")
    private String canadaMakeOrBuyProduct;
    @JsonProperty("insideOutsideFlag")
    private String insideOutsideFlag;
    @JsonProperty("majorProductClass")
    private String majorProductClass;
    @JsonProperty("oracleItemType")
    private String oracleItemType;
    @JsonProperty("isProductInventoriable")
    private String isProductInventoriable;
    @JsonProperty("ecmsPrimaryCoverImageFlag")
    private String ecmsPrimaryCoverImageFlag;
    @JsonProperty("epicWestonWoodsID")
    private String epicWestonWoodsID;
    @JsonProperty("Planning_ISBN")
    private String planningISBN;
    @JsonProperty("sourceSystem")
    private String sourceSystem;
    @JsonProperty("formattedISBN")
    private String formattedISBN;
    @JsonProperty("formattedISBN10")
    private String formattedISBN10;
    @JsonProperty("WEP_Property")
    private List<WEPProperty> wEPProperty = null;
    @JsonProperty("SRCPoints")
    private String sRCPoints;
    @JsonProperty("SRCQuizNumber")
    private String sRCQuizNumber;
    @JsonProperty("SRCQuizzes")
    private String sRCQuizzes;
    @JsonProperty("ARQuizFlag")
    private String aRQuizFlag;
    @JsonProperty("DRA_Level")
    private String dRALevel;
    @JsonProperty("EBook_Enriched")
    private String eBookEnriched;
    @JsonProperty("EPIC_Title_Grouping")
    private String ePICTitleGrouping;
    @JsonProperty("EPICTitleGroupEditionCode")
    private String ePICTitleGroupEditionCode;
    @JsonProperty("ISBN_10")
    private String iSBN10;
    @JsonProperty("PCD_Product_ID")
    private String pCDProductID;
    @JsonProperty("OPP_OutsidePublisher_ISBN")
    private String oPPOutsidePublisherISBN;
    @JsonProperty("UPN")
    private String uPN;
    @JsonProperty("ISBN_13")
    private String iSBN13;
    @JsonProperty("GR_WE_Contributor_Text")
    private List<GRWEContributorText> gRWEContributorText = null;
    @JsonProperty("GR_WE_Contributor_Art")
    private List<GRWEContributorArt> gRWEContributorArt = null;
    
    @JsonProperty("GR_WEP_Series")
    private List<GRWEPSeries> gRWEPSeries = null;
    
    
    
    public List<GRWEPSeries> getgRWEPSeries() {
		return gRWEPSeries;
	}

	public void setgRWEPSeries(List<GRWEPSeries> gRWEPSeries) {
		this.gRWEPSeries = gRWEPSeries;
	}

	@JsonProperty("EAN")
    private String EAN;
    
    @JsonProperty("bookFairsHasLicenseFlag")
    private String bookFairsHasLicenseFlag;
    
    
    
    
    public String getBookFairsHasLicenseFlag() {
		return bookFairsHasLicenseFlag;
	}

	public void setBookFairsHasLicenseFlag(String bookFairsHasLicenseFlag) {
		this.bookFairsHasLicenseFlag = bookFairsHasLicenseFlag;
	}

	public String getEAN() {
		return EAN;
	}

	public void setEAN(String eAN) {
		EAN = eAN;
	}

	@JsonProperty("clubsMarketingSeriesName")
    private String clubsMarketingSeriesName;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
    public String getClubsMarketingSeriesName() {
		return clubsMarketingSeriesName;
	}

	public void setClubsMarketingSeriesName(String clubsMarketingSeriesName) {
		this.clubsMarketingSeriesName = clubsMarketingSeriesName;
	}

    @JsonProperty("channels")
    public Channels getChannels() {
        return channels;
    }

    @JsonProperty("channels")
    public void setChannels(Channels channels) {
        this.channels = channels;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("bookTypeID")
    public String getBookTypeID() {
        return bookTypeID;
    }

    @JsonProperty("bookTypeID")
    public void setBookTypeID(String bookTypeID) {
        this.bookTypeID = bookTypeID;
    }

    @JsonProperty("Border_ID")
    public String getBorderID() {
        return borderID;
    }

    @JsonProperty("Border_ID")
    public void setBorderID(String borderID) {
        this.borderID = borderID;
    }

    @JsonProperty("corpListPrice")
    public String getCorpListPrice() {
        return corpListPrice;
    }

    @JsonProperty("corpListPrice")
    public void setCorpListPrice(String corpListPrice) {
        this.corpListPrice = corpListPrice;
    }

    @JsonProperty("deweyDecimal")
    public String getDeweyDecimal() {
        return deweyDecimal;
    }

    @JsonProperty("deweyDecimal")
    public void setDeweyDecimal(String deweyDecimal) {
        this.deweyDecimal = deweyDecimal;
    }

    @JsonProperty("English_Guided_Reading_Level")
    public String getEnglishGuidedReadingLevel() {
        return englishGuidedReadingLevel;
    }

    @JsonProperty("English_Guided_Reading_Level")
    public void setEnglishGuidedReadingLevel(String englishGuidedReadingLevel) {
        this.englishGuidedReadingLevel = englishGuidedReadingLevel;
    }

    @JsonProperty("englishLexileLevel")
    public String getEnglishLexileLevel() {
        return englishLexileLevel;
    }

    @JsonProperty("englishLexileLevel")
    public void setEnglishLexileLevel(String englishLexileLevel) {
        this.englishLexileLevel = englishLexileLevel;
    }

    @JsonProperty("epicBindingTypeDesc")
    public String getEpicBindingTypeDesc() {
        return epicBindingTypeDesc;
    }

    @JsonProperty("epicBindingTypeDesc")
    public void setEpicBindingTypeDesc(String epicBindingTypeDesc) {
        this.epicBindingTypeDesc = epicBindingTypeDesc;
    }

    @JsonProperty("epicMajorProductClass")
    public String getEpicMajorProductClass() {
        return epicMajorProductClass;
    }

    @JsonProperty("epicMajorProductClass")
    public void setEpicMajorProductClass(String epicMajorProductClass) {
        this.epicMajorProductClass = epicMajorProductClass;
    }

    @JsonProperty("epicProductSpecType")
    public String getEpicProductSpecType() {
        return epicProductSpecType;
    }

    @JsonProperty("epicProductSpecType")
    public void setEpicProductSpecType(String epicProductSpecType) {
        this.epicProductSpecType = epicProductSpecType;
    }

    @JsonProperty("epicProductTypeCode")
    public String getEpicProductTypeCode() {
        return epicProductTypeCode;
    }

    @JsonProperty("epicProductTypeCode")
    public void setEpicProductTypeCode(String epicProductTypeCode) {
        this.epicProductTypeCode = epicProductTypeCode;
    }

    @JsonProperty("epicProductTypeDesc")
    public String getEpicProductTypeDesc() {
        return epicProductTypeDesc;
    }

    @JsonProperty("epicProductTypeDesc")
    public void setEpicProductTypeDesc(String epicProductTypeDesc) {
        this.epicProductTypeDesc = epicProductTypeDesc;
    }

    @JsonProperty("fictionNonfiction")
    public String getFictionNonfiction() {
        return fictionNonfiction;
    }

    @JsonProperty("fictionNonfiction")
    public void setFictionNonfiction(String fictionNonfiction) {
        this.fictionNonfiction = fictionNonfiction;
    }

    @JsonProperty("Golden_Record_Source")
    public List<GoldenRecordSource> getGoldenRecordSource() {
        return goldenRecordSource;
    }

    @JsonProperty("Golden_Record_Source")
    public void setGoldenRecordSource(List<GoldenRecordSource> goldenRecordSource) {
        this.goldenRecordSource = goldenRecordSource;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("imageLinks")
    public List<String> getImageLinks() {
        return imageLinks;
    }

    @JsonProperty("imageLinks")
    public void setImageLinks(List<String> imageLinks) {
        this.imageLinks = imageLinks;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(String link) {
        this.link = link;
    }

    @JsonProperty("Page_Count")
    public String getPageCount() {
        return pageCount;
    }

    @JsonProperty("Page_Count")
    public void setPageCount(String pageCount) {
        this.pageCount = pageCount;
    }

    @JsonProperty("Playing_Time_Min")
    public String getPlayingTimeMin() {
        return playingTimeMin;
    }

    @JsonProperty("Playing_Time_Min")
    public void setPlayingTimeMin(String playingTimeMin) {
        this.playingTimeMin = playingTimeMin;
    }

    @JsonProperty("Product_Channels")
    public List<String> getProductChannels() {
        return productChannels;
    }

    @JsonProperty("Product_Channels")
    public void setProductChannels(List<String> productChannels) {
        this.productChannels = productChannels;
    }

    @JsonProperty("Publication_Date")
    public String getPublicationDate() {
        return publicationDate;
    }

    @JsonProperty("Publication_Date")
    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    @JsonProperty("Owner")
    public String getOwner() {
        return owner;
    }

    @JsonProperty("Owner")
    public void setOwner(String owner) {
        this.owner = owner;
    }

    @JsonProperty("productStatus")
    public String getProductStatus() {
        return productStatus;
    }

    @JsonProperty("productStatus")
    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    @JsonProperty("Title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("Title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("Trade_Onix_Imprint")
    public String getTradeOnixImprint() {
        return tradeOnixImprint;
    }

    @JsonProperty("Trade_Onix_Imprint")
    public void setTradeOnixImprint(String tradeOnixImprint) {
        this.tradeOnixImprint = tradeOnixImprint;
    }

    @JsonProperty("Trade_Onix_Publisher")
    public String getTradeOnixPublisher() {
        return tradeOnixPublisher;
    }

    @JsonProperty("Trade_Onix_Publisher")
    public void setTradeOnixPublisher(String tradeOnixPublisher) {
        this.tradeOnixPublisher = tradeOnixPublisher;
    }

    @JsonProperty("Trim_Size_Height")
    public String getTrimSizeHeight() {
        return trimSizeHeight;
    }

    @JsonProperty("Trim_Size_Height")
    public void setTrimSizeHeight(String trimSizeHeight) {
        this.trimSizeHeight = trimSizeHeight;
    }

    @JsonProperty("Trim_Size_Length")
    public String getTrimSizeLength() {
        return trimSizeLength;
    }

    @JsonProperty("Trim_Size_Length")
    public void setTrimSizeLength(String trimSizeLength) {
        this.trimSizeLength = trimSizeLength;
    }

    @JsonProperty("Trim_Size_Width")
    public String getTrimSizeWidth() {
        return trimSizeWidth;
    }

    @JsonProperty("Trim_Size_Width")
    public void setTrimSizeWidth(String trimSizeWidth) {
        this.trimSizeWidth = trimSizeWidth;
    }

    @JsonProperty("weightPounds")
    public String getWeightPounds() {
        return weightPounds;
    }

    @JsonProperty("weightPounds")
    public void setWeightPounds(String weightPounds) {
        this.weightPounds = weightPounds;
    }

    @JsonProperty("Word_Count")
    public String getWordCount() {
        return wordCount;
    }

    @JsonProperty("Word_Count")
    public void setWordCount(String wordCount) {
        this.wordCount = wordCount;
    }

    @JsonProperty("Work_Channel")
    public List<String> getWorkChannel() {
        return workChannel;
    }

    @JsonProperty("Work_Channel")
    public void setWorkChannel(List<String> workChannel) {
        this.workChannel = workChannel;
    }

    @JsonProperty("workGenre")
    public List<WorkGenre> getWorkGenre() {
        return workGenre;
    }

    @JsonProperty("workGenre")
    public void setWorkGenre(List<WorkGenre> workGenre) {
        this.workGenre = workGenre;
    }

    @JsonProperty("workID")
    public String getWorkID() {
        return workID;
    }

    @JsonProperty("workID")
    public void setWorkID(String workID) {
        this.workID = workID;
    }

    @JsonProperty("workInterestAge")
    public List<String> getWorkInterestAge() {
        return workInterestAge;
    }

    @JsonProperty("workInterestAge")
    public void setWorkInterestAge(List<String> workInterestAge) {
        this.workInterestAge = workInterestAge;
    }

    @JsonProperty("workInterestGrades")
    public List<String> getWorkInterestGrades() {
        return workInterestGrades;
    }

    @JsonProperty("workInterestGrades")
    public void setWorkInterestGrades(List<String> workInterestGrades) {
        this.workInterestGrades = workInterestGrades;
    }

    @JsonProperty("workSubject")
    public List<WorkSubject> getWorkSubject() {
        return workSubject;
    }

    @JsonProperty("workSubject")
    public void setWorkSubject(List<WorkSubject> workSubject) {
        this.workSubject = workSubject;
    }

    @JsonProperty("writingStyle")
    public List<WritingStyle> getWritingStyle() {
        return writingStyle;
    }

    @JsonProperty("writingStyle")
    public void setWritingStyle(List<WritingStyle> writingStyle) {
        this.writingStyle = writingStyle;
    }

    @JsonProperty("clubsNote")
    public List<String> getClubsNote() {
        return clubsNote;
    }

    @JsonProperty("clubsNote")
    public void setClubsNote(List<String> clubsNote) {
        this.clubsNote = clubsNote;
    }

    @JsonProperty("clubsTheme")
    public List<String> getClubsTheme() {
        return clubsTheme;
    }

    @JsonProperty("clubsTheme")
    public void setClubsTheme(List<String> clubsTheme) {
        this.clubsTheme = clubsTheme;
    }

    @JsonProperty("clubsGenre")
    public List<String> getClubsGenre() {
        return clubsGenre;
    }

    @JsonProperty("clubsGenre")
    public void setClubsGenre(List<String> clubsGenre) {
        this.clubsGenre = clubsGenre;
    }

    @JsonProperty("awards")
    public List<String> getAwards() {
        return awards;
    }

    @JsonProperty("awards")
    public void setAwards(List<String> awards) {
        this.awards = awards;
    }

    @JsonProperty("firstUseDate")
    public String getFirstUseDate() {
        return firstUseDate;
    }

    @JsonProperty("firstUseDate")
    public void setFirstUseDate(String firstUseDate) {
        this.firstUseDate = firstUseDate;
    }

    @JsonProperty("inventoryOnHand")
    public String getInventoryOnHand() {
        return inventoryOnHand;
    }

    @JsonProperty("inventoryOnHand")
    public void setInventoryOnHand(String inventoryOnHand) {
        this.inventoryOnHand = inventoryOnHand;
    }

    @JsonProperty("publisher")
    public String getPublisher() {
        return publisher;
    }

    @JsonProperty("publisher")
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @JsonProperty("clubsBookType")
    public String getClubsBookType() {
        return clubsBookType;
    }

    @JsonProperty("clubsBookType")
    public void setClubsBookType(String clubsBookType) {
        this.clubsBookType = clubsBookType;
    }

    @JsonProperty("isShippedSeparate")
    public String getIsShippedSeparate() {
        return isShippedSeparate;
    }

    @JsonProperty("isShippedSeparate")
    public void setIsShippedSeparate(String isShippedSeparate) {
        this.isShippedSeparate = isShippedSeparate;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("clubsInterestAgeLow")
    public String getClubsInterestAgeLow() {
        return clubsInterestAgeLow;
    }

    @JsonProperty("clubsInterestAgeLow")
    public void setClubsInterestAgeLow(String clubsInterestAgeLow) {
        this.clubsInterestAgeLow = clubsInterestAgeLow;
    }

    @JsonProperty("clubsInterestAgeHigh")
    public String getClubsInterestAgeHigh() {
        return clubsInterestAgeHigh;
    }

    @JsonProperty("clubsInterestAgeHigh")
    public void setClubsInterestAgeHigh(String clubsInterestAgeHigh) {
        this.clubsInterestAgeHigh = clubsInterestAgeHigh;
    }

    @JsonProperty("clubsInterestGradeLow")
    public String getClubsInterestGradeLow() {
        return clubsInterestGradeLow;
    }

    @JsonProperty("clubsInterestGradeLow")
    public void setClubsInterestGradeLow(String clubsInterestGradeLow) {
        this.clubsInterestGradeLow = clubsInterestGradeLow;
    }

    @JsonProperty("clubsInterestGradeHigh")
    public String getClubsInterestGradeHigh() {
        return clubsInterestGradeHigh;
    }

    @JsonProperty("clubsInterestGradeHigh")
    public void setClubsInterestGradeHigh(String clubsInterestGradeHigh) {
        this.clubsInterestGradeHigh = clubsInterestGradeHigh;
    }

    @JsonProperty("clubsProductType")
    public String getClubsProductType() {
        return clubsProductType;
    }

    @JsonProperty("clubsProductType")
    public void setClubsProductType(String clubsProductType) {
        this.clubsProductType = clubsProductType;
    }

    @JsonProperty("clubsIllustratorDisplayName")
    public String getClubsIllustratorDisplayName() {
        return clubsIllustratorDisplayName;
    }

    @JsonProperty("clubsIllustratorDisplayName")
    public void setClubsIllustratorDisplayName(String clubsIllustratorDisplayName) {
        this.clubsIllustratorDisplayName = clubsIllustratorDisplayName;
    }

    @JsonProperty("clubsAuthorDisplayName")
    public String getClubsAuthorDisplayName() {
        return clubsAuthorDisplayName;
    }

    @JsonProperty("clubsAuthorDisplayName")
    public void setClubsAuthorDisplayName(String clubsAuthorDisplayName) {
        this.clubsAuthorDisplayName = clubsAuthorDisplayName;
    }

    @JsonProperty("clubsTitle")
    public String getClubsTitle() {
        return clubsTitle;
    }

    @JsonProperty("clubsTitle")
    public void setClubsTitle(String clubsTitle) {
        this.clubsTitle = clubsTitle;
    }

    @JsonProperty("englishLexileLevelLow")
    public String getEnglishLexileLevelLow() {
        return englishLexileLevelLow;
    }

    @JsonProperty("englishLexileLevelLow")
    public void setEnglishLexileLevelLow(String englishLexileLevelLow) {
        this.englishLexileLevelLow = englishLexileLevelLow;
    }

    @JsonProperty("lexilePrefixLow")
    public String getLexilePrefixLow() {
        return lexilePrefixLow;
    }

    @JsonProperty("lexilePrefixLow")
    public void setLexilePrefixLow(String lexilePrefixLow) {
        this.lexilePrefixLow = lexilePrefixLow;
    }

    @JsonProperty("countryOfOriginCode")
    public String getCountryOfOriginCode() {
        return countryOfOriginCode;
    }

    @JsonProperty("countryOfOriginCode")
    public void setCountryOfOriginCode(String countryOfOriginCode) {
        this.countryOfOriginCode = countryOfOriginCode;
    }

    @JsonProperty("countryOfOrigin")
    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    @JsonProperty("countryOfOrigin")
    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    @JsonProperty("epic30CharTitle")
    public String getEpic30CharTitle() {
        return epic30CharTitle;
    }

    @JsonProperty("epic30CharTitle")
    public void setEpic30CharTitle(String epic30CharTitle) {
        this.epic30CharTitle = epic30CharTitle;
    }

    @JsonProperty("harmonizedTariffSystemCode")
    public String getHarmonizedTariffSystemCode() {
        return harmonizedTariffSystemCode;
    }

    @JsonProperty("harmonizedTariffSystemCode")
    public void setHarmonizedTariffSystemCode(String harmonizedTariffSystemCode) {
        this.harmonizedTariffSystemCode = harmonizedTariffSystemCode;
    }

    @JsonProperty("inkPaperIndicator")
    public String getInkPaperIndicator() {
        return inkPaperIndicator;
    }

    @JsonProperty("inkPaperIndicator")
    public void setInkPaperIndicator(String inkPaperIndicator) {
        this.inkPaperIndicator = inkPaperIndicator;
    }

    @JsonProperty("safetyTestingRequired")
    public String getSafetyTestingRequired() {
        return safetyTestingRequired;
    }

    @JsonProperty("safetyTestingRequired")
    public void setSafetyTestingRequired(String safetyTestingRequired) {
        this.safetyTestingRequired = safetyTestingRequired;
    }

    @JsonProperty("customerOrdersEnabled")
    public String getCustomerOrdersEnabled() {
        return customerOrdersEnabled;
    }

    @JsonProperty("customerOrdersEnabled")
    public void setCustomerOrdersEnabled(String customerOrdersEnabled) {
        this.customerOrdersEnabled = customerOrdersEnabled;
    }

    @JsonProperty("defaultBuyerNonCommandPlanner")
    public String getDefaultBuyerNonCommandPlanner() {
        return defaultBuyerNonCommandPlanner;
    }

    @JsonProperty("defaultBuyerNonCommandPlanner")
    public void setDefaultBuyerNonCommandPlanner(String defaultBuyerNonCommandPlanner) {
        this.defaultBuyerNonCommandPlanner = defaultBuyerNonCommandPlanner;
    }

    @JsonProperty("expenseItemIndicator")
    public String getExpenseItemIndicator() {
        return expenseItemIndicator;
    }

    @JsonProperty("expenseItemIndicator")
    public void setExpenseItemIndicator(String expenseItemIndicator) {
        this.expenseItemIndicator = expenseItemIndicator;
    }

    @JsonProperty("inventoryAssetValue")
    public String getInventoryAssetValue() {
        return inventoryAssetValue;
    }

    @JsonProperty("inventoryAssetValue")
    public void setInventoryAssetValue(String inventoryAssetValue) {
        this.inventoryAssetValue = inventoryAssetValue;
    }

    @JsonProperty("isProductPurchasable")
    public String getIsProductPurchasable() {
        return isProductPurchasable;
    }

    @JsonProperty("isProductPurchasable")
    public void setIsProductPurchasable(String isProductPurchasable) {
        this.isProductPurchasable = isProductPurchasable;
    }

    @JsonProperty("locationIdPrimaryVendor")
    public String getLocationIdPrimaryVendor() {
        return locationIdPrimaryVendor;
    }

    @JsonProperty("locationIdPrimaryVendor")
    public void setLocationIdPrimaryVendor(String locationIdPrimaryVendor) {
        this.locationIdPrimaryVendor = locationIdPrimaryVendor;
    }

    @JsonProperty("makeOrBuyProduct")
    public String getMakeOrBuyProduct() {
        return makeOrBuyProduct;
    }

    @JsonProperty("makeOrBuyProduct")
    public void setMakeOrBuyProduct(String makeOrBuyProduct) {
        this.makeOrBuyProduct = makeOrBuyProduct;
    }

    @JsonProperty("manufacturingBookTrimSize")
    public String getManufacturingBookTrimSize() {
        return manufacturingBookTrimSize;
    }

    @JsonProperty("manufacturingBookTrimSize")
    public void setManufacturingBookTrimSize(String manufacturingBookTrimSize) {
        this.manufacturingBookTrimSize = manufacturingBookTrimSize;
    }

    @JsonProperty("bookFairsBrandCode")
    public String getBookFairsBrandCode() {
        return bookFairsBrandCode;
    }

    @JsonProperty("bookFairsBrandCode")
    public void setBookFairsBrandCode(String bookFairsBrandCode) {
        this.bookFairsBrandCode = bookFairsBrandCode;
    }

    @JsonProperty("bookFairsBrandDesc")
    public String getBookFairsBrandDesc() {
        return bookFairsBrandDesc;
    }

    @JsonProperty("bookFairsBrandDesc")
    public void setBookFairsBrandDesc(String bookFairsBrandDesc) {
        this.bookFairsBrandDesc = bookFairsBrandDesc;
    }

    @JsonProperty("bookFairsCaseCode")
    public String getBookFairsCaseCode() {
        return bookFairsCaseCode;
    }

    @JsonProperty("bookFairsCaseCode")
    public void setBookFairsCaseCode(String bookFairsCaseCode) {
        this.bookFairsCaseCode = bookFairsCaseCode;
    }

    @JsonProperty("bookFairsCaseDesc")
    public String getBookFairsCaseDesc() {
        return bookFairsCaseDesc;
    }

    @JsonProperty("bookFairsCaseDesc")
    public void setBookFairsCaseDesc(String bookFairsCaseDesc) {
        this.bookFairsCaseDesc = bookFairsCaseDesc;
    }

    @JsonProperty("bookFairsGroup")
    public String getBookFairsGroup() {
        return bookFairsGroup;
    }

    @JsonProperty("bookFairsGroup")
    public void setBookFairsGroup(String bookFairsGroup) {
        this.bookFairsGroup = bookFairsGroup;
    }

    @JsonProperty("bookFairsItemNumber")
    public String getBookFairsItemNumber() {
        return bookFairsItemNumber;
    }

    @JsonProperty("bookFairsItemNumber")
    public void setBookFairsItemNumber(String bookFairsItemNumber) {
        this.bookFairsItemNumber = bookFairsItemNumber;
    }

    @JsonProperty("bookFairsActiveStatus")
    public String getBookFairsActiveStatus() {
        return bookFairsActiveStatus;
    }

    @JsonProperty("bookFairsActiveStatus")
    public void setBookFairsActiveStatus(String bookFairsActiveStatus) {
        this.bookFairsActiveStatus = bookFairsActiveStatus;
    }

    @JsonProperty("bookFairsItemType")
    public String getBookFairsItemType() {
        return bookFairsItemType;
    }

    @JsonProperty("bookFairsItemType")
    public void setBookFairsItemType(String bookFairsItemType) {
        this.bookFairsItemType = bookFairsItemType;
    }

    @JsonProperty("bookFairsItemTypeDesc")
    public String getBookFairsItemTypeDesc() {
        return bookFairsItemTypeDesc;
    }

    @JsonProperty("bookFairsItemTypeDesc")
    public void setBookFairsItemTypeDesc(String bookFairsItemTypeDesc) {
        this.bookFairsItemTypeDesc = bookFairsItemTypeDesc;
    }

    @JsonProperty("bookFairsPublisher")
    public String getBookFairsPublisher() {
        return bookFairsPublisher;
    }

    @JsonProperty("bookFairsPublisher")
    public void setBookFairsPublisher(String bookFairsPublisher) {
        this.bookFairsPublisher = bookFairsPublisher;
    }

    @JsonProperty("bookFairsPublisherID")
    public String getBookFairsPublisherID() {
        return bookFairsPublisherID;
    }

    @JsonProperty("bookFairsPublisherID")
    public void setBookFairsPublisherID(String bookFairsPublisherID) {
        this.bookFairsPublisherID = bookFairsPublisherID;
    }

    @JsonProperty("bookFairsTitle")
    public String getBookFairsTitle() {
        return bookFairsTitle;
    }

    @JsonProperty("bookFairsTitle")
    public void setBookFairsTitle(String bookFairsTitle) {
        this.bookFairsTitle = bookFairsTitle;
    }

    @JsonProperty("bookFairsSupplier")
    public String getBookFairsSupplier() {
        return bookFairsSupplier;
    }

    @JsonProperty("bookFairsSupplier")
    public void setBookFairsSupplier(String bookFairsSupplier) {
        this.bookFairsSupplier = bookFairsSupplier;
    }

    @JsonProperty("bookFairsSupplierID")
    public String getBookFairsSupplierID() {
        return bookFairsSupplierID;
    }

    @JsonProperty("bookFairsSupplierID")
    public void setBookFairsSupplierID(String bookFairsSupplierID) {
        this.bookFairsSupplierID = bookFairsSupplierID;
    }

    @JsonProperty("bookFairsCategoryClassCode")
    public String getBookFairsCategoryClassCode() {
        return bookFairsCategoryClassCode;
    }

    @JsonProperty("bookFairsCategoryClassCode")
    public void setBookFairsCategoryClassCode(String bookFairsCategoryClassCode) {
        this.bookFairsCategoryClassCode = bookFairsCategoryClassCode;
    }

    @JsonProperty("bookFairsCategoryClassDesc")
    public String getBookFairsCategoryClassDesc() {
        return bookFairsCategoryClassDesc;
    }

    @JsonProperty("bookFairsCategoryClassDesc")
    public void setBookFairsCategoryClassDesc(String bookFairsCategoryClassDesc) {
        this.bookFairsCategoryClassDesc = bookFairsCategoryClassDesc;
    }

    @JsonProperty("bookFairsFirstPurchaseSeason")
    public String getBookFairsFirstPurchaseSeason() {
        return bookFairsFirstPurchaseSeason;
    }

    @JsonProperty("bookFairsFirstPurchaseSeason")
    public void setBookFairsFirstPurchaseSeason(String bookFairsFirstPurchaseSeason) {
        this.bookFairsFirstPurchaseSeason = bookFairsFirstPurchaseSeason;
    }

    @JsonProperty("bookFairsLastPurchaseSeason")
    public String getBookFairsLastPurchaseSeason() {
        return bookFairsLastPurchaseSeason;
    }

    @JsonProperty("bookFairsLastPurchaseSeason")
    public void setBookFairsLastPurchaseSeason(String bookFairsLastPurchaseSeason) {
        this.bookFairsLastPurchaseSeason = bookFairsLastPurchaseSeason;
    }

    @JsonProperty("bookFairsLatestSeason")
    public String getBookFairsLatestSeason() {
        return bookFairsLatestSeason;
    }

    @JsonProperty("bookFairsLatestSeason")
    public void setBookFairsLatestSeason(String bookFairsLatestSeason) {
        this.bookFairsLatestSeason = bookFairsLatestSeason;
    }

    @JsonProperty("bookFairsLeadTime")
    public String getBookFairsLeadTime() {
        return bookFairsLeadTime;
    }

    @JsonProperty("bookFairsLeadTime")
    public void setBookFairsLeadTime(String bookFairsLeadTime) {
        this.bookFairsLeadTime = bookFairsLeadTime;
    }

    @JsonProperty("bookFairsPALCost")
    public String getBookFairsPALCost() {
        return bookFairsPALCost;
    }

    @JsonProperty("bookFairsPALCost")
    public void setBookFairsPALCost(String bookFairsPALCost) {
        this.bookFairsPALCost = bookFairsPALCost;
    }

    @JsonProperty("bookFairsPermChangeReasonCode")
    public String getBookFairsPermChangeReasonCode() {
        return bookFairsPermChangeReasonCode;
    }

    @JsonProperty("bookFairsPermChangeReasonCode")
    public void setBookFairsPermChangeReasonCode(String bookFairsPermChangeReasonCode) {
        this.bookFairsPermChangeReasonCode = bookFairsPermChangeReasonCode;
    }

    @JsonProperty("bookFairsPermChangeReason")
    public String getBookFairsPermChangeReason() {
        return bookFairsPermChangeReason;
    }

    @JsonProperty("bookFairsPermChangeReason")
    public void setBookFairsPermChangeReason(String bookFairsPermChangeReason) {
        this.bookFairsPermChangeReason = bookFairsPermChangeReason;
    }

    @JsonProperty("bookFairsPermanentPriceAmount")
    public String getBookFairsPermanentPriceAmount() {
        return bookFairsPermanentPriceAmount;
    }

    @JsonProperty("bookFairsPermanentPriceAmount")
    public void setBookFairsPermanentPriceAmount(String bookFairsPermanentPriceAmount) {
        this.bookFairsPermanentPriceAmount = bookFairsPermanentPriceAmount;
    }

    @JsonProperty("bookFairsPermanentPriceEffectiveDate")
    public String getBookFairsPermanentPriceEffectiveDate() {
        return bookFairsPermanentPriceEffectiveDate;
    }

    @JsonProperty("bookFairsPermanentPriceEffectiveDate")
    public void setBookFairsPermanentPriceEffectiveDate(String bookFairsPermanentPriceEffectiveDate) {
        this.bookFairsPermanentPriceEffectiveDate = bookFairsPermanentPriceEffectiveDate;
    }

    @JsonProperty("bookFairsPurchaseReasonCode")
    public String getBookFairsPurchaseReasonCode() {
        return bookFairsPurchaseReasonCode;
    }

    @JsonProperty("bookFairsPurchaseReasonCode")
    public void setBookFairsPurchaseReasonCode(String bookFairsPurchaseReasonCode) {
        this.bookFairsPurchaseReasonCode = bookFairsPurchaseReasonCode;
    }

    @JsonProperty("bookFairsPurchaseReason")
    public String getBookFairsPurchaseReason() {
        return bookFairsPurchaseReason;
    }

    @JsonProperty("bookFairsPurchaseReason")
    public void setBookFairsPurchaseReason(String bookFairsPurchaseReason) {
        this.bookFairsPurchaseReason = bookFairsPurchaseReason;
    }

    @JsonProperty("bookFairsPurchaseUM")
    public String getBookFairsPurchaseUM() {
        return bookFairsPurchaseUM;
    }

    @JsonProperty("bookFairsPurchaseUM")
    public void setBookFairsPurchaseUM(String bookFairsPurchaseUM) {
        this.bookFairsPurchaseUM = bookFairsPurchaseUM;
    }

    @JsonProperty("bookFairsReorderLeadTime")
    public String getBookFairsReorderLeadTime() {
        return bookFairsReorderLeadTime;
    }

    @JsonProperty("bookFairsReorderLeadTime")
    public void setBookFairsReorderLeadTime(String bookFairsReorderLeadTime) {
        this.bookFairsReorderLeadTime = bookFairsReorderLeadTime;
    }

    @JsonProperty("bookFairsRepackCartonQuantity")
    public String getBookFairsRepackCartonQuantity() {
        return bookFairsRepackCartonQuantity;
    }

    @JsonProperty("bookFairsRepackCartonQuantity")
    public void setBookFairsRepackCartonQuantity(String bookFairsRepackCartonQuantity) {
        this.bookFairsRepackCartonQuantity = bookFairsRepackCartonQuantity;
    }

    @JsonProperty("cartonQuantity")
    public String getCartonQuantity() {
        return cartonQuantity;
    }

    @JsonProperty("cartonQuantity")
    public void setCartonQuantity(String cartonQuantity) {
        this.cartonQuantity = cartonQuantity;
    }

    @JsonProperty("bookFairsShelfLifeMonths")
    public String getBookFairsShelfLifeMonths() {
        return bookFairsShelfLifeMonths;
    }

    @JsonProperty("bookFairsShelfLifeMonths")
    public void setBookFairsShelfLifeMonths(String bookFairsShelfLifeMonths) {
        this.bookFairsShelfLifeMonths = bookFairsShelfLifeMonths;
    }

    @JsonProperty("bookFairsUnitOfMeasure")
    public String getBookFairsUnitOfMeasure() {
        return bookFairsUnitOfMeasure;
    }

    @JsonProperty("bookFairsUnitOfMeasure")
    public void setBookFairsUnitOfMeasure(String bookFairsUnitOfMeasure) {
        this.bookFairsUnitOfMeasure = bookFairsUnitOfMeasure;
    }

    @JsonProperty("bookFairsSubCategoryCode")
    public String getBookFairsSubCategoryCode() {
        return bookFairsSubCategoryCode;
    }

    @JsonProperty("bookFairsSubCategoryCode")
    public void setBookFairsSubCategoryCode(String bookFairsSubCategoryCode) {
        this.bookFairsSubCategoryCode = bookFairsSubCategoryCode;
    }

    @JsonProperty("bookFairsSubCategory")
    public String getBookFairsSubCategory() {
        return bookFairsSubCategory;
    }

    @JsonProperty("bookFairsSubCategory")
    public void setBookFairsSubCategory(String bookFairsSubCategory) {
        this.bookFairsSubCategory = bookFairsSubCategory;
    }

    @JsonProperty("bookFairsTempPriceAmt")
    public String getBookFairsTempPriceAmt() {
        return bookFairsTempPriceAmt;
    }

    @JsonProperty("bookFairsTempPriceAmt")
    public void setBookFairsTempPriceAmt(String bookFairsTempPriceAmt) {
        this.bookFairsTempPriceAmt = bookFairsTempPriceAmt;
    }

    @JsonProperty("epicMillenniumTitle")
    public String getEpicMillenniumTitle() {
        return epicMillenniumTitle;
    }

    @JsonProperty("epicMillenniumTitle")
    public void setEpicMillenniumTitle(String epicMillenniumTitle) {
        this.epicMillenniumTitle = epicMillenniumTitle;
    }

    @JsonProperty("epicMillenniumRCDNO")
    public String getEpicMillenniumRCDNO() {
        return epicMillenniumRCDNO;
    }

    @JsonProperty("epicMillenniumRCDNO")
    public void setEpicMillenniumRCDNO(String epicMillenniumRCDNO) {
        this.epicMillenniumRCDNO = epicMillenniumRCDNO;
    }

    @JsonProperty("bookFairsPriorityCode")
    public String getBookFairsPriorityCode() {
        return bookFairsPriorityCode;
    }

    @JsonProperty("bookFairsPriorityCode")
    public void setBookFairsPriorityCode(String bookFairsPriorityCode) {
        this.bookFairsPriorityCode = bookFairsPriorityCode;
    }

    @JsonProperty("bookFairsPriorityDesc")
    public String getBookFairsPriorityDesc() {
        return bookFairsPriorityDesc;
    }

    @JsonProperty("bookFairsPriorityDesc")
    public void setBookFairsPriorityDesc(String bookFairsPriorityDesc) {
        this.bookFairsPriorityDesc = bookFairsPriorityDesc;
    }

    @JsonProperty("majorProductClassCode")
    public String getMajorProductClassCode() {
        return majorProductClassCode;
    }

    @JsonProperty("majorProductClassCode")
    public void setMajorProductClassCode(String majorProductClassCode) {
        this.majorProductClassCode = majorProductClassCode;
    }

    @JsonProperty("majorProductClassDesc")
    public String getMajorProductClassDesc() {
        return majorProductClassDesc;
    }

    @JsonProperty("majorProductClassDesc")
    public void setMajorProductClassDesc(String majorProductClassDesc) {
        this.majorProductClassDesc = majorProductClassDesc;
    }

    @JsonProperty("epicTitle")
    public String getEpicTitle() {
        return epicTitle;
    }

    @JsonProperty("epicTitle")
    public void setEpicTitle(String epicTitle) {
        this.epicTitle = epicTitle;
    }

    @JsonProperty("tradeInStoreDate")
    public String getTradeInStoreDate() {
        return tradeInStoreDate;
    }

    @JsonProperty("tradeInStoreDate")
    public void setTradeInStoreDate(String tradeInStoreDate) {
        this.tradeInStoreDate = tradeInStoreDate;
    }

    @JsonProperty("editorialPageCount")
    public String getEditorialPageCount() {
        return editorialPageCount;
    }

    @JsonProperty("editorialPageCount")
    public void setEditorialPageCount(String editorialPageCount) {
        this.editorialPageCount = editorialPageCount;
    }

    @JsonProperty("lexileLevelLow")
    public String getLexileLevelLow() {
        return lexileLevelLow;
    }

    @JsonProperty("lexileLevelLow")
    public void setLexileLevelLow(String lexileLevelLow) {
        this.lexileLevelLow = lexileLevelLow;
    }

    @JsonProperty("strippableIndicator")
    public String getStrippableIndicator() {
        return strippableIndicator;
    }

    @JsonProperty("strippableIndicator")
    public void setStrippableIndicator(String strippableIndicator) {
        this.strippableIndicator = strippableIndicator;
    }

    @JsonProperty("canadaMakeOrBuyProduct")
    public String getCanadaMakeOrBuyProduct() {
        return canadaMakeOrBuyProduct;
    }

    @JsonProperty("canadaMakeOrBuyProduct")
    public void setCanadaMakeOrBuyProduct(String canadaMakeOrBuyProduct) {
        this.canadaMakeOrBuyProduct = canadaMakeOrBuyProduct;
    }

    @JsonProperty("insideOutsideFlag")
    public String getInsideOutsideFlag() {
        return insideOutsideFlag;
    }

    @JsonProperty("insideOutsideFlag")
    public void setInsideOutsideFlag(String insideOutsideFlag) {
        this.insideOutsideFlag = insideOutsideFlag;
    }

    @JsonProperty("majorProductClass")
    public String getMajorProductClass() {
        return majorProductClass;
    }

    @JsonProperty("majorProductClass")
    public void setMajorProductClass(String majorProductClass) {
        this.majorProductClass = majorProductClass;
    }

    @JsonProperty("oracleItemType")
    public String getOracleItemType() {
        return oracleItemType;
    }

    @JsonProperty("oracleItemType")
    public void setOracleItemType(String oracleItemType) {
        this.oracleItemType = oracleItemType;
    }

    @JsonProperty("isProductInventoriable")
    public String getIsProductInventoriable() {
        return isProductInventoriable;
    }

    @JsonProperty("isProductInventoriable")
    public void setIsProductInventoriable(String isProductInventoriable) {
        this.isProductInventoriable = isProductInventoriable;
    }

    @JsonProperty("ecmsPrimaryCoverImageFlag")
    public String getEcmsPrimaryCoverImageFlag() {
        return ecmsPrimaryCoverImageFlag;
    }

    @JsonProperty("ecmsPrimaryCoverImageFlag")
    public void setEcmsPrimaryCoverImageFlag(String ecmsPrimaryCoverImageFlag) {
        this.ecmsPrimaryCoverImageFlag = ecmsPrimaryCoverImageFlag;
    }

    @JsonProperty("epicWestonWoodsID")
    public String getEpicWestonWoodsID() {
        return epicWestonWoodsID;
    }

    @JsonProperty("epicWestonWoodsID")
    public void setEpicWestonWoodsID(String epicWestonWoodsID) {
        this.epicWestonWoodsID = epicWestonWoodsID;
    }

    @JsonProperty("Planning_ISBN")
    public String getPlanningISBN() {
        return planningISBN;
    }

    @JsonProperty("Planning_ISBN")
    public void setPlanningISBN(String planningISBN) {
        this.planningISBN = planningISBN;
    }

    @JsonProperty("sourceSystem")
    public String getSourceSystem() {
        return sourceSystem;
    }

    @JsonProperty("sourceSystem")
    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    @JsonProperty("formattedISBN")
    public String getFormattedISBN() {
        return formattedISBN;
    }

    @JsonProperty("formattedISBN")
    public void setFormattedISBN(String formattedISBN) {
        this.formattedISBN = formattedISBN;
    }

    @JsonProperty("formattedISBN10")
    public String getFormattedISBN10() {
        return formattedISBN10;
    }

    @JsonProperty("formattedISBN10")
    public void setFormattedISBN10(String formattedISBN10) {
        this.formattedISBN10 = formattedISBN10;
    }

    @JsonProperty("WEP_Property")
    public List<WEPProperty> getWEPProperty() {
        return wEPProperty;
    }

    @JsonProperty("WEP_Property")
    public void setWEPProperty(List<WEPProperty> wEPProperty) {
        this.wEPProperty = wEPProperty;
    }

    @JsonProperty("SRCPoints")
    public String getSRCPoints() {
        return sRCPoints;
    }

    @JsonProperty("SRCPoints")
    public void setSRCPoints(String sRCPoints) {
        this.sRCPoints = sRCPoints;
    }

    @JsonProperty("SRCQuizNumber")
    public String getSRCQuizNumber() {
        return sRCQuizNumber;
    }

    @JsonProperty("SRCQuizNumber")
    public void setSRCQuizNumber(String sRCQuizNumber) {
        this.sRCQuizNumber = sRCQuizNumber;
    }

    @JsonProperty("SRCQuizzes")
    public String getSRCQuizzes() {
        return sRCQuizzes;
    }

    @JsonProperty("SRCQuizzes")
    public void setSRCQuizzes(String sRCQuizzes) {
        this.sRCQuizzes = sRCQuizzes;
    }

    @JsonProperty("ARQuizFlag")
    public String getARQuizFlag() {
        return aRQuizFlag;
    }

    @JsonProperty("ARQuizFlag")
    public void setARQuizFlag(String aRQuizFlag) {
        this.aRQuizFlag = aRQuizFlag;
    }

    @JsonProperty("DRA_Level")
    public String getDRALevel() {
        return dRALevel;
    }

    @JsonProperty("DRA_Level")
    public void setDRALevel(String dRALevel) {
        this.dRALevel = dRALevel;
    }

    @JsonProperty("EBook_Enriched")
    public String getEBookEnriched() {
        return eBookEnriched;
    }

    @JsonProperty("EBook_Enriched")
    public void setEBookEnriched(String eBookEnriched) {
        this.eBookEnriched = eBookEnriched;
    }

    @JsonProperty("EPIC_Title_Grouping")
    public String getEPICTitleGrouping() {
        return ePICTitleGrouping;
    }

    @JsonProperty("EPIC_Title_Grouping")
    public void setEPICTitleGrouping(String ePICTitleGrouping) {
        this.ePICTitleGrouping = ePICTitleGrouping;
    }

    @JsonProperty("EPICTitleGroupEditionCode")
    public String getEPICTitleGroupEditionCode() {
        return ePICTitleGroupEditionCode;
    }

    @JsonProperty("EPICTitleGroupEditionCode")
    public void setEPICTitleGroupEditionCode(String ePICTitleGroupEditionCode) {
        this.ePICTitleGroupEditionCode = ePICTitleGroupEditionCode;
    }

    @JsonProperty("ISBN_10")
    public String getISBN10() {
        return iSBN10;
    }

    @JsonProperty("ISBN_10")
    public void setISBN10(String iSBN10) {
        this.iSBN10 = iSBN10;
    }

    @JsonProperty("PCD_Product_ID")
    public String getPCDProductID() {
        return pCDProductID;
    }

    @JsonProperty("PCD_Product_ID")
    public void setPCDProductID(String pCDProductID) {
        this.pCDProductID = pCDProductID;
    }

    @JsonProperty("OPP_OutsidePublisher_ISBN")
    public String getOPPOutsidePublisherISBN() {
        return oPPOutsidePublisherISBN;
    }

    @JsonProperty("OPP_OutsidePublisher_ISBN")
    public void setOPPOutsidePublisherISBN(String oPPOutsidePublisherISBN) {
        this.oPPOutsidePublisherISBN = oPPOutsidePublisherISBN;
    }

    @JsonProperty("UPN")
    public String getUPN() {
        return uPN;
    }

    @JsonProperty("UPN")
    public void setUPN(String uPN) {
        this.uPN = uPN;
    }

    @JsonProperty("ISBN_13")
    public String getISBN13() {
        return iSBN13;
    }

    @JsonProperty("ISBN_13")
    public void setISBN13(String iSBN13) {
        this.iSBN13 = iSBN13;
    }

    @JsonProperty("GR_WE_Contributor_Text")
    public List<GRWEContributorText> getGRWEContributorText() {
        return gRWEContributorText;
    }

    @JsonProperty("GR_WE_Contributor_Text")
    public void setGRWEContributorText(List<GRWEContributorText> gRWEContributorText) {
        this.gRWEContributorText = gRWEContributorText;
    }

    @JsonProperty("GR_WE_Contributor_Art")
    public List<GRWEContributorArt> getGRWEContributorArt() {
        return gRWEContributorArt;
    }

    @JsonProperty("GR_WE_Contributor_Art")
    public void setGRWEContributorArt(List<GRWEContributorArt> gRWEContributorArt) {
        this.gRWEContributorArt = gRWEContributorArt;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
