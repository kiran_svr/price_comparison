package com.scholastic.cs.pricecompare;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.scholastic.cs.pricecompare.MDMTOJSON.Product;
import com.scholastic.cs.pricecompare.aem.AEM_Data;
import com.scholastic.cs.pricecompare.dw.DWData;
import com.scholastic.cs.pricecompare.unmanagedMdm.UnManagedMDM;

public class PriceCompareControlller {
	private static final Logger log = Logger.getLogger(PriceCompareControlller.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("CompatreOutput.xlsx").getPath();
	private static final String FILE_NAME_READ = ClassLoader.getSystemClassLoader().getResource("ISBNInputData.xlsx").getPath();
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) {
		log.log(Level.INFO, "Before the readUrlsFromExcel");
		Map<String, List<String>> mapList = readUrlsFromExcel();
		log.log(Level.INFO, "Map values", mapList);
		generateDataToExcel(mapList);
	}

	private static void generateDataToExcel(Map<String, List<String>> mapList) {
		log.log(Level.INFO, "INTO the generateDataToExcel", mapList);

		try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
			XSSFSheet worksheet = workbook.getSheet("ComparePrices");
			int rowNumber = 1;
			XSSFRow row1 = worksheet.createRow(0);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue("ISBN_NUMBER");
			Cell cell2 = row1.createCell(1);
			cell2.setCellValue("MDM ID");
			Cell cell3 = row1.createCell(2);
			cell3.setCellValue("AEM COST PRICES");
			Cell cell4 = row1.createCell(3);
			cell4.setCellValue("MDM LIST PRICES");
			Cell cell5 = row1.createCell(4);
			cell5.setCellValue("DW LISTPRICES");
			Cell cell6 = row1.createCell(5);
			cell6.setCellValue("MDMUnmanaged LISTPRICES");
			for (Map.Entry<String, List<String>> entry : mapList.entrySet()) {
				log.info("Item : " + entry.getKey() + " Count : " + entry.getValue());
				List<String> stringList = entry.getValue();
				XSSFRow row = worksheet.createRow(rowNumber);
				Cell cell = row.createCell(0);
				cell.setCellValue(entry.getKey());
				for (int j = 1; j < entry.getValue().size() + 1; j++) {

					cell = row.createCell(j);
					cell.setCellValue(stringList.get(j - 1));

				}
				rowNumber++;

			}
			// Save the workbook in .xls file
			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);
					){
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
			}
			// Save the workbook in .xls file
			
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

		}

	}
	private static MultiValueMap<String, String> getHeaders() {
		log.log(Level.INFO, "into the  getHeaders");

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");
		headers.add("Authorization", "Bearer " + "8b802a4f8fe513a6501f2abdab448275");
		return headers;
	}
	private static Map<String, List<String>> readUrlsFromExcel() {
		log.log(Level.INFO, "into the  readUrlsFromExcel");

		Map<String, List<String>> mapList = new HashMap<>();
		System.out.println(ClassLoader.getSystemClassLoader().getResource("").getPath());
		try (FileInputStream excelFile = new FileInputStream(FILE_NAME_READ);
				
				XSSFWorkbook workbook = new XSSFWorkbook(excelFile);) {
			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				String lastTwo = null;

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					if (!cell.getStringCellValue().isEmpty()) {
						String cellValue = cell.getStringCellValue();
						log.info(cellValue);
						if (cellValue != null && cellValue.length() >= 2) {
							lastTwo = cellValue.substring(cellValue.length() - 2);
						}

						mapList.put(cellValue, retreiveDataFromURLToCompare(cellValue, lastTwo));

					} else {
						log.log(Level.INFO, "into the  readUrlsFromExcel check whether the cell type is"
							);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.INFO, "into the exception in the readURL", e.fillInStackTrace());
		}
		return mapList;
	}
	private static List<String> retreiveDataFromURLToCompare(String cellValue, String lastTwo) {
		lastTwo = trimLeadingZeroes(lastTwo);

		
		// QA Unmanaged MDM (Boomerang API)
		String urlUnManagedMDM = "https://nonprod.api.scholastic.com/qa/ondemand/2.1/aso/" + cellValue +"tso-us?StoreID=tso-us" ;
		
	// 	SSO
		//String urlUnManagedMDM = "https://nonprod.api.scholastic.com/qa/ondemand/2.1/aso/" + cellValue +"sso-us?StoreID=tso-us" ;
		// https://nonprod.api.scholastic.com/qa/ondemand/2.1/aso/9781338121377tso-us?StoreID=tso-us
		//PROD Unmanaged MDM (Boomerang API)
		//https://api.scholastic.com/ondemand/2.1/aso/9780590659994tso-us?StoreID=tso-us
		
		//QA AEM
		//String urlOne = "https://www-qa2.scholastic.com/content/scholastic/books/aso/tso/" + lastTwo + "/" + cellValue
			//	+ "/data.json";
		
		String urlOne = "https://shop-stage.scholastic.com/content/scholastic/books/aso/tso/" + lastTwo + "/" + cellValue
				+ "/data.json";
		
		// PROD AEM
		// https://shop.scholastic.com/content/scholastic/books/aso/tso/75/9781338310375/data.json
        
		
		//SSO
		//String urlOne = "https://www-qa2.scholastic.com/content/scholastic/books/aso/sso/" + lastTwo + "/" + cellValue	+ "/data.json";
		
		
		// QA MDM Managed MDM API (STEP)
		String urlTwo = "https://nonprod.api.scholastic.com/qa/ondemand/2.1/product/2.1/isbn13/" + cellValue;
		// PROD MDM Managed MDM API (STEP)
		// https://api.scholastic.com/ondemand/2.1/product/2.1/isbn13/608369?channel=teacherStore
		//SSo
		//String urlTwo = "https://nonprod.api.scholastic.com/qa/ondemand/2.1/product/2.1/isbn13/" + cellValue;
		
		//QA Demandware
		//String urlThree = "https://development-rco-scholastic.demandware.net/s/Sites-tso-us-Site/dw/shop/v16_9/products/"
		//		+ cellValue + "-tso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8";
		
		
		String urlThree = "https://staging-rco-scholastic.demandware.net/s/Sites-tso-us-Site/dw/shop/v16_9/products/"
				+ cellValue + "-tso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8";
		
// https://development-rco-scholastic.demandware.net/s/Sites-tso-us-Site/dw/shop/v16_9/products/9781338139051-sso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8
	// PROD DW
	// https://staging.rco.scholastic.demandware.net/s/Sites-tso-us-Site/dw/shop/v16_9/products/9780545134477-tso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8

		List<String> list = new ArrayList<>();
		String splitData = getReponseEntity(urlOne);
		String[] zee = splitData.split(";");
		list.add(zee[0]);
		list.add(zee[1]);
		list.add(getReponseSecondEntity(urlTwo));
		list.add(getReponseThirdEntity(urlThree));
		list.add(getReponseUnManagedMDM(urlUnManagedMDM));
		log.log(Level.INFO, "get the list of data ", list);

		return list;
	}
	private static String getReponseEntity(String url) {
		log.log(Level.INFO, url, "URL in THE RESPONSE");
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		ResponseEntity<AEM_Data> exdate = null;
		try {
			exdate = template.exchange(url, HttpMethod.GET, null, AEM_Data.class);
			if (exdate.getBody().getListPrice() != null && exdate.getStatusCode().equals(HttpStatus.OK)) {
				jsonValue.append(exdate.getBody().getProductID()).append(";").append(exdate.getBody().getListPrice());
			} else {
				jsonValue.append(EMPTY_STRING).append(";").append(EMPTY_STRING);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonValue.append(EMPTY_STRING).append(";").append(EMPTY_STRING);
			// jsonValue.append(exdate.getBody().getProductID()).append(";").append(EMPTY_STRING);
			log.log(Level.INFO, "jsonValue in exception of responseSecondEntity" + jsonValue);
		}
		log.log(Level.INFO, jsonValue.toString());
		return jsonValue.toString();
	}

	private static String trimLeadingZeroes(String value) {
		while (value.length() > 1 && value.indexOf('0') == 0) {
			value = value.substring(1);
		}
		return value;
	}

	private static String getReponseSecondEntity(String url) {
		log.log(Level.INFO, "INTO getReponseSecondEntity", url);

		String jsonValue;
		RestTemplate template = new RestTemplate();

		try {
			ResponseEntity<Product> exdate = template.exchange(url, HttpMethod.GET,
					new HttpEntity<String>(getHeaders()), Product.class);

			if (exdate.getBody().getProduct().getData().getCorpListPrice() != null
					&& exdate.getStatusCode().equals(HttpStatus.OK)) {
				jsonValue = exdate.getBody().getProduct().getData().getCorpListPrice();
			} else {
				jsonValue = EMPTY_STRING;
			}
		} catch (Exception e) {
			e.printStackTrace();

			jsonValue = EMPTY_STRING;
			log.log(Level.INFO, jsonValue + "in exception");
		}
		log.log(Level.INFO, jsonValue);

		return jsonValue;
	}
	
	private static String getReponseThirdEntity(String url) {
		log.log(Level.INFO, "INTO getReponseSecondEntity", url);
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		try {
			ResponseEntity<DWData> exdate = template.exchange(url, HttpMethod.GET, null, DWData.class);
			if (exdate.getBody().getCListPrice() != null && exdate.getStatusCode().equals(HttpStatus.OK)) {
				jsonValue.append(String.valueOf(exdate.getBody().getCListPrice()));
			} else {
				jsonValue.append(EMPTY_STRING);
			}
		} catch (Exception e) {
			jsonValue.append(EMPTY_STRING);
			log.log(Level.INFO, jsonValue + "in exception");
		}
		return jsonValue.toString();
	}
	
	private static String getReponseUnManagedMDM(String url) {
		System.out.println(url);
		log.log(Level.INFO, "INTO getReponseSecondEntity", url);
		
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		try {
			ResponseEntity<UnManagedMDM> exdate = template.exchange(url, HttpMethod.GET, new HttpEntity<String>(getHeaders()), UnManagedMDM.class);
			if (exdate.getBody().getRecord().getData().getListPrice() != null && exdate.getStatusCode().equals(HttpStatus.OK)) {
				System.out.println(exdate.getBody().getRecord().getData().getListPrice());
				jsonValue.append(String.valueOf(exdate.getBody().getRecord().getData().getListPrice()));
			} else {
				jsonValue.append(EMPTY_STRING);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonValue.append(EMPTY_STRING);
			log.log(Level.INFO, jsonValue + "in exception");
		}
		return jsonValue.toString();
	}
	
	private static MultiValueMap<String, String> getUnManagedMDM() {
		log.log(Level.INFO, "into the  getHeaders");

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");
		headers.add("Authorization", "Bearer " + "8b802a4f8fe513a6501f2abdab448275");
		return headers;
	}
}
