package com.scholastic.cs.pricecompare.BVFeed;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Vector;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class DownloadFromFtp {
	
	private static final String PATHSEPARATOR = "/";
static ChannelSftp sftpChannel=null;

	 
	 public static void main(String[] args) {
         DownloadFromFtp downloadFTP = new DownloadFromFtp();
         downloadFTP.downloadFromFTP();
   }

   private void downloadFromFTP() {
	   String sourcePath = "/testimport";
	   String destinationPath = "C:\\Users\\VenkaSee\\Downloads\\BVFEED";
	   JSch jsch = new JSch();
       Session session = null;
       try {
           session = jsch.getSession("scholastic", "sftp-stg.bazaarvoice.com", 22);
           session.setConfig("StrictHostKeyChecking", "no");
           session.setPassword("99u5r@ck!");
           session.connect();

      
     //      location: /testimport
           
           
           
           Channel channel = session.openChannel("sftp");
           channel.connect();
            sftpChannel = (ChannelSftp) channel;
            sftpChannel.cd(sourcePath);
           System.out.println(sftpChannel.pwd());
           recursiveFolderDownload(sourcePath,destinationPath);
           System.out.println("all the files been copied to local machinepath---a..."+destinationPath);
       } catch (JSchException e) {
           e.printStackTrace();  
       } catch (SftpException e) {
           e.printStackTrace();
       }finally {
    	   session.disconnect();
       }
       
       
    		   
   }
   
   
   
   /**
    * This method is called recursively to download the folder content from SFTP server
    * 
    * @param sourcePath
    * @param destinationPath
    * @throws SftpException
    */
   @SuppressWarnings("unchecked")
   private static void recursiveFolderDownload(String sourcePath, String destinationPath) throws SftpException {
       Vector<ChannelSftp.LsEntry> fileAndFolderList = sftpChannel.ls(sourcePath); // Let list of folder content
       
       //Iterate through list of folder content
       for (ChannelSftp.LsEntry item : fileAndFolderList) {
           
           if (!item.getAttrs().isDir()) { // Check if it is a file (not a directory).
               if (!(new File(destinationPath + PATHSEPARATOR + item.getFilename())).exists()
                       || (item.getAttrs().getMTime() > Long
                               .valueOf(new File(destinationPath + PATHSEPARATOR + item.getFilename()).lastModified()
                                       / (long) 1000)
                               .intValue())) { // Download only if changed later.

                   new File(destinationPath + PATHSEPARATOR + item.getFilename());
                   sftpChannel.get(sourcePath + PATHSEPARATOR + item.getFilename(),
                           destinationPath + PATHSEPARATOR + item.getFilename()); // Download file from source (source filename, destination filename).
               }
           } else if (!(".".equals(item.getFilename()) || "..".equals(item.getFilename()))) {
               new File(destinationPath + PATHSEPARATOR + item.getFilename()).mkdirs(); // Empty folder copy.
               recursiveFolderDownload(sourcePath + PATHSEPARATOR + item.getFilename(),
                       destinationPath + PATHSEPARATOR + item.getFilename()); // Enter found folder on server to read its contents and create locally.
           }
       }
   }

}

