package com.scholastic.cs.pricecompare.BVFeed;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.scholastic.cs.pricecompare.BVFeed.Feed;
import com.scholastic.cs.pricecompare.BVFeed.Feed.Products.Product;

public class BVFeedController {
	
	private static final Logger log = Logger.getLogger(BVFeedController.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("BVFEED.xlsx")
			.getPath();
	private static final String EMPTY_STRING = "empty";
	
	public static void main(String[] args) {

		 try {

		//	File file = new File("C:\\Users\\venkasee\\Desktop\\BV-Feed-2018-04-30-15-58-51.xml");
		//   bug 	File file = new File("C:\\Users\\VenkaSee\\Downloads\\BVFEED\\BV-Feed-2018-06-07-11-30-25.xml");
			File file = new File("C:\\Users\\VenkaSee\\Downloads\\BVFEED\\BV-Feed-2018-06-13-14-30-26.xml");
			
			JAXBContext jaxbContext = JAXBContext.newInstance(Feed.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Feed customer = (Feed) jaxbUnmarshaller.unmarshal(file);
			List<String> externalIdsList=new ArrayList<String>();
			List<String> categoryExternalIdsList=new ArrayList<String>();
			List<String> descriptionList=new ArrayList<String>();
			System.out.println(customer.getProducts().getProduct().size());
			List<Product> li=customer.getProducts().getProduct();	   
			   for (Product product : li) {
				   externalIdsList.add(product.getExternalId());
				   descriptionList.add(product.getDescription());
				   categoryExternalIdsList.add(product.getCategoryExternalId());
			    System.out.println(product.getExternalId()+"<-ExternalId->------<-Description->"+product.getDescription()+"--getCategoryExternalId---"+product.getCategoryExternalId());
			    
			     
			   }
			   generateDataToExcel(externalIdsList,descriptionList,categoryExternalIdsList);

		  } catch (JAXBException e) {
			e.printStackTrace();
		  }
	}
		 
		 private static void generateDataToExcel(List<String> externalIdList,List<String> descriptionsList,List<String> categoryExternalIdsList) {
				try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);
						XSSFWorkbook workbook = new XSSFWorkbook();) {
					XSSFSheet worksheet = workbook.createSheet("BVFEEDXML");
					XSSFRow row1 = worksheet.createRow(0);
					Cell cell1 = row1.createCell(0);
					cell1.setCellValue("BAZARVOICE");
					
					for (int i = 0; i < externalIdList.size(); i++) {
						
						XSSFRow row = worksheet.createRow(i + 1);
						row.createCell(0).setCellValue(externalIdList.get(i));
						row.createCell(1).setCellValue(descriptionsList.get(i));
						row.createCell(2).setCellValue(categoryExternalIdsList.get(i));
						
						
					}
					// Save the workbook in .xls file
					workbook.write(fos);
					fos.flush();
					log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
				} catch (FileNotFoundException e) {
					e.getMessage();
					log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

				} catch (IOException e) {
					log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

				}

			}

		} 


