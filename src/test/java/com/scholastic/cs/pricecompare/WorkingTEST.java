package com.scholastic.cs.pricecompare;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.scholastic.cs.pricecompare.MDMTOJSON.GRWEContributorArt;
import com.scholastic.cs.pricecompare.MDMTOJSON.GRWEContributorText;
import com.scholastic.cs.pricecompare.MDMTOJSON.GRWEPSeries;
import com.scholastic.cs.pricecompare.MDMTOJSON.GoldenRecordSource;
import com.scholastic.cs.pricecompare.MDMTOJSON.Product;
import com.scholastic.cs.pricecompare.MDMTOJSON.WEPProperty;
import com.scholastic.cs.pricecompare.aem.AEM_Data;
import com.scholastic.cs.pricecompare.dw.DWData;
import com.scholastic.cs.pricecompare.unmanagedMdm.UnManagedMDM;

public class WorkingTEST {
	private static final Logger log = Logger.getLogger(SSOMDMIntegration.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("SSOMDMOutput.xlsx")
			.getPath();
	private static final String FILE_NAME_READ = ClassLoader.getSystemClassLoader().getResource("SSOMDMInput.xlsx")
			.getPath();
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) {
		Object obj = new Object();
		log.log(Level.INFO, "Before the readUrlsFromExcel");
		List<String> cellValuesList = readUrlsFromExcel();
		log.log(Level.INFO, "Map values", cellValuesList);
		synchronized (obj) {
			getReponseMDMEntity(cellValuesList);
			getReponseDemandWare(cellValuesList);
			getReponseUnManagedMDM(cellValuesList);
			getAEMReponseEntity(cellValuesList);

			log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");

			// http://nonprod.api.scholastic.com/qa/ondemand/2.1/aso/9780590409230sso-us?StoreID=sso-us
		}

	}

	private static void generateDataToExcel(Map<String, List<String>> mapList, String sheetName, String[] cellHeading) {
		log.log(Level.INFO, "INTO the generateDataToExcel", mapList);

		try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
			XSSFSheet worksheet = workbook.getSheet(sheetName);
			int rowNumber = 1;
			XSSFRow row1 = worksheet.createRow(0);
			for (int i = 0; i <= cellHeading.length - 1; i++) {

				Cell celli = row1.createCell(i);
				celli.setCellValue(cellHeading[i]);
			}

			for (Map.Entry<String, List<String>> entry : mapList.entrySet()) {
				log.info("Item : " + entry.getKey() + " Count : " + entry.getValue());
				List<String> stringList = entry.getValue();
				XSSFRow row = worksheet.createRow(rowNumber);
				/*Cell cell = row.createCell(0);
				cell.setCellValue(entry.getKey());*/
				for (int j = 1; j < entry.getValue().size() + 1; j++) {
					Cell	cell = row.createCell(j-1);
					cell.setCellValue(stringList.get(j-1));

				}
				rowNumber++;

			}
			// Save the workbook in .xls file
			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);) {
				workbook.write(fos);
				fos.flush();
			}
			// Save the workbook in .xls file

		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			e.printStackTrace();
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

		}catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static MultiValueMap<String, String> getHeaders() {
		log.log(Level.INFO, "into the  getHeaders");

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");
		headers.add("Authorization", "Bearer " + "8b802a4f8fe513a6501f2abdab448275");
		return headers;
	}

	private static List<String> readUrlsFromExcel() {
		log.log(Level.INFO, "into the  readUrlsFromExcel");
		List<String> cellValuesList = new ArrayList<>();
		// Map<String, List<String>> mapList = new HashMap<>();
		System.out.println(ClassLoader.getSystemClassLoader().getResource("").getPath());
		try (FileInputStream excelFile = new FileInputStream(FILE_NAME_READ);

				XSSFWorkbook workbook = new XSSFWorkbook(excelFile);) {
			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					if (!cell.getStringCellValue().isEmpty()) {
						String cellValue = cell.getStringCellValue();
						log.info(cellValue);

						cellValuesList.add(cellValue);

					} else {
						log.log(Level.INFO, "into the  readUrlsFromExcel check whether the cell type is");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.INFO, "into the exception in the readURL", e.fillInStackTrace());
		}
		return cellValuesList;
	}

	private static String getAEMReponseEntity(List<String> cellValuesList) {
		log.log(Level.INFO, "URL in THE RESPONSE");
		String lastTwo = null;
		List<String> addlist;
		
	
		String[] cellHeading = { 	"ISBN13",
		"ISBN",
		"BorderID",
		"HighAge",
		"LowAge",
		"Brand",
		"BudgetCode",
		"LongDescription",
		"DisplayTitle",
		"BookType",
		"ListPrice",
		"OurPrice",
		"SSOFormat",
		"ProductStartDate",
		"BackOrderFlag",
		"SSOClass",
		"SubClass",
		"Department",
		"FictionType",
		"HighGrade",
		"LowGrade",
		"WorkID",
		"Language",
		"ProductType",
		"Publisher",
		"InventoryThreshold",
		"Manufacturer",
		"Pages",
		"Keywords",
		"Illustrator",
		"NumberofAlternateImages",
		"Searchable",
		"CharactersandSeries",
		"ShippingMessagePDP",
		"UnitCost",
		"ViewableFlag",
		"Weight",
		"NumberofSeeInsidePages",
		"Depth",
		"Height",
		"Width",
		"Shops",
		"Vendor1",};


		Map<String, List<String>> mapList = new HashMap<>();
		for (String cellValue : cellValuesList) {
			addlist = new ArrayList<>();
			if (cellValue != null && cellValue.length() >= 2) {
				lastTwo = cellValue.substring(cellValue.length() - 2);
			}
			lastTwo = trimLeadingZeroes(lastTwo);
			String AEM_URL = "https://www-qa2.scholastic.com/content/scholastic/books/aso/sso/" + lastTwo + "/"
					+ cellValue + "/data.json";
			StringBuilder jsonValue = new StringBuilder();
			RestTemplate template = new RestTemplate();
			ResponseEntity<AEM_Data> exdate = null;
			try {
				exdate = template.exchange(AEM_URL, HttpMethod.GET, null, AEM_Data.class);

				if (exdate.getStatusCode().equals(HttpStatus.OK)) {

							
					addlist.add(exdate.getBody().getISBN13().isEmpty() ? "EMPTY" : exdate.getBody().getISBN13());
					addlist.add(exdate.getBody().getISBN().isEmpty() ? "EMPTY" : exdate.getBody().getISBN());
					addlist.add(exdate.getBody().getBorderID().isEmpty() ? "EMPTY" : exdate.getBody().getBorderID());
					addlist.add(exdate.getBody().getHighAge().isEmpty() ? "EMPTY" : exdate.getBody().getHighAge());
					addlist.add(exdate.getBody().getLowAge().isEmpty() ? "EMPTY" : exdate.getBody().getLowAge());
					addlist.add(exdate.getBody().getBrand().isEmpty() ? "EMPTY" : exdate.getBody().getBrand());
					addlist.add(exdate.getBody().getBudgetCode().isEmpty() ? "EMPTY" : exdate.getBody().getBudgetCode());
					addlist.add(exdate.getBody().getLongDescription().isEmpty() ? "EMPTY" : exdate.getBody().getLongDescription());
					addlist.add(exdate.getBody().getDisplayTitle().isEmpty() ? "EMPTY" : exdate.getBody().getDisplayTitle());
					addlist.add(exdate.getBody().getBookType().isEmpty() ? "EMPTY" : exdate.getBody().getBookType());
					addlist.add(exdate.getBody().getListPrice().isEmpty() ? "EMPTY" : exdate.getBody().getListPrice());
					addlist.add(exdate.getBody().getOurPrice().isEmpty() ? "EMPTY" : exdate.getBody().getOurPrice());
					addlist.add(exdate.getBody().getsSOFormat().isEmpty() ? "EMPTY" : exdate.getBody().getsSOFormat());
					addlist.add(exdate.getBody().getProductStartDate().isEmpty() ? "EMPTY" : exdate.getBody().getProductStartDate());
					addlist.add(exdate.getBody().getBackOrderFlag().isEmpty() ? "EMPTY" : exdate.getBody().getBackOrderFlag());
					addlist.add(exdate.getBody().getsSOClass().isEmpty() ? "EMPTY" : exdate.getBody().getsSOClass());
					addlist.add(exdate.getBody().getSubClass().isEmpty() ? "EMPTY" : exdate.getBody().getSubClass());
					addlist.add(exdate.getBody().getDepartment().isEmpty() ? "EMPTY" : exdate.getBody().getDepartment());
					addlist.add(exdate.getBody().getFictionType().isEmpty() ? "EMPTY" : exdate.getBody().getFictionType());
					addlist.add(exdate.getBody().getHighGrade().isEmpty() ? "EMPTY" : exdate.getBody().getHighGrade());
					addlist.add(exdate.getBody().getLowGrade().isEmpty() ? "EMPTY" : exdate.getBody().getLowGrade());
					addlist.add(exdate.getBody().getWorkID().isEmpty() ? "EMPTY" : exdate.getBody().getWorkID());
					addlist.add(exdate.getBody().getLanguage().isEmpty() ? "EMPTY" : exdate.getBody().getLanguage());
					addlist.add(exdate.getBody().getProductType().isEmpty() ? "EMPTY" : exdate.getBody().getProductType());
					addlist.add(exdate.getBody().getPublisher().isEmpty() ? "EMPTY" : exdate.getBody().getPublisher());
					addlist.add(exdate.getBody().getInventoryThreshold().isEmpty() ? "EMPTY" : exdate.getBody().getInventoryThreshold());
					addlist.add(exdate.getBody().getManufacturer().isEmpty() ? "EMPTY" : exdate.getBody().getManufacturer());
					addlist.add(exdate.getBody().getPages().isEmpty() ? "EMPTY" : exdate.getBody().getPages());
					addlist.add(exdate.getBody().getKeywords().isEmpty() ? "EMPTY" : exdate.getBody().getKeywords());
					addlist.add(exdate.getBody().getIllustrator().isEmpty() ? "EMPTY" : exdate.getBody().getIllustrator());
					addlist.add(exdate.getBody().getNumberofAlternateImages().isEmpty() ? "EMPTY" : exdate.getBody().getNumberofAlternateImages());
					addlist.add(exdate.getBody().getSearchable().isEmpty() ? "EMPTY" : exdate.getBody().getSearchable());
					addlist.add(exdate.getBody().getCharactersandSeries().isEmpty() ? "EMPTY" : exdate.getBody().getCharactersandSeries());
					addlist.add(exdate.getBody().getShippingMessagePDP().isEmpty() ? "EMPTY" : exdate.getBody().getShippingMessagePDP());
					addlist.add(exdate.getBody().getUnitCost().isEmpty() ? "EMPTY" : exdate.getBody().getUnitCost());
					addlist.add(exdate.getBody().getViewableFlag().isEmpty() ? "EMPTY" : exdate.getBody().getViewableFlag());
					addlist.add(exdate.getBody().getWeight().isEmpty() ? "EMPTY" : exdate.getBody().getWeight());
					addlist.add(exdate.getBody().getNumberofSeeInsidePages().isEmpty() ? "EMPTY" : exdate.getBody().getNumberofSeeInsidePages());
					addlist.add(exdate.getBody().getDepth().isEmpty() ? "EMPTY" : exdate.getBody().getDepth());
					addlist.add(exdate.getBody().getHeight().isEmpty() ? "EMPTY" : exdate.getBody().getHeight());
					addlist.add(exdate.getBody().getWidth().isEmpty() ? "EMPTY" : exdate.getBody().getWidth());
					addlist.add(exdate.getBody().getShops().isEmpty() ? "EMPTY" : exdate.getBody().getShops());
					addlist.add(exdate.getBody().getVendor1().isEmpty() ? "EMPTY" : exdate.getBody().getVendor1());


				} else {
					addlist.add(EMPTY_STRING);
				}
				mapList.put(cellValue, addlist);
			} catch (Exception e) {
				addlist = new ArrayList<>();
				addlist.add(EMPTY_STRING);
				// jsonValue.append(exdate.getBody().getProductID()).append(";").append(EMPTY_STRING);
				mapList.put(cellValue, addlist);
				e.printStackTrace();

				log.log(Level.INFO, "jsonValue in exception of responseSecondEntity" + jsonValue);
			}

		}
		generateDataToExcel(mapList, "AEMDATA", cellHeading);
		return null;
	}

	private static String trimLeadingZeroes(String value) {
		while (value.length() > 1 && value.indexOf('0') == 0) {
			value = value.substring(1);
		}
		return value;
	}

	/**
	 * if comment is double quote{//} then the value is not in json value if comment
	 * is doubledouble{////} quote then the return value is not string
	 * 
	 * @param cellValuesList
	 * @return
	 */
	
		private static String getReponseMDMEntity(List<String> cellValuesList) {
			log.log(Level.INFO, "INTO getReponseSecondEntity", cellValuesList);
			String[] cellHeading = { "ISBN13","ISBN10","BorderID","ScholasticStoreAgeHigh","ScholasticStoreAgeLow","ScholasticStoreBrand","ScholasticStoreBudCode",
					"ScholasticStoreLongDescriptionByOwner","ScholasticStoreTitleByOwner","ScholasticStoreBookType","ScholasticStoreListPrice",
					"ScholasticStoreOurPrice","ScholasticStoreSSOFormat","ScholasticStoreProductStartDate","ScholasticStoreBackOrderFlag","ScholasticStoreSSOClass",
					"ScholasticStoreSubClass","ScholasticStoreDepartment","fictionNonfiction","ScholasticStoreGradeHigh","ScholasticStoreGradeLow","ScholasticStoreWorkID",
					"Language",	"ScholasticStoreProductType","ScholasticStorePublisher","ScholasticStoreInventoryThreshold","ScholasticStoreManufacturer","PageCount",
					"ScholasticStoreKeywords","GRWEContributorArt","scholasticStoreNumberOfAlternateImages",
					"ScholasticStoreSearchableFlag","ScholasticStoreCharacterAndSeries","ScholasticStoreShippingMessagePDP","ScholasticStoreUnitCost",
					"ScholasticStoreViewableFlag","WeightPounds","scholasticStoreNumberOfSeeInsidePages","Trim_Size_Length","Trim_Size_Height",
					"Trim_Size_Width","ScholasticStoreShops","ScholasticStoreVendor1","Planning_ISBN","sourceSystem","formattedISBN","formattedISBN10"};
		
	
		RestTemplate template = new RestTemplate();
		List<String> listAdd;
		Map<String, List<String>> mapList = new HashMap<>();
		for (String cellValue : cellValuesList) {
			listAdd = new ArrayList<>();
			// QA MDM Managed MDM API (STEP)
			String MDM_URL = "https://nonprod.api.scholastic.com/qa/ondemand/2.1/product/2.1/isbn13/" + cellValue
					+ "?channel=scholasticStore";
			try {
				ResponseEntity<Product> exdate = template.exchange(MDM_URL, HttpMethod.GET,
						new HttpEntity<String>(getHeaders()), Product.class);

				if (exdate.getStatusCode().equals(HttpStatus.OK)) {
				
					System.out.println("----into floww *****************************************----");
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getISBN13())? "EMPTY": exdate.getBody().getProduct().getData().getISBN13());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getISBN10())? "EMPTY": exdate.getBody().getProduct().getData().getISBN10());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getBorderID())? "EMPTY": exdate.getBody().getProduct().getData().getBorderID());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreAgeHigh())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreAgeHigh());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreAgeLow())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreAgeLow());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreBrand())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreBrand());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreBudgetCode())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreBudgetCode());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreLongDescriptionByOwner())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreLongDescriptionByOwner());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreTitleByOwner())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreTitleByOwner());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreBookType())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreBookType());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreListPrice())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreListPrice());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreOurPrice())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreOurPrice());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreSSOFormat())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreSSOFormat());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreProductStartDate())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreProductStartDate());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreBackOrderFlag())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreBackOrderFlag());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreSSOClass())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreSSOClass());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreSubClass())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreSubClass());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreDepartment())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreDepartment());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getFictionNonfiction())? "EMPTY": exdate.getBody().getProduct().getData().getFictionNonfiction());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreGradeHigh())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreGradeHigh());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreGradeLow())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreGradeLow());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreWorkID())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreWorkID());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getLanguage())? "EMPTY": exdate.getBody().getProduct().getData().getLanguage());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreProductType())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreProductType());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStorePublisher())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStorePublisher());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreInventoryThreshold())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreInventoryThreshold());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreManufacturer())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreManufacturer());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getPageCount())? "EMPTY": exdate.getBody().getProduct().getData().getPageCount());
					listAdd.add(groupOfStringData(org.apache.commons.collections4.ListUtils.emptyIfNull(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreKeywords())));
					listAdd.add(listOfStringData(exdate.getBody().getProduct().getData().getGRWEContributorText()));
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreNumberOfAlternateImages())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreNumberOfAlternateImages());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreSearchableFlag())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreSearchableFlag());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreCharacterAndSeries())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreCharacterAndSeries());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreShippingMessagePDP())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreShippingMessagePDP());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreUnitCost())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreUnitCost());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreViewableFlag())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreViewableFlag());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getWeightPounds())? "EMPTY": exdate.getBody().getProduct().getData().getWeightPounds());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreNumberOfSeeInsidePages())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreNumberOfSeeInsidePages());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getTrimSizeLength())? "EMPTY": exdate.getBody().getProduct().getData().getTrimSizeLength());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getTrimSizeHeight())? "EMPTY": exdate.getBody().getProduct().getData().getTrimSizeHeight());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getTrimSizeWidth())? "EMPTY": exdate.getBody().getProduct().getData().getTrimSizeWidth());
					listAdd.add(groupOfStringData(org.apache.commons.collections4.ListUtils.emptyIfNull(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreShops())));
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreVendor1())? "EMPTY": exdate.getBody().getProduct().getData().getChannels().getScholasticStore().getScholasticStoreVendor1());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getPlanningISBN())? "EMPTY": exdate.getBody().getProduct().getData().getISBN13());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getSourceSystem())? "EMPTY": exdate.getBody().getProduct().getData().getISBN13());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getFormattedISBN())? "EMPTY": exdate.getBody().getProduct().getData().getISBN13());
					listAdd.add(StringUtils.isEmpty(exdate.getBody().getProduct().getData().getFormattedISBN10())? "EMPTY": exdate.getBody().getProduct().getData().getISBN13());
					


				

		
				} else {
					listAdd.add(EMPTY_STRING);
				}
				System.out.println(cellValue);
				System.out.println(listAdd);
				mapList.put(cellValue, listAdd);
			} catch (Exception e) {
				e.printStackTrace();
				listAdd = new ArrayList<>();
				listAdd.add(EMPTY_STRING);
				mapList.put(cellValue, listAdd);
				log.log(Level.INFO, listAdd + "in exception");
			}
		}
		System.out.println(mapList);

		generateDataToExcel(mapList, "Managed_MDM", cellHeading);

		return null;
	}

	/*
	 * single pi at end of each append indicates continuation
	 * double pi at end of each append indicates end of data
	 */
	private static String listOfStringData(List<?> listOfStringData) {
		StringBuilder listOfRecords= new StringBuilder();
		for ( Object listObject : listOfStringData) {
			if(listObject instanceof GoldenRecordSource) {
				listOfRecords.append("id:"+(((GoldenRecordSource)listObject).getId())).append("|");
				listOfRecords.append("Source:"+(((GoldenRecordSource)listObject).getSource())).append("||");
			}else if(listObject instanceof GRWEContributorText) {
				
					listOfRecords.append("sequenceNumber:"+(((GRWEContributorText)listObject).getSequenceNumber())).append("|");
					listOfRecords.append("role:"+(((GRWEContributorText)listObject).getRole())).append("|");
					listOfRecords.append("role:"+(((GRWEContributorText)listObject).getId())).append("|");	
					listOfRecords.append("Link:"+(((GRWEContributorText)listObject).getLink())).append("|");		
					listOfRecords.append("sequenceForEpic:"+(((GRWEContributorText)listObject).getSequenceForEpic())).append("||");
		
			}else if(listObject instanceof GRWEContributorArt) {		
				listOfRecords.append("sequenceNumber:"+(((GRWEContributorArt)listObject).getSequenceNumber())).append("|");
				listOfRecords.append("role:"+(((GRWEContributorArt)listObject).getRole())).append("|");
				listOfRecords.append("role:"+(((GRWEContributorArt)listObject).getId())).append("|");		
				listOfRecords.append("Link:"+(((GRWEContributorArt)listObject).getLink())).append("|");		
				listOfRecords.append("sequenceForEpic:"+(((GRWEContributorArt)listObject).getSequenceForEpic())).append("||");

						
			}else if(listObject instanceof WEPProperty) {
				
				
				listOfRecords.append("role:"+(((WEPProperty)listObject).getId())).append("|");
				
				listOfRecords.append("PropertyCode:"+(((WEPProperty)listObject).getPropertyCode())).append("|");
				
				listOfRecords.append("PropertyTitle:"+(((WEPProperty)listObject).getPropertyTitle())).append("||");
	
				
				
				
		}else if(listObject instanceof GRWEPSeries) {
			
			
			listOfRecords.append("role:"+(((GRWEPSeries)listObject).getId())).append("|");
			
			listOfRecords.append("Link:"+(((GRWEPSeries)listObject).getLink())).append("||");
			
	
			
			
			
	}else {
		listOfRecords.append("EMPTY");
	}

			
		}
		return listOfRecords.toString();
	}

	private static String groupOfStringData(List<String> listOfStringData) {
		StringBuilder GrpOfStringData= new StringBuilder();
		try {
			for (String stringData : listOfStringData) {
				GrpOfStringData.append(stringData);
				if(listOfStringData.size()>1) {
					GrpOfStringData.append(",");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			GrpOfStringData.append("EMPTY");
		}
		
		return GrpOfStringData.toString();
	}

	private static String getReponseDemandWare(List<String> cellValuesList) {
		log.log(Level.INFO, "INTO getReponseSecondEntity");
		StringBuilder jsonValue = new StringBuilder();
		
		String[] cellHeading = { 	"CIsbn13",
				"Cisbn",
				"CBorderId",
				"CHighAge",
				"CLowAge",
				"Brand",
				"BudgetCode",
				"LongDescription",
				"CDisplayTitle",
				"CBookType",
				"CListPrice",
				"//ourPrice",
				"CFormat",
				"//online-from (productStartDate)",
				"CBackOrderFlag",
				"CClassValue",
				"CSubClass",
				"CDepartment",
				"CFictionType",
				"CHighGradeTSOonly",
				"CLowGradeTSOonly",
				"CWorkId",
				"CLanguage",
				"CProductType",
				"CPublisher",
				"CInventoryThreshold",
				"ManufacturerName",
				"CPages",
				"CKeywords",
				"CIllustrator",
				"CNumberofAlternateImages",
				"CSearchable",
				"CCharacterAndSeries",
				"CShippingMessagePDP",
				"CUnitCost",
				"//available-flag",
				"CWeight",
				"//numberofSeeInsidePages",
				"CDepth",
				"CHeight",
				"CWidth",
				"CShops",
				"CVendor1",};
		
		
		
		
		
		
		RestTemplate template = new RestTemplate();
		Map<String, List<String>> mapList = new HashMap<>();
		List<String> listAdd;
		System.out.println("cellValuesList--->>>" + cellValuesList);
		for (String cellvalue : cellValuesList) {
			try {
				// QA Demandware
				listAdd = new ArrayList<>();
				String cellSSalue=cellvalue;
				String demandWareURL = "https://development-rco-scholastic.demandware.net/s/Sites-sso-us-Site/dw/shop/v16_9/products/"
						+ cellvalue + "-sso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8";
				ResponseEntity<DWData> exdate = template.exchange(demandWareURL, HttpMethod.GET, null, DWData.class);
				if (exdate.getStatusCode().equals(HttpStatus.OK)) {

					
					
					listAdd.add(String.valueOf(exdate.getBody().getCIsbn13()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCIsbn13()));
					listAdd.add(String.valueOf(exdate.getBody().getCIsbn()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCIsbn()));
					listAdd.add(String.valueOf(exdate.getBody().getCBorderId()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCBorderId()));
					listAdd.add(String.valueOf(exdate.getBody().getCHighAge()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCHighAge()));
					listAdd.add(String.valueOf(exdate.getBody().getCLowAge()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCLowAge()));
				//	listAdd.add(String.valueOf(exdate.getBody().get().isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().get()));
					listAdd.add("EMPTY");
				//	listAdd.add(String.valueOf(exdate.getBody().get().isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().get()));
					listAdd.add("EMPTY");
					listAdd.add(String.valueOf(exdate.getBody().getLongDescription()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getLongDescription()));
					listAdd.add(String.valueOf(exdate.getBody().getCDisplayTitle()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCDisplayTitle()));
					listAdd.add(String.valueOf(exdate.getBody().getCBookType()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCBookType()));
					listAdd.add(String.valueOf(exdate.getBody().getCListPrice()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCListPrice()));
					//listAdd.add(String.valueOf(exdate.getBody().getourPrice().isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getourPrice()));
					listAdd.add("OUR PRICE Flag not defined");
					listAdd.add(String.valueOf(exdate.getBody().getCFormat()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCFormat()));
				//	listAdd.add(String.valueOf(exdate.getBody().getonline().isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().get//online-from (productStartDate)()));
					listAdd.add("online product  Flag not defined");
					listAdd.add(String.valueOf(exdate.getBody().getCBackOrderFlag()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCBackOrderFlag()));
					listAdd.add(String.valueOf(exdate.getBody().getCClassValue()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCClassValue()));
					listAdd.add(String.valueOf(exdate.getBody().getCSubClass()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCSubClass()));
					listAdd.add(String.valueOf(exdate.getBody().getCDepartment()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCDepartment()));
					listAdd.add(String.valueOf(exdate.getBody().getCFictionType()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCFictionType()));
					listAdd.add(String.valueOf(exdate.getBody().getCHighGradeTSOonly()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCHighGradeTSOonly()));
					listAdd.add(String.valueOf(exdate.getBody().getCLowGradeTSOonly()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCLowGradeTSOonly()));
					listAdd.add(String.valueOf(exdate.getBody().getCWorkId()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCWorkId()));
					listAdd.add(String.valueOf(exdate.getBody().getCLanguage()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCLanguage()));
					listAdd.add(String.valueOf(exdate.getBody().getCProductType()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCProductType()));
					listAdd.add(String.valueOf(exdate.getBody().getCPublisher()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCPublisher()));
					listAdd.add(String.valueOf(exdate.getBody().getCInventoryThreshold()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCInventoryThreshold()));
					listAdd.add(String.valueOf(exdate.getBody().getManufacturerName()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getManufacturerName()));
					listAdd.add(String.valueOf(exdate.getBody().getCPages()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCPages()));
					listAdd.add(String.valueOf(exdate.getBody().getCKeywords()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCKeywords()));
					listAdd.add(String.valueOf(exdate.getBody().getCIllustrator()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCIllustrator()));
					listAdd.add(String.valueOf(exdate.getBody().getCNumberofAlternateImages()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCNumberofAlternateImages()));
					listAdd.add(String.valueOf(exdate.getBody().getCSearchable()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCSearchable()));
					listAdd.add(String.valueOf(exdate.getBody().getCCharacterAndSeries()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCCharacterAndSeries()));
					listAdd.add(String.valueOf(exdate.getBody().getCShippingMessagePDP()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCShippingMessagePDP()));
					listAdd.add(String.valueOf(exdate.getBody().getCUnitCost()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCUnitCost()));
					//listAdd.add(String.valueOf(exdate.getBody().getavailableflag().isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getavailable-flag()));
					listAdd.add("Avaliable Flag not defined");
					listAdd.add(String.valueOf(exdate.getBody().getCWeight()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCWeight()));
					//listAdd.add(String.valueOf(exdate.getBody().getnumberofSeeInsidePages()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getnumberofSeeInsidePages()));
					listAdd.add("numberofSeeInsidePages not defined");
					listAdd.add(String.valueOf(exdate.getBody().getCDepth()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCDepth()));
					listAdd.add(String.valueOf(exdate.getBody().getCHeight()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCHeight()));
					listAdd.add(String.valueOf(exdate.getBody().getCWidth()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCWidth()));
					listAdd.add(String.valueOf(exdate.getBody().getCShops()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCShops()));
					listAdd.add(String.valueOf(exdate.getBody().getCVendor1()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getCVendor1()));
								
				} else {
					listAdd.add(EMPTY_STRING);
				}
				mapList.put(cellvalue, listAdd);
			} catch (Exception e) {
				e.printStackTrace();
				listAdd = new ArrayList<>();
				listAdd.add(e.getLocalizedMessage());
				mapList.put(cellvalue, listAdd);
				log.log(Level.INFO, jsonValue + "in exception");
			}

		}
		generateDataToExcel(mapList, "Demand_Ware", cellHeading);

		return null;
	}

	private static String getReponseUnManagedMDM(List<String> cellValuesList) {

		log.log(Level.INFO, "INTO getReponseSecondEntity", cellValuesList);
/*		String[] cellHeading = {"ISBN_NUMBER", "Active","ISBN 13","Title","SSO Format","Department","SSO Class","Sub Class","Shops","Our Price","List Price","Sale Price",
				"Viewable Flag","Searchable","Long Description","Border ID","Inventory Threshold","Back Order Flag","AvailabilityDate(Presell)",
				"Budget Code","Height","Width","Weight","Author","Editor","Narrator","Publisher","Warning List","Standard Rating","Award List",
				"Pages","Keywords","Brand","Genre","Characters and Series","Subject","Collection Titles","Cross-Sell ISBNs","Book Type","Language",
				"High Age","Low Age","High Grade","Low Grade","High Lexile Level","Low Lexile Level","High DRA Level","Low DRA Level",
				"High Scholastic Level","Low Scholastic Level","Low Guided Reading Level","High Guided Reading Level","Low Accelerated Reader ",
				"High Accelerated Reader","Multimedia","Unit Cost","ISBN","Work ID","Manufacturer Part Number","UPC","Vendor 1","Manufacturer",
				"Legacy Product Type","Component Listing","Depth","Extra Large Image","Extra Small Image","Large Image","Small Image","Fiction Type",
				"Format Description","Show DRA Level","Show Guided Reading Level","Show Language","Special Note","Product Type",
				"Other format ","Shipping Message PDP","Display Title","Product Email","Product End Date","Product Start Date","Reviews",
				"Sale End Date","Sale Start Date","Sale Message","See Inside Description","Number of See Inside Pages","Number of Alternate Images",
				"Custom Grade","SSO Custom Grade","Show ACR? (TRUE/FALSE)","StoreID","Illustrator","Rank" };*/
		
		
		
		
		String[] cellHeading = {	"ISBN13",
		"ISBN",
		"BorderID",
		"HighAge",
		"LowAge",
		"Brand",
		"BudgetCode",
		"LongDescription",
		"DisplayTitle",
		"//BookType",
		"ListPrice",
		"OurPrice",
		"//SSOFormat",
		"ProductStartDate",
		"//BackOrderFlag",
		"//SSOClass",
		"//SubClass",
		"//Department",
		"FictionType",
		"HighGradeTSOOnly",
		"LowGradeTSOOnly",
		"WorkID",
		"Language",
		"ProductType",
		"Publisher",
		"InventoryThreshold",
		"//Manufacturer",
		"Pages",
		"Keywords",
		"Illustrator",
		"NumberOfAlternateImages",
		"Searchable",
		"CharactersAndSeries",
		"//ShippingMessagePDP",
		"UnitCost",
		"ViewableFlag",
		"Weight",
		"Number of See Inside Pages",
		"Depth",
		"Height",
		"Width",
		"Shops",
		"Vendor1",};

		
		
		
		
		
		
		
		
		
		
		
		
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		Map<String, List<String>> mapList = new HashMap<>();
		List<String> listAdd;
		for (String cellValue : cellValuesList) {
			listAdd = new ArrayList<>();
			try {

				// QA Unmanaged MDM (Boomerang API)
				// http://nonprod.api.scholastic.com/qa/ondemand/2.1/aso/9780590409230sso-us?StoreID=sso-us
				String urlUnManagedMDM = "http://nonprod.api.scholastic.com/qa/ondemand/2.1/aso/" + cellValue
						+ "sso-us?StoreID=sso-us";
				ResponseEntity<UnManagedMDM> exdate = template.exchange(urlUnManagedMDM, HttpMethod.GET,
						new HttpEntity<String>(getHeaders()), UnManagedMDM.class);
				if (exdate.getStatusCode().equals(HttpStatus.OK)) {
					
	
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getISBN13()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getISBN13()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getISBN()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getISBN()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getBorderID()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getBorderID()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getHighAge()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getHighAge()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getLowAge()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getLowAge()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getBrand()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getBrand()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getBudgetCode()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getBudgetCode()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getLongDescription()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getLongDescription()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getDisplayTitle()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getDisplayTitle()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getBookType()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getBookType()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getListPrice()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getListPrice()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getOurPrice()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getOurPrice()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getsSOFormat()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getsSOFormat()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getProductStartDate()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getProductStartDate()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getBackOrderFlag()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getBackOrderFlag()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getsSOClass()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getsSOClass()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getSubClass()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getSubClass()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getDepartment()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getDepartment()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getFictionType()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getFictionType()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getHighGradeTSOOnly()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getHighGradeTSOOnly()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getLowGradeTSOOnly()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getLowGradeTSOOnly()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getWorkID()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getWorkID()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getLanguage()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getLanguage()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getProductType()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getProductType()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getPublisher()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getPublisher()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getInventoryThreshold()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getInventoryThreshold()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getManufacturer()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getManufacturer()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getPages()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getPages()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getKeywords()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getKeywords()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getIllustrator()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getIllustrator()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getNumberOfAlternateImages()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getNumberOfAlternateImages()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getSearchable()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getSearchable()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getCharactersAndSeries()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getCharactersAndSeries()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getShippingMessagePDP()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getShippingMessagePDP()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getUnitCost()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getUnitCost()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getViewableFlag()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getViewableFlag()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getWeight()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getWeight()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getNumberOfSeeInsidePages()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getNumberOfSeeInsidePages()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getDepth()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getDepth()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getHeight()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getHeight()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getWidth()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getWidth()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getShops()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getShops()));
					listAdd.add(String.valueOf(exdate.getBody().getRecord().getData().getVendor1()).isEmpty() ? "EMPTY": String.valueOf(exdate.getBody().getRecord().getData().getVendor1()));

					
					
					
					
					
					

				} else {
					listAdd.add(EMPTY_STRING);
				}
				mapList.put(cellValue, listAdd);
			} catch (Exception e) {
				listAdd = new ArrayList<>();
				e.printStackTrace();
				listAdd.add(EMPTY_STRING);
				mapList.put(cellValue, listAdd);
				log.log(Level.INFO, jsonValue + "in exception");
			}
		}
		generateDataToExcel(mapList, "BOOMERANG", cellHeading);
		return null;
	}

}
