package com.scholastic.cs.pricecompare;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.scholastic.cs.pricecompare.MDMTOJSON.Product;
import com.scholastic.cs.pricecompare.aem.AEM_Data;
import com.scholastic.cs.pricecompare.dw.DWData;
import com.scholastic.cs.pricecompare.unmanagedMdm.UnManagedMDM;

public class AEMDWControlller {
	private static final Logger log = Logger.getLogger(AEMDWControlller.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("CompatreOutput.xlsx").getPath();
	private static final String FILE_NAME_READ = ClassLoader.getSystemClassLoader().getResource("ISBNInputData.xlsx").getPath();
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) {
		log.log(Level.INFO, "Before the readUrlsFromExcel");
		Map<String, List<String>> mapList = readUrlsFromExcel();
		log.log(Level.INFO, "Map values", mapList);
		generateDataToExcel(mapList);
	}

	private static void generateDataToExcel(Map<String, List<String>> mapList) {
		log.log(Level.INFO, "INTO the generateDataToExcel", mapList);

		try (FileInputStream excelFile = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(excelFile);) {
			XSSFSheet worksheet = workbook.getSheetAt(0);
			// Get first/desired sheet from the workbook
			int rowNumber = 1;
			XSSFRow row1 = worksheet.createRow(0);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue("ISBN_NUMBER");
			Cell cell2 = row1.createCell(1);
			cell2.setCellValue("MDM ID");
			Cell cell3 = row1.createCell(2);
			cell3.setCellValue("AEM LIST PRICES");
			Cell cell4 = row1.createCell(3);
			cell4.setCellValue("AEM SALE PRICE");
			Cell cell5 = row1.createCell(4);
			cell5.setCellValue("AEM OUR PRICE");
			Cell cell6 = row1.createCell(5);
			cell6.setCellValue("DW LISTPRICES");
			Cell cell7 = row1.createCell(6);
			cell7.setCellValue("DW SALE PRICES");
			Cell cell8 = row1.createCell(7);
			cell8.setCellValue("DW  OUR PRICES");
			for (Map.Entry<String, List<String>> entry : mapList.entrySet()) {
				log.info("Item : " + entry.getKey() + " Count : " + entry.getValue());
				List<String> stringList = entry.getValue();
				XSSFRow row = worksheet.createRow(rowNumber);
				Cell cell = row.createCell(0);
				cell.setCellValue(entry.getKey());
				for (int j = 1; j < entry.getValue().size() + 1; j++) {

					cell = row.createCell(j);
					cell.setCellValue(stringList.get(j - 1));

				}
				rowNumber++;

			}
			
			
			
			
			
			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);
					){
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
			}
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

		}

	}
	
	private static Map<String, List<String>> readUrlsFromExcel() {
		log.log(Level.INFO, "into the  readUrlsFromExcel");

		Map<String, List<String>> mapList = new HashMap<>();
		System.out.println(ClassLoader.getSystemClassLoader().getResource("").getPath());
		try (FileInputStream excelFile = new FileInputStream(FILE_NAME_READ);
				
				XSSFWorkbook workbook = new XSSFWorkbook(excelFile);) {
			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				String lastTwo = null;

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					if (!cell.getStringCellValue().isEmpty()) {
						String cellValue = cell.getStringCellValue();
						log.info(cellValue);
						if (cellValue != null && cellValue.length() >= 2) {
							lastTwo = cellValue.substring(cellValue.length() - 2);
						}

						mapList.put(cellValue, retreiveDataFromURLToCompare(cellValue, lastTwo));

					} else {
						log.log(Level.INFO, "into the  readUrlsFromExcel check whether the cell type is"
							);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.INFO, "into the exception in the readURL", e.fillInStackTrace());
		}
		return mapList;
	}
	private static List<String> retreiveDataFromURLToCompare(String cellValue, String lastTwo) {
		lastTwo = trimLeadingZeroes(lastTwo);

		
		//AEM
		String urlOne = "https://shop-stage.scholastic.com/content/scholastic/books/aso/tso/" + lastTwo + "/" + cellValue
				+ "/data.json";
		
		
		
		
		//QA Demandware
		
		
		String urlThree = "https://staging-rco-scholastic.demandware.net/s/Sites-tso-us-Site/dw/shop/v16_9/products/"
				+ cellValue + "-tso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8";
		
// https://development-rco-scholastic.demandware.net/s/Sites-tso-us-Site/dw/shop/v16_9/products/9781338139051-sso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8
	// PROD DW
	// https://staging.rco.scholastic.demandware.net/s/Sites-tso-us-Site/dw/shop/v16_9/products/9780545134477-tso-us?client_id=ec2f5cac-1682-4e84-b30e-5d995a2664d8

		List<String> list = new ArrayList<>();
		String splitData = getReponseEntity(urlOne);
		String[] zee = splitData.split(";");
		list.add(zee[0]);
		list.add(zee[1]);
		list.add(zee[2]);
		list.add(zee[3]);
		String splitData2 = getReponseThirdEntity(urlThree);
		String[] zee2 = splitData2.split(";");
		list.add(zee2[0]);
		list.add(zee2[1]);
		list.add(zee2[2]);
		
		
		
		log.log(Level.INFO, "get the list of data ", list);

		return list;
	}
	private static String getReponseEntity(String url) {
		log.log(Level.INFO, url, "URL in THE RESPONSE");
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		ResponseEntity<AEM_Data> exdate = null;
		try {
			exdate = template.exchange(url, HttpMethod.GET, null, AEM_Data.class);
			if (exdate.getBody().getListPrice() != null && exdate.getStatusCode().equals(HttpStatus.OK)) {
				jsonValue.append(exdate.getBody().getProductID()).append(";").
				append(exdate.getBody().getListPrice().isEmpty()? "EMPTY":exdate.getBody().getListPrice() ).
				append(";").
				append(exdate.getBody().getOurPrice().isEmpty()? "EMPTY":exdate.getBody().getOurPrice()).
				append(";").
				append(exdate.getBody().getSalePrice().isEmpty()? "EMPTY" :exdate.getBody().getSalePrice());
			} else {
				jsonValue.append(EMPTY_STRING).append(";").append(EMPTY_STRING);
			}
		} catch (Exception e) {
			e.printStackTrace();
			jsonValue.append(EMPTY_STRING).append(";").append(EMPTY_STRING).append(";").append("NOT FOUND")
			.append(";").append("NOT FOUND");;
			// jsonValue.append(exdate.getBody().getProductID()).append(";").append(EMPTY_STRING);
			log.log(Level.INFO, "jsonValue in exception of responseSecondEntity" + jsonValue);
		}
		log.log(Level.INFO, jsonValue.toString());
		return jsonValue.toString();
	}

	private static String trimLeadingZeroes(String value) {
		while (value.length() > 1 && value.indexOf('0') == 0) {
			value = value.substring(1);
		}
		return value;
	}
	private static String getReponseThirdEntity(String url) {
		log.log(Level.INFO, "INTO getReponseSecondEntity", url);
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		
		try {
			ResponseEntity<DWData> exdate = template.exchange(url, HttpMethod.GET, null, DWData.class);
			//exdate.getBody().getSalePrice();
			if (exdate.getBody().getCListPrice() != null && exdate.getStatusCode().equals(HttpStatus.OK)) {
				jsonValue.append(String.valueOf(exdate.getBody().getCListPrice()))
				.append(";").append("NOT FOUND")
				.append(";").append("NOT FOUND");
			} else {
				jsonValue.append(EMPTY_STRING);
			}
		} catch (Exception e) {
			jsonValue.append(EMPTY_STRING).append(";").append("NOT FOUND")
			.append(";").append("NOT FOUND");;
			log.log(Level.INFO, jsonValue + "in exception");
		}
		return jsonValue.toString();
	}
	
}
