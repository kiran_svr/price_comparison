package com.scholastic.cs.SolrAEMServices;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.junit.Assert;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.scholastic.coreApiService.Example;
import com.scholastic.coreApiService.Product;


/**
 * 
 * @author Raghu Seethamraju
 *
 */
public class TSOSorting {
	private static final Logger log = Logger.getLogger(TSOSorting.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("TSOSorting.xlsx")
			.getPath();
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) throws SolrServerException, IOException {

		String SortingValue = "Relevance";
		
		generateDataToExcel(SortingValue);

	}
	/**
	 * 
	 * @param FundingType
	 * @throws SolrServerException
	 * @throws IOException
	 */
	
	public static Boolean generateDataToExcel(String SortingValue) throws SolrServerException, IOException {
		
		XSSFRow row1;
		XSSFSheet  worksheet ;
		
		List<List<String>> completeSolrList=solrSearch(TSOSortingData.Sortingsolr(SortingValue));
		List<List<String>> completeApiList=getReponseThirdEntity(TSOSortingData.SortingAPI(SortingValue));
		
		List<String> apiGrades = completeApiList.get(2);
		List<String> apiIsbn = completeApiList.get(0);
		List<String> apiUrl = completeApiList.get(1);
		List<String> solrUrl = completeSolrList.get(1);
		List<String> solrIsbn = completeSolrList.get(0);

		
		
		Boolean validationFlag=compareTwoList(apiIsbn,solrIsbn);
		System.out.println("validationFlag---"+validationFlag);
		
		
		try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
			
			try {
				worksheet = workbook.getSheet(SortingValue);
				 row1 = worksheet.createRow(0);
				
			}catch (Exception e) {
				  worksheet = workbook.createSheet(SortingValue);
				 row1 = worksheet.createRow(0);
			}
			
			
			Cell cell2 = row1.createCell(0);
			cell2.setCellValue("SOLR");
			Cell cell1 = row1.createCell(1);
			cell1.setCellValue("CORE_API");
			Cell cell3 = row1.createCell(2);
			cell3.setCellValue("CORE_APIQuery");
			Cell cell4 = row1.createCell(3);
			cell4.setCellValue("SolrQuery");
			Cell cell5 = row1.createCell(4);
			cell5.setCellValue("API_GRADES");
			for (int i = 0; i < apiIsbn.size(); i++) {

				XSSFRow row = worksheet.createRow(i + 1);
				row.createCell(0).setCellValue(solrIsbn.get(i));
				row.createCell(1).setCellValue(apiIsbn.get(i));
				row.createCell(2).setCellValue(apiUrl.toString());
				row.createCell(3).setCellValue(solrUrl.toString());
				row.createCell(4).setCellValue(apiGrades.get(i).toString());

			}
			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);) {
				// Save the workbook in .xls file
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
			}
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

			
		}
		if (!validationFlag) {
			Assert.fail("Solr didnt match with API");
		}
	
		return false;
		
	}
	

	/**
	 * 
	 * @param reponseThirdEntity
	 * @param solrSearch
	 * @return
	 */
	private static Boolean compareTwoList(List<String> reponseThirdEntity, List<String> solrSearch) {
		
		
		//Collections.sort(reponseThirdEntity);
		//Collections.sort(solrSearch);

		return solrSearch.equals(reponseThirdEntity);
		

			
	}
	


	
	/**
	 * 
	 * @param SortingValue
	 * @return
	 */

	

	/**
	 * 
	 * @param SortingValue
	 * @return
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public static List<List<String>> solrSearch(String SortingValue) throws SolrServerException, IOException {
		List<String> list1 = new ArrayList<>();
	
		List<String> solrUrlList = new ArrayList<>();
		List<List<String>> completeList = new ArrayList<>();
		String urlString = "http://10.45.150.78:8983/solr/global_shard1_replica1/";
		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		((HttpSolrClient) solr).setParser(new XMLResponseParser());
		SolrQuery query = new SolrQuery();
		
	

		//query.setRequestHandler("tchecommfacetsort");
		query.setParam("shards.qt", "/tchecommfacetsort");
		query.setParam("facet", "true");	
	
		query.setQuery("site_id_s:tso-scholastic-books");
	 // 	query.addSort("score",ORDER.desc);
		//query.addSort("UNITS_SOLD_f", ORDER.desc);
		//query.addSort("SortPrice_f", ORDER.desc);
		//query.addSort("ProductStartDate_dt", ORDER.desc);
		//query.addSort("id", ORDER.desc);
		query.setRows(20);
		//query.setParam("wt", "xml");
	  // query.setParam("indent", "true");
		query.setParam("fq", "Searchable_s:TRUE");

		
	    
	    if (SortingValue.contains("Relevance")) {	
	    	query.addSort("score",ORDER.desc);
	    	query.addSort("UNITS_SOLD_f", ORDER.desc);
	    	
	    	query.addSort("ProductStartDate_dt", ORDER.desc);
	    	query.addSort("id", ORDER.desc);
	    } else if  (SortingValue.contains("Title A-Z")) {
	    	query.addSort("title_s",ORDER.asc);    	    	
	     } else if  (SortingValue.contains("Title Z-A")) {
    	query.addSort("title_s",ORDER.desc);
      } else if (SortingValue.contains("Price Low to High")) {
    	  query.addSort("SortPrice_f", ORDER.asc);
      } else if (SortingValue.contains("Price High to Low")) {
    	  query.addSort("SortPrice_f", ORDER.desc);
      } else if (SortingValue.contains("Most Recent")) {
    	  query.addSort("ProductStartDate_dt", ORDER.desc);
     } else if  (SortingValue.contains("Best Sellers")) {
    	 query.addSort("UNITS_SOLD_f", ORDER.desc);
     }
	    
	    System.out.println("SortValue:"+ SortingValue +"-------------------"+ query);
	    
	    
	    
	//	query.setParam("fq", "Fund_tsi:"+SortingValue);
		query.setParam("fl", "ISBN13_s");	
		
		
		QueryResponse response = solr.query(query);
		
		System.out.println("http://10.45.150.78:8983/solr/global_shard1_replica1/select?"+query);
		solrUrlList.add("http://10.45.150.78:8983/solr/global_shard1_replica1/select?" + query);
		org.apache.solr.common.SolrDocumentList list = response.getResults();
		for (SolrDocument solrDocument : list) {
			list1.add(solrDocument.getFieldValue("ISBN13_s").toString());
		//	System.out.println(list1);
		}
		completeList.add(list1);
		completeList.add(solrUrlList);
		return completeList;
	}
	
	
	



	
	
	/**
	 * 
	 * @param SortingValue
	 * @return
	 */
	private static List<List<String>> getReponseThirdEntity(String SortingValue) {
			String url="http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&sort="+SortingValue;
			List<String> isbnList = new ArrayList<>();
			List<String> gradeList = new ArrayList<>();
			List<String> urlList = new ArrayList<>();
			List<List<String>> completeApiList = new ArrayList<>();
			System.out.println("SortValue:"+SortingValue);
			System.out.println("in API---" + completeApiList.size());
			System.out.println(url);
			List<String> list = new ArrayList<>();
		RestTemplate template = new RestTemplate();
		try {
			ResponseEntity<Example> exdate = template.exchange(url, HttpMethod.GET, null, Example.class);

			List<Product> y = exdate.getBody().getProducts();
			for (Product solrDocumentList : y) {
				list.add(solrDocumentList.getIsbn13());
				gradeList.add(solrDocumentList.getGrade().toString());
			//	System.out.println(list);
			}
			urlList.add(url);
			completeApiList.add(list);
			completeApiList.add(urlList);
			completeApiList.add(gradeList);
			System.out.println("in API---" + completeApiList.size());
			
			System.out.println("in API---" + gradeList);
		} catch (Exception e) {
			list.add("in exception");
		}
		return completeApiList;
	}

}
