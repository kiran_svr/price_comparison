package com.scholastic.cs.SolrAEMServices;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrServerException;
import com.scholastic.cs.SolrAEMServices.SolrSearchMain;;



public class  RunSolraem {
	


	public static void main(String[] args) throws SolrServerException, IOException {
		
	//	GoogleSheetFunctions.writeToSheet(sheetName, CellRanges.TSO_STORE_LOGIN, Utility.createObject());
		
		System.out.println("#################AEM###########################");
		AEMController.readUrlsFromExcel();
		
		System.out.println("#################Solr###########################");
		SolrSearchMain.readUrlsFromExcel();

	} 
	
}
