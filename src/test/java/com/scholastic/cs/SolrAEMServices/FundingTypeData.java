package com.scholastic.cs.SolrAEMServices;

public class FundingTypeData {

   public static  String FundingTypesolr(String FundingTypeValue){
	   
	   if(FundingTypeValue.equals("TitleI Part A")) {
			FundingTypeValue=("\"Title I, Part A\"");
		} else 	if(FundingTypeValue.equals("TitleI School Improvement")) {
			FundingTypeValue=("Title I, School Improvement");		
		 } else if(FundingTypeValue.equals("Title I DirectStudentServices")) {
			 FundingTypeValue=("\"Title I, Direct Student Services\"");
		 } else if(FundingTypeValue.equals("Title II PartA")) {
			 FundingTypeValue=("\"Title II, PartA\"");
		 } else if(FundingTypeValue.equals("Title II PartB LEARN")) {
			 FundingTypeValue=("\"Title II Part B LEARN\""); 
		 } else if(FundingTypeValue.equals("TitleIII")) {
			 FundingTypeValue=("TitleIII");
		 } else if(FundingTypeValue.equals("TitleIV PartA SSAEG")) {
			 FundingTypeValue=("TitleIV, PartA, SSAEG"); 	 
		 } else if(FundingTypeValue.equals("TitleIV PartB 21stCCLC")) {
			 FundingTypeValue=("TitleIV, PartB, 21stCCLC");
		 } else if(FundingTypeValue.equals("IDEA")) {
			 FundingTypeValue=("IDEA");		
		 } else if(FundingTypeValue.equals("SpecialEducationRTI")) {
			 FundingTypeValue=("\"Special Education/RTI\""); 
			 
		}
		
		return FundingTypeValue;
	}
   
   
 public static  String FundingTypeAPI(String FundingTypeValue){
	   
	   if(FundingTypeValue.equals("TitleI Part A")) {
			FundingTypeValue=("Title I, Part A");
		} else 	if(FundingTypeValue.equals("TitleI School Improvement")) {
			FundingTypeValue=("Title I, School Improvement");		
		 } else if(FundingTypeValue.equals("Title I DirectStudentServices")) {
			 FundingTypeValue=("Title I, Direct Student Services");
		 } else if(FundingTypeValue.equals("Title II PartA")) {
			 FundingTypeValue=("Title II, PartA");
		 } else if(FundingTypeValue.equals("Title II PartB LEARN")) {
			 FundingTypeValue=("Title II, Part B, LEARN"); 
		 } else if(FundingTypeValue.equals("TitleIII")) {
			 FundingTypeValue=("TitleIII");
		 } else if(FundingTypeValue.equals("TitleIV PartA SSAEG")) {
			 FundingTypeValue=("TitleIV, PartA, SSAEG"); 	 
		 } else if(FundingTypeValue.equals("TitleIV PartB 21stCCLC")) {
			 FundingTypeValue=("TitleIV, PartB, 21stCCLC");
		 } else if(FundingTypeValue.equals("IDEA")) {
			 FundingTypeValue=("IDEA");		
		 } else if(FundingTypeValue.equals("SpecialEducationRTI")) {
			 FundingTypeValue=("Special Education/RTI"); 
			 
		}
		
		return FundingTypeValue;
	}
   }

