package com.scholastic.cs.SolrAEMServices;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.junit.Assert;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import com.scholastic.coreApiService.Example;
import com.scholastic.coreApiService.Product;

/**
 * 
 * @author Raghu Seethamraju
 *
 */
public class SolrAemController {
	private static final Logger log = Logger.getLogger(SolrAemController.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("Grade.xlsx").getPath();
	private static final String FILE_NAME_GRADE_PRICE = ClassLoader.getSystemClassLoader().getResource("Grade_Price.xlsx").getPath();
	private static final String FILE_NAME_PRICE = ClassLoader.getSystemClassLoader().getResource("Price.xlsx").getPath();
	private static final String FILE_NAME_FUNDINGTYPE = ClassLoader.getSystemClassLoader().getResource("FundingType.xlsx").getPath();
	private static final String FILE_NAME_GENRE = ClassLoader.getSystemClassLoader().getResource("Genre.xlsx").getPath();
	private static final String FILE_NAME_FICTION = ClassLoader.getSystemClassLoader().getResource("fiction.xlsx").getPath();
	private static final String FILE_NAME_NONFICTION = ClassLoader.getSystemClassLoader().getResource("nonfiction.xlsx").getPath();
	private static final String FILE_NAME_SUBJECT = ClassLoader.getSystemClassLoader().getResource("Subject.xlsx").getPath();
//	private static final String FILE_NAME_AGE = ClassLoader.getSystemClassLoader().getResource("Age.xlsx").getPath();
	private static final String FILE_NAME_FORMAT = ClassLoader.getSystemClassLoader().getResource("Format.xlsx").getPath();
//	private static final String FILE_NAME_GUIDEDREADING = ClassLoader.getSystemClassLoader().getResource("GuidedReading.xlsx").getPath();
	
	
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) throws SolrServerException, IOException {

		String gradeValue = "PreK;Under $5;TitleI Part A";

		generateDataToExcel(gradeValue);

	}

	
	/**
	 * 
	 * @param passedArgument
	 * @throws SolrServerException
	 * @throws IOException
	 */

	public static Boolean generateDataToExcel(String passedArgument) throws SolrServerException, IOException {

		XSSFRow row1;
		XSSFSheet worksheet;
		String fileName = null;
		if (passedArgument.contains(";")) {
			if(Arrays.asList(CollectionsOfTypes.Fiction__Gener_types).contains(passedArgument)||Arrays.asList(CollectionsOfTypes.NonFinction_Gener_types).contains(passedArgument)) {
				fileName = FILE_NAME_NONFICTION;	
			}else {
				fileName = FILE_NAME_GRADE_PRICE;	
			}
			
		} else if (passedArgument.contains("$")) {
			fileName = FILE_NAME_PRICE;
		}else if(Arrays.asList(CollectionsOfTypes.funding_types).contains(passedArgument)) {
			fileName = FILE_NAME_FUNDINGTYPE;
		}else if(Arrays.asList(CollectionsOfTypes.genre_types).contains(passedArgument)) {
			fileName = FILE_NAME_GENRE;
		}else if(Arrays.asList(CollectionsOfTypes.Fiction_NonFinction_types).contains(passedArgument)) {
			fileName = FILE_NAME_FICTION;	
		}else if(Arrays.asList(CollectionsOfTypes.Format).contains(passedArgument)) {
			fileName = FILE_NAME_FORMAT;
		}else if(Arrays.asList(CollectionsOfTypes.Subject).contains(passedArgument)) {
			fileName = FILE_NAME_SUBJECT;	
			
		}	
		
		else {	
			fileName = FILE_NAME_WRITE;
		}
		List<List<String>> completeSolrList = solrSearch(passedArgument);
		List<List<String>> completeApiList = getReponseThirdEntity(passedArgument);
		List<String> apiGrades = completeApiList.get(2);
		List<String> apiIsbn = completeApiList.get(0);
		List<String> apiUrl = completeApiList.get(1);
		List<String> solrUrl = completeSolrList.get(1);
		List<String> solrIsbn = completeSolrList.get(0);

		System.out.println(apiGrades + "---" + apiUrl + "---" + apiIsbn);
		System.out.println("gradeValue---" + passedArgument);

		Boolean validationFlag = compareTwoList(apiIsbn, solrIsbn);
		log.info("validationFlag---" + validationFlag);

		try (FileInputStream fis = new FileInputStream(fileName); XSSFWorkbook workbook = new XSSFWorkbook(fis);) {

			try {
				worksheet = workbook.getSheet(passedArgument);
				row1 = worksheet.createRow(0);

			} catch (Exception e) {
				worksheet = workbook.createSheet(passedArgument);
				row1 = worksheet.createRow(0);
			}

			Cell cell2 = row1.createCell(0);
			cell2.setCellValue("SOLR");
			Cell cell1 = row1.createCell(1);
			cell1.setCellValue("CORE_API");
			Cell cell3 = row1.createCell(2);
			cell3.setCellValue("CORE_APIQuery");
			Cell cell4 = row1.createCell(3);
			cell4.setCellValue("SolrQuery");
			Cell cell5 = row1.createCell(4);
			cell5.setCellValue("API_GRADES");

			for (int i = 0; i < apiIsbn.size(); i++) {

				XSSFRow row = worksheet.createRow(i + 1);
				row.createCell(0).setCellValue(solrIsbn.get(i));
				row.createCell(1).setCellValue(apiIsbn.get(i));
				row.createCell(2).setCellValue(apiUrl.toString());
				row.createCell(3).setCellValue(solrUrl.toString());
				row.createCell(4).setCellValue(apiGrades.get(i).toString());

			}

			try (FileOutputStream fos = new FileOutputStream(fileName);) {
				// Save the workbook in .xls file
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@---"+fileName);
			}
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

		}
		if (!validationFlag) {
			Assert.fail("Solr didnt match with API");
		}

		return false;

	}

	/**
	 * 
	 * @param reponseThirdEntity
	 * @param solrSearch
	 * @return
	 */
	private static Boolean compareTwoList(List<String> reponseThirdEntity, List<String> solrSearch) {

		// Collections.sort(reponseThirdEntity);
		// Collections.sort(solrSearch);

		return solrSearch.equals(reponseThirdEntity);

	}



	/**
	 * 
	 * @param passedArgument
	 * @return
	 */
	private static List<List<String>> getReponseThirdEntity(String passedArgument) {
		System.out.println("passed Value---" + passedArgument);



		List<String> isbnList = new ArrayList<>();
		List<String> gradeList = new ArrayList<>();
		List<String> urlList = new ArrayList<>();
		List<List<String>> completeApiList = new ArrayList<>();
			
		
		RestTemplate template = new RestTemplate();
		String passedParam = null;
		String passedParam2 = null;
	//	String passedParam3=null;
		String url = null;
		if (passedArgument.contains(";")) {
			
			 if(Arrays.asList(CollectionsOfTypes.Fiction__Gener_types).contains(passedArgument)) {
				 String[] splitValue = passedArgument.split(";");
				 passedParam = GenreData.GenreAPI(splitValue[0]);
				passedParam2 =   GenreData.GenreAPI(splitValue[1]);
				
					url = "http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&genre="
							+ passedParam2 + "&fiction_type=" + passedParam;
					
	
					
					
					
				} else if(Arrays.asList(CollectionsOfTypes.NonFinction_Gener_types).contains(passedArgument)) {
					String[] splitValue = passedArgument.split(";");
					passedParam = GenreData.GenreAPI(splitValue[0]);
					passedParam2 =   GenreData.GenreAPI(splitValue[1]);

					url = "http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&genre="
							+ passedParam2 + "&fiction_type=" + passedParam;
					
				}
			 
			 
			 else {

						String[] splitValue = passedArgument.split(";");
						passedParam = GradeValue.gradeValueForApi(splitValue[0]);
						passedParam2 = PriceValue.priceValueForApi(splitValue[1]);
					//	passedParam3 =   GenreData.GenreAPI(splitValue[2]);
					//	System.out.println("ttttt"+passedParam3);
						url = "http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&grade="
								+ passedParam + "&price=" + passedParam2;
				}
		} else {

			if (passedArgument.contains("$")) {
				passedParam = PriceValue.priceValueForApi(passedArgument);
				url = "http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&price="
						+ passedParam;

			}else if(Arrays.asList(CollectionsOfTypes.funding_types).contains(passedArgument)) {
				passedParam = FundingTypeData.FundingTypeAPI(passedArgument);
				System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<passedParam>>>>>>>>>>>>>>>"+passedParam);
				 url="http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&funding_type="+passedParam;
				
			}
		 else if(Arrays.asList(CollectionsOfTypes.genre_types).contains(passedArgument)) {
			 passedParam = GenreData.GenreAPI(passedArgument);
				System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<passedParam>>>>>>>>>>>>>>>"+passedParam);
				 url="http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&genre="+passedParam;
			
		} else if(Arrays.asList(CollectionsOfTypes.Fiction_NonFinction_types).contains(passedArgument)) {
			 passedParam = GenreData.GenreAPI(passedArgument);
				System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<passedParam>>>>>>>>>>>>>>>"+passedParam);
				 url="http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&fiction_type="+passedParam;
			
		} else if(Arrays.asList(CollectionsOfTypes.Format).contains(passedArgument)) {
			 passedParam = FormatData.FormatAPI(passedArgument);
				System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<passedParam>>>>>>>>>>>>>>>"+passedParam);
				 url="http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&format="+passedParam;
	
		} else if(Arrays.asList(CollectionsOfTypes.Subject).contains(passedArgument)) {
			 passedParam = SubjectData.SubjectAPI(passedArgument);
				System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<passedParam>>>>>>>>>>>>>>>"+passedParam);
				 url="http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&subject="+passedParam;
		}
			
			else {
				passedParam = GradeValue.gradeValueForApi(passedArgument);
				url = "http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&grade="
						+ passedParam;

			}
			

		}

		log.info("gradeValue in API---->>" + passedArgument);
		log.info("URL--->" + url);

		try {
			ResponseEntity<Example> exdate = template.exchange(url, HttpMethod.GET, null, Example.class);

			List<Product> y = exdate.getBody().getProducts();
			for (Product apiProductList : y) {
				isbnList.add(apiProductList.getIsbn13());
				gradeList.add(apiProductList.getGrade().toString());
				// log.info("API--LIST--"+list);
				// System.out.println(apiProductList.getGrade().toString());
			}

			urlList.add(url);
			completeApiList.add(isbnList);
			completeApiList.add(urlList);
			completeApiList.add(gradeList);
			System.out.println("in API---" + completeApiList.size());
		} catch (Exception e) {
			e.printStackTrace();
			isbnList.add(EMPTY_STRING + "--in exception--");
		}
		return completeApiList;
	}

	/**
	 * 
	 * @param passedArugment
	 * @return
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public static List<List<String>> solrSearch(String passedArugment) throws SolrServerException, IOException {
		List<String> solrIsbnList = new ArrayList<>();
		List<String> solrUrlList = new ArrayList<>();
		List<List<String>> completeList = new ArrayList<>();
		
		String passedParam1 = null;
		String passedParam2 = null;
	//	String passedParam3 = null;
		
		String urlString = "http://10.45.150.78:8983/solr/global_shard1_replica1/";
		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		((HttpSolrClient) solr).setParser(new XMLResponseParser());
		SolrQuery query = new SolrQuery();
		query.setParam("shards.qt", "/tchecommfacetsort");
		query.setParam("facet", "true");
		query.addSort("score", ORDER.desc);
		query.addSort("UNITS_SOLD_f", ORDER.desc);
		query.addSort("ProductStartDate_dt", ORDER.desc);
		query.addSort("id", ORDER.desc);
		query.setRows(20);
		query.setParam("wt", "xml");
		query.setParam("indent", "true");
		
		if (passedArugment.contains(";")) {
			if(Arrays.asList(CollectionsOfTypes.Fiction__Gener_types).contains(passedArugment)) {
				String[] splitValue = passedArugment.split(";");
				passedParam1 = GenreData.Genresolr(splitValue[0]);				
				passedParam2 = GenreData.Genresolr(splitValue[1]);
			//	query.setParam("fq", "FictionType_s:" + passedParam1);
				query.setParam("fq", "Searchable_s:TRUE", "FictionType_s:" + passedParam1);
				query.addFilterQuery("Genre_ss:" + passedParam2);
				
				query.setQuery( "site_id_s:tso-scholastic-books");
			}else if(Arrays.asList(CollectionsOfTypes.NonFinction_Gener_types).contains(passedArugment)) {
				String[] splitValue = passedArugment.split(";");
				passedParam1 = GenreData.Genresolr(splitValue[0]);				
				passedParam2 = GenreData.Genresolr(splitValue[1]);
			//	query.setParam("fq", "FictionType_s:" + passedParam1);
				query.setParam("fq", "Searchable_s:TRUE", "FictionType_s:" + passedParam1);
				query.setQuery("Genre_ss:" + passedParam2);
				query.addFilterQuery( "site_id_s:tso-scholastic-books");
			}
			
			else {
				String[] splitValue = passedArugment.split(";");
				passedParam1 = GradeValue.gradeValueForSolr(splitValue[0]);
				//query.setParam("fq", "Grade_ss:" + passedParam1);
				query.setParam("fq", "Searchable_s:TRUE", "Grade_ss:" + passedParam1);
				passedParam2 = PriceValue.priceValueForSolr(splitValue[1]);
			//	passedParam3 = PriceValue.priceValueForSolr(splitValue[2]);
			//	System.out.println("-------"+passedParam3);
				query.addFilterQuery("site_id_s:tso-scholastic-books","SortPrice_f:" + passedParam2);
			}
			
			
		} else {
			query.setQuery( "site_id_s:tso-scholastic-books");
			if (passedArugment.contains("$")) {
				//query.setParam("fq", "SortPrice_f:" + PriceValue.priceValueForSolr(passedArugment));
				query.setParam("fq", "Searchable_s:TRUE", "SortPrice_f:" + PriceValue.priceValueForSolr(passedArugment));
			} else if(Arrays.asList(CollectionsOfTypes.funding_types).contains(passedArugment)) {
			//	query.setParam("fq", "Fund_tsi:"+FundingTypeData.FundingTypesolr(passedArugment));
				query.setParam("fq", "Searchable_s:TRUE", "Fund_tsi:"+FundingTypeData.FundingTypesolr(passedArugment));
			} else if(Arrays.asList(CollectionsOfTypes.genre_types).contains(passedArugment)) {
//				query.setParam("fq", "Genre_ss:"+GenreData.Genresolr(passedArugment));
				query.setParam("fq", "Searchable_s:TRUE", "Genre_ss:" + GenreData.Genresolr(passedArugment) );
				
				
			} else if(Arrays.asList(CollectionsOfTypes.Fiction_NonFinction_types).contains(passedArugment)) {
			//	query.setParam("fq", "FictionType_s:"+GenreData.Genresolr(passedArugment));
				query.setParam("fq", "Searchable_s:TRUE", "FictionType_s:"+GenreData.Genresolr(passedArugment));
				
			} else if(Arrays.asList(CollectionsOfTypes.Format).contains(passedArugment)) {
			//	query.setParam("fq", "FictionType_s:"+GenreData.Genresolr(passedArugment));
				query.setParam("fq", "Searchable_s:TRUE", "NewProductType_ss_dp:"+FormatData.Formatsolr(passedArugment));	
			
			} else if(Arrays.asList(CollectionsOfTypes.Subject).contains(passedArugment)) {
				//	query.setParam("fq", "FictionType_s:"+GenreData.Genresolr(passedArugment));
					query.setParam("fq", "Searchable_s:TRUE", "Subject_ss_dp:"+SubjectData.Subjectsolr(passedArugment));	
				
				
			}else {
				query.setParam("fq", "Grade_ss:" + GradeValue.gradeValueForSolr(passedArugment));
				query.setParam("fq", "Searchable_s:TRUE", "Grade_ss:" + GradeValue.gradeValueForSolr(passedArugment));
			}
		}

		query.setParam("fl", "ISBN13_s");

		try {
			QueryResponse response = solr.query(query);

			log.info("gradeValue in Solr---->>" + passedArugment);
			log.info("URL--->http://10.45.150.78:8983/solr/global_shard1_replica1/select?" + query);

			solrUrlList.add("http://10.45.150.78:8983/solr/global_shard1_replica1/select?" + query);
			SolrDocumentList list = response.getResults();
			for (SolrDocument solrDocument : list) {
				solrIsbnList.add(solrDocument.getFieldValue("ISBN13_s").toString());
	
			}
			completeList.add(solrIsbnList);
			completeList.add(solrUrlList);
		} catch (Exception e) {
			e.printStackTrace();
			solrIsbnList.add(EMPTY_STRING + "--in exception--");
		}

		return completeList;
	}

}
