package com.scholastic.cs.SolrAEMServices;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.junit.Assert;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.scholastic.coreApiService.Example;
import com.scholastic.coreApiService.Product;


/**
 * 
 * @author Raghu Seethamraju
 *
 */
public class Grade_Price {
	private static final Logger log = Logger.getLogger(Grade_Price.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("Grade_Price.xlsx")
			.getPath();
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) throws SolrServerException, IOException {

		String gradePriceValue = "(1 OR 2) [200 TO *]";
		
		generateDataToExcel(gradePriceValue);

	}
	/**
	 * 
	 * @param gradeValue
	 * @throws SolrServerException
	 * @throws IOException
	 */
	
	public static Boolean generateDataToExcel(String gradePriceValue) throws SolrServerException, IOException {
		
		XSSFRow row1;
		XSSFSheet  worksheet ;
		
		List<String> solrSearch=solrSearch(changeGradeValue(gradePriceValue));
		List<String> reponseThirdEntity=getReponseThirdEntity(changeGradeValue(gradePriceValue));
		
		
		
		Boolean validationFlag=compareTwoList(reponseThirdEntity,solrSearch);
		log.info("validationFlag---"+validationFlag);
		
		
		try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
			
			try {
				worksheet = workbook.getSheet(gradePriceValue);
				 row1 = worksheet.createRow(0);
				
			}catch (Exception e) {
				  worksheet = workbook.createSheet(gradePriceValue);
				 row1 = worksheet.createRow(0);
			}
			
			
			Cell cell2 = row1.createCell(0);
			cell2.setCellValue("SOLR");
			Cell cell1 = row1.createCell(1);
			cell1.setCellValue("CORE_API");
	
			
			for (int i = 0; i < reponseThirdEntity.size(); i++) {

				XSSFRow row = worksheet.createRow(i + 1);
				row.createCell(0).setCellValue(solrSearch.get(i));
				row.createCell(1).setCellValue(reponseThirdEntity.get(i));
				
				
			
			}
			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);) {
				// Save the workbook in .xls file
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
			}
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

			
		}
		if (!validationFlag) {
			Assert.fail("Solr didnt match with API");
		}
	
		return false;
		
	}
	
	/**
	 * 
	 * @param reponseThirdEntity
	 * @param solrSearch
	 * @return
	 */
	private static Boolean compareTwoList(List<String> reponseThirdEntity, List<String> solrSearch) {
		
		
		//Collections.sort(reponseThirdEntity);
		//Collections.sort(solrSearch);

		return solrSearch.equals(reponseThirdEntity);
		

			
	}
	


	
	/**
	 * 
	 * @param gradeValue
	 * @return
	 */
	private static String changeGradeValue(String gradePriceValue) {
			if(gradePriceValue.equals("PreK")) {
				gradePriceValue="\"-1\"";
			} else 	if(gradePriceValue.equals("K")) {
				gradePriceValue="0";		
			 } else if(gradePriceValue.equals("Grade 1")) {
				 gradePriceValue="1";
			} else if(gradePriceValue.equals("Grade 2")) {
				gradePriceValue="2"; 			
			}  else if(gradePriceValue.equals("Grade 3")) {
				gradePriceValue="3"; 	
			}  else if(gradePriceValue.equals("Grade 4")) {
				gradePriceValue="4";
			}  else if(gradePriceValue.equals("Grade 5")) {
				gradePriceValue="5";	
			}  else if(gradePriceValue.equals("Grade 6")) {
				gradePriceValue="6"; 		
			} else 	if(gradePriceValue.equals("Grade 7")) {
				gradePriceValue="7"; 
			}  else if(gradePriceValue.equals("Grade 8")) {
				gradePriceValue="8";
			} else if(gradePriceValue.equals("Grade 9")) {
				gradePriceValue="9";		
			}else if(gradePriceValue.equals("Grade 10")) {
				gradePriceValue="10";
			} else if(gradePriceValue.equals("Grade 11")) {
				gradePriceValue="11";
			}else if(gradePriceValue.equals("Grade 12")) {
				gradePriceValue="12";		
			}else if(gradePriceValue.equals("Grade 1 Grade 5")) {
				gradePriceValue="(\"1\" OR \"5\")";	
			}else if(gradePriceValue.equals("Grade 2 Grade 8")) {
				gradePriceValue="(\"2\" OR \"8\")";
			}else if(gradePriceValue.equals("Grade prek Grade k")) {
				gradePriceValue="(-1 OR 0)";				
			}else if(gradePriceValue.equals("Grade prek Grade 12")) {
				gradePriceValue="(-1 OR 12)";	
				
				
				
				
			}
			return gradePriceValue;
		}
	
	/**
	 * 
	 * @param gradeVaue
	 * @return
	 */
	private static List<String> getReponseThirdEntity(String gradePriceValue) {
			
		
			List<String> list = new ArrayList<>();
			RestTemplate template = new RestTemplate();
			
			                 
			String url="http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&grade="+gradePriceValue;
			log.info("gradeValue in API---->>"+gradePriceValue);
			log.info("URL--->"+url);
			
		
		try {
			ResponseEntity<Example> exdate = template.exchange(url, HttpMethod.GET, null, Example.class);

			List<Product> y = exdate.getBody().getProducts();
			for (Product apiProductList : y) {
				list.add(apiProductList.getIsbn13());
				
				//log.info("API--LIST--"+list);
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			list.add(EMPTY_STRING+"--in exception--");
		}
		return list;
	}
	
	/**
	 * 
	 * @param gradeValue
	 * @return
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public static List<String> solrSearch(String gradePriceValue) throws SolrServerException, IOException {
		List<String> list1 = new ArrayList<>();
		
		
		
	 // API= http://advancedfilters.qa.apps.scholastic.tech/search/tso?grade="-1"||0&price=Over $200&pageSize=20&pageNumber=1	
	  // Solr=	http://10.45.150.78:8983/solr/global/tchecommfacetsort?q=SortPrice_f:[200 TO *]&fq=site_id_s:tso-scholastic-books&fq=(Grade_ss:"-1" OR Grade_ss:0)&rows=20&wt=xml&sort=score desc,UNITS_SOLD_f desc,ProductStartDate_dt desc,id desc&fl=ISBN13_s
		

		
		String urlString = "http://10.45.150.78:8983/solr/global_shard1_replica1/";
		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		((HttpSolrClient) solr).setParser(new XMLResponseParser());
		SolrQuery query = new SolrQuery();
		

		query.setParam("shards.qt", "/tchecommfacetsort");
		query.setParam("facet", "true");
		query.setQuery("site_id_s:tso-scholastic-books");
		query.addSort("score",ORDER.desc);
		query.addSort("UNITS_SOLD_f", ORDER.desc);
		query.addSort("ProductStartDate_dt", ORDER.desc);
		query.addSort("id", ORDER.desc);
		query.setRows(20);
		query.setParam("wt", "xml");
	    query.setParam("indent", "true");
		query.setParam("fq", "Grade_ss:"+gradePriceValue);
		query.setParam("q", "SortPrice_f:"+gradePriceValue);
		//query.setParam("fq", "Grade_ss:" , "SortPrice_f:" +gradePriceValue);
	
		
		query.setParam("fl", "ISBN13_s");	
		
		
		try {
		QueryResponse response = solr.query(query);
		
		log.info("gradeValue in Solr---->>"+gradePriceValue);
		log.info("URL--->http://10.45.150.78:8983/solr/global_shard1_replica1/select?"+query);
		
		SolrDocumentList list = response.getResults();
		for (SolrDocument solrDocument : list) {
			list1.add(solrDocument.getFieldValue("ISBN13_s").toString());
			//log.info("SOLR--LIST--"+list);
		}
		}catch (Exception e) {
			e.printStackTrace();
			list1.add(EMPTY_STRING+"--in exception--");
		}
		
		return list1;
	}

}
