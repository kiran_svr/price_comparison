package com.scholastic.cs.SolrAEMServices;

public class GuidedReadingData {

	public static String GuidedReadingsolr(String GuidedReadingValue) {

		if(GuidedReadingValue.equals("A")) {
			GuidedReadingValue="1";  
		} else 	if(GuidedReadingValue.equals("B")) {
			GuidedReadingValue="2";		
		 } else if(GuidedReadingValue.equals("C")) {
			 GuidedReadingValue="3";
		} else if(GuidedReadingValue.equals("D")) {
			GuidedReadingValue="4"; 			
		}  else if(GuidedReadingValue.equals("E")) {
			GuidedReadingValue="5"; 		
		}  else if(GuidedReadingValue.equals("F")) {
			GuidedReadingValue="6"; 
		}  else if(GuidedReadingValue.equals("G")) {
			GuidedReadingValue="7"; 
		}  else if(GuidedReadingValue.equals("H")) {
			GuidedReadingValue="8"; 
		}  else if(GuidedReadingValue.equals("I")) {
			GuidedReadingValue="9"; 
		}  else if(GuidedReadingValue.equals("J")) {
			GuidedReadingValue="10"; 
		}  else if(GuidedReadingValue.equals("K")) {
			GuidedReadingValue="11"; 
		}  else if(GuidedReadingValue.equals("L")) {
			GuidedReadingValue="12"; 
		}  else if(GuidedReadingValue.equals("M")) {
			GuidedReadingValue="13"; 
		}  else if(GuidedReadingValue.equals("N")) {
			GuidedReadingValue="14"; 
		}  else if(GuidedReadingValue.equals("O")) {
			GuidedReadingValue="15"; 
		}  else if(GuidedReadingValue.equals("P")) {
			GuidedReadingValue="16"; 
		}  else if(GuidedReadingValue.equals("Q")) {
			GuidedReadingValue="17"; 
		}  else if(GuidedReadingValue.equals("R")) {
			GuidedReadingValue="18"; 
		}  else if(GuidedReadingValue.equals("S")) {
			GuidedReadingValue="19"; 
		}  else if(GuidedReadingValue.equals("T")) {
			GuidedReadingValue="20"; 
		}  else if(GuidedReadingValue.equals("U")) {
			GuidedReadingValue="21"; 
		}  else if(GuidedReadingValue.equals("V")) {
			GuidedReadingValue="22"; 
		}  else if(GuidedReadingValue.equals("W")) {
			GuidedReadingValue="23"; 
		}  else if(GuidedReadingValue.equals("X")) {
			GuidedReadingValue="24"; 
		}  else if(GuidedReadingValue.equals("Y")) {
			GuidedReadingValue="25"; 
		}  else if(GuidedReadingValue.equals("Z")) {
			GuidedReadingValue="26";
		}  else if(GuidedReadingValue.equals("Z+")) {
			GuidedReadingValue="27";
			
		}

		return GuidedReadingValue;
	}

	public static String GuidedReadingAPI(String GuidedReadingValue) {

		if(GuidedReadingValue.equals("A")) {
			GuidedReadingValue="A";  
		} else 	if(GuidedReadingValue.equals("B")) {
			GuidedReadingValue="B";		
		 } else if(GuidedReadingValue.equals("C")) {
			 GuidedReadingValue="C";
		} else if(GuidedReadingValue.equals("D")) {
			GuidedReadingValue="D"; 			
		}  else if(GuidedReadingValue.equals("E")) {
			GuidedReadingValue="E"; 		
		}  else if(GuidedReadingValue.equals("F")) {
			GuidedReadingValue="F"; 
		}  else if(GuidedReadingValue.equals("G")) {
			GuidedReadingValue="G"; 
		}  else if(GuidedReadingValue.equals("H")) {
			GuidedReadingValue="H"; 
		}  else if(GuidedReadingValue.equals("I")) {
			GuidedReadingValue="I"; 
		}  else if(GuidedReadingValue.equals("J")) {
			GuidedReadingValue="J"; 
		}  else if(GuidedReadingValue.equals("K")) {
			GuidedReadingValue="K"; 
		}  else if(GuidedReadingValue.equals("L")) {
			GuidedReadingValue="L"; 
		}  else if(GuidedReadingValue.equals("M")) {
			GuidedReadingValue="M"; 
		}  else if(GuidedReadingValue.equals("N")) {
			GuidedReadingValue="N"; 
		}  else if(GuidedReadingValue.equals("O")) {
			GuidedReadingValue="O"; 
		}  else if(GuidedReadingValue.equals("P")) {
			GuidedReadingValue="P"; 
		}  else if(GuidedReadingValue.equals("Q")) {
			GuidedReadingValue="Q"; 
		}  else if(GuidedReadingValue.equals("R")) {
			GuidedReadingValue="R"; 
		}  else if(GuidedReadingValue.equals("S")) {
			GuidedReadingValue="S"; 
		}  else if(GuidedReadingValue.equals("T")) {
			GuidedReadingValue="T"; 
		}  else if(GuidedReadingValue.equals("U")) {
			GuidedReadingValue="U"; 
		}  else if(GuidedReadingValue.equals("V")) {
			GuidedReadingValue="V"; 
		}  else if(GuidedReadingValue.equals("W")) {
			GuidedReadingValue="W"; 
		}  else if(GuidedReadingValue.equals("X")) {
			GuidedReadingValue="X"; 
		}  else if(GuidedReadingValue.equals("Y")) {
			GuidedReadingValue="Y"; 
		}  else if(GuidedReadingValue.equals("Z")) {
			GuidedReadingValue="Z";
		}  else if(GuidedReadingValue.equals("Z+")) {
			GuidedReadingValue="";
		}

		return GuidedReadingValue;
	}
}
