package com.scholastic.cs.SolrAEMServices;

public class GenreData {

	public static String Genresolr(String GenreValue) {

		if (GenreValue.equals("Fiction")) {
			GenreValue = ("Fiction");
		} else if (GenreValue.equals("Nonfiction")) {
			GenreValue = ("Nonfiction");
			
		} else if (GenreValue.equals("Adventure")) {
			GenreValue = ("Adventure");
		} else if (GenreValue.equals("Advice and Self-Help")) {
			GenreValue = ("\"Advice and Self-Help\"");
		} else if (GenreValue.equals("Anthologies")) {
			GenreValue = ("Anthologies");
		} else if (GenreValue.equals("Biography and Autobiography")) {
			GenreValue = ("\"Biography and Autobiography\"");
		} else if (GenreValue.equals("Classics")) {
			GenreValue = ("Classics");
		} else if (GenreValue.equals("Comedy and Humor")) {
			GenreValue = ("\"Comedy and Humor\"");
		} else if (GenreValue.equals("Comic Books and Graphic Novels")) {
			GenreValue = ("\"Comic Books and Graphic Novels\"");
		} else if (GenreValue.equals("Cookbooks")) {
			GenreValue = ("Cookbooks");
		} else if (GenreValue.equals("Diaries and Journals")) {
			GenreValue = ("\"Diaries and Journals\"");
		} else if (GenreValue.equals("Drama")) {
			GenreValue = ("Drama");
		} else if (GenreValue.equals("Dystopian Fiction")) {
			GenreValue = ("\"Dystopian Fiction\"");
		} else if (GenreValue.equals("Essays and Speeches")) {
			GenreValue = ("\"Essays and Speeches\"");
		} else if (GenreValue.equals("Fairy Tales, Folk Tales, Fables")) {
			GenreValue = ("\"Fairy Tales, Folk Tales, Fables\"");
		} else if (GenreValue.equals("Fantasy")) {
			GenreValue = ("Fantasy");
		} else if (GenreValue.equals("Functional and How-To")) {
			GenreValue = ("\"Functional and How-To\"");
		} else if (GenreValue.equals("Historical Fiction")) {
			GenreValue = ("\"Historical Fiction\"");
		} else if (GenreValue.equals("Horror")) {
			GenreValue = ("Horror");
		} else if (GenreValue.equals("Informational Text")) {
			GenreValue = ("\"Informational Text\"");
		} else if (GenreValue.equals("Interviews")) {
			GenreValue = ("Interviews");
		} else if (GenreValue.equals("Jokes and Riddles")) {
			GenreValue = ("\"Jokes and Riddles\"");
		} else if (GenreValue.equals("Media Tie-Ins")) {
			GenreValue = ("\"Media Tie-Ins\"");
		} else if (GenreValue.equals("Mystery and Suspense")) {
			GenreValue = ("\"Mystery and Suspense\"");
		} else if (GenreValue.equals("Myths and Legends")) {
			GenreValue = ("\"Myths and Legends\"");
		} else if (GenreValue.equals("Poetry, Songs, Verse")) {
			GenreValue = ("\"Poetry, Songs, Verse\"");
		} else if (GenreValue.equals("Realistic Fiction")) {
			GenreValue = ("\"Realistic Fiction\"");
		} else if (GenreValue.equals("Reference")) {
			GenreValue = ("Reference");
		} else if (GenreValue.equals("Romance")) {
			GenreValue = ("Romance");
		} else if (GenreValue.equals("Science Fiction")) {
			GenreValue = ("\"Science Fiction\"");
		} else if (GenreValue.equals("Short Stories")) {
			GenreValue = ("\"Short Stories\"");
		} else if (GenreValue.equals("Young Adult")) {
			GenreValue = ("\"Young Adult\"");

		}

		return GenreValue;
	}

	public static String GenreAPI(String GenreValue) {

		if (GenreValue.equals("Fiction")) {
			GenreValue = ("Fiction");
		} else if (GenreValue.equals("Nonfiction")) {
			GenreValue = ("Nonfiction");
			
		} else if (GenreValue.equals("Adventure")) {
			GenreValue = ("Adventure");
		} else if (GenreValue.equals("Advice and Self-Help")) {
			GenreValue = ("Advice and Self-Help");
		} else if (GenreValue.equals("Anthologies")) {
			GenreValue = ("Anthologies");
		} else if (GenreValue.equals("Biography and Autobiography")) {
			GenreValue = ("Biography and Autobiography");
		} else if (GenreValue.equals("Classics")) {
			GenreValue = ("Classics");
		} else if (GenreValue.equals("Comedy and Humor")) {
			GenreValue = ("Comedy and Humor");
		} else if (GenreValue.equals("Comic Books and Graphic Novels")) {
			GenreValue = ("Comic Books and Graphic Novels");
		} else if (GenreValue.equals("Cookbooks")) {
			GenreValue = ("Cookbooks");
		} else if (GenreValue.equals("Diaries and Journals")) {
			GenreValue = ("Diaries and Journals");
		} else if (GenreValue.equals("Drama")) {
			GenreValue = ("Drama");
		} else if (GenreValue.equals("Dystopian Fiction")) {
			GenreValue = ("Dystopian Fiction");
		} else if (GenreValue.equals("Essays and Speeches")) {
			GenreValue = ("Essays and Speeches");
		} else if (GenreValue.equals("Fairy Tales, Folk Tales, Fables")) {
			GenreValue = ("Fairy Tales, Folk Tales, Fables");
		} else if (GenreValue.equals("Fantasy")) {
			GenreValue = ("Fantasy");
		} else if (GenreValue.equals("Functional and How-To")) {
			GenreValue = ("Functional and How-To");
		} else if (GenreValue.equals("Historical Fiction")) {
			GenreValue = ("Historical Fiction");
		} else if (GenreValue.equals("Horror")) {
			GenreValue = ("Horror");
		} else if (GenreValue.equals("Informational Text")) {
			GenreValue = ("Informational Text");
		} else if (GenreValue.equals("Interviews")) {
			GenreValue = ("Interviews");
		} else if (GenreValue.equals("Jokes and Riddles")) {
			GenreValue = ("Jokes and Riddles");
		} else if (GenreValue.equals("Media Tie-Ins")) {
			GenreValue = ("Media Tie-Ins");
		} else if (GenreValue.equals("Mystery and Suspense")) {
			GenreValue = ("Mystery and Suspense");
		} else if (GenreValue.equals("Myths and Legends")) {
			GenreValue = ("Myths and Legends");
		} else if (GenreValue.equals("Poetry, Songs, Verse")) {
			GenreValue = ("Poetry, Songs, Verse");
		} else if (GenreValue.equals("Realistic Fiction")) {
			GenreValue = ("Realistic Fiction");
		} else if (GenreValue.equals("Reference")) {
			GenreValue = ("Reference");
		} else if (GenreValue.equals("Romance")) {
			GenreValue = ("Romance");
		} else if (GenreValue.equals("Science Fiction")) {
			GenreValue = ("Science Fiction");
		} else if (GenreValue.equals("Short Stories")) {
			GenreValue = ("Short Stories");
		} else if (GenreValue.equals("Young Adult")) {
			GenreValue = ("Young Adult");

		}

		return GenreValue;
	}

}
