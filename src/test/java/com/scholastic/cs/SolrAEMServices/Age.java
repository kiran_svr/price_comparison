package com.scholastic.cs.SolrAEMServices;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.junit.Assert;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.scholastic.coreApiService.Example;
import com.scholastic.coreApiService.Product;


/**
 * 
 * @author Raghu Seethamraju
 *
 */
public class Age {
	private static final Logger log = Logger.getLogger(Age.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("Age.xlsx")
			.getPath();
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) throws SolrServerException, IOException {

		String priceValue = "\"Adventures+and+Adventurers\")";
		
		generateDataToExcel(priceValue);

	}
	
	/**
	 * 
	 * @param AgeValue
	 * @throws SolrServerException
	 * @throws IOException
	 */
	
	public static Boolean generateDataToExcel(String AgeValue) throws SolrServerException, IOException {
		
		XSSFRow row1;
		XSSFSheet  worksheet ;
		
		List<String> reponseThirdEntity=getReponseThirdEntity(SubjectData.SubjectAPI(AgeValue));
		List<String> solrSearch=solrSearch(SubjectData.SubjectAPI(AgeValue));
		
		
		Boolean validationFlag=compareTwoList(reponseThirdEntity,solrSearch);
		System.out.println("validationFlag---"+validationFlag);
		
		
		try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
			
			try {
				worksheet = workbook.getSheet(AgeValue);
				 row1 = worksheet.createRow(0);
				
			}catch (Exception e) {
				  worksheet = workbook.createSheet(AgeValue);
				 row1 = worksheet.createRow(0);
			}
			
			
			Cell cell2 = row1.createCell(0);
			cell2.setCellValue("SOLR");
			Cell cell1 = row1.createCell(1);
			cell1.setCellValue("CORE_API");
	
			
			for (int i = 0; i < solrSearch.size(); i++) {

				XSSFRow row = worksheet.createRow(i + 1);
				row.createCell(0).setCellValue(solrSearch.get(i));
				row.createCell(1).setCellValue(reponseThirdEntity.get(i));

			
			}
			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);) {
				// Save the workbook in .xls file
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
			}
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

			
		}
		if (!validationFlag) {
			Assert.fail("Solr didnt match with API");
		}
	
		return false;
		
	}
	
	/**
	 * 
	 * @param reponseThirdEntity
	 * @param solrSearch
	 * @return
	 */
	private static Boolean compareTwoList(List<String> reponseThirdEntity, List<String> solrSearch) {
		
		
		//Collections.sort(reponseThirdEntity);
		//Collections.sort(solrSearch);

		return solrSearch.equals(reponseThirdEntity);
		

			
	}
	


	
	

	
	/**
	 * 
	 * @param SubjectValue
	 * @return
	 */
	private static List<String> getReponseThirdEntity(String AgeValue) {
			String url="http://advancedfilters.qa.apps.scholastic.tech/search/tso?pageSize=20&pageNumber=1&AGE="+AgeValue;
			System.out.println("Price:"+AgeValue);
			System.out.println(url);
			List<String> list = new ArrayList<>();
		RestTemplate template = new RestTemplate();
		try {
			ResponseEntity<Example> exdate = template.exchange(url, HttpMethod.GET, null, Example.class);

			List<Product> y = exdate.getBody().getProducts();
			for (Product solrDocumentList : y) {
				list.add(solrDocumentList.getIsbn13());
			
			}
			

		} catch (Exception e) {
			list.add("in exception");
		}
		return list;
	}
	
	/**
	 * 
	 * @param priceValue
	 * @return
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public static List<String> solrSearch(String AgeValue) throws SolrServerException, IOException {
		List<String> list1 = new ArrayList<>();
		String urlString = "http://10.45.150.78:8983/solr/global_shard1_replica1/";
		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		((HttpSolrClient) solr).setParser(new XMLResponseParser());
		SolrQuery query = new SolrQuery();
		
	/*	http://10.45.150.78:8983/solr/global_shard1_replica1/select?qt=tchecommfacetsort&q=site_id_s:tso-scholastic-books&
		fq=SortPrice_f:(0+TO+5)&sort=score+desc,units_sold_f+desc,ProductStartDate_dt+desc,id+desc&rows=20&wt=json&indent=true&fl=ISBN13_s   */
		
	//	query.setRequestHandler("tchecommfacetsort");
		query.setParam("shards.qt", "/tchecommfacetsort");
		query.setParam("facet", "true");
		query.setQuery("site_id_s:tso-scholastic-books");
		query.addSort("score",ORDER.desc);
		query.addSort("UNITS_SOLD_f", ORDER.desc);
		query.addSort("ProductStartDate_dt", ORDER.desc);
		query.addSort("id", ORDER.desc);
		query.setRows(20);
		query.setParam("wt", "xml");
	    query.setParam("indent", "true");
		query.setParam("fq", "Age_ss:"+AgeValue);
		query.setParam("fl", "ISBN13_s");	
		QueryResponse response = solr.query(query);
		System.out.println("http://10.45.150.78:8983/solr/global_shard1_replica1/select?"+query);
		org.apache.solr.common.SolrDocumentList list = response.getResults();
		for (SolrDocument solrDocument : list) {
			list1.add(solrDocument.getFieldValue("ISBN13_s").toString());
			System.out.println(list);
		}
		
		return list1;
	}

}
