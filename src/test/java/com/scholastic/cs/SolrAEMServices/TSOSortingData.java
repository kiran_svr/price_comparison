package com.scholastic.cs.SolrAEMServices;

public class TSOSortingData {

	public static String Sortingsolr(String SortingValue) {

		if (SortingValue.equals("Relevance")) {
			SortingValue = ("Relevance");
		} else if (SortingValue.equals("Title A-Z")) {
			SortingValue = ("Title A-Z");		
		} else if (SortingValue.equals("Title Z-A")) {
			SortingValue = ("Title Z-A");
		} else if (SortingValue.equals("Price Low to High")) {
			SortingValue = ("Price Low to High");
		} else if (SortingValue.equals("Price High to Low")) {
			SortingValue = ("Price High to Low");
		} else if (SortingValue.equals("Most Recent")) {
			SortingValue = ("Most Recent");
		} else if (SortingValue.equals("Best Sellers")) {
			SortingValue = ("Best Sellers");
		}

		return SortingValue;
	}

	public static String SortingAPI(String SortingValue) {

		if (SortingValue.equals("Relevance")) {
			SortingValue = ("relevance");
		} else if (SortingValue.equals("Title A-Z")) {
			SortingValue = ("title-asc");		
		} else if (SortingValue.equals("Title Z-A")) {
			SortingValue = ("title-desc");
		} else if (SortingValue.equals("Price Low to High")) {
			SortingValue = ("price-asc");
		} else if (SortingValue.equals("Price High to Low")) {
			SortingValue = ("price-desc");
		} else if (SortingValue.equals("Most Recent")) {
			SortingValue = ("most-recent");
		} else if (SortingValue.equals("Best Sellers")) {
			SortingValue = ("best-sellers");
		}

		return SortingValue;
	}


}
