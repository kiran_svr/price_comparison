package com.scholastic.cs.SolrAEMServices;

public class GradeValue {
	
	public static String gradeValueForSolr(String gradeValue) {
		if(gradeValue.equals("PreK")) {
		gradeValue="\"-1\"";
	} else 	if(gradeValue.equals("K")) {
			gradeValue="0";		
	 } else if(gradeValue.equals("Grade 1")) {
			gradeValue="1";
	} else if(gradeValue.equals("Grade 2")) {
		gradeValue="2"; 			
	}  else if(gradeValue.equals("Grade 3")) {
		gradeValue="3"; 	
	}  else if(gradeValue.equals("Grade 4")) {
		gradeValue="4";
	}  else if(gradeValue.equals("Grade 5")) {
		gradeValue="5";	
	}  else if(gradeValue.equals("Grade 6")) {
		gradeValue="6"; 		
	} else 	if(gradeValue.equals("Grade 7")) {
		gradeValue="7"; 
	}  else if(gradeValue.equals("Grade 8")) {
		gradeValue="8";
	} else if(gradeValue.equals("Grade 9")) {
		gradeValue="9";		
	}else if(gradeValue.equals("Grade 10")) {
		gradeValue="10";
	} else if(gradeValue.equals("Grade 11")) {
		gradeValue="11";
	}else if(gradeValue.equals("Grade 12")) {
		gradeValue="12";		
	}else if(gradeValue.equals("Grade Pre k Grade 1")) {
		gradeValue="(\"1\" OR \"1\")";	
	}else if(gradeValue.equals("Grade 2 Grade 8")) {
		gradeValue="(\"2\" OR \"8\")";
	}else if(gradeValue.equals("Grade prek Grade k")) {
		gradeValue="(\"-1\" OR 0)";				
	}else if(gradeValue.equals("Grade prek Grade 12")) {
		gradeValue="(\"-1\" OR 12)";	
		
		
		
		
	}
		System.out.println("gradeValue---->>>>"+gradeValue);
	return gradeValue;
	}
	
	
	
	public static String gradeValueForApi(String gradeValue) {
		if(gradeValue.equals("PreK")) {
		gradeValue="\"-1\"";
	} else 	if(gradeValue.equals("K")) {
			gradeValue="0";		
	 } else if(gradeValue.equals("Grade 1")) {
			gradeValue="1";
	} else if(gradeValue.equals("Grade 2")) {
		gradeValue="2"; 			
	}  else if(gradeValue.equals("Grade 3")) {
		gradeValue="3"; 	
	}  else if(gradeValue.equals("Grade 4")) {
		gradeValue="4";
	}  else if(gradeValue.equals("Grade 5")) {
		gradeValue="5";	
	}  else if(gradeValue.equals("Grade 6")) {
		gradeValue="6"; 		
	} else 	if(gradeValue.equals("Grade 7")) {
		gradeValue="7"; 
	}  else if(gradeValue.equals("Grade 8")) {
		gradeValue="8";
	} else if(gradeValue.equals("Grade 9")) {
		gradeValue="9";		
	}else if(gradeValue.equals("Grade 10")) {
		gradeValue="10";
	} else if(gradeValue.equals("Grade 11")) {
		gradeValue="11";
	}else if(gradeValue.equals("Grade 12")) {
		gradeValue="12";		
	}else if(gradeValue.equals("Grade Pre k Grade 1")) {
		gradeValue="(\"1\" OR \"1\")";	
	}else if(gradeValue.equals("Grade 2 Grade 8")) {
		gradeValue="(\"2\" OR \"8\")";
	}else if(gradeValue.equals("Grade prek Grade k")) {
		gradeValue="(-1 OR 0)";				
	}else if(gradeValue.equals("Grade prek Grade 12")) {
		gradeValue="(-1 OR 12)";	
		
		
		
		
	}
		System.out.println("gradeValue---->>>>"+gradeValue);
	return gradeValue;
	}

}
