package com.scholastic.cs.SolrAEMServices;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.scholastic.cs.pricecompare.aem.MyPojo;
import com.scholastic.cs.pricecompare.aem.SolrDocumentList;


public class AEMController {
	
	private static final Logger log = Logger.getLogger(AEMController.class.getName());
	private static final String FILE_NAME_READ = ClassLoader.getSystemClassLoader().getResource("SolrInputData.xlsx").getPath();
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("SolrOutputData.xlsx").getPath();

		
	
	public static void main(String[] args) {
		readUrlsFromExcel();
	}
	
	
	public static List<String> getReponseThirdEntity(String url) {
		String url12="https://www-qa2.scholastic.com/bin/scholastic/teachers"
				+ "/ecommerce/common-search.json?appType=tso&isSSO=false&rows="
				+ "40&siteID=tso-scholastic-books&sort=UNITS_SOLD_f_desc,"
				+ "ProductStartDate_dt_desc&text="+url;
		List<String> list=new ArrayList<>();
		StringBuilder jsonValue = new StringBuilder();
		RestTemplate template = new RestTemplate();
		try {
			ResponseEntity<MyPojo> exdate = template.exchange(url12, HttpMethod.GET, null, MyPojo.class);
			
			
			System.out.println(exdate.getBody().getSolrDocumentList());
			SolrDocumentList[] y=exdate.getBody().getSolrDocumentList();
			for (SolrDocumentList solrDocumentList : y) {
				System.out.println(solrDocumentList.getId());
				list.add(solrDocumentList.getId());
			}
			
		} catch (Exception e) {
			jsonValue.append("in exception");
		}
		return list;
	}
	
	public static String readUrlsFromExcel() {
		log.log(Level.INFO, "into the  readUrlsFromExcel");
		Map<String, List<String>> map=new HashMap<>();
		String cellValue = null;
		try (FileInputStream excelFile = new FileInputStream(FILE_NAME_READ);
				XSSFWorkbook workbook = new XSSFWorkbook(excelFile);) {
			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				String lastTwo = null;

				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					if (!cell.getStringCellValue().isEmpty()) {
						 cellValue = cell.getStringCellValue();
						log.info(cellValue);
						
						map.put(cellValue, getReponseThirdEntity( cellValue));

					} else {
						log.log(Level.INFO, "into the  readUrlsFromExcel check whether the cell type is",
								CellType.NUMERIC);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.log(Level.INFO, "into the exception in the readURL", e.fillInStackTrace());
		}
		System.out.println("map---"+map);
		generateDataToExcel(map);
		return cellValue;
	}
	
	

	private static void generateDataToExcel(Map<String, List<String>> map) {
		try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
			XSSFSheet worksheet = workbook.getSheet("AEMOUTPUT");
			

			
			int headingNumber=0;
			int rowNumber=1;
			for (Map.Entry<String, List<String>> entry : map.entrySet()) {
				log.info("Item : " + entry.getKey() + " Count : " + entry.getValue());
				List<String> stringList = entry.getValue();
				XSSFCellStyle  style = workbook.createCellStyle();
				style.setFillForegroundColor(IndexedColors.AQUA.getIndex());
				XSSFFont font = workbook.createFont();
				font.setBold(true);	
				style.setFont(font);   
				if(rowNumber>1){
					headingNumber=rowNumber+2;
					rowNumber=headingNumber+1;
				}
				
				XSSFRow row = worksheet.createRow(headingNumber);				
				Cell cell = row.createCell(0);
				cell.setCellValue(entry.getKey());
				cell.setCellStyle(style);
				
				for (int j = 1; j < entry.getValue().size() + 1; j++) {
					System.out.println("--->>"+rowNumber);
					XSSFRow row1 = worksheet.createRow(rowNumber);
					
					Cell cell1 = row1.createCell(0);
					cell1.setCellValue(stringList.get(j - 1));
					rowNumber++;

				}
				

			}

			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);
					){
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
			}
			// Save the workbook in .xls file
			
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

		}

	}

}
