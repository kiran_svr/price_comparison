package com.scholastic.cs.SolrAEMServices;

public class CollectionsOfTypes {
	
		 
		   
		   public static final String [] funding_types= {"TitleI Part A","Title I SchoolImprovement",
				   "Title I DirectStudentServices","Title II PartA","Title II PartB LEARN","TitleIII",
				   "TitleIV PartA SSAEG","TitleIV PartB 21stCCLC","IDEA","TitleII PartB Learn","SpecialEducationRTI"};

		   public static final String [] genre_types= {"Adventure","Advice and Self-Help","Anthologies",
				   "Biography and Autobiography","Classics","Comedy and Humor","Comic Books and Graphic Novels",
				   "Cookbooks","Diaries and Journals","Drama","Dystopian Fiction","Essays and Speeches","Fairy Tales,"
				   		+ " Folk Tales, Fables","Fantasy","Functional and How-To","Historical Fiction","Horror",
				   		"Informational Text","Interviews","Jokes and Riddles","Media Tie-Ins","Mystery and Suspense",
				   		"Myths and Legends","Poetry, Songs, Verse","Realistic Fiction","Reference","Romance","Science Fiction",
				   		"Short Stories","Young Adult"};
		   
		  public static final String [] Fiction_NonFinction_types= {"Fiction","Nonfiction"};
		  
		  
		  public static final String [] Fiction__Gener_types= {"Fiction;Adventure","Fiction;Advice and Self-Help","Fiction;Anthologies","Fiction;Biography and Autobiography","Fiction;Classics",
				  "Fiction;Comedy and Humor","Fiction;Comic Books and Graphic Novels","Fiction;Cookbooks",
				  "Fiction;Diaries and Journals","Fiction;Drama","Fiction;Dystopian Fiction","Fiction;Essays and Speeches",
				  "Fiction;Fairy Tales, Folk Tales, Fables","Fiction;Fantasy","Fiction;Functional and How-To","Fiction;Historical Fiction",
				  "Fiction;Horror","Fiction;Informational Text","Fiction;Interviews","Fiction;Jokes and Riddles","Fiction;Media Tie-Ins",
				  "Fiction;Mystery and Suspense","Fiction;Myths and Legends","Fiction;Poetry, Songs, Verse","Fiction;Realistic Fiction","Fiction;Reference"
				  ,"Fiction;Romance","Fiction;Science Fiction","Fiction;Short Stories","Fiction;Young Adult"};
		  


		  public static final String [] NonFinction_Gener_types= {"Nonfiction;Adventure","Nonfiction;Advice and Self-Help","Nonfiction;Anthologies","Nonfiction;Biography and Autobiography","Nonfiction;Classics","Nonfiction;Comedy and Humor",
				  "Nonfiction;Comic Books and Graphic Novels","Nonfiction;Cookbooks","Nonfiction;Diaries and Journals","Nonfiction;Drama",
				  "Nonfiction;Dystopian Nonfiction","Nonfiction;Essays and Speeches","Nonfiction;Fairy Tales, Folk Tales, Fables","Nonfiction;Fantasy",
				  "Nonfiction;Functional and How-To","Nonfiction;Historical Fiction","Nonfiction;Horror","Nonfiction;Informational Text",
				  "Nonfiction;Interviews","Nonfiction;Jokes and Riddles","Nonfiction;Media Tie-Ins","Nonfiction;Mystery and Suspense",
				  "Nonfiction;Myths and Legends","Nonfiction;Poetry, Songs, Verse","Nonfiction;Realistic Fiction","Nonfiction;Reference",
				  "Nonfiction;Romance","Nonfiction;Science Fiction","Nonfiction;Short Stories","Nonfiction;Young Adult"};
		  
		  
		  
		  public static final String [] Format= {"Books","Books | Individual Titles","Books | IT | Activity Book","Books | IT | Activity Sheet","Books | IT | Big_Book","Books | IT | Board_Books","Books | IT | eBook","Books | IT | Hardcover_Books"
				  ,"Books | IT | HB_Hardcover_Book_and_CD","Books | IT | Interactive_&_Novelty_Book","Books | IT | Library_Binding",
				  "Books | IT | Paperback Book","Books | IT | PB_Paperback_Book_and_Cassette","Books | IT | PB_and_CD","Books| IT | Picture_Book",
				  "Books | Collections_and_Libraries","Books | CL | Assessment_Collection","Books | CL | Big_Book_Collection","Books | CL | Board_Book_Collection",
				  "Books | CL | Boxed_Set","Books | CL | Classroom_Library","Books | CL | Classroom_Program","Books | CL | Family_Engagement_Collection",
				  "Books | CL | Guided_Reading_Collection","Books | CL | Hardcover_Book_Collection","Books | CL | Literacy_Collection",
				  "Books | CL | Mentoring_Collection","Books | CL | Paperback_Book_Collection","Books | CL | Take_Home_Collection","Books | Magazines","Books | Magazines | Classroom_Magazines","Books | Magazines | Professional_Resources",
				  "Books | Magazines | Activity_Pack","Books | Magazines | eBook","Books | Magazines | Literature_Guide","Books | Magazines | Professional_Book",
				  "Books | Magazines | PB_and_CD","Books | Magazines | PB_and_DVD","Books | Magazines | Teaching_Card",
				  "Books | Magazines | Teaching_Guide","Books | Magazines | TG_Student_Edition","Books | Magazines | TG_Teacher_Edition",
				  "Digital Media Resources","DMR | Audio","DMR | Audio | CD","DMR | Audio | CD | Hardcover_Book_and_CD",
				  "DMR | Audio | CD | Paperback_Book_and_CD","DMR | Audio | CD | Paperback_Books_and_CD","DMR | Audio | CD_Playaway_Audio",
				  "DMR | Audio | DVD-Audio","DMR | Collections","DMR | Collections | CD_Collection","DMR | Collections | Software_Collection",
				  "DMR | Software","DMR | Software | DVD","DMR | Subscriptions","DMR | Subscriptions | FreedomFlix",
				  "DMR | Subscriptions | New_Book_of_Knowledge","DMR | Subscriptions | Scholastic_Teachables","DMR | Subscriptions | ScienceFlix",
				  "DMR | Subscriptions | Storia","DMR | Video","DMR | Video | DVD","DMR | Video | Playaway_View","Classroom Materials","Sales_Materials"};



		   
		  public static final String [] Subject= {"Adventures and Adventurers","Animals","Animals | African_Animals","Arts and Creativity","Bedtime, Sleep, Dreams","Character and Values"
				  ,"Child Development and Behavior","Families and Relationships","Feelings and Emotions",
				  "Food and Drinks","FL and EL Learning","Health and Safety",
				  "Hobbies, Play, Recreation","Holidays and Celebrations","Language Arts",
				  "Life Experiences","Magic and Supernatural","Popular Culture","Religion and Philosophy",
				  "School Life","Science and Technology","Seasons","Social Studies","Sports","Transportation"};
}


