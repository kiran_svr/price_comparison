package com.scholastic.cs.SolrAEMServices;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

public class SolrSearchMain {
	private static final Logger log = Logger.getLogger(SolrSearchMain.class.getName());
	private static final String FILE_NAME_READ = ClassLoader.getSystemClassLoader().getResource("InputData.xlsx").getPath();
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("OutputData.xlsx").getPath();

		
		public static List<String> solrSearchmethod(String urlValue) throws SolrServerException, IOException {
			System.out.println("urlValue---"+urlValue);
			List<String> list1=new LinkedList<>();
		String urlString = "http://10.45.150.78:8983/solr/global";
		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		((HttpSolrClient) solr).setParser(new XMLResponseParser());

	//	Solr Quary which was provided by AEM 
	//	http://10.45.150.78:8983/solr/global/select?qt=/tchecommfacet&fq=site_id_s:tso-scholastic-books&fq=Searchable_s:%22TRUE%22&q=harry&sort=UNITS_SOLD_f+desc,ProductStartDate_dt+desc&rows=20&start=0&spellcheck=true&shards.qt=/tchecommfacet
		
		SolrQuery query = new SolrQuery();
		query.setQuery(urlString);
		query.setParam("fq", "site_id_s:tso-scholastic-books AND Searchable_s:TRUE");
		query.addSort("UNITS_SOLD_f", ORDER.desc);
		query.addSort("ProductStartDate_dt", ORDER.desc);
		query.setParam("spellcheck", "true");
		query.setRequestHandler("tchecommfacet");
		query.setParam("shards.qt", "/tchecommfacet");
		query.setRows(40);
		query.setStart(0);
		QueryResponse response = solr.query(query);
		System.out.println(query);
		SolrDocumentList list = response.getResults();
		
		for (SolrDocument solrDocument : list) {

			list1.add(solrDocument.getFieldValue("id").toString());
			System.out.println(solrDocument.getFieldValue("id"));
		}
		
		return list1;

	}
		public static void main(String[] args) throws SolrServerException, IOException {
//solrSearchmethod();
readUrlsFromExcel();
	}
		
		
		public static String readUrlsFromExcel() {
			log.log(Level.INFO, "into the  readUrlsFromExcel");
			Map<String, List<String>> map=new HashMap<>();
			String cellValue = null;
			try (FileInputStream excelFile = new FileInputStream(FILE_NAME_READ);
					XSSFWorkbook workbook = new XSSFWorkbook(excelFile);) {
				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);
				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();

				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();
					String lastTwo = null;

					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();

						if (!cell.getStringCellValue().isEmpty()) {
							 cellValue = cell.getStringCellValue();
							log.info(cellValue);
							
							map.put(cellValue, solrSearchmethod(cellValue));

						} else {
							log.log(Level.INFO, "into the  readUrlsFromExcel check whether the cell type is",
									CellType.NUMERIC);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.log(Level.INFO, "into the exception in the readURL", e.fillInStackTrace());
			}
			System.out.println("map---"+map);
			generateDataToExcel(map);
			return cellValue;
		}
		
		

		private static void generateDataToExcel(Map<String, List<String>> map) {
			try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
					XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
				XSSFSheet worksheet = workbook.getSheet("SolrOUTPUT");
				

			int headingNumber=0;
			int rowNumber=1;
			for (Map.Entry<String, List<String>> entry : map.entrySet()) {
				log.info("Item : " + entry.getKey() + " Count : " + entry.getValue());
				List<String> stringList = entry.getValue();
				XSSFCellStyle  style = workbook.createCellStyle();
				style.setFillForegroundColor(IndexedColors.AQUA.getIndex());
				XSSFFont font = workbook.createFont();
				font.setBold(true);	
				style.setFont(font);   
				if(rowNumber>1){
					headingNumber=rowNumber+2;
					rowNumber=headingNumber+1;
				}
				
				XSSFRow row = worksheet.createRow(headingNumber);				
				Cell cell = row.createCell(0);
				cell.setCellValue(entry.getKey());
				cell.setCellStyle(style);
				
				for (int j = 1; j < entry.getValue().size() + 1; j++) {
					System.out.println("--->>"+rowNumber);
					XSSFRow row1 = worksheet.createRow(rowNumber);
					
					Cell cell1 = row1.createCell(0);
					cell1.setCellValue(stringList.get(j - 1));
					rowNumber++;

				}
				

			}

				try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE)){
					// Save the workbook in .xls file
					workbook.write(fos);
					fos.flush();
					log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
				}
				
			} catch (FileNotFoundException e) {
				e.getMessage();
				log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

			} catch (IOException e) {
				log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

			}

		}

}
