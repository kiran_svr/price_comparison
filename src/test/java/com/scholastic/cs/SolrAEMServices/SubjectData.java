package com.scholastic.cs.SolrAEMServices;

public class SubjectData {

	public static String Subjectsolr(String SubjectValue) {
		
		
		if(SubjectValue.equals("Adventures and Adventurers")) {
			SubjectValue="Adventures_and_Adventurers";
		} else 	if(SubjectValue.equals("Animals")) {
			SubjectValue="Animals";	
		} else 	if(SubjectValue.equals("Animals | African_Animals")) {
			SubjectValue="Animals/African_Animals";	
		 } else if(SubjectValue.equals("Arts and Creativity")) {
			 SubjectValue="Arts_and_Creativity";
		} else if(SubjectValue.equals("Bedtime, Sleep, Dreams")) {
			SubjectValue="Bedtime,_Sleep,_Dreams"; 			
		}  else if(SubjectValue.equals("Character and Values")) {
			SubjectValue="Character_and_Values"; 		
		}  else if(SubjectValue.equals("Child Development and Behavior")) {
			SubjectValue="Child_Development_and_Behavior"; 
		}  else if(SubjectValue.equals("Families and Relationships")) {
			SubjectValue="Families_and_Relationships"; 
		}  else if(SubjectValue.equals("Feelings and Emotions")) {
			SubjectValue="Feelings_and_Emotions"; 
		}  else if(SubjectValue.equals("Food and Drinks")) {
			SubjectValue="Food_and_Drinks"; 
		}  else if(SubjectValue.equals("FL and EL Learning")) {
			SubjectValue="Foreign_Languages_and_English_Language_Learning"; 
		}  else if(SubjectValue.equals("Health and Safety")) {
			SubjectValue="Health_and_Safety"; 
		}  else if(SubjectValue.equals("Hobbies, Play, Recreation")) {
			SubjectValue="Hobbies,_Play,_Recreation"; 
		}  else if(SubjectValue.equals("Holidays and Celebrations")) {
			SubjectValue="Holidays_and_Celebrations"; 
		}  else if(SubjectValue.equals("Language Arts")) {
			SubjectValue="Language_Arts"; 
		}  else if(SubjectValue.equals("Life Experiences")) {
			SubjectValue="Life_Experiences"; 
		}  else if(SubjectValue.equals("Magic and Supernatural")) {
			SubjectValue="Magic_and_Supernatural"; 
		}  else if(SubjectValue.equals("Popular Culture")) {
			SubjectValue="Popular_Culture"; 
		}  else if(SubjectValue.equals("Religion and Philosophy")) {
			SubjectValue="Religion_and_Philosophy"; 
		}  else if(SubjectValue.equals("School Life")) {
			SubjectValue="School_Life"; 
		}  else if(SubjectValue.equals("Science and Technology")) {
			SubjectValue="Science_and_Technology"; 
		}  else if(SubjectValue.equals("Seasons")) {
			SubjectValue="Seasons"; 
		}  else if(SubjectValue.equals("Social Studies")) {
			SubjectValue="Social_Studies"; 
		}  else if(SubjectValue.equals("Sports")) {
			SubjectValue="Sports"; 
		}  else if(SubjectValue.equals("Transportation ")) {
			SubjectValue="Transportation "; 
		}

		

		return SubjectValue;
	}

	public static String SubjectAPI(String SubjectValue) {

		if(SubjectValue.equals("Adventures and Adventurers")) {
			SubjectValue="Adventures_and_Adventurers";
		} else 	if(SubjectValue.equals("Animals")) {
			SubjectValue="Animals";	
		} else 	if(SubjectValue.equals("Animals | African_Animals")) {
			SubjectValue="Animals/African_Animals";	
		 } else if(SubjectValue.equals("Arts and Creativity")) {
			 SubjectValue="Arts_and_Creativity";
		} else if(SubjectValue.equals("Bedtime, Sleep, Dreams")) {
			SubjectValue="Bedtime,_Sleep,_Dreams"; 			
		}  else if(SubjectValue.equals("Character and Values")) {
			SubjectValue="Character_and_Values"; 		
		}  else if(SubjectValue.equals("Child Development and Behavior")) {
			SubjectValue="Child_Development_and_Behavior"; 
		}  else if(SubjectValue.equals("Families and Relationships")) {
			SubjectValue="Families_and_Relationships"; 
		}  else if(SubjectValue.equals("Feelings and Emotions")) {
			SubjectValue="Feelings_and_Emotions"; 
		}  else if(SubjectValue.equals("Food and Drinks")) {
			SubjectValue="Food_and_Drinks"; 
		}  else if(SubjectValue.equals("FL and EL Learning")) {
			SubjectValue="Foreign_Languages_and_English_Language_Learning"; 
		}  else if(SubjectValue.equals("Health and Safety")) {
			SubjectValue="Health_and_Safety"; 
		}  else if(SubjectValue.equals("Hobbies, Play, Recreation")) {
			SubjectValue="Hobbies,_Play,_Recreation"; 
		}  else if(SubjectValue.equals("Holidays and Celebrations")) {
			SubjectValue="Holidays_and_Celebrations"; 
		}  else if(SubjectValue.equals("Language Arts")) {
			SubjectValue="Language_Arts"; 
		}  else if(SubjectValue.equals("Life Experiences")) {
			SubjectValue="Life_Experiences"; 
		}  else if(SubjectValue.equals("Magic and Supernatural")) {
			SubjectValue="Magic_and_Supernatural"; 
		}  else if(SubjectValue.equals("Popular Culture")) {
			SubjectValue="Popular_Culture"; 
		}  else if(SubjectValue.equals("Religion and Philosophy")) {
			SubjectValue="Religion_and_Philosophy"; 
		}  else if(SubjectValue.equals("School Life")) {
			SubjectValue="School_Life"; 
		}  else if(SubjectValue.equals("Science and Technology")) {
			SubjectValue="Science_and_Technology"; 
		}  else if(SubjectValue.equals("Seasons")) {
			SubjectValue="Seasons"; 
		}  else if(SubjectValue.equals("Social Studies")) {
			SubjectValue="Social_Studies"; 
		}  else if(SubjectValue.equals("Sports")) {
			SubjectValue="Sports"; 
		}  else if(SubjectValue.equals("Transportation ")) {
			SubjectValue="Transportation "; 
		}

		return SubjectValue;
	}
}
