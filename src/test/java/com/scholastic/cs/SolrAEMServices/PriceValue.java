package com.scholastic.cs.SolrAEMServices;

public class PriceValue {
	
	public static String priceValueForApi(String priceValue) {
		if(priceValue.equals("Under $5")) {
			priceValue="Under $5";
		} else 	if(priceValue.equals("$5 - $25")) {
			priceValue="$5 - $25";			
		} else 	if(priceValue.equals("$25 - $50")) {
				priceValue="$25 - $50";		
		 } else if(priceValue.equals("$50 - $100")) {
				priceValue="$50 - $100";
		} else if(priceValue.equals("$100 - $200")) {
			priceValue="$100 - $200"; 			
		}  else if(priceValue.equals("$200")) {
			priceValue="Over $200"; 			
		}
		return priceValue;
	}
	
	public static String priceValueForSolr(String priceValue) {
		if(priceValue.equals("Under $5")) {
			priceValue="[0 TO 5]";
		} else 	if(priceValue.equals("$5 - $25")) {
			priceValue="[5  TO 25]";	
		} else 	if(priceValue.equals("$25 - $50")) {
				priceValue="[25 TO 50]";		
		 } else if(priceValue.equals("$50 - $100")) {
				priceValue="[50 TO 100]";
		} else if(priceValue.equals("$100 - $200")) {
			priceValue="[100 TO 200]"; 			
		}  else if(priceValue.equals("$200")) {
			priceValue="[200 TO *]"; 			
		}
		return priceValue;
		
		
	}

}
