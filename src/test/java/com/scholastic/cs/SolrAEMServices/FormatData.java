package com.scholastic.cs.SolrAEMServices;

public class FormatData {

	public static String Formatsolr(String FormatValue) {

		if (FormatValue.equals("Books")) {
			FormatValue = "Books";	
			
		} else if (FormatValue.equals("Books | Individual Titles")) {
			FormatValue = "Books/Individual_Titles";

		} else if (FormatValue.equals("Books | IT | Activity Book")) {
			FormatValue = "Books/Individual_Titles/Activity_Book";

		} else if (FormatValue.equals("Books | IT | Activity Sheet")) {
			FormatValue = "Books/Individual_Titles/Activity_Sheet";

		} else if (FormatValue.equals("Books | IT | Big_Book")) {
			FormatValue = "Books/Individual_Titles/Big_Book";

		} else if (FormatValue.equals("Books | IT | Board_Books")) {
			FormatValue = "Books/Individual_Titles/Board_Books";
			
		} else if (FormatValue.equals("Books | IT | eBook")) {
			FormatValue = "Books/Individual_Titles/eBook";

		} else if (FormatValue.equals("Books | IT | Hardcover_Books")) {
			FormatValue = "Books/Individual_Titles/Hardcover_Books";
			
		} else if (FormatValue.equals("Books | IT | HB_Hardcover_Book_and_CD")) {
			FormatValue = "Books/Individual_Titles/Hardcover_Books/Hardcover_Book_and_CD";

		} else if (FormatValue.equals("Books | IT | Interactive_&_Novelty_Book")) {
			FormatValue = "Books/Individual_Titles/Interactive_&_Novelty_Book";

		} else if (FormatValue.equals("Books | IT | Library_Binding")) {
			FormatValue = "Books/Individual_Titles/Library_Binding";

		} else if (FormatValue.equals("Books | IT | Paperback Book")) {
			FormatValue = "Books/Individual_Titles/Paperback_Book";
			
		} else if (FormatValue.equals("Books | IT | PB_Paperback_Book_and_Cassette")) {
			FormatValue = "Books/Individual_Titles/Paperback_Book/Paperback_Book_and_Cassette";
			
		} else if (FormatValue.equals("Books | IT | PB_and_CD")) {
			FormatValue = "Books/Individual_Titles/Paperback_Book/Paperback_Book_and_CD";

		} else if (FormatValue.equals("Books| IT | Picture_Book")) {
			FormatValue = "Books/Individual_Titles/Picture_Book";
		
		} else if (FormatValue.equals("Books | Collections_and_Libraries")) {
			FormatValue = "Books/Collections_and_Libraries";

		} else if (FormatValue.equals("Books | CL | Assessment_Collection")) {
			FormatValue = "Books/Collections_and_Libraries/Assessment_Collection";
			
		} else if (FormatValue.equals("Books | CL | Big_Book_Collection")) {
			FormatValue = "Books/Collections_and_Libraries/Big_Book_Collection";
			
		} else if (FormatValue.equals("Books | CL | Board_Book_Collection")) {
			FormatValue = "Books/Collections_and_Libraries/Board_Book_Collection";
					
		} else if (FormatValue.equals("Books | CL | Boxed_Set")) {
			FormatValue = "Books/Collections_and_Libraries/Boxed_Set";
					
       } else if (FormatValue.equals("Books | CL | Classroom_Library")) {
     	FormatValue = "Books/Collections_and_Libraries/Classroom_Library";
     
       } else if (FormatValue.equals("Books | CL | Classroom_Program")) {
     	FormatValue = "Books/Collections_and_Libraries/Classroom_Program";
     	
       } else if (FormatValue.equals("Books | CL | Family_Engagement_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Family_Engagement_Collection";
     	
       } else if (FormatValue.equals("Books | CL | Guided_Reading_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Guided_Reading_Collection";
     	
       } else if (FormatValue.equals("Books | CL | Hardcover_Book_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Hardcover_Book_Collection";
     	
       } else if (FormatValue.equals("Books | CL | Literacy_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Literacy_Collection";

       } else if (FormatValue.equals("Books | CL | Mentoring_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Mentoring_Collection";
     	
       } else if (FormatValue.equals("Books | CL | Paperback_Book_Collection")) {
        	FormatValue = "Books/Collections_and_Libraries/Paperback_Book_Collection";

       } else if (FormatValue.equals("Books | CL | Take_Home_Collection")) {
       	FormatValue = "Books/Collections_and_Libraries/Take_Home_Collection ";
     	

       } else if (FormatValue.equals("Books | Magazines")) {
       	FormatValue = "Books/Magazines";

       } else if (FormatValue.equals("Books | Magazines | Classroom_Magazines")) {
          	FormatValue = "Books/Magazines/Classroom_Magazines";

       } else if (FormatValue.equals("Books | Magazines | CM_Classroom_Subscription_-_Print")) {
         	FormatValue = "Books/Magazines/Classroom_Magazines/Classroom_Subscription_-_Print";
     
       } else if (FormatValue.equals("Books | Magazines | Professional_Resources")) {
         	FormatValue = "Books/Professional_Resources";
         	
       } else if (FormatValue.equals("Books | Magazines | Activity_Pack")) {
        	FormatValue = "Books/Professional_Resources/Activity_Pack";
         	
       } else if (FormatValue.equals("Books | Magazines | eBook")) {
       	FormatValue = "Books/Professional_Resources/eBook";
       	
       } else if (FormatValue.equals("Books | Magazines | Literature_Guide")) {
          	FormatValue = "Books/Professional_Resources/Literature_Guide";	
          	
          	
       } else if (FormatValue.equals("Books | Magazines | Professional_Book")) {
         	FormatValue = "Books/Professional_Resources/Professional_Book";	
         	
         	
       } else if (FormatValue.equals("Books | Magazines | PB_and_CD")) {
        	FormatValue = "Books/Professional_Resources/Professional_Book/Professional_Book_and_CD";	
        	
       } else if (FormatValue.equals("Books | Magazines | PB_and_DVD")) {
       	FormatValue = "Books/Professional_Resources/Professional_Book/Professional_Book_and_DVD";
       	
       } else if (FormatValue.equals("Books | Magazines | Teaching_Card")) {
        	FormatValue = "Books/Professional_Resources/Teaching_Card";	
        	
        	
       } else if (FormatValue.equals("Books | Magazines | Teaching_Guide")) {
       	FormatValue = "Books/Professional_Resources/Teaching_Guide";	
       	
       } else if (FormatValue.equals("Books | Magazines | TG_Student_Edition")) {
          	FormatValue = "Books/Professional_Resources/Teaching_Guide/Student_Edition";	
       	
       } else if (FormatValue.equals("Books | Magazines | TG_Teacher_Edition")) {
         	FormatValue = "Books/Professional_Resources/Teaching_Guide/Teacher_Edition";

		} else if (FormatValue.equals("Digital Media Resources")) {
			FormatValue = "Digital_&_Media_Resources";
			
		} else if (FormatValue.equals("DMR | Audio")) {
			FormatValue = "Digital_&_Media_Resources/Audio";
				
		} else if (FormatValue.equals("DMR | Audio | CD")) {
			FormatValue = "Digital_&_Media_Resources/Audio";
			
		} else if (FormatValue.equals("DMR | Audio | CD | Hardcover_Book_and_CD")) {
			FormatValue = "Digital_&_Media_Resources/Audio/CD/Hardcover_Book_and_CD";	
			
		} else if (FormatValue.equals("DMR | Audio | CD | Paperback_Book_and_CD")) {
			FormatValue = "Digital_&_Media_Resources/Audio/CD/Paperback_Book_and_CD";
		
		} else if (FormatValue.equals("DMR | Audio | CD_Playaway_Audio")) {
			FormatValue = "Digital_&_Media_Resources/Audio/CD_Playaway_Audio";
			
		} else if (FormatValue.equals("DMR | Audio | DVD-Audio")) {
			FormatValue = "Digital_&_Media_Resources/Audio/DVD-Audio";

		} else if (FormatValue.equals("DMR | Collections")) {
			FormatValue = "Digital_&_Media_Resources/Collections";
			
			
		} else if (FormatValue.equals("DMR | Collections | CD_Collection")) {
			FormatValue = "Digital_&_Media_Resources/Collections/CD_Collection";
			
			
		} else if (FormatValue.equals("DMR | Collections | Software_Collection")) {
			FormatValue = "	Digital_&_Media_Resources/Collections/Software_Collection";

		} else if (FormatValue.equals("DMR | Software")) {
			FormatValue = "Digital_&_Media_Resources/Software";
		
		} else if (FormatValue.equals("DMR | Software | DVD")) {
			FormatValue = "Digital_&_Media_Resources/Software/DVD";
			
		} else if (FormatValue.equals("DMR | Subscriptions")) {
			FormatValue = "Digital_&_Media_Resources/Subscriptions";

		} else if (FormatValue.equals("DMR | Subscriptions | FreedomFlix")) {
			FormatValue = "Digital_&_Media_Resources/Subscriptions/FreedomFlix";
			
		} else if (FormatValue.equals("DMR | Subscriptions | New_Book_of_Knowledge")) {
			FormatValue = "Digital_&_Media_Resources/Subscriptions/New_Book_of_Knowledge";
			
		} else if (FormatValue.equals("DMR | Subscriptions | Scholastic_Teachables")) {
			FormatValue = "Digital_&_Media_Resources/Subscriptions/Scholastic_Teachables";
		
		} else if (FormatValue.equals("DMR | Subscriptions | ScienceFlix")) {
			FormatValue = "Digital_&_Media_Resources/Subscriptions/ScienceFlix";
		
		} else if (FormatValue.equals("DMR | Subscriptions | Storia")) {
			FormatValue = "Digital_&_Media_Resources/Subscriptions/Storia";
			
		} else if (FormatValue.equals("DMR | Video")) {
			FormatValue = "Digital_&_Media_Resources/Video";

		} else if (FormatValue.equals("DMR | Video | DVD")) {
			FormatValue = "Digital_&_Media_Resources/Video/DVD";
			
		} else if (FormatValue.equals("DMR | Video | Playaway_View")) {
			FormatValue = "Digital_&_Media_Resources/Video/Playaway_View";
			
			/*
			Classroom_Materials
			Classroom_Materials/Classroom_Games
			Classroom_Materials/Classroom_Games/Games_&_Puzzles
			Classroom_Materials/Classroom_Games/Learning_Centers
		
			Classroom_Materials/D&#233;cor
			Classroom_Materials/D&#233;cor/Accents_and_Cut-Outs
			Classroom_Materials/D&#233;cor/Borders_and_Trimmers
			Classroom_Materials/D&#233;cor/Bulletin_Boards
			Classroom_Materials/D&#233;cor/Bulletin_Boards/Mini_Bulletin_Boards
			Classroom_Materials/D&#233;cor/Calendar
			Classroom_Materials/D&#233;cor/Calendars
			Classroom_Materials/D&#233;cor/Chart
			Classroom_Materials/D&#233;cor/Classroom_Accessories
			Classroom_Materials/D&#233;cor/Flip_Chart
			Classroom_Materials/D&#233;cor/Journal
			Classroom_Materials/D&#233;cor/Pocket_Chart
			Classroom_Materials/D&#233;cor/Poster
			
			Classroom_Materials/Storage_&_Organization
			Classroom_Materials/Storage_&_Organization/Bags
			Classroom_Materials/Storage_&_Organization/Bins
			Classroom_Materials/Storage_&_Organization/Shelves
			
			Classroom_Materials/Supplies
			Classroom_Materials/Supplies/Design_Paper
			Classroom_Materials/Supplies/Folder
			Classroom_Materials/Supplies/Name_Plates
			Classroom_Materials/Supplies/Notebooks_and_Notepads
			Classroom_Materials/Supplies/Planner_Record_Book
			Classroom_Materials/Supplies/Printer_Paper
			Classroom_Materials/Supplies/Rewards_&_Incentives
			Classroom_Materials/Supplies/Stationery
			Classroom_Materials/Supplies/Stickers
	*/

		} else if (FormatValue.equals("Classroom Materials")) {
			FormatValue = "Classroom_Materials";
			


		} else if (FormatValue.equals("Sales_Materials")) {
			FormatValue = "Sales_Materials";
			
			
		}

		return FormatValue;
	}

	
	
	public static String FormatAPI(String FormatValue) {

		if (FormatValue.equals("Books")) {
			FormatValue = "Books";

		} else if (FormatValue.equals("Books | Individual Titles")) {
			FormatValue = "Books/Individual_Titles";

		} else if (FormatValue.equals("Books | IT | Activity Book")) {
			FormatValue = "Books/Individual_Titles/Activity_Book";

		} else if (FormatValue.equals("Books | IT | Activity Sheet")) {
			FormatValue = "Books/Individual_Titles/Activity_Sheet";

		} else if (FormatValue.equals("Books | IT | Big_Book")) {
			FormatValue = "Books/Individual_Titles/Big_Book";

		} else if (FormatValue.equals("Books | IT | Board_Books")) {
			FormatValue = "Books/Individual_Titles/Board_Books";
			
		} else if (FormatValue.equals("Books | IT | eBook")) {
			FormatValue = "Books/Individual_Titles/eBook";

		} else if (FormatValue.equals("Books | IT | Hardcover_Books")) {
			FormatValue = "Books/Individual_Titles/Hardcover_Books";
			
		} else if (FormatValue.equals("Books | IT | HB_Hardcover_Book_and_CD")) {
			FormatValue = "Books/Individual_Titles/Hardcover_Books/Hardcover_Book_and_CD";

		} else if (FormatValue.equals("Books | IT | Interactive_&_Novelty_Book")) {
			FormatValue = "Books/Individual_Titles/Interactive_%26_Novelty_Book";

		} else if (FormatValue.equals("Books | IT | Library_Binding")) {
			FormatValue = "Books/Individual_Titles/Library_Binding";

		} else if (FormatValue.equals("Books | IT | Paperback Book")) {
			FormatValue = "Books/Individual_Titles/Paperback_Book";
			
		} else if (FormatValue.equals("Books | IT | PB_Paperback_Book_and_Cassette")) {
			FormatValue = "Books/Individual_Titles/Paperback_Book/Paperback_Book_and_Cassette";
			
		} else if (FormatValue.equals("Books | IT | PB_and_CD")) {
			FormatValue = "Books/Individual_Titles/Paperback_Book/Paperback_Book_and_CD";

		} else if (FormatValue.equals("Books| IT | Picture_Book")) {
			FormatValue = "Books/Individual_Titles/Picture_Book";
			

	//----------------------------------------------------------------------------------------------------------------------
			
		} else if (FormatValue.equals("Books | Collections_and_Libraries")) {
			FormatValue = "Books/Collections_and_Libraries";

		} else if (FormatValue.equals("Books | CL | Assessment_Collection")) {
			FormatValue = "Books/Collections_and_Libraries/Assessment_Collection";
			
		} else if (FormatValue.equals("Books | CL | Big_Book_Collection")) {
			FormatValue = "Books/Collections_and_Libraries/Big_Book_Collection";
			
		} else if (FormatValue.equals("Books | CL | Board_Book_Collection")) {
			FormatValue = "Books/Collections_and_Libraries/Board_Book_Collection";
					
		} else if (FormatValue.equals("Books | CL | Boxed_Set")) {
			FormatValue = "Books/Collections_and_Libraries/Boxed_Set";
					
       } else if (FormatValue.equals("Books | CL | Classroom_Library")) {
     	FormatValue = "Books/Collections_and_Libraries/Classroom_Library";
     
       } else if (FormatValue.equals("Books | CL | Classroom_Program")) {
     	FormatValue = "Books/Collections_and_Libraries/Classroom_Program";
     	
       } else if (FormatValue.equals("Books | CL | Family_Engagement_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Family_Engagement_Collection";
     	
       } else if (FormatValue.equals("Books | CL | Guided_Reading_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Guided_Reading_Collection";
     	
       } else if (FormatValue.equals("Books | CL | Hardcover_Book_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Hardcover_Book_Collection";
     	
       } else if (FormatValue.equals("Books | CL | Literacy_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Literacy_Collection";

       } else if (FormatValue.equals("Books | CL | Mentoring_Collection")) {
     	FormatValue = "Books/Collections_and_Libraries/Mentoring_Collection";
     	
       } else if (FormatValue.equals("Books | CL | Paperback_Book_Collection")) {
        	FormatValue = "Books/Collections_and_Libraries/Paperback_Book_Collection";

       } else if (FormatValue.equals("Books | CL | Take_Home_Collection")) {
       	FormatValue = "Books/Collections_and_Libraries/Take_Home_Collection ";

       	//------------------------------------------------------------------------------------------
			
       } else if (FormatValue.equals("Books | Magazines")) {
          	FormatValue = "Books/Magazines";

          } else if (FormatValue.equals("Books | Magazines | Classroom_Magazines")) {
             	FormatValue = "Books/Magazines/Classroom_Magazines";

          } else if (FormatValue.equals("Books | Magazines | CM_Classroom_Subscription_-_Print")) {
            	FormatValue = "Books/Magazines/Classroom_Magazines/Classroom_Subscription_-_Print";
        
          } else if (FormatValue.equals("Books | Magazines | Professional_Resources")) {
            	FormatValue = "Books/Professional_Resources";
            	
          } else if (FormatValue.equals("Books | Magazines | Activity_Pack")) {
           	FormatValue = "Books/Professional_Resources/Activity_Pack";
            	
          } else if (FormatValue.equals("Books | Magazines | eBook")) {
          	FormatValue = "Books/Professional_Resources/eBook";
          	
          } else if (FormatValue.equals("Books | Magazines | Literature_Guide")) {
             	FormatValue = "Books/Professional_Resources/Literature_Guide";	
 	
          } else if (FormatValue.equals("Books | Magazines | Professional_Book")) {
            	FormatValue = "Books/Professional_Resources/Professional_Book";	
	
          } else if (FormatValue.equals("Books | Magazines | PB_and_CD")) {
           	FormatValue = "Books/Professional_Resources/Professional_Book/Professional_Book_and_CD";	
           	
          } else if (FormatValue.equals("Books | Magazines | PB_and_DVD")) {
          	FormatValue = "Books/Professional_Resources/Professional_Book/Professional_Book_and_DVD";
          	
          } else if (FormatValue.equals("Books | Magazines | Teaching_Card")) {
           	FormatValue = "Books/Professional_Resources/Teaching_Card";	
           	
           	
          } else if (FormatValue.equals("Books | Magazines | Teaching_Guide")) {
          	FormatValue = "Books/Professional_Resources/Teaching_Guide";	
          	
          } else if (FormatValue.equals("Books | Magazines | TG_Student_Edition")) {
             	FormatValue = "Books/Professional_Resources/Teaching_Guide/Student_Edition";	
          	
          } else if (FormatValue.equals("Books | Magazines | TG_Teacher_Edition")) {
            	FormatValue = "Books/Professional_Resources/Teaching_Guide/Teacher_Edition";
       	
       	//--------------------------------------------------------------------------------------------
    			

  		} else if (FormatValue.equals("Digital Media Resources")) {
			FormatValue = "Digital_%26_Media_Resources";			
			
		} else if (FormatValue.equals("DMR | Audio")) {
			FormatValue = "Digital_%26_Media_Resources/Audio";		
			
		} else if (FormatValue.equals("DMR | Audio | CD")) {
			FormatValue = "Digital_%26_Media_Resources/Audio";
			
		} else if (FormatValue.equals("DMR | Audio | CD | Hardcover_Book_and_CD")) {
			FormatValue = "Digital_%26_Media_Resources/Audio/CD/Hardcover_Book_and_CD";	
			
		} else if (FormatValue.equals("DMR | Audio | CD | Paperback_Book_and_CD")) {
			FormatValue = "Digital_%26_Media_Resources/Audio/CD/Paperback_Book_and_CD";
			
		} else if (FormatValue.equals("DMR | Audio | CD | Paperback_Books_and_CD")) {
			FormatValue = "Digital_%26_Media_Resources/Audio/CD/Paperback_Books_and_CD";
			
		} else if (FormatValue.equals("DMR | Audio | CD_Playaway_Audio")) {
			FormatValue = "Digital_%26_Media_Resources/Audio/CD_Playaway_Audio";
			
		} else if (FormatValue.equals("DMR | Audio | DVD-Audio")) {
			FormatValue = "Digital_%26_Media_Resources/Audio/DVD-Audio";
            	
        //--------------------------------------------------------------------------------------------    	
            
			
		} else if (FormatValue.equals("DMR | Software")) {
			FormatValue = "Digital_%26_Media_Resources/Software";
		
		} else if (FormatValue.equals("DMR | Software | DVD")) {
			FormatValue = "Digital_%26_Media_Resources/Software/DVD";
			
		} else if (FormatValue.equals("DMR | Subscriptions")) {
			FormatValue = "Digital_%26_Media_Resources/Subscriptions";

			
		} else if (FormatValue.equals("DMR | Subscriptions | FreedomFlix")) {
			FormatValue = "Digital_%26_Media_Resources/Subscriptions/FreedomFlix";
			
		} else if (FormatValue.equals("DMR | Subscriptions | New_Book_of_Knowledge")) {
			FormatValue = "Digital_%26_Media_Resources/Subscriptions/New_Book_of_Knowledge";
			
		} else if (FormatValue.equals("DMR | Subscriptions | Scholastic_Teachables")) {
			FormatValue = "Digital_%26_Media_Resources/Subscriptions/Scholastic_Teachables";
		
		} else if (FormatValue.equals("DMR | Subscriptions | ScienceFlix")) {
			FormatValue = "Digital_%26_Media_Resources/Subscriptions/ScienceFlix";
		
		} else if (FormatValue.equals("DMR | Subscriptions | Storia")) {
			FormatValue = "Digital_%26_Media_Resources/Subscriptions/Storia";
			
		} else if (FormatValue.equals("DMR | Video")) {
			FormatValue = "Digital_%26_Media_Resources/Video";

		} else if (FormatValue.equals("DMR | Video | DVD")) {
			FormatValue = "Digital_%26_Media_Resources/Video/DVD";
			
		} else if (FormatValue.equals("DMR | Video | Playaway_View")) {
			FormatValue = "Digital_%26_Media_Resources/Video/Playaway_View";
			

        //--------------------------------------------------------------------------------------------

	
		} else if (FormatValue.equals("Classroom Materials")) {
			FormatValue = "Classroom_Materials";
		
	} else if (FormatValue.equals("Sales_Materials")) {
		FormatValue = "Sales_Materials"; 

	}
		return FormatValue;
	}
}
