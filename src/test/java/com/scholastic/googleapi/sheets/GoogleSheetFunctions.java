package com.scholastic.googleapi.sheets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.CellData;
import com.google.api.services.sheets.v4.model.CellFormat;
import com.google.api.services.sheets.v4.model.ClearValuesRequest;
import com.google.api.services.sheets.v4.model.Color;
import com.google.api.services.sheets.v4.model.GridRange;
import com.google.api.services.sheets.v4.model.RepeatCellRequest;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.TextFormat;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.scholastic.torque.common.TestBaseProvider;
import com.torque.automation.core.TestDataUtils;

import Utils.CellRanges;
import Utils.Utility;

public class GoogleSheetFunctions extends Schemas {
	private static Sheets service = null;

	static {
		try {
			service = getSheetsService();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static boolean writeToSheet(String sheetName, String cellRange, List<Object> obj) {
		try {
			String spreadsheetId = TestDataUtils.getString("spreadsheetId");

			List<List<Object>> values = Arrays.asList(obj);
			ValueRange body = new ValueRange().setValues(values);
			System.out.println("SheetId: " + spreadsheetId + ", Values: " + obj + ", sheetName: " + sheetName
					+ ", cellRange: " + cellRange);
			service.spreadsheets().values().update(spreadsheetId, sheetName + "!" + cellRange, body)
					.setValueInputOption("RAW").execute();
			return true;
		} catch (Exception ex) {
			org.testng.Assert.fail("Exception occurred while writing to the sheet. Exception = " + ex.getMessage());
			return false;
		}
	}

	// row and column : 0 based indexing
	public Request getRequestForColor(int row, int column, Color background, Color forground) {
		return new Request().setRepeatCell(new RepeatCellRequest()
				.setRange(new GridRange().setSheetId(0).setStartRowIndex(row).setEndRowIndex(row + 1)
						.setStartColumnIndex(column).setEndColumnIndex(column + 1))
				.setCell(new CellData().setUserEnteredFormat(new CellFormat().setBackgroundColor(background)
						.setTextFormat(new TextFormat().setForegroundColor(forground))
						.setHorizontalAlignment("CENTER")))
				.setFields("*"));
	}

	public void setBackgroundColor(List<Request> requests) {
		try {
			String spreadsheetId = TestDataUtils.getString("spreadsheetId");
			BatchUpdateSpreadsheetRequest body = new BatchUpdateSpreadsheetRequest().setRequests(requests);
			service.spreadsheets().batchUpdate(spreadsheetId, body).execute();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public boolean writeToSheetMultipleRows(String sheetName, String cellRange, List<List<Object>> values) {
		try {
			String spreadsheetId = TestDataUtils.getString("spreadsheetId");
			ValueRange body = new ValueRange().setValues(values);
			service.spreadsheets().values().update(spreadsheetId, sheetName + "!" + cellRange, body)
					.setValueInputOption("RAW").execute();
			return true;
		} catch (Exception ex) {
			org.testng.Assert.fail("Exception occurred while writing to the sheet. Exception = " + ex.getMessage());
			return false;
		}
	}

	public static void clearData(String sheetName, String cellRange) {
		try {
			service.spreadsheets().values().clear(TestDataUtils.getString("spreadsheetId"), sheetName + "!" + cellRange,
					new ClearValuesRequest()).execute();
		} catch (Exception ex) {
			org.testng.Assert.fail("Exception occurred while writing to the sheet. Exception = " + ex.getMessage());
		}
	}

	public void clearFormating(int row, int column) {
		List<Request> requests = new ArrayList<>();
		requests.add(new Request().setRepeatCell(new RepeatCellRequest()
				.setRange(new GridRange().setSheetId(0).setStartRowIndex(row).setEndRowIndex(7 + 50)
						.setStartColumnIndex(column).setEndColumnIndex(column + 11))
				.setCell(new CellData().setUserEnteredFormat(
						new CellFormat().setBackgroundColor(com.scholastic.googleapi.constants.Color.WHITE)
								.setTextFormat(new TextFormat()
										.setForegroundColor(com.scholastic.googleapi.constants.Color.BLACK))))
				.setFields("*")));
		setBackgroundColor(requests);
	}

	public static List<List<Object>> readFromSheet(String sheetName, String cellRange) {
		try {
			String spreadsheetId = TestDataUtils.getString("spreadsheetId");
			ValueRange result = service.spreadsheets().values().get(spreadsheetId, sheetName + "!" + cellRange)
					.execute();
			int numRows = result.getValues() != null ? result.getValues().size() : 0;
			System.out.println("Total Number of Rows are: " + numRows);
			return result.getValues();
		} catch (Exception ex) {
			org.testng.Assert.fail("Exception occurred while reading from sheet. Exception = " + ex.getMessage());
		}
		return null;
	}
}
