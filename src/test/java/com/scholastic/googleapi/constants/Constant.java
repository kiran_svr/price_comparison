package com.scholastic.googleapi.constants;

import com.torque.automation.core.TestDataUtils;

public interface Constant {
	String sheetName = TestDataUtils.getString("sheetName");
	String JIRA_PROJECT_INPUT_RANGE="D3:E3";
}
