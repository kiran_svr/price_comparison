package com.scholastic.googleapi.constants;

public class Color {
	public static com.google.api.services.sheets.v4.model.Color RED = getColor(255, 0, 0);
	public static com.google.api.services.sheets.v4.model.Color GREEN = getColor(0, 128, 0);
	public static com.google.api.services.sheets.v4.model.Color YELLOW = getColor(255, 255, 0);
	public static com.google.api.services.sheets.v4.model.Color WHITE = getColor(255, 255, 255);
	public static com.google.api.services.sheets.v4.model.Color BLACK = getColor(0, 0, 0);

	public static com.google.api.services.sheets.v4.model.Color getColor(int r, int g, int b) {
		java.awt.Color color = new java.awt.Color(r, g, b);// green
		float red = (float) color.getRed();
		float green = (float) color.getGreen();
		float blue = (float) color.getBlue();
		float denominator = (float) 255.0;
		return new com.google.api.services.sheets.v4.model.Color().setBlue(blue / denominator)
				.setGreen(green / denominator).setRed(red / denominator);
	}
}
