package Utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.RandomStringUtils;

import com.scholastic.aso.automation.pages.web.HomePage;
import com.scholastic.googleapi.sheets.GoogleSheetFunctions;
import com.scholastic.torque.common.TestBaseProvider;

public class Utility {

	public static String getDateAndTime() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm:ss");
		Date today = new Date();
		return simpleDateFormat.format(today);
	}

	public static List<Object> createObject() {
		List<Object> obj = new ArrayList<Object>();
		obj.add(GoogleSheetRowModel.getStatus());
		obj.add(Utility.getDateAndTime());
		obj.add(GoogleSheetRowModel.getExecutionTime());
		return obj;
	}
	
	public static String convertMillisToHHMMSS_Format(long milliseconds) {
		long millis = milliseconds * 1000;
	    String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
	            TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
	            TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
	    //System.out.println(hms);
	    return hms;
	}

	public static List<Object> statusCreateObject() {
		List<Object> obj = new ArrayList<Object>();
		obj.add(GoogleSheetRowModel.getStatus());
		obj.add(Utility.getDateAndTime());
		return obj;
	}

	public static void setAllCellsSkipped(int k, int l, String storeStatus, String sheetName) {

		int count = 0;
		for (int i = k; i <= l; i++) {
			if (count == 0) {
				GoogleSheetRowModel.setStatus("Fail");

				count = count + 1;
			} else {
				GoogleSheetRowModel.setStatus("Skipped");
			}
			GoogleSheetFunctions.writeToSheet(sheetName, "D" + i + ":F" + i, Utility.createObject());
			GoogleSheetRowModel.setStatus("Fail");
			GoogleSheetFunctions.writeToSheet(sheetName, storeStatus, Utility.statusCreateObject());
			System.out.println("(TestBaseProvider.getTestBase().getString(\"tso.url\"));"
					+ (TestBaseProvider.getTestBase().getString("tso.url")));
		}
	}
	
	public static String generateRandomString() {
		String generatedString = RandomStringUtils.randomAlphanumeric(20);
		return generatedString;
	}
}
