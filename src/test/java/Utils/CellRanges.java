package Utils;

public class CellRanges {

	public static final String SSO_STORE_LOGIN = "D3:F3";
	public static final String SSO_STORE_SEARCH_ISBNSEARCH = "D4:F4";
	public static final String SSO_STORE_SEARCH_KEYWORDSEARCH = "D5:F5";
	public static final String SSO_STORE_SEARCH_PDP = "D6:F6";
	public static final String SSO_STORE_SEARCH_AUTHOR = "D7:F7";
	public static final String SSO_STORE_ADDITEM_TO_CART = "D8:F8";
	public static final String SSO_STORE_SHOPPINGCART = "D9:F9";
	public static final String SSO_STORE_CHECKOUT = "D10:F10";
	public static final String SSO_STORE_LOGOUT = "D11:F11";
	public static final String SSO_STORE_STATUS = "D2:F2";

	public static final String TSO_STORE_LOGIN = "D13:F13";
	public static final String TSO_STORE_SEARCH_ISBNSEARCH = "D14:F14";
	public static final String TSO_STORE_SEARCH_KEYWORDSEARCH = "D15:F15";
	public static final String TSO_STORE_SEARCH_PDP = "D16:F16";
	public static final String TSO_STORE_SEARCH_AUTHOR = "D17:F17";
	public static final String TSO_STORE_ADDITEM_TO_CART = "D18:F18";
	public static final String TSO_STORE_SHOPPINGCART = "D19:F19";
	public static final String TSO_STORE_CHECKOUT = "D20:F20";
	public static final String TSO_STORE_LOGOUT = "D21:F21";
	public static final String TSO_STORE_STATUS = "D12:F12";

	public static final String TCB_STORE_LOGIN = "D23:F23";
	public static final String TCB_STORE_SEARCH_ISBNSEARCH = "D24:F24";
	public static final String TCB_STORE_Add_SUBSCRIPTION = "D27:F27";
	public static final String TCB_STORE_CHECKOUT_SUBSCRIPTION = "D28:F28";
	public static final String TCB_STORE_LOGOUT = "D29:F29";
	public static final String TCB_STORE_SEARCH_KEYWORD = "D25:F25";
	public static final String TCB_STORE_SEARCH_PDP = "D26:F26";
	public static final String TCB_STORE_STATUS = "D22:E22";
}
