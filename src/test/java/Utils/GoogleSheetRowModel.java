package Utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;

public class GoogleSheetRowModel {

	static long startTime;
	static long endTime;
	static long executionTime;
	static String status;

	static NumberFormat formatter = new DecimalFormat("#0");

	public static long getStartTime() {
		return startTime;
	}

	public static void setStartTime() {
		startTime = System.currentTimeMillis();
		System.out.println("Start time is: " + startTime);
	}

	public static long getEndTime() {
		return endTime;
	}

	public static void setEndTime() {
		endTime = System.currentTimeMillis();
		System.out.println("End time is: " + endTime);
		setExecutionTime();
	}

	public static long getExecutionTime() {
		return executionTime;
	}

	public static void setExecutionTime() {
		long minutes = TimeUnit.MILLISECONDS.toMinutes(getEndTime() - getStartTime());
		long seconds = TimeUnit.MILLISECONDS.toSeconds(getEndTime() - getStartTime());

		if (minutes > 0) {
			executionTime = minutes;
		} else {
			executionTime = seconds;
			System.out.println(executionTime + "Time Take for each step");
		}

	}

	public static String getStatus() {
		return status;
	}

	public static void setStatus(String status) {
		GoogleSheetRowModel.status = status;
	}

}