
package com.unmanagedMDM;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "parentId",
    "active",
    "offers",
    "Author",
    "Author Research",
    "AvailabilityDate(Presell)",
    "Award List",
    "Border ID",
    "Brand",
    "Budget Code",
    "Bundle Type (if applicable)",
    "Characters and Series",
    "Class",
    "Compiled by",
    "Composer",
    "Cross-Sell ISBNs",
    "Custom Grade (TSO only)",
    "Depth",
    "Dewey",
    "Display Subtitle in Droplet? (TRUE/FALSE)",
    "Editor",
    "Endorsement Review",
    "Extra Large Image",
    "Extra Small Image",
    "Fiction Type (Fiction/Non-Fiction)",
    "Fund",
    "Genre",
    "Height",
    "High Accelerated Reader",
    "High Age",
    "High DRA Level",
    "High Grade (TSO only)",
    "High Guided Reading Level",
    "High Lexile Level",
    "Illustrator",
    "Imprint",
    "Instructional Program",
    "Introduction by",
    "Inventory Threshold",
    "ISBN",
    "ISBN 13",
    "Keywords",
    "Language",
    "Large Image",
    "List Price",
    "Long Description",
    "Low Accelerated Reader",
    "Low Age",
    "Low DRA Level",
    "Low Grade (TSO only)",
    "Low Guided Reading Level",
    "Low Lexile Level",
    "Manufacturer Part Number",
    "Multimedia",
    "Narrator",
    "Number in Series",
    "Other format",
    "Our Price",
    "Pages",
    "Photographs by",
    "Platform",
    "Product Email",
    "Product End Date",
    "Product Start Date",
    "Product Type",
    "Publication Date",
    "Publisher",
    "Rank",
    "Resources",
    "Reviews",
    "Sale End Date",
    "Sale Message",
    "Sale Price",
    "Sale Start Date",
    "Searchable",
    "Shops",
    "Show ACR? (TRUE/FALSE)",
    "Show BonusBook? (TRUE/FALSE)",
    "Show Dewey? (TRUE/FALSE)",
    "Show DRA Level? (TRUE/FALSE)",
    "Show Guided Reading Level? (TRUE/FALSE)",
    "Show Language? (TRUE/FALSE)",
    "Show Lexile? (TRUE/FALSE)",
    "Show Spanish DRA Level? (TRUE/FALSE)",
    "Show Spanish Guided Reading Level? (TRUE/FALSE)",
    "Show Spanish Lexile? (TRUE/FALSE)",
    "Small Image",
    "SOLD_IN_TBW",
    "Spanish High DRA Level",
    "Spanish High Guided Reading Level",
    "Spanish High Lexile Level",
    "Spanish Low DRA Level",
    "Spanish Low Guided Reading Level",
    "Spanish Low Lexile Level",
    "Special Note",
    "Subject",
    "Subtitle",
    "System Requirements",
    "Themes",
    "Title",
    "Translator",
    "Unit Cost",
    "Vendor 1",
    "Vendor 2",
    "Vendor Item #",
    "Weight",
    "Width",
    "CCSS Lexile",
    "Show CCSS Lexile?(TRUE/FALSE)",
    "Boutiques",
    "Viewable Flag",
    "Teacher Tips",
    "Teaching Format",
    "ePage ISBNs",
    "Number of See Inside Pages",
    "Number of Alternate Images",
    "Number of IWB Preview Images",
    "File Format",
    "Legacy TE Sku",
    "Content Units",
    "User Benefits",
    "Online Resources",
    "REAL ISBNs",
    "Format Description",
    "SkipInventory",
    "UNITS_SOLD",
    "Audio",
    "Taxonomy",
    "Other Languages (Language Selector)",
    "Collection Titles",
    "Display Title",
    "Component Listing",
    "See Inside Description",
    "Legacy Product Type",
    "StoreID",
    "ECMS Primary Cover Image Flag",
    "Product ID",
    "Work ID"
})
public class Data {

    @JsonProperty("parentId")
    private String parentId;
    @JsonProperty("active")
    private String active;
    @JsonProperty("offers")
    private List<Offer> offers = null;
    @JsonProperty("Author")
    private String author;
    @JsonProperty("Author Research")
    private String authorResearch;
    @JsonProperty("AvailabilityDate(Presell)")
    private String availabilityDatePresell;
    @JsonProperty("Award List")
    private String awardList;
    @JsonProperty("Border ID")
    private String borderID;
    @JsonProperty("Brand")
    private String brand;
    @JsonProperty("Budget Code")
    private String budgetCode;
    @JsonProperty("Bundle Type (if applicable)")
    private String bundleTypeIfApplicable;
    @JsonProperty("Characters and Series")
    private String charactersAndSeries;
    @JsonProperty("Class")
    private String _class;
    @JsonProperty("Compiled by")
    private String compiledBy;
    @JsonProperty("Composer")
    private String composer;
    @JsonProperty("Cross-Sell ISBNs")
    private String crossSellISBNs;
    @JsonProperty("Custom Grade (TSO only)")
    private String customGradeTSOOnly;
    @JsonProperty("Depth")
    private String depth;
    @JsonProperty("Dewey")
    private String dewey;
    @JsonProperty("Display Subtitle in Droplet? (TRUE/FALSE)")
    private String displaySubtitleInDropletTRUEFALSE;
    @JsonProperty("Editor")
    private String editor;
    @JsonProperty("Endorsement Review")
    private String endorsementReview;
    @JsonProperty("Extra Large Image")
    private String extraLargeImage;
    @JsonProperty("Extra Small Image")
    private String extraSmallImage;
    @JsonProperty("Fiction Type (Fiction/Non-Fiction)")
    private String fictionTypeFictionNonFiction;
    @JsonProperty("Fund")
    private String fund;
    @JsonProperty("Genre")
    private String genre;
    @JsonProperty("Height")
    private String height;
    @JsonProperty("High Accelerated Reader")
    private String highAcceleratedReader;
    @JsonProperty("High Age")
    private String highAge;
    @JsonProperty("High DRA Level")
    private String highDRALevel;
    @JsonProperty("High Grade (TSO only)")
    private String highGradeTSOOnly;
    @JsonProperty("High Guided Reading Level")
    private String highGuidedReadingLevel;
    @JsonProperty("High Lexile Level")
    private String highLexileLevel;
    @JsonProperty("Illustrator")
    private String illustrator;
    @JsonProperty("Imprint")
    private String imprint;
    @JsonProperty("Instructional Program")
    private String instructionalProgram;
    @JsonProperty("Introduction by")
    private String introductionBy;
    @JsonProperty("Inventory Threshold")
    private String inventoryThreshold;
    @JsonProperty("ISBN")
    private String iSBN;
    @JsonProperty("ISBN 13")
    private String iSBN13;
    @JsonProperty("Keywords")
    private String keywords;
    @JsonProperty("Language")
    private String language;
    @JsonProperty("Large Image")
    private String largeImage;
    @JsonProperty("List Price")
    private String listPrice;
    @JsonProperty("Long Description")
    private String longDescription;
    @JsonProperty("Low Accelerated Reader")
    private String lowAcceleratedReader;
    @JsonProperty("Low Age")
    private String lowAge;
    @JsonProperty("Low DRA Level")
    private String lowDRALevel;
    @JsonProperty("Low Grade (TSO only)")
    private String lowGradeTSOOnly;
    @JsonProperty("Low Guided Reading Level")
    private String lowGuidedReadingLevel;
    @JsonProperty("Low Lexile Level")
    private String lowLexileLevel;
    @JsonProperty("Manufacturer Part Number")
    private String manufacturerPartNumber;
    @JsonProperty("Multimedia")
    private String multimedia;
    @JsonProperty("Narrator")
    private String narrator;
    @JsonProperty("Number in Series")
    private String numberInSeries;
    @JsonProperty("Other format")
    private String otherFormat;
    @JsonProperty("Our Price")
    private String ourPrice;
    @JsonProperty("Pages")
    private String pages;
    @JsonProperty("Photographs by")
    private String photographsBy;
    @JsonProperty("Platform")
    private String platform;
    @JsonProperty("Product Email")
    private String productEmail;
    @JsonProperty("Product End Date")
    private String productEndDate;
    @JsonProperty("Product Start Date")
    private String productStartDate;
    @JsonProperty("Product Type")
    private String productType;
    @JsonProperty("Publication Date")
    private String publicationDate;
    @JsonProperty("Publisher")
    private String publisher;
    @JsonProperty("Rank")
    private String rank;
    @JsonProperty("Resources")
    private String resources;
    @JsonProperty("Reviews")
    private String reviews;
    @JsonProperty("Sale End Date")
    private String saleEndDate;
    @JsonProperty("Sale Message")
    private String saleMessage;
    @JsonProperty("Sale Price")
    private String salePrice;
    @JsonProperty("Sale Start Date")
    private String saleStartDate;
    @JsonProperty("Searchable")
    private String searchable;
    @JsonProperty("Shops")
    private String shops;
    @JsonProperty("Show ACR? (TRUE/FALSE)")
    private String showACRTRUEFALSE;
    @JsonProperty("Show BonusBook? (TRUE/FALSE)")
    private String showBonusBookTRUEFALSE;
    @JsonProperty("Show Dewey? (TRUE/FALSE)")
    private String showDeweyTRUEFALSE;
    @JsonProperty("Show DRA Level? (TRUE/FALSE)")
    private String showDRALevelTRUEFALSE;
    @JsonProperty("Show Guided Reading Level? (TRUE/FALSE)")
    private String showGuidedReadingLevelTRUEFALSE;
    @JsonProperty("Show Language? (TRUE/FALSE)")
    private String showLanguageTRUEFALSE;
    @JsonProperty("Show Lexile? (TRUE/FALSE)")
    private String showLexileTRUEFALSE;
    @JsonProperty("Show Spanish DRA Level? (TRUE/FALSE)")
    private String showSpanishDRALevelTRUEFALSE;
    @JsonProperty("Show Spanish Guided Reading Level? (TRUE/FALSE)")
    private String showSpanishGuidedReadingLevelTRUEFALSE;
    @JsonProperty("Show Spanish Lexile? (TRUE/FALSE)")
    private String showSpanishLexileTRUEFALSE;
    @JsonProperty("Small Image")
    private String smallImage;
    @JsonProperty("SOLD_IN_TBW")
    private String sOLDINTBW;
    @JsonProperty("Spanish High DRA Level")
    private String spanishHighDRALevel;
    @JsonProperty("Spanish High Guided Reading Level")
    private String spanishHighGuidedReadingLevel;
    @JsonProperty("Spanish High Lexile Level")
    private String spanishHighLexileLevel;
    @JsonProperty("Spanish Low DRA Level")
    private String spanishLowDRALevel;
    @JsonProperty("Spanish Low Guided Reading Level")
    private String spanishLowGuidedReadingLevel;
    @JsonProperty("Spanish Low Lexile Level")
    private String spanishLowLexileLevel;
    @JsonProperty("Special Note")
    private String specialNote;
    @JsonProperty("Subject")
    private String subject;
    @JsonProperty("Subtitle")
    private String subtitle;
    @JsonProperty("System Requirements")
    private String systemRequirements;
    @JsonProperty("Themes")
    private String themes;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("Translator")
    private String translator;
    @JsonProperty("Unit Cost")
    private String unitCost;
    @JsonProperty("Vendor 1")
    private String vendor1;
    @JsonProperty("Vendor 2")
    private String vendor2;
    @JsonProperty("Vendor Item #")
    private String vendorItem;
    @JsonProperty("Weight")
    private String weight;
    @JsonProperty("Width")
    private String width;
    @JsonProperty("CCSS Lexile")
    private String cCSSLexile;
    @JsonProperty("Show CCSS Lexile?(TRUE/FALSE)")
    private String showCCSSLexileTRUEFALSE;
    @JsonProperty("Boutiques")
    private String boutiques;
    @JsonProperty("Viewable Flag")
    private String viewableFlag;
    @JsonProperty("Teacher Tips")
    private String teacherTips;
    @JsonProperty("Teaching Format")
    private String teachingFormat;
    @JsonProperty("ePage ISBNs")
    private String ePageISBNs;
    @JsonProperty("Number of See Inside Pages")
    private String numberOfSeeInsidePages;
    @JsonProperty("Number of Alternate Images")
    private String numberOfAlternateImages;
    @JsonProperty("Number of IWB Preview Images")
    private String numberOfIWBPreviewImages;
    @JsonProperty("File Format")
    private String fileFormat;
    @JsonProperty("Legacy TE Sku")
    private String legacyTESku;
    @JsonProperty("Content Units")
    private String contentUnits;
    @JsonProperty("User Benefits")
    private String userBenefits;
    @JsonProperty("Online Resources")
    private String onlineResources;
    @JsonProperty("REAL ISBNs")
    private String rEALISBNs;
    @JsonProperty("Format Description")
    private String formatDescription;
    @JsonProperty("SkipInventory")
    private String skipInventory;
    @JsonProperty("UNITS_SOLD")
    private String uNITSSOLD;
    @JsonProperty("Audio")
    private String audio;
    @JsonProperty("Taxonomy")
    private String taxonomy;
    @JsonProperty("Other Languages (Language Selector)")
    private String otherLanguagesLanguageSelector;
    @JsonProperty("Collection Titles")
    private String collectionTitles;
    @JsonProperty("Display Title")
    private String displayTitle;
    @JsonProperty("Component Listing")
    private String componentListing;
    @JsonProperty("See Inside Description")
    private String seeInsideDescription;
    @JsonProperty("Legacy Product Type")
    private String legacyProductType;
    @JsonProperty("StoreID")
    private String storeID;
    @JsonProperty("ECMS Primary Cover Image Flag")
    private String eCMSPrimaryCoverImageFlag;
    @JsonProperty("Product ID")
    private String productID;
    @JsonProperty("Work ID")
    private String workID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("parentId")
    public String getParentId() {
        return parentId;
    }

    @JsonProperty("parentId")
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @JsonProperty("active")
    public String getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(String active) {
        this.active = active;
    }

    @JsonProperty("offers")
    public List<Offer> getOffers() {
        return offers;
    }

    @JsonProperty("offers")
    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    @JsonProperty("Author")
    public String getAuthor() {
        return author;
    }

    @JsonProperty("Author")
    public void setAuthor(String author) {
        this.author = author;
    }

    @JsonProperty("Author Research")
    public String getAuthorResearch() {
        return authorResearch;
    }

    @JsonProperty("Author Research")
    public void setAuthorResearch(String authorResearch) {
        this.authorResearch = authorResearch;
    }

    @JsonProperty("AvailabilityDate(Presell)")
    public String getAvailabilityDatePresell() {
        return availabilityDatePresell;
    }

    @JsonProperty("AvailabilityDate(Presell)")
    public void setAvailabilityDatePresell(String availabilityDatePresell) {
        this.availabilityDatePresell = availabilityDatePresell;
    }

    @JsonProperty("Award List")
    public String getAwardList() {
        return awardList;
    }

    @JsonProperty("Award List")
    public void setAwardList(String awardList) {
        this.awardList = awardList;
    }

    @JsonProperty("Border ID")
    public String getBorderID() {
        return borderID;
    }

    @JsonProperty("Border ID")
    public void setBorderID(String borderID) {
        this.borderID = borderID;
    }

    @JsonProperty("Brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("Brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonProperty("Budget Code")
    public String getBudgetCode() {
        return budgetCode;
    }

    @JsonProperty("Budget Code")
    public void setBudgetCode(String budgetCode) {
        this.budgetCode = budgetCode;
    }

    @JsonProperty("Bundle Type (if applicable)")
    public String getBundleTypeIfApplicable() {
        return bundleTypeIfApplicable;
    }

    @JsonProperty("Bundle Type (if applicable)")
    public void setBundleTypeIfApplicable(String bundleTypeIfApplicable) {
        this.bundleTypeIfApplicable = bundleTypeIfApplicable;
    }

    @JsonProperty("Characters and Series")
    public String getCharactersAndSeries() {
        return charactersAndSeries;
    }

    @JsonProperty("Characters and Series")
    public void setCharactersAndSeries(String charactersAndSeries) {
        this.charactersAndSeries = charactersAndSeries;
    }

    @JsonProperty("Class")
    public String getClass_() {
        return _class;
    }

    @JsonProperty("Class")
    public void setClass_(String _class) {
        this._class = _class;
    }

    @JsonProperty("Compiled by")
    public String getCompiledBy() {
        return compiledBy;
    }

    @JsonProperty("Compiled by")
    public void setCompiledBy(String compiledBy) {
        this.compiledBy = compiledBy;
    }

    @JsonProperty("Composer")
    public String getComposer() {
        return composer;
    }

    @JsonProperty("Composer")
    public void setComposer(String composer) {
        this.composer = composer;
    }

    @JsonProperty("Cross-Sell ISBNs")
    public String getCrossSellISBNs() {
        return crossSellISBNs;
    }

    @JsonProperty("Cross-Sell ISBNs")
    public void setCrossSellISBNs(String crossSellISBNs) {
        this.crossSellISBNs = crossSellISBNs;
    }

    @JsonProperty("Custom Grade (TSO only)")
    public String getCustomGradeTSOOnly() {
        return customGradeTSOOnly;
    }

    @JsonProperty("Custom Grade (TSO only)")
    public void setCustomGradeTSOOnly(String customGradeTSOOnly) {
        this.customGradeTSOOnly = customGradeTSOOnly;
    }

    @JsonProperty("Depth")
    public String getDepth() {
        return depth;
    }

    @JsonProperty("Depth")
    public void setDepth(String depth) {
        this.depth = depth;
    }

    @JsonProperty("Dewey")
    public String getDewey() {
        return dewey;
    }

    @JsonProperty("Dewey")
    public void setDewey(String dewey) {
        this.dewey = dewey;
    }

    @JsonProperty("Display Subtitle in Droplet? (TRUE/FALSE)")
    public String getDisplaySubtitleInDropletTRUEFALSE() {
        return displaySubtitleInDropletTRUEFALSE;
    }

    @JsonProperty("Display Subtitle in Droplet? (TRUE/FALSE)")
    public void setDisplaySubtitleInDropletTRUEFALSE(String displaySubtitleInDropletTRUEFALSE) {
        this.displaySubtitleInDropletTRUEFALSE = displaySubtitleInDropletTRUEFALSE;
    }

    @JsonProperty("Editor")
    public String getEditor() {
        return editor;
    }

    @JsonProperty("Editor")
    public void setEditor(String editor) {
        this.editor = editor;
    }

    @JsonProperty("Endorsement Review")
    public String getEndorsementReview() {
        return endorsementReview;
    }

    @JsonProperty("Endorsement Review")
    public void setEndorsementReview(String endorsementReview) {
        this.endorsementReview = endorsementReview;
    }

    @JsonProperty("Extra Large Image")
    public String getExtraLargeImage() {
        return extraLargeImage;
    }

    @JsonProperty("Extra Large Image")
    public void setExtraLargeImage(String extraLargeImage) {
        this.extraLargeImage = extraLargeImage;
    }

    @JsonProperty("Extra Small Image")
    public String getExtraSmallImage() {
        return extraSmallImage;
    }

    @JsonProperty("Extra Small Image")
    public void setExtraSmallImage(String extraSmallImage) {
        this.extraSmallImage = extraSmallImage;
    }

    @JsonProperty("Fiction Type (Fiction/Non-Fiction)")
    public String getFictionTypeFictionNonFiction() {
        return fictionTypeFictionNonFiction;
    }

    @JsonProperty("Fiction Type (Fiction/Non-Fiction)")
    public void setFictionTypeFictionNonFiction(String fictionTypeFictionNonFiction) {
        this.fictionTypeFictionNonFiction = fictionTypeFictionNonFiction;
    }

    @JsonProperty("Fund")
    public String getFund() {
        return fund;
    }

    @JsonProperty("Fund")
    public void setFund(String fund) {
        this.fund = fund;
    }

    @JsonProperty("Genre")
    public String getGenre() {
        return genre;
    }

    @JsonProperty("Genre")
    public void setGenre(String genre) {
        this.genre = genre;
    }

    @JsonProperty("Height")
    public String getHeight() {
        return height;
    }

    @JsonProperty("Height")
    public void setHeight(String height) {
        this.height = height;
    }

    @JsonProperty("High Accelerated Reader")
    public String getHighAcceleratedReader() {
        return highAcceleratedReader;
    }

    @JsonProperty("High Accelerated Reader")
    public void setHighAcceleratedReader(String highAcceleratedReader) {
        this.highAcceleratedReader = highAcceleratedReader;
    }

    @JsonProperty("High Age")
    public String getHighAge() {
        return highAge;
    }

    @JsonProperty("High Age")
    public void setHighAge(String highAge) {
        this.highAge = highAge;
    }

    @JsonProperty("High DRA Level")
    public String getHighDRALevel() {
        return highDRALevel;
    }

    @JsonProperty("High DRA Level")
    public void setHighDRALevel(String highDRALevel) {
        this.highDRALevel = highDRALevel;
    }

    @JsonProperty("High Grade (TSO only)")
    public String getHighGradeTSOOnly() {
        return highGradeTSOOnly;
    }

    @JsonProperty("High Grade (TSO only)")
    public void setHighGradeTSOOnly(String highGradeTSOOnly) {
        this.highGradeTSOOnly = highGradeTSOOnly;
    }

    @JsonProperty("High Guided Reading Level")
    public String getHighGuidedReadingLevel() {
        return highGuidedReadingLevel;
    }

    @JsonProperty("High Guided Reading Level")
    public void setHighGuidedReadingLevel(String highGuidedReadingLevel) {
        this.highGuidedReadingLevel = highGuidedReadingLevel;
    }

    @JsonProperty("High Lexile Level")
    public String getHighLexileLevel() {
        return highLexileLevel;
    }

    @JsonProperty("High Lexile Level")
    public void setHighLexileLevel(String highLexileLevel) {
        this.highLexileLevel = highLexileLevel;
    }

    @JsonProperty("Illustrator")
    public String getIllustrator() {
        return illustrator;
    }

    @JsonProperty("Illustrator")
    public void setIllustrator(String illustrator) {
        this.illustrator = illustrator;
    }

    @JsonProperty("Imprint")
    public String getImprint() {
        return imprint;
    }

    @JsonProperty("Imprint")
    public void setImprint(String imprint) {
        this.imprint = imprint;
    }

    @JsonProperty("Instructional Program")
    public String getInstructionalProgram() {
        return instructionalProgram;
    }

    @JsonProperty("Instructional Program")
    public void setInstructionalProgram(String instructionalProgram) {
        this.instructionalProgram = instructionalProgram;
    }

    @JsonProperty("Introduction by")
    public String getIntroductionBy() {
        return introductionBy;
    }

    @JsonProperty("Introduction by")
    public void setIntroductionBy(String introductionBy) {
        this.introductionBy = introductionBy;
    }

    @JsonProperty("Inventory Threshold")
    public String getInventoryThreshold() {
        return inventoryThreshold;
    }

    @JsonProperty("Inventory Threshold")
    public void setInventoryThreshold(String inventoryThreshold) {
        this.inventoryThreshold = inventoryThreshold;
    }

    @JsonProperty("ISBN")
    public String getISBN() {
        return iSBN;
    }

    @JsonProperty("ISBN")
    public void setISBN(String iSBN) {
        this.iSBN = iSBN;
    }

    @JsonProperty("ISBN 13")
    public String getISBN13() {
        return iSBN13;
    }

    @JsonProperty("ISBN 13")
    public void setISBN13(String iSBN13) {
        this.iSBN13 = iSBN13;
    }

    @JsonProperty("Keywords")
    public String getKeywords() {
        return keywords;
    }

    @JsonProperty("Keywords")
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    @JsonProperty("Language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("Language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("Large Image")
    public String getLargeImage() {
        return largeImage;
    }

    @JsonProperty("Large Image")
    public void setLargeImage(String largeImage) {
        this.largeImage = largeImage;
    }

    @JsonProperty("List Price")
    public String getListPrice() {
        return listPrice;
    }

    @JsonProperty("List Price")
    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }

    @JsonProperty("Long Description")
    public String getLongDescription() {
        return longDescription;
    }

    @JsonProperty("Long Description")
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    @JsonProperty("Low Accelerated Reader")
    public String getLowAcceleratedReader() {
        return lowAcceleratedReader;
    }

    @JsonProperty("Low Accelerated Reader")
    public void setLowAcceleratedReader(String lowAcceleratedReader) {
        this.lowAcceleratedReader = lowAcceleratedReader;
    }

    @JsonProperty("Low Age")
    public String getLowAge() {
        return lowAge;
    }

    @JsonProperty("Low Age")
    public void setLowAge(String lowAge) {
        this.lowAge = lowAge;
    }

    @JsonProperty("Low DRA Level")
    public String getLowDRALevel() {
        return lowDRALevel;
    }

    @JsonProperty("Low DRA Level")
    public void setLowDRALevel(String lowDRALevel) {
        this.lowDRALevel = lowDRALevel;
    }

    @JsonProperty("Low Grade (TSO only)")
    public String getLowGradeTSOOnly() {
        return lowGradeTSOOnly;
    }

    @JsonProperty("Low Grade (TSO only)")
    public void setLowGradeTSOOnly(String lowGradeTSOOnly) {
        this.lowGradeTSOOnly = lowGradeTSOOnly;
    }

    @JsonProperty("Low Guided Reading Level")
    public String getLowGuidedReadingLevel() {
        return lowGuidedReadingLevel;
    }

    @JsonProperty("Low Guided Reading Level")
    public void setLowGuidedReadingLevel(String lowGuidedReadingLevel) {
        this.lowGuidedReadingLevel = lowGuidedReadingLevel;
    }

    @JsonProperty("Low Lexile Level")
    public String getLowLexileLevel() {
        return lowLexileLevel;
    }

    @JsonProperty("Low Lexile Level")
    public void setLowLexileLevel(String lowLexileLevel) {
        this.lowLexileLevel = lowLexileLevel;
    }

    @JsonProperty("Manufacturer Part Number")
    public String getManufacturerPartNumber() {
        return manufacturerPartNumber;
    }

    @JsonProperty("Manufacturer Part Number")
    public void setManufacturerPartNumber(String manufacturerPartNumber) {
        this.manufacturerPartNumber = manufacturerPartNumber;
    }

    @JsonProperty("Multimedia")
    public String getMultimedia() {
        return multimedia;
    }

    @JsonProperty("Multimedia")
    public void setMultimedia(String multimedia) {
        this.multimedia = multimedia;
    }

    @JsonProperty("Narrator")
    public String getNarrator() {
        return narrator;
    }

    @JsonProperty("Narrator")
    public void setNarrator(String narrator) {
        this.narrator = narrator;
    }

    @JsonProperty("Number in Series")
    public String getNumberInSeries() {
        return numberInSeries;
    }

    @JsonProperty("Number in Series")
    public void setNumberInSeries(String numberInSeries) {
        this.numberInSeries = numberInSeries;
    }

    @JsonProperty("Other format")
    public String getOtherFormat() {
        return otherFormat;
    }

    @JsonProperty("Other format")
    public void setOtherFormat(String otherFormat) {
        this.otherFormat = otherFormat;
    }

    @JsonProperty("Our Price")
    public String getOurPrice() {
        return ourPrice;
    }

    @JsonProperty("Our Price")
    public void setOurPrice(String ourPrice) {
        this.ourPrice = ourPrice;
    }

    @JsonProperty("Pages")
    public String getPages() {
        return pages;
    }

    @JsonProperty("Pages")
    public void setPages(String pages) {
        this.pages = pages;
    }

    @JsonProperty("Photographs by")
    public String getPhotographsBy() {
        return photographsBy;
    }

    @JsonProperty("Photographs by")
    public void setPhotographsBy(String photographsBy) {
        this.photographsBy = photographsBy;
    }

    @JsonProperty("Platform")
    public String getPlatform() {
        return platform;
    }

    @JsonProperty("Platform")
    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @JsonProperty("Product Email")
    public String getProductEmail() {
        return productEmail;
    }

    @JsonProperty("Product Email")
    public void setProductEmail(String productEmail) {
        this.productEmail = productEmail;
    }

    @JsonProperty("Product End Date")
    public String getProductEndDate() {
        return productEndDate;
    }

    @JsonProperty("Product End Date")
    public void setProductEndDate(String productEndDate) {
        this.productEndDate = productEndDate;
    }

    @JsonProperty("Product Start Date")
    public String getProductStartDate() {
        return productStartDate;
    }

    @JsonProperty("Product Start Date")
    public void setProductStartDate(String productStartDate) {
        this.productStartDate = productStartDate;
    }

    @JsonProperty("Product Type")
    public String getProductType() {
        return productType;
    }

    @JsonProperty("Product Type")
    public void setProductType(String productType) {
        this.productType = productType;
    }

    @JsonProperty("Publication Date")
    public String getPublicationDate() {
        return publicationDate;
    }

    @JsonProperty("Publication Date")
    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    @JsonProperty("Publisher")
    public String getPublisher() {
        return publisher;
    }

    @JsonProperty("Publisher")
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    @JsonProperty("Rank")
    public String getRank() {
        return rank;
    }

    @JsonProperty("Rank")
    public void setRank(String rank) {
        this.rank = rank;
    }

    @JsonProperty("Resources")
    public String getResources() {
        return resources;
    }

    @JsonProperty("Resources")
    public void setResources(String resources) {
        this.resources = resources;
    }

    @JsonProperty("Reviews")
    public String getReviews() {
        return reviews;
    }

    @JsonProperty("Reviews")
    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    @JsonProperty("Sale End Date")
    public String getSaleEndDate() {
        return saleEndDate;
    }

    @JsonProperty("Sale End Date")
    public void setSaleEndDate(String saleEndDate) {
        this.saleEndDate = saleEndDate;
    }

    @JsonProperty("Sale Message")
    public String getSaleMessage() {
        return saleMessage;
    }

    @JsonProperty("Sale Message")
    public void setSaleMessage(String saleMessage) {
        this.saleMessage = saleMessage;
    }

    @JsonProperty("Sale Price")
    public String getSalePrice() {
        return salePrice;
    }

    @JsonProperty("Sale Price")
    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    @JsonProperty("Sale Start Date")
    public String getSaleStartDate() {
        return saleStartDate;
    }

    @JsonProperty("Sale Start Date")
    public void setSaleStartDate(String saleStartDate) {
        this.saleStartDate = saleStartDate;
    }

    @JsonProperty("Searchable")
    public String getSearchable() {
        return searchable;
    }

    @JsonProperty("Searchable")
    public void setSearchable(String searchable) {
        this.searchable = searchable;
    }

    @JsonProperty("Shops")
    public String getShops() {
        return shops;
    }

    @JsonProperty("Shops")
    public void setShops(String shops) {
        this.shops = shops;
    }

    @JsonProperty("Show ACR? (TRUE/FALSE)")
    public String getShowACRTRUEFALSE() {
        return showACRTRUEFALSE;
    }

    @JsonProperty("Show ACR? (TRUE/FALSE)")
    public void setShowACRTRUEFALSE(String showACRTRUEFALSE) {
        this.showACRTRUEFALSE = showACRTRUEFALSE;
    }

    @JsonProperty("Show BonusBook? (TRUE/FALSE)")
    public String getShowBonusBookTRUEFALSE() {
        return showBonusBookTRUEFALSE;
    }

    @JsonProperty("Show BonusBook? (TRUE/FALSE)")
    public void setShowBonusBookTRUEFALSE(String showBonusBookTRUEFALSE) {
        this.showBonusBookTRUEFALSE = showBonusBookTRUEFALSE;
    }

    @JsonProperty("Show Dewey? (TRUE/FALSE)")
    public String getShowDeweyTRUEFALSE() {
        return showDeweyTRUEFALSE;
    }

    @JsonProperty("Show Dewey? (TRUE/FALSE)")
    public void setShowDeweyTRUEFALSE(String showDeweyTRUEFALSE) {
        this.showDeweyTRUEFALSE = showDeweyTRUEFALSE;
    }

    @JsonProperty("Show DRA Level? (TRUE/FALSE)")
    public String getShowDRALevelTRUEFALSE() {
        return showDRALevelTRUEFALSE;
    }

    @JsonProperty("Show DRA Level? (TRUE/FALSE)")
    public void setShowDRALevelTRUEFALSE(String showDRALevelTRUEFALSE) {
        this.showDRALevelTRUEFALSE = showDRALevelTRUEFALSE;
    }

    @JsonProperty("Show Guided Reading Level? (TRUE/FALSE)")
    public String getShowGuidedReadingLevelTRUEFALSE() {
        return showGuidedReadingLevelTRUEFALSE;
    }

    @JsonProperty("Show Guided Reading Level? (TRUE/FALSE)")
    public void setShowGuidedReadingLevelTRUEFALSE(String showGuidedReadingLevelTRUEFALSE) {
        this.showGuidedReadingLevelTRUEFALSE = showGuidedReadingLevelTRUEFALSE;
    }

    @JsonProperty("Show Language? (TRUE/FALSE)")
    public String getShowLanguageTRUEFALSE() {
        return showLanguageTRUEFALSE;
    }

    @JsonProperty("Show Language? (TRUE/FALSE)")
    public void setShowLanguageTRUEFALSE(String showLanguageTRUEFALSE) {
        this.showLanguageTRUEFALSE = showLanguageTRUEFALSE;
    }

    @JsonProperty("Show Lexile? (TRUE/FALSE)")
    public String getShowLexileTRUEFALSE() {
        return showLexileTRUEFALSE;
    }

    @JsonProperty("Show Lexile? (TRUE/FALSE)")
    public void setShowLexileTRUEFALSE(String showLexileTRUEFALSE) {
        this.showLexileTRUEFALSE = showLexileTRUEFALSE;
    }

    @JsonProperty("Show Spanish DRA Level? (TRUE/FALSE)")
    public String getShowSpanishDRALevelTRUEFALSE() {
        return showSpanishDRALevelTRUEFALSE;
    }

    @JsonProperty("Show Spanish DRA Level? (TRUE/FALSE)")
    public void setShowSpanishDRALevelTRUEFALSE(String showSpanishDRALevelTRUEFALSE) {
        this.showSpanishDRALevelTRUEFALSE = showSpanishDRALevelTRUEFALSE;
    }

    @JsonProperty("Show Spanish Guided Reading Level? (TRUE/FALSE)")
    public String getShowSpanishGuidedReadingLevelTRUEFALSE() {
        return showSpanishGuidedReadingLevelTRUEFALSE;
    }

    @JsonProperty("Show Spanish Guided Reading Level? (TRUE/FALSE)")
    public void setShowSpanishGuidedReadingLevelTRUEFALSE(String showSpanishGuidedReadingLevelTRUEFALSE) {
        this.showSpanishGuidedReadingLevelTRUEFALSE = showSpanishGuidedReadingLevelTRUEFALSE;
    }

    @JsonProperty("Show Spanish Lexile? (TRUE/FALSE)")
    public String getShowSpanishLexileTRUEFALSE() {
        return showSpanishLexileTRUEFALSE;
    }

    @JsonProperty("Show Spanish Lexile? (TRUE/FALSE)")
    public void setShowSpanishLexileTRUEFALSE(String showSpanishLexileTRUEFALSE) {
        this.showSpanishLexileTRUEFALSE = showSpanishLexileTRUEFALSE;
    }

    @JsonProperty("Small Image")
    public String getSmallImage() {
        return smallImage;
    }

    @JsonProperty("Small Image")
    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    @JsonProperty("SOLD_IN_TBW")
    public String getSOLDINTBW() {
        return sOLDINTBW;
    }

    @JsonProperty("SOLD_IN_TBW")
    public void setSOLDINTBW(String sOLDINTBW) {
        this.sOLDINTBW = sOLDINTBW;
    }

    @JsonProperty("Spanish High DRA Level")
    public String getSpanishHighDRALevel() {
        return spanishHighDRALevel;
    }

    @JsonProperty("Spanish High DRA Level")
    public void setSpanishHighDRALevel(String spanishHighDRALevel) {
        this.spanishHighDRALevel = spanishHighDRALevel;
    }

    @JsonProperty("Spanish High Guided Reading Level")
    public String getSpanishHighGuidedReadingLevel() {
        return spanishHighGuidedReadingLevel;
    }

    @JsonProperty("Spanish High Guided Reading Level")
    public void setSpanishHighGuidedReadingLevel(String spanishHighGuidedReadingLevel) {
        this.spanishHighGuidedReadingLevel = spanishHighGuidedReadingLevel;
    }

    @JsonProperty("Spanish High Lexile Level")
    public String getSpanishHighLexileLevel() {
        return spanishHighLexileLevel;
    }

    @JsonProperty("Spanish High Lexile Level")
    public void setSpanishHighLexileLevel(String spanishHighLexileLevel) {
        this.spanishHighLexileLevel = spanishHighLexileLevel;
    }

    @JsonProperty("Spanish Low DRA Level")
    public String getSpanishLowDRALevel() {
        return spanishLowDRALevel;
    }

    @JsonProperty("Spanish Low DRA Level")
    public void setSpanishLowDRALevel(String spanishLowDRALevel) {
        this.spanishLowDRALevel = spanishLowDRALevel;
    }

    @JsonProperty("Spanish Low Guided Reading Level")
    public String getSpanishLowGuidedReadingLevel() {
        return spanishLowGuidedReadingLevel;
    }

    @JsonProperty("Spanish Low Guided Reading Level")
    public void setSpanishLowGuidedReadingLevel(String spanishLowGuidedReadingLevel) {
        this.spanishLowGuidedReadingLevel = spanishLowGuidedReadingLevel;
    }

    @JsonProperty("Spanish Low Lexile Level")
    public String getSpanishLowLexileLevel() {
        return spanishLowLexileLevel;
    }

    @JsonProperty("Spanish Low Lexile Level")
    public void setSpanishLowLexileLevel(String spanishLowLexileLevel) {
        this.spanishLowLexileLevel = spanishLowLexileLevel;
    }

    @JsonProperty("Special Note")
    public String getSpecialNote() {
        return specialNote;
    }

    @JsonProperty("Special Note")
    public void setSpecialNote(String specialNote) {
        this.specialNote = specialNote;
    }

    @JsonProperty("Subject")
    public String getSubject() {
        return subject;
    }

    @JsonProperty("Subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @JsonProperty("Subtitle")
    public String getSubtitle() {
        return subtitle;
    }

    @JsonProperty("Subtitle")
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @JsonProperty("System Requirements")
    public String getSystemRequirements() {
        return systemRequirements;
    }

    @JsonProperty("System Requirements")
    public void setSystemRequirements(String systemRequirements) {
        this.systemRequirements = systemRequirements;
    }

    @JsonProperty("Themes")
    public String getThemes() {
        return themes;
    }

    @JsonProperty("Themes")
    public void setThemes(String themes) {
        this.themes = themes;
    }

    @JsonProperty("Title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("Title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("Translator")
    public String getTranslator() {
        return translator;
    }

    @JsonProperty("Translator")
    public void setTranslator(String translator) {
        this.translator = translator;
    }

    @JsonProperty("Unit Cost")
    public String getUnitCost() {
        return unitCost;
    }

    @JsonProperty("Unit Cost")
    public void setUnitCost(String unitCost) {
        this.unitCost = unitCost;
    }

    @JsonProperty("Vendor 1")
    public String getVendor1() {
        return vendor1;
    }

    @JsonProperty("Vendor 1")
    public void setVendor1(String vendor1) {
        this.vendor1 = vendor1;
    }

    @JsonProperty("Vendor 2")
    public String getVendor2() {
        return vendor2;
    }

    @JsonProperty("Vendor 2")
    public void setVendor2(String vendor2) {
        this.vendor2 = vendor2;
    }

    @JsonProperty("Vendor Item #")
    public String getVendorItem() {
        return vendorItem;
    }

    @JsonProperty("Vendor Item #")
    public void setVendorItem(String vendorItem) {
        this.vendorItem = vendorItem;
    }

    @JsonProperty("Weight")
    public String getWeight() {
        return weight;
    }

    @JsonProperty("Weight")
    public void setWeight(String weight) {
        this.weight = weight;
    }

    @JsonProperty("Width")
    public String getWidth() {
        return width;
    }

    @JsonProperty("Width")
    public void setWidth(String width) {
        this.width = width;
    }

    @JsonProperty("CCSS Lexile")
    public String getCCSSLexile() {
        return cCSSLexile;
    }

    @JsonProperty("CCSS Lexile")
    public void setCCSSLexile(String cCSSLexile) {
        this.cCSSLexile = cCSSLexile;
    }

    @JsonProperty("Show CCSS Lexile?(TRUE/FALSE)")
    public String getShowCCSSLexileTRUEFALSE() {
        return showCCSSLexileTRUEFALSE;
    }

    @JsonProperty("Show CCSS Lexile?(TRUE/FALSE)")
    public void setShowCCSSLexileTRUEFALSE(String showCCSSLexileTRUEFALSE) {
        this.showCCSSLexileTRUEFALSE = showCCSSLexileTRUEFALSE;
    }

    @JsonProperty("Boutiques")
    public String getBoutiques() {
        return boutiques;
    }

    @JsonProperty("Boutiques")
    public void setBoutiques(String boutiques) {
        this.boutiques = boutiques;
    }

    @JsonProperty("Viewable Flag")
    public String getViewableFlag() {
        return viewableFlag;
    }

    @JsonProperty("Viewable Flag")
    public void setViewableFlag(String viewableFlag) {
        this.viewableFlag = viewableFlag;
    }

    @JsonProperty("Teacher Tips")
    public String getTeacherTips() {
        return teacherTips;
    }

    @JsonProperty("Teacher Tips")
    public void setTeacherTips(String teacherTips) {
        this.teacherTips = teacherTips;
    }

    @JsonProperty("Teaching Format")
    public String getTeachingFormat() {
        return teachingFormat;
    }

    @JsonProperty("Teaching Format")
    public void setTeachingFormat(String teachingFormat) {
        this.teachingFormat = teachingFormat;
    }

    @JsonProperty("ePage ISBNs")
    public String getEPageISBNs() {
        return ePageISBNs;
    }

    @JsonProperty("ePage ISBNs")
    public void setEPageISBNs(String ePageISBNs) {
        this.ePageISBNs = ePageISBNs;
    }

    @JsonProperty("Number of See Inside Pages")
    public String getNumberOfSeeInsidePages() {
        return numberOfSeeInsidePages;
    }

    @JsonProperty("Number of See Inside Pages")
    public void setNumberOfSeeInsidePages(String numberOfSeeInsidePages) {
        this.numberOfSeeInsidePages = numberOfSeeInsidePages;
    }

    @JsonProperty("Number of Alternate Images")
    public String getNumberOfAlternateImages() {
        return numberOfAlternateImages;
    }

    @JsonProperty("Number of Alternate Images")
    public void setNumberOfAlternateImages(String numberOfAlternateImages) {
        this.numberOfAlternateImages = numberOfAlternateImages;
    }

    @JsonProperty("Number of IWB Preview Images")
    public String getNumberOfIWBPreviewImages() {
        return numberOfIWBPreviewImages;
    }

    @JsonProperty("Number of IWB Preview Images")
    public void setNumberOfIWBPreviewImages(String numberOfIWBPreviewImages) {
        this.numberOfIWBPreviewImages = numberOfIWBPreviewImages;
    }

    @JsonProperty("File Format")
    public String getFileFormat() {
        return fileFormat;
    }

    @JsonProperty("File Format")
    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    @JsonProperty("Legacy TE Sku")
    public String getLegacyTESku() {
        return legacyTESku;
    }

    @JsonProperty("Legacy TE Sku")
    public void setLegacyTESku(String legacyTESku) {
        this.legacyTESku = legacyTESku;
    }

    @JsonProperty("Content Units")
    public String getContentUnits() {
        return contentUnits;
    }

    @JsonProperty("Content Units")
    public void setContentUnits(String contentUnits) {
        this.contentUnits = contentUnits;
    }

    @JsonProperty("User Benefits")
    public String getUserBenefits() {
        return userBenefits;
    }

    @JsonProperty("User Benefits")
    public void setUserBenefits(String userBenefits) {
        this.userBenefits = userBenefits;
    }

    @JsonProperty("Online Resources")
    public String getOnlineResources() {
        return onlineResources;
    }

    @JsonProperty("Online Resources")
    public void setOnlineResources(String onlineResources) {
        this.onlineResources = onlineResources;
    }

    @JsonProperty("REAL ISBNs")
    public String getREALISBNs() {
        return rEALISBNs;
    }

    @JsonProperty("REAL ISBNs")
    public void setREALISBNs(String rEALISBNs) {
        this.rEALISBNs = rEALISBNs;
    }

    @JsonProperty("Format Description")
    public String getFormatDescription() {
        return formatDescription;
    }

    @JsonProperty("Format Description")
    public void setFormatDescription(String formatDescription) {
        this.formatDescription = formatDescription;
    }

    @JsonProperty("SkipInventory")
    public String getSkipInventory() {
        return skipInventory;
    }

    @JsonProperty("SkipInventory")
    public void setSkipInventory(String skipInventory) {
        this.skipInventory = skipInventory;
    }

    @JsonProperty("UNITS_SOLD")
    public String getUNITSSOLD() {
        return uNITSSOLD;
    }

    @JsonProperty("UNITS_SOLD")
    public void setUNITSSOLD(String uNITSSOLD) {
        this.uNITSSOLD = uNITSSOLD;
    }

    @JsonProperty("Audio")
    public String getAudio() {
        return audio;
    }

    @JsonProperty("Audio")
    public void setAudio(String audio) {
        this.audio = audio;
    }

    @JsonProperty("Taxonomy")
    public String getTaxonomy() {
        return taxonomy;
    }

    @JsonProperty("Taxonomy")
    public void setTaxonomy(String taxonomy) {
        this.taxonomy = taxonomy;
    }

    @JsonProperty("Other Languages (Language Selector)")
    public String getOtherLanguagesLanguageSelector() {
        return otherLanguagesLanguageSelector;
    }

    @JsonProperty("Other Languages (Language Selector)")
    public void setOtherLanguagesLanguageSelector(String otherLanguagesLanguageSelector) {
        this.otherLanguagesLanguageSelector = otherLanguagesLanguageSelector;
    }

    @JsonProperty("Collection Titles")
    public String getCollectionTitles() {
        return collectionTitles;
    }

    @JsonProperty("Collection Titles")
    public void setCollectionTitles(String collectionTitles) {
        this.collectionTitles = collectionTitles;
    }

    @JsonProperty("Display Title")
    public String getDisplayTitle() {
        return displayTitle;
    }

    @JsonProperty("Display Title")
    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    @JsonProperty("Component Listing")
    public String getComponentListing() {
        return componentListing;
    }

    @JsonProperty("Component Listing")
    public void setComponentListing(String componentListing) {
        this.componentListing = componentListing;
    }

    @JsonProperty("See Inside Description")
    public String getSeeInsideDescription() {
        return seeInsideDescription;
    }

    @JsonProperty("See Inside Description")
    public void setSeeInsideDescription(String seeInsideDescription) {
        this.seeInsideDescription = seeInsideDescription;
    }

    @JsonProperty("Legacy Product Type")
    public String getLegacyProductType() {
        return legacyProductType;
    }

    @JsonProperty("Legacy Product Type")
    public void setLegacyProductType(String legacyProductType) {
        this.legacyProductType = legacyProductType;
    }

    @JsonProperty("StoreID")
    public String getStoreID() {
        return storeID;
    }

    @JsonProperty("StoreID")
    public void setStoreID(String storeID) {
        this.storeID = storeID;
    }

    @JsonProperty("ECMS Primary Cover Image Flag")
    public String getECMSPrimaryCoverImageFlag() {
        return eCMSPrimaryCoverImageFlag;
    }

    @JsonProperty("ECMS Primary Cover Image Flag")
    public void setECMSPrimaryCoverImageFlag(String eCMSPrimaryCoverImageFlag) {
        this.eCMSPrimaryCoverImageFlag = eCMSPrimaryCoverImageFlag;
    }

    @JsonProperty("Product ID")
    public String getProductID() {
        return productID;
    }

    @JsonProperty("Product ID")
    public void setProductID(String productID) {
        this.productID = productID;
    }

    @JsonProperty("Work ID")
    public String getWorkID() {
        return workID;
    }

    @JsonProperty("Work ID")
    public void setWorkID(String workID) {
        this.workID = workID;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
