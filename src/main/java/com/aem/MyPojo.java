package com.aem;



public class MyPojo
{
    private String total;

    private String callToPlaceOrderNumber;

    private String printablesUrl;

    private String totalAlternate;

    private String classRoomMagUrl;


    private String isAlternate;

    private String printablesFelixUrl;

    private SolrDocumentList[] solrDocumentList;


    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getCallToPlaceOrderNumber ()
    {
        return callToPlaceOrderNumber;
    }

    public void setCallToPlaceOrderNumber (String callToPlaceOrderNumber)
    {
        this.callToPlaceOrderNumber = callToPlaceOrderNumber;
    }

    public String getPrintablesUrl ()
    {
        return printablesUrl;
    }

    public void setPrintablesUrl (String printablesUrl)
    {
        this.printablesUrl = printablesUrl;
    }

    public String getTotalAlternate ()
    {
        return totalAlternate;
    }

    public void setTotalAlternate (String totalAlternate)
    {
        this.totalAlternate = totalAlternate;
    }

    public String getClassRoomMagUrl ()
    {
        return classRoomMagUrl;
    }

    public void setClassRoomMagUrl (String classRoomMagUrl)
    {
        this.classRoomMagUrl = classRoomMagUrl;
    }

    

    public String getIsAlternate ()
    {
        return isAlternate;
    }

    public void setIsAlternate (String isAlternate)
    {
        this.isAlternate = isAlternate;
    }

    public String getPrintablesFelixUrl ()
    {
        return printablesFelixUrl;
    }

    public void setPrintablesFelixUrl (String printablesFelixUrl)
    {
        this.printablesFelixUrl = printablesFelixUrl;
    }

    public SolrDocumentList[] getSolrDocumentList ()
    {
        return solrDocumentList;
    }

    public void setSolrDocumentList (SolrDocumentList[] solrDocumentList)
    {
        this.solrDocumentList = solrDocumentList;
    }

   
    @Override
    public String toString()
    {
        return "ClassPojo [total = "+total+", callToPlaceOrderNumber = "+callToPlaceOrderNumber+", printablesUrl = "+printablesUrl+", totalAlternate = "+totalAlternate+", classRoomMagUrl = "+classRoomMagUrl+", isAlternate = "+isAlternate+", printablesFelixUrl = "+printablesFelixUrl+", solrDocumentList = "+solrDocumentList+", intervalFacets = ]";
    }
}
			
