package com.compareprices.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.aem.MyPojo;
import com.aem.SolrDocumentList;

public class SolrAemController {
	private static final Logger log = Logger.getLogger(SolrAemController.class.getName());
	private static final String FILE_NAME_WRITE = ClassLoader.getSystemClassLoader().getResource("Solr-AEM.xlsx")
			.getPath();
	private static final String EMPTY_STRING = "empty";

	public static void main(String[] args) throws SolrServerException, IOException {

		String url12 = "https://www-qa2.scholastic.com/bin/scholastic/teachers/ecommerce/common-search.json?appType=tso&isSSO=false&rows=40&siteID=tso-scholastic-books&sort=UNITS_SOLD_f_desc,ProductStartDate_dt_desc&text=harry";

		generateDataToExcel(getReponseThirdEntity(url12), solrSearch());

	}
	private static void generateDataToExcel(List<String> reponseThirdEntity, List<String> solrSearch) {
		try (FileInputStream fis = new FileInputStream(FILE_NAME_WRITE);
				XSSFWorkbook workbook = new XSSFWorkbook(fis);) {
			XSSFSheet worksheet = workbook.getSheet("SolarAEMKeyword");
			XSSFRow row1 = worksheet.createRow(0);
			Cell cell1 = row1.createCell(0);
			cell1.setCellValue("SOLR");
			Cell cell2 = row1.createCell(1);
			cell2.setCellValue("AEM");

			for (int i = 0; i < reponseThirdEntity.size(); i++) {

				XSSFRow row = worksheet.createRow(i + 1);
				row.createCell(0).setCellValue(reponseThirdEntity.get(i));
				row.createCell(1).setCellValue(solrSearch.get(i));
			}
			try (FileOutputStream fos = new FileOutputStream(FILE_NAME_WRITE);) {
				// Save the workbook in .xls file
				workbook.write(fos);
				fos.flush();
				log.info("@@@@@@@@ SHEET UPDATED WITH THE VALUES @@@@@@@@");
			}
		} catch (FileNotFoundException e) {
			e.getMessage();
			log.log(Level.INFO, "into the exception in the write to URL when file not found", e.fillInStackTrace());

		} catch (IOException e) {
			log.log(Level.INFO, "into the exception in the write to URL", e.fillInStackTrace());

		}
	}
	private static List<String> getReponseThirdEntity(String url) {
		List<String> list = new ArrayList<>();
		RestTemplate template = new RestTemplate();
		try {
			ResponseEntity<MyPojo> exdate = template.exchange(url, HttpMethod.GET, null, MyPojo.class);

			SolrDocumentList[] y = exdate.getBody().getSolrDocumentList();
			for (SolrDocumentList solrDocumentList : y) {
				list.add(solrDocumentList.getId());
			}

		} catch (Exception e) {
			list.add("in exception");
		}
		return list;
	}
	public static List<String> solrSearch() throws SolrServerException, IOException {
		List<String> list1 = new ArrayList<>();
		String urlString = "http://10.45.150.78:8983/solr/global";
		SolrClient solr = new HttpSolrClient.Builder(urlString).build();
		((HttpSolrClient) solr).setParser(new XMLResponseParser());

		// Solr Quary which was provided by AEM
		// http://10.45.150.78:8983/solr/global/select?qt=/tchecommfacet&fq=site_id_s:tso-scholastic-books&fq=Searchable_s:%22TRUE%22&q=harry&sort=UNITS_SOLD_f+desc,ProductStartDate_dt+desc&rows=20&start=0&spellcheck=true&shards.qt=/tchecommfacet

		SolrQuery query = new SolrQuery();
		query.setQuery("harry");
		query.setParam("fq", "site_id_s:tso-scholastic-books AND Searchable_s:TRUE");
		query.addSort("UNITS_SOLD_f", ORDER.desc);
		query.addSort("ProductStartDate_dt", ORDER.desc);
		query.setParam("spellcheck", "true");
		query.setRequestHandler("tchecommfacet");
		query.setParam("shards.qt", "/tchecommfacet");
		query.setRows(40);
		query.setStart(0);
		QueryResponse response = solr.query(query);
		org.apache.solr.common.SolrDocumentList list = response.getResults();
		for (SolrDocument solrDocument : list) {

			list1.add((String) solrDocument.getFieldValue("id"));
		}

		return list1;
	}

}
